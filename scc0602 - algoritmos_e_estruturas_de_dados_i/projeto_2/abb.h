typedef int Item;

typedef struct no {
	Item info;
    struct no *esquerda, *direita;
} No;

typedef struct abb {
	No* raiz;
} ABB;


void criarABB(ABB* arvore);
void destruirABB(No** no);
void imprimirABB(No** no);
int buscarEmABB(No** no,int);
int inserirEmABB(No** no,int);
int removerDeABB(No** no,int);
int alturaABB(No *no);
