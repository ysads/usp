#include "AVL.h"
#include <stdlib.h>
#include <stdio.h>

//Devolve o maior de 2 inteiros
int maiorInt(int a, int b);

//Devolve a altura do no
int alturaNoAVL(noAVL *no) {
    if (no == NULL)
        return 0;
    return no->altura;
}

//Devolve o maior de dois inteiros
int maiorInt(int a, int b) {
    return (a > b)? a : b;
}

//Aloca um novo no com a informacao recebida e com filhos nulos
noAVL* novoNoAVL(int info) {
    noAVL* novoNo = (noAVL*)malloc(sizeof(noAVL));
    novoNo->info = info;
    novoNo->esquerda = NULL;
    novoNo->direita  = NULL;
    novoNo->altura = 1;  // Inicialmente, um novo no eh criado como uma folha. Pode vir a ser alterado posteriormente
    return novoNo;
}

//Roda uma subarvore para a direita, a fim de manter o balanceamento
noAVL *rotacaoDireitaAVL(noAVL *centro) {
    noAVL* filho1 = centro->esquerda;
    noAVL* filho2 = filho1->direita;

    //Faz a rotacao
    filho1->direita = centro;
    centro->esquerda = filho2;

    //Atualiza as alturas
    centro->altura = maiorInt(alturaNoAVL(centro->esquerda), alturaNoAVL(centro->direita))+1;
    filho1->altura = maiorInt(alturaNoAVL(filho1->esquerda), alturaNoAVL(filho1->direita))+1;

    //Devolve o no que passou a ser a nova raiz da subarvore balanceada
    return filho1;
}

//Roda uma subarvore para a esquerda, a fim de manter o balanceamento
noAVL *rotacaoEsquerdaAVL(noAVL *centro) {
    noAVL *filho1 = centro->direita;
    noAVL *filho2 = filho1->esquerda;

    //Faz a rotacao
    filho1->esquerda = centro;
    centro->direita = filho2;

    //Atualiza as alturas
    centro->altura = maiorInt(alturaNoAVL(centro->esquerda), alturaNoAVL(centro->direita))+1;
    filho1->altura = maiorInt(alturaNoAVL(filho1->esquerda), alturaNoAVL(filho1->direita))+1;

    //Devole o no que passou a ser a nova raiz da subarvore balanceada
    return filho1;
}

//Devolve o fator de balanceamento do no recebido, subtraindo as alturas de suas 2 subarvores
int fatorDeBalanceamentoAVL(noAVL *no) {
    if (no == NULL)
        return 0;
    return alturaNoAVL(no->esquerda) - alturaNoAVL(no->direita);
}

//Insere um no na arvore, balanceando-a no processo
noAVL *insereNoAVL(noAVL *noAVL, int info) {
    //Se o no recebido eh nulo, significa que representa uma arvore vazia. Cria-se um novo no
    if (noAVL == NULL)
        return(novoNoAVL(info));
    //Insere um no a esquerda se sua informacao for menor do que a raiz, e a direita se for maior
    if (info < noAVL->info)
        noAVL->esquerda = insereNoAVL(noAVL->esquerda, info);
    else
        noAVL->direita= insereNoAVL(noAVL->direita, info);

    //Atualiza a altura do no parente
    noAVL->altura = maiorInt(alturaNoAVL(noAVL->esquerda), alturaNoAVL(noAVL->direita)) + 1;

    //Recalcula o fator de balanceamento do no atual, para verificar se a arvore precisa ser
    //rebalanceada
    int fatorDeBalanceamento = fatorDeBalanceamentoAVL(noAVL);

    //Se a arvore esta desbalanceada, o sistema pode cair em 4 casos
    //Rotacao dupla pra direita
    if (fatorDeBalanceamento > 1 && info < noAVL->esquerda->info)
        return rotacaoDireitaAVL(noAVL);

    //Rotacao dupla pra esquerda
    if (fatorDeBalanceamento < -1 && info > noAVL->direita->info)
        return rotacaoEsquerdaAVL(noAVL);

    //Rotacao pra esquerda, entao pra direita
    if (fatorDeBalanceamento > 1 && info > noAVL->esquerda->info) {
        noAVL->esquerda =  rotacaoEsquerdaAVL(noAVL->esquerda);
        return rotacaoDireitaAVL(noAVL);
    }

    //Rotacao pra direita, entao pra esquerda
    if (fatorDeBalanceamento < -1 && info < noAVL->direita->info) {
        noAVL->direita = rotacaoDireitaAVL(noAVL->direita);
        return rotacaoEsquerdaAVL(noAVL);
    }

    //Retorna o no
    return noAVL;
}

//Retorna o menor no da arvore. Ele necessariamente sera aquele localizado na extrema esquerda
//da estrutura.
noAVL* menorNoAVL(noAVL* no) {
    noAVL* noAtual = no;

    //Varre ate achar o no na extrema esquerda
    while (noAtual->esquerda != NULL)
        noAtual = noAtual->esquerda;

    return noAtual;
}

//Remove um no da arvore
noAVL* removeNoAVL(noAVL* raiz, int info) {
    //Se recebeu uma arvore vazia, nao tem o que fazer
    if (raiz == NULL)
        return raiz;

    //Se a informacao a ser removida eh menor do que a da raiz, entao ela esta
    //a esquerda desta. Portanto, da um passo a esquerda na varredura da arvore
    if ( info < raiz->info )
        raiz->esquerda = removeNoAVL(raiz->esquerda, info);

    //Se a informacao recebida eh maior, varre pra direita
    else if( info > raiz->info )
        raiz->direita = removeNoAVL(raiz->direita, info);

    //Se a informacao eh igual a do no atual, significa que foi encontrado o no a ser removido
    //executa-se o procedimento de remocao
    else {
        //Procedimento para nos sem filhos ou com 1 unico filho
        if( (raiz->esquerda == NULL) || (raiz->direita == NULL) ) {
            //Recebe o conteudo de um dos filhos, se houver
            noAVL *temp = raiz->esquerda ? raiz->esquerda : raiz->direita;

            //Para nos sem filhos, apaga a raiz
            if(temp == NULL) {
                temp = raiz;
                raiz = NULL;
            }
            else // Para nos com 1 filho, copia o conteudo do filho
             *raiz = *temp;

            free(temp);
        }
        else {
            //Pega o menor no da subarvore do filho da direita
            noAVL* temp = menorNoAVL(raiz->direita);

            //Copia o conteudo desse filho para o no atual
            raiz->info = temp->info;

            //Remove-se o filho da direita
            raiz->direita = removeNoAVL(raiz->direita, temp->info);
        }
    }

    // Caso da arvore ter um unico no
    if (raiz == NULL)
      return raiz;

    //Atualiza a altura do no atual
    raiz->altura = maiorInt(alturaNoAVL(raiz->esquerda), alturaNoAVL(raiz->direita)) + 1;

    //Recalcula o fator de balanceamento, para verificar se a arvore precisa ser rebalanceada
    int fatorDeBalanceamento = fatorDeBalanceamentoAVL(raiz);

    //4 casos possiveis de rebalanceamento
    //Rotacao dupla pra direita
    if (fatorDeBalanceamento > 1 && fatorDeBalanceamentoAVL(raiz->esquerda) >= 0)
        return rotacaoDireitaAVL(raiz);

    //Rotacao pra esquerda, entao pra direita
    if (fatorDeBalanceamento > 1 && fatorDeBalanceamentoAVL(raiz->esquerda) < 0)
    {
        raiz->esquerda =  rotacaoEsquerdaAVL(raiz->esquerda);
        return rotacaoDireitaAVL(raiz);
    }

    //Dupla rotacao pra direita
    if (fatorDeBalanceamento < -1 && fatorDeBalanceamentoAVL(raiz->direita) <= 0)
        return rotacaoEsquerdaAVL(raiz);

    //Rotacao pra direita, entao pra esquerda
    if (fatorDeBalanceamento < -1 && fatorDeBalanceamentoAVL(raiz->direita) > 0)
    {
        raiz->direita = rotacaoDireitaAVL(raiz->direita);
        return rotacaoEsquerdaAVL(raiz);
    }

    return raiz;
}

//Imprime a AVL, comecando pelos menores nos (imprime tudo pela esquerda, entao avanca 1 pra direita e vai
//ate o fim a esquerda, e assim sucessivamente)
void imprimeAVL(noAVL *no) {
    if (no != NULL) {
        printf("%d(", no->info);
        imprimeAVL(no->esquerda);
        printf(",");
        imprimeAVL(no->direita);
        printf(")");
    } else {
        printf("null");
    }
}

int buscarAVL(noAVL *noAtual, int informacaoBuscada) {
    if (noAtual==NULL)
       //Sabe-se que o elemento nao existe na arvore se a varredura cair em um no nulo
       //Nesse caso, retorna zero
       return 0;
    else if (informacaoBuscada < noAtual->info)
         //Continua a varredura da arvore pela esquerda, se a informacao recebida eh
         //menor do que o no atual
         return(buscarAVL(noAtual->esquerda, informacaoBuscada));
    else if (informacaoBuscada> noAtual ->info)
         //Continua a varredura da arvore pela direita, se a informacao recebida eh
         //maior do que o no atual
         return(buscarAVL(noAtual->direita, informacaoBuscada));
    //Se nao precisa continuar a varredura pela direita nem pela esquerda (o no atual nao
    //tem filhos) e o no atual nao eh nulo, entao a informacao buscada foi encontrada
    else return 1;
}

//Calcula a altura absoluta da AVL
int calculaAlturaAbsolutaAVL(noAVL* no) {
    //Quando achar um no nulo, significa que acabou a varredura. retorna -1
    if(no == NULL)
        return -1;
    //Chamada recursiva pra achar a altura a esquerda
    int alturaEsquerda = calculaAlturaAbsolutaAVL(no->esquerda);
    //Mesma coisa a direita
    int alturaDireita = calculaAlturaAbsolutaAVL(no->direita);

    //Verifica qual a maior altura calculada
    if(alturaEsquerda > alturaDireita)
        //Como o no nulo retorna -1, soma 1 ao resultado para ter o numero correto para a altura
        return alturaEsquerda + 1;
    else
        //Mesma coisa do anterior
        return alturaDireita +1;
}
