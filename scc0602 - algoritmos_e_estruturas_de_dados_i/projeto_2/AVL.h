
//No da arvore AVL
typedef struct noAVL {
    int info;
    struct noAVL *esquerda;
    struct noAVL *direita;
    int altura;
} noAVL;

noAVL *insereNoAVL(noAVL *noAVL, int info);
noAVL* removeNoAVL(noAVL* raiz, int info);
void imprimeAVL(noAVL *raiz);
int buscarAVL(noAVL *noAtual, int informacaoBuscada);
int calculaAlturaAbsolutaAVL(noAVL* no);
