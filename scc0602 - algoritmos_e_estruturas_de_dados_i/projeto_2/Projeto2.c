#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "AVL.h"
#include "abb.h"


//Função que avalia a inserção em AVL, considerando as entradas fornecidas,
//o tempo de inicio e de fim de todas as inserções, imprimindo a diferença de tempo no final
noAVL* avaliar_insercaoAVL(noAVL *arvore,int* entrada){
	int i;
	noAVL *ret = NULL;
	struct timeval inicial, final, total;
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//inserindo dados
	for (i = 0; i < 15000; i++){
		ret = insereNoAVL(ret,entrada[i]);
		if(ret == NULL){
			exit(0);
		}
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem AVL: %ld.%06ld segundos\n", (long int)total.tv_sec, (long int)total.tv_usec);

	return ret;
}

//Função que avalia a remoção em AVL, considerando as entradas fornecidas,
//a altura inicial e final da árvore e o tempo de inicio e de fim de todas as inserções
//imprimindo a diferença de tempo no final
noAVL* avaliar_remocaoAVL(noAVL *arvore,int* entrada){
	int i, altura1,altura2;;
	struct timeval inicial, final, total;
	noAVL *ret = arvore;
	//coletando altura inicial da árvore
	altura1 = alturaNoAVL(ret);
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//removendo dados
	for (i = 0; i < 5000; i++){
		ret = removeNoAVL(ret,entrada[i]);
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//coletando altura final da árvore
	altura2 = alturaNoAVL(ret);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem AVL: %ld.%06ld segundos\n", (long int)total.tv_sec, (long int)total.tv_usec);
	printf("\tAltura da árvore: \n");
	printf("\t\tAntes da remoção: %d\n", altura1);
	printf("\t\tDepois da remoção: %d\n\n",altura2);
	return ret;
}
//Função que avalia a busca em AVL, considerando as entradas fornecidas
//o tempo de inicio e de fim de todas as inserções, imprimindo a diferença de tempo no final
int avaliar_buscaAVL(noAVL **arvore,int* entrada){
	int i;
	struct timeval inicial, final, total;
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//buscando dados
	for (i = 0; i < 5000; i++){
		buscarAVL(*arvore,entrada[i]);
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem AVL: %ld.%06ld segundos\n", (long int)total.tv_sec, (long int)total.tv_usec);
}

///Função que avalia a inserção em ABB, de mesmo funcionamento que a avaliação em AVL
void avaliar_insercaoABB(ABB *arvore,int* entrada){
	int i,ret;
	struct timeval inicial, final, total;
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//inserindo dados
	for (i = 0; i < 15000; i++){
		ret = inserirEmABB(&(arvore->raiz),entrada[i]);
		if(ret == 0){
			exit(0);
		}
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem ABB: %ld.%06ld segundos\n\n", (long int)total.tv_sec, (long int)total.tv_usec);
}

//Função que avalia a remoção em ABB, de mesmo funcionamento que a avaliação em AVL
void avaliar_remocaoABB(ABB *arvore,int* entrada){
	int i,altura1,altura2;
	struct timeval inicial, final, total;
	//coletando altura inicial da árvore
	altura1 = alturaABB(arvore->raiz);
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//removendo dados
	for (i = 0; i < 5000; i++){
		removerDeABB(&(arvore->raiz),entrada[i]);
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//coletando altura final da árvore
	altura2 = alturaABB(arvore->raiz);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem ABB: %ld.%06ld segundos\n", (long int)total.tv_sec, (long int)total.tv_usec);
	printf("\tAltura da árvore: \n");
	printf("\t\tAntes da remoção: %d\n", altura1);
	printf("\t\tDepois da remoção: %d\n\n",altura2);
}

//Função que avalia a busca em ABB, de mesmo funcionamento que a avaliação em AVL
void avaliar_buscaABB(ABB *arvore,int* entrada){
	int i;
	struct timeval inicial, final, total;
	//coletando tempo inicial
	gettimeofday(&inicial,NULL);
	//buscando dados
	for (i = 0; i < 5000; i++){
		buscarEmABB(&(arvore->raiz),entrada[i]);
	}
	//coletando tempo final
	gettimeofday(&final,NULL);
	//tempo de execução 
	timersub(&final,&inicial,&total);
	printf("\tem ABB: %ld.%06ld segundos\n\n", (long int)total.tv_sec, (long int)total.tv_usec);
}

//valormax é o maior valor possível a ser gerado
//saída é o vetor por onde os numeros serão retornados
//tam é o tamanho desse vetor (quantidade de aleatorios distintos a serem utilizados)
void gerar_aleatorios(int valormax, int* saida, int tam){
	int i,val_gerado,*campo_total;
	campo_total = (int*) calloc(valormax,sizeof(int));

	for(i = 0;i<tam;i++){
		val_gerado = rand() % valormax;
		if(campo_total[val_gerado] == 0){
			campo_total[val_gerado] = 1;
			saida[i] = val_gerado;
		}else{
			while(campo_total[val_gerado] != 0){
				val_gerado = rand() % valormax;
			}
			campo_total[val_gerado] = 1;
			saida[i] = val_gerado;
		}
	}
	free(campo_total);
}

int main(void){
	int *chaves_inser,*chaves_rem,*chaves_busca;
	//criando arvores
	ABB arvorebin;
	noAVL *root = NULL;
	criarABB(&arvorebin);

	//allocando espaços de memoria
	chaves_inser = (int*) malloc(15000*sizeof(int));
	chaves_rem = (int*) malloc(5000*sizeof(int));
	chaves_busca = (int*) malloc(5000*sizeof(int));

	//gerando valores de teste
	srand((unsigned)time(NULL));
	gerar_aleatorios(50000,chaves_inser,15000);
	gerar_aleatorios(50000,chaves_rem,5000);
	gerar_aleatorios(50000,chaves_busca,5000);

	//analisando desempenhos
	printf("Tempo de inserção: \n");
	root = avaliar_insercaoAVL(root,chaves_inser);
	avaliar_insercaoABB(&arvorebin,chaves_inser);

	printf("Tempo de remoção: \n");
	root = avaliar_remocaoAVL(root,chaves_rem);
	avaliar_remocaoABB(&arvorebin,chaves_rem);

	printf("Tempo de busca: \n");
	avaliar_buscaAVL(&root,chaves_busca);
	avaliar_buscaABB(&arvorebin,chaves_busca);

	//apagar arvores
	while(root != NULL){
		root = removeNoAVL(root,root->info);
	}
	destruirABB(&(arvorebin.raiz));
	//frees
	free(chaves_inser);
	free(chaves_rem);
	free(chaves_busca);
	return 0;
}