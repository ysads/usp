#include <stdio.h>
#include <stdlib.h>
#include "abb.h"


//Fun��o que recebe um ponteiro para uma �rvore bin�ria de busca, e
//a inicializa, isto �, faz sua raiz apontar para NULL
void criarABB(ABB *arvore) {
  arvore->raiz = NULL;
}


//Fun��o que, recursivamente, destroi uma �rvore bin�ria de busca
void destruirABB(No **no) {
    
    //Verifica se o n� recebido n�o � um n�-folha. Se n�o for, destroi
    //suas sub�rvores esquerda e direita e depois desaloca o n�
    if (*no != NULL) {
        destruirABB(&(*no)->esquerda);
        destruirABB(&(*no)->direita);
        free(*no);
    }
}


//Procedimento que recebe um ponteiro para a raiz de uma �rvore bin�ria
//de busca e imprime o conte�do de todos os seus n�s recursivamente.
//Note-se que a impress�o � feita de modo a preservar o car�ter
//hier�rquico das informa��es
void imprimirABB(No **no) {

    //Verifica se o n� recebido � um n�-folha. Se n�o for, imprime o
    //conte�do do n� seguido do conte�do de suas sub�rvores esquerda e
    //direita.
    //No caso de um n� folha, � impressa a palavra 'null'
    if (*no != NULL) {
        printf("%d(", (*no)->info);
        imprimirABB(&(*no)->esquerda);
        printf(",");
        imprimirABB(&(*no)->direita);
        printf(")");
    }
    else {
        printf("null");
    }
}


//Fun��o delegada � busca de determinado item em uma �rvore bin�ria
//de busca. Para tanto, ela recebe o item que deve ser buscado e um
//ponteiro para o n�-raiz da �rvore em quest�o
int buscarEmABB(No **no, Item item) {

    //Se o n� em an�lise for nulo, significa que o item buscado n�o
    //est� na �rvore; por isso, o valor 0 � retornado
    //Se o item buscado for menor do que aquele analisado, a fun��o
    //� chamada, recursivamente, passando um ponteiro para sua
    //sub�rvore esquerda; caso contr�rio, � feita mesma opera��o,
    //por�m, passsando-se um ponteiro para a sub�rvore direita do n�
    //Por fim, se nenhuma das condi��es anteriores for satisfeita,
    //tem-se que o item buscado foi, enfim, localizado, e o valor 1
    //�, ent�o retornado
    if (*no == NULL) {
        return 0;
    }
    else if (item < (*no)->info) {
        return buscarEmABB(&(*no)->esquerda, item);
    }
    else if (item > (*no)->info) {
        return buscarEmABB(&(*no)->direita, item);
    }
    else {
        return 1;
    }
}


//Fun��o que objetiva � inser��o de determinado item numa �rvore
//bin�ria de busca. Recebe, como par�metro, o item a ser inserido e
//um ponteiro para o n�-raiz da �rvore em que o elemento deve ser
//inserido.
int inserirEmABB(No **no, Item item) {

    //Checa se um ponteiro nulo foi localizado. Se isso acontecer,
    //ent�o a posi��o do elemento a ser inserido na �rvore foi
    //determinada. Nesse caso, aloca, na mem�ria, o espa�o necess�rio
    //para o item e o relaciona com o restante da �rvore
    //Se um ponteiro nulo n�o for localizado, continua, ent�o,
    //recursivamente, a busca pela posi��o do elemento na �rvore.
    if (*no == NULL) {
        *no = (No*) malloc(sizeof(No));
        (*no)->info = item;
        (*no)->esquerda = NULL;
        (*no)->direita = NULL;
        return 1;
    }
    else if (item < (*no)->info) {
        
        //Se o n� em an�lise for maior do que o item a ser inserido,
        //chama a fun��o de inser��o, recursivamente, passando um
        //ponteiro para a sub�rvore esquerda do referido n�
        return inserirEmABB(&(*no)->esquerda, item);
    }
    else if (item > (*no)->info) {

        //Se o n� em an�lise for menor do que o item a ser inserido,
        //chama a fun��o de inser��o, recursivamente, passando um
        //ponteiro para a sub�rvore direita do referido n�
        return inserirEmABB(&(*no)->direita, item);
    }
    else {
        return 0;
    }
}


//Fun��o auxiliar que visa � localiza��o do maior elemento contido em
//uma sub�rvore. Basicamente, percorre toda a sub�rvore, indo sempre
//para a direita, em busca do �ltimo elemento n�o-nulo que for poss�vel
//localizar. Esse elemento �, ent�o, retornado, pois � o maior dentre
//todos os itens da sub�rvore analisada.
Item buscarMaiorItemDaSubArvore(No *no) {
    while (no->direita != NULL) {
        no = no->direita;
    }
    return no->info;
}


//Fun��o que visa � remo��o de um elemento d'uma �rvore bin�ria de
//busca. Seus par�metros s�o o item a ser removido e um ponteiro para
//o n�-raiz de uma �rvore
int removerDeABB(No **no, Item item) {
    No *aux;

    //Se o n� recebido for nulo, ent�o o elemento buscado n�o est�
    //presente na �rvore. Por isso, o valor 0 � retornado e nada �
    //removido
    if (*no == NULL) {
       return 0;
    }
    else if (item < (*no)->info) {

        //Se o n� em an�lise for maior do que o item a ser removido,
        //chama a fun��o de remo��o, recursivamente, passando um
        //ponteiro para a sub�rvore esquerda do referido n�
        return removerDeABB(&(*no)->esquerda, item);
    }
    else if (item > (*no)->info) {

        //Se o n� em an�lise for menor do que o item a ser removido,
        //chama a fun��o de remo��o, recursivamente, passando um
        //ponteiro para a sub�rvore direita do referido n�
        return removerDeABB(&(*no)->direita, item);
    }
    else {

        //Se nenhuma das condi��es anteriores for satisfeita, ent�o
        //o item que deve ser removido foi localizado. Nessa situa��o,
        //devem ser avaliados todos os poss�veis casos de remo��o, a
        //saber:
        if (((*no)->esquerda == NULL) && ((*no)->direita == NULL)) {
            
            //Caso 1: o n� a ser removido n�o possui filhos. Nesse caso,
            //o n� � desalocado da mem�ria e nenhuma outra opera��o
            //adicional se faz necess�ria
            free(*no);
            *no = NULL;
            return 1;
        }
        else if ((*no)->esquerda == NULL) {

            //Caso 2: o n� a ser removido possui, somente, filhos �
            //direita. A fun��o simplesmente desaloca o n� apropriado,
            //reposicionando o seu filho da direita, para que ocupe a
            //posi��o que outrora o seu pai ocupara
            aux = *no;
            *no = (*no)->direita;
            free(aux);
            return 1;
        }
        else if ((*no)->direita == NULL) {

            //Caso 3: o n� a ser removido possui, somente, filhos �
            //esquerda. A fun��o simplesmente desaloca o n� apropriado,
            //reposicionando o seu filho esquerdo, para que ocupe a
            //posi��o que outrora o seu pai ocupara
            aux = *no;
            *no = (*no)->esquerda;
            free(aux);
            return 1;
        }
        else {

            //Caso 4: o n� a ser removido possui ambos os filhos. Nessa
            //situa��o, o maior elemento contido na sua sub�rvore esquerda
            //� localizado e posicionado no local onde o elemento a ser
            //removido est�. Logo ap�s, esse elemento que foi reposicionado
            //� retirado do local que inicialmente ocupava na �rvore
            (*no)->info = buscarMaiorItemDaSubArvore((*no)->esquerda);
            return removerDeABB(&(*no)->esquerda,(*no)->info);
        }
    }
}


//Fun��o que determina a altura de uma �rvore bin�ria de busca. Para
//isso, verifica as alturas das sub�rvores direita e esquerda, e
//retorna a maior delas. No caso de uma �rvore com n�-raiz nulo, a
//altura 0 � retornada.
int alturaABB(No *no) {
    int alturaSubArvoreEsquerda;
    int alturaSubArvoreDireita;

    if (no == NULL) {
        return 0;
    }
    else {

        //Computa as alturas das sub�rvores direita e esquerda
        alturaSubArvoreEsquerda = alturaABB(no->esquerda);
        alturaSubArvoreDireita = alturaABB(no->direita);

        //Determina qual � a maior altura de sub�rvore e a retorna
        if (alturaSubArvoreEsquerda > alturaSubArvoreDireita) {
            return alturaSubArvoreEsquerda + 1;
        }
        else {
            return alturaSubArvoreDireita + 1;
        }
    }
}