result([ _ , E | L ] , [ E | M ]) :-
    ! , result(L , M ).
result(_ , []).

homem(americo).
homem(daniel).
homem(paulo).
homem(carlos).
homem(joaquim).
homem(filipe).
mulher(teresa).
mulher(sonia).
mulher(ana).
mulher(carla).
mulher(barbara).
mulher(maria).
idade(americo, 18).
idade(daniel, 60).
idade(paulo, 25).
idade(carlos, 37).
idade(joaquim, 80).
idade(filipe, 32).
idade(teresa, 18).
idade(sonia, 28).
idade(ana, 17).
idade(carla, 26).
idade(barbara, 51).
idade(maria, 79).
irmaos(americo, paulo).
irmaos(carlos, sonia).
pai(carlos, teresa).
pai(daniel, americo).
pai(daniel, paulo).
pai(joaquim, daniel).
mae(maria, daniel).
mae(barbara, ana).
casados(filipe, carla).
casados(americo, teresa).
casados(joaquim, maria).

avof(Mul, Pess) :-
    mae(Mul, X), mae(X, Pess);
    mae(Mul, X), pai(X, Pess).

avom(Hom, Pess) :-
    pai(Hom, X), mae(X, Pess);
    pai(Hom, X), pai(X, Pess).

bisavom(Hom, Pess) :-
    avom(Hom, X), mae(X, Pess);
    avom(Hom, X), pai(X, Pess).

primo_1(P1, P2) :-
    avof(Mul, P1), avof(Mul, P2),
    avom(Hom, P1), avom(Hom, P2),
    \+(irmaos(P2, P1)), \+(irmaos(P1, P2)),
    P1 \= P2,
    P1 @< P2.

primo(P1, P2) :-
    primo_1(P1, P2).

primo(P1, P2) :-
    mae(X, P1), mae(Y, P2), primo(X, Y);
    mae(X, P1), pai(Y, P2), primo(X, Y);
    pai(X, P1), mae(Y, P2), primo(X, Y);
    pai(X, P1), pai(Y, P2), primo(X, Y).

% Considerando que ser maior de idade significa
% ter idade superior a 18 anos
maior_de_idade(Pess) :-
    idade(Pess, I), I >= 18.

pessoa(X) :-
    homem(X); mulher(X).
pessoas(Lista) :-
    findall(P, pessoa(P), Lista).

mais_velho(Pess) :-
    idade(Pess, X), \+((idade(_, Y), Y > X)).

lista_pessoas(Lista, Sexo) :-
    Sexo = m, findall([P, I], (homem(P), idade(P, I)), Lista);
    Sexo = f, findall([P, I], (mulher(P), idade(P, I)), Lista).

progenitor(X, Y) :-
    pai(X, Y);
    mae(X, Y).

ancestral(X, Y) :-
    progenitor(X, Y);
    progenitor(X, Z), ancestral(Z, Y).

possuem_parentesco(P1, P2) :-
    irmaos(P1, P2);
    irmaos(P2, P1);
    (ancestral(P1, X), ancestral(P2, X)).

adequados(Hom, Mul) :-
    homem(Hom), mulher(Mul),
    %idade(Hom, IdadeHom), idade(Mul, IdadeMul),
    %(IdadeHom >= IdadeMul - 2), (IdadeHom <= IdadeMul + 10),
    not(casados(Hom, _)), not(casados(_, Hom)),
    not(casados(Mul, _)), not(casados(_, Mul)),
    not(possuem_parentesco(Hom, Mul)).
