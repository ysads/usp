%{
  int chars = 0, words = 0, lines = 0;
%}

%%
\n        { lines++; chars++; }
[^ \t\n]+ { words++; chars += yyleng; }
.         { chars++; }
%%

int main() {
  yylex();
  printf("chars: %d\twords: %d\tlines: %d\n", chars, words, lines);
}