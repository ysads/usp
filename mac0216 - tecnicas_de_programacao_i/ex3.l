%{
  #include <math.h>
  float sum = 0.0;
%}

%%
[0-9]\.[0-9][ ][a-z]+  { printf("%0.2f\n", atof(yytext)); sum += atof(yytext);}
[0-9]\.[ ]  { printf("Q(%d): ", atoi(yytext)); }
.  ;
\n ;
%%

int main() {
  yylex();
  printf("a prova vale: %0.2f pontos\n", sum);
}
