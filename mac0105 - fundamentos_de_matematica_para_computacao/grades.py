def mean(list):
  sum = 0.0
  for i in list:
    sum += i
  return sum / len(list)


def of_ten(mean):
  return mean * 2

s = [0, 1, 5, 3, 5, 5]
e = [3, 5, 5, 5, 0, 0, 5, 5, 5, 0, 5, 4, 5, 0]

print("Média dos S: " + str(mean(s)) + " / " + str(of_ten(mean(s))))
print("Média dos E: " + str(mean(e)) + " / " + str(of_ten(mean(e))))

print("-------------------------")

e = [3, 5, 5, 5, 0, 0, 5, 5, 5, 0, 5, 4, 5, 0, 5, 5, 5, 5]
print("Provável média nos próx. E: " + str(mean(e)) + " / " + str(of_ten(mean(e))))