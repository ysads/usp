package com;

import java.math.BigInteger;

public class Fraction {
	
	/*
		Os objetos desta classe são frações feitas de
		numerador e denominador BigInteger.
	 */
	private BigInteger numerator;
	private BigInteger denominator;	
	

	/*
		Construtor responsável por criar uma Fraction a partir de um
		numerador e de um denominador inteiros
	 */
	public Fraction(BigInteger numerator, BigInteger denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
	}
	

	/*
		Construtor responsável por criar uma Fraction a partir de um
		double (número racional)
	 */
	public Fraction(double d) {
		String s = String.valueOf(d);
		int numberOfDecimalDigits = s.length() - 1 - s.indexOf('.');
		this.denominator = BigInteger.ONE;

		for (int i = 0; i < numberOfDecimalDigits; i++){
			d *= 10;
			this.denominator = this.denominator.multiply(BigInteger.TEN);
		}

		this.numerator = BigInteger.valueOf(Math.round(d));
	}
	

	/*
		Método cujo objetivo é colocar uma fração em sua forma irredutível  
	 */
	public void reduce() {
		/*
			Encontra o máximo divisor comum entre o numerador e o denominador
		 */
		BigInteger gcd = this.getNumerator().gcd(this.getDenominator());
		
		/*
			Divide tanto numerador quanto denominador pelo máximo divisor comum,
			de modo a simplificar a fração
		 */
		numerator = numerator.divide(gcd);
		denominator = denominator.divide(gcd);
		
		this.setNumerator(numerator);
		this.setDenominator(denominator);
	}
	

	/*
		Método cujo propósito é fazer subtrações com frações. Sua implementação
		foi necessária para que a fração resultante ficasse na forma a/b - c/d,
		com a,b,c,d inteiros. Do contrário, teríamos como resultado um número real.
	 */
	public static Fraction subtract(Fraction f1, Fraction f2) {
		BigInteger a = f1.getNumerator();
		BigInteger b = f1.getDenominator();
		BigInteger c = f2.getNumerator();
		BigInteger d = f2.getDenominator();
		
		return new Fraction(((a.multiply(d)).subtract(b.multiply(c))), (b.multiply(d)));
	}


	/*
		Getters, setters e toString
	 */
	public BigInteger getNumerator() {
		return numerator;
	}

	public void setNumerator(BigInteger numerator) {
		this.numerator = numerator;
	}

	public BigInteger getDenominator() {
		return denominator;
	}

	public void setDenominator(BigInteger denominator) {
		this.denominator = denominator;
	}
	
	@Override
	public String toString() {
		return numerator + " / " + denominator;
	}
	
}
