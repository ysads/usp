package com;

import java.math.BigInteger;
import java.util.ArrayList;

public class EgyptianFraction {

	/* 
	 * Os objetos desta classe são frações egípcias, ou seja, 
	 * frações representadas na forma de uma soma de frações unitárias.
	 */
	private ArrayList<Fraction> egyptianFraction = new ArrayList<Fraction>();	
	

	/*
		Construtor responsável por criar um objeto EgyptianFraction a partir de uma
		Fraction (a/b, com a,b inteiros)
	 */
	public EgyptianFraction(Fraction f) {
		
		/*
			Inicialmente é preciso deixar a fração em sua forma irredutível
		 */
		f.reduce();
			
		/*
			Em seguida, verifica se a fração já está na forma unitária. Caso já esteja,
			nada precisa ser feito, e o número já está na forma de uma fração egípcia 
		 */
		if (f.getNumerator().equals(BigInteger.ONE)) {
			this.egyptianFraction.add(f);
		}
		
		/*
			Caso contrário, computa iterativamente as próximas frações egípcias relacionadas
			à Fraction f em questão
		 */
		else {
			/*
				Calcula q, que por definição é o teto do inverso da Fraction f
			 */
			BigInteger q = ( (f.getDenominator()).divide(f.getNumerator()) ).add(BigInteger.ONE);
			
			/*
				f1 é equivalente à Fraction 1/q
			 */
			Fraction f1 = new Fraction(BigInteger.ONE, q);
			this.egyptianFraction.add(f1);
			
			/*
				f2 é a fração egípcia feita a partir de f - f1
			 */
			Fraction f2 = Fraction.subtract(f, f1);
			this.egyptianFraction.addAll(new EgyptianFraction(f2).getEgyptianFraction());
		}
	}
	

	/*
		Getters, setters e toString
	 */
	public ArrayList<Fraction> getEgyptianFraction() {
		return egyptianFraction;
	}

	@Override
	public String toString() {
		String s = new String();
		for (Fraction f : egyptianFraction) {
			s += f.toString() + " + ";
		}
		return s.substring(0, s.length()-3);
	}
	
}
