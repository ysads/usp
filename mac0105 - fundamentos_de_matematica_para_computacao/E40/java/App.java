package com;

import java.math.BigInteger;

public class App {
	
	public static void main(String[] args) {
		
		/*
			O construtor de Fraction é polimórfico, e, por isso, o programa pode iniciar tanto
			a partir de um double, quanto a partir de um numerador e um denominador BigInteger
			
			Para iniciar a partir de um double basta fazer o seguinte:
			  double d = 0.125;
			  Fraction f = new Fraction(d);
		*/
		Fraction f = new Fraction(BigInteger.valueOf(5), BigInteger.valueOf(121));

		System.out.println("fração inicial: " + f);
		
		EgyptianFraction ef = new EgyptianFraction(f);
		System.out.println("frações egípcias: " + ef);
	}
	
}
