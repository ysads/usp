class Fraction

  attr_accessor :numerator, :denominator

  def initialize(numerator, denominator)
    @numerator = numerator
    @denominator = denominator
  end

  def reduce
    gcd = @numerator.gcd(denominator)
    @numerator /= gcd
    @denominator /= gcd
    self
  end

  def self.subtract(f1, f2)
    a = f1.numerator
    b = f1.denominator
    c = f2.numerator
    d = f2.denominator

    Fraction.new((a * d) - (b * c), (b * d))
  end

  def to_s
    "#{numerator} / #{denominator}"
  end
end


class EgyptianFraction

  attr_accessor :fractions

  def initialize(base_fraction)
    base_fraction.reduce

    if base_fraction.numerator == 1
      @fractions << base_fraction 
    else
      q = (base_fraction.denominator / base_fraction.numerator) + 1
    end
  end
end


f1 = Fraction.new(2, 3)
f2 = Fraction.new(2, 8)
f3 = Fraction.subtract(f1, f2)