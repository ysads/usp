# AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
# DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
# TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
# DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
# DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
# OU PLÁGIO.  
# DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
# DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
# SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
# DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
# DIVULGADOS NA PÁGINA DA DISCIPLINA.
# ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
# AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

# Nome : Ygor Sad Machado
# NUSP : 8910368
# Exer.: E54.iii
# Prof.: Yoshiharu Kohayakawa


class GDC

  attr_reader :alpha, :betha, :value

  def initialize(a, b)
    @a = a
    @b = b
    evaluate
  end

  def coeficients
    {
      alpha: @alpha,
      betha: @betha
    }
  end

  def to_s
    evaluate if @value.nil?
    "gdc(#{@a},#{@b}) = #{@alpha}a + #{@betha}b = #{@value}"
  end

  private

  def evaluate
    m = [[@a, 1, 0], [@b, 0, 1]]

    while m[1][0] != 0
      q = m[0][0] / m[1][0]
      n = [[0, 1], [1, -q]]

      m = multiply_matrix n, m
    end
    
    @alpha = m[0][1]
    @betha = m[0][2]
    @value = m[0][0]
  end

  def multiply_matrix(mat_1, mat_2)
    result = []

    lines = mat_1.length - 1
    columns = mat_2[0].length - 1
    element_count = mat_1[0].length - 1

    for i in (0..lines)
      line = []
      for j in (0..columns)
        sum = 0
        for k in (0..element_count)
          sum += mat_1[i][k] * mat_2[k][j]
        end
        line << sum
      end
      result << line
    end
    result
  end
  
end


puts "Type a, b (one in each line): "
a = gets.to_i
b = gets.to_i

puts GDC.new(a, b)