# AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
# DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
# TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
# DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
# DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
# OU PLÁGIO.  
# DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
# DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
# SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
# DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
# DIVULGADOS NA PÁGINA DA DISCIPLINA.
# ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
# AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

# Nome : Ygor Sad Machado
# NUSP : 8910368
# Exer.: E54.iii
# Prof.: Yoshiharu Kohayakawa


def multiply_matrix(mat_1, mat_2)
  result = []

  mat_1_lin = mat_1.length - 1
  mat_1_col = mat_1[0].length - 1

  mat_2_lin = mat_2.length - 1
  mat_2_col = mat_2[0].length - 1

  for i in (0..mat_1_lin)
    line = []

    for j in (0..mat_2_col)
      sum = 0
    
      for k in (0..mat_1_col)
        sum += mat_1[i][k] * mat_2[k][j]
      end

      line << sum
    end

    result << line
  end

  result
end


def gdc(a, b)
  m = [[a, 1, 0], [b, 0, 1]]

  loop do
    quot = m[0][0] / m[1][0]
    coef = [[0, 1], [1, -quot]]

    m_next = multiply_matrix coef, m

    if m_next[1][0] == 0
      alpha = m_next[0][1]
      betha = m_next[0][2]
      gdc   = m_next[0][0]

      return {
        alpha: alpha,
        betha: betha,
        gdc: gdc
      }
    end

    m = m_next
  end
end


puts gdc(294, 108).to_s
puts gdc(39, 13).to_s
puts gdc(39, 2).to_s
puts gdc(40, 28).to_s
