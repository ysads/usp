# coding=utf-8
from satispy import Variable, Cnf
from satispy.solver import Minisat


def can_write_cell(sudoku_board, row, col):
    possible_values = [0, 1, 2, 3]

    for val in possible_values:

        # Tenta preencher a célula com algum valor
        sudoku_board[row][col] = val
        collision = False
        
        # Trava coluna e depois linha pra ver se há
        # algum choque com o valor posicionado
        for i in range(4):
            if sudoku_board[row][col] in [sudoku_board[i][col], sudoku_board[row][i]]:
                collision = True
                break

        # Define os limites dos subquadrados que serão
        # analisados em busca e colisões
        i = row if (row % 2 == 0) else row - 1
        j = col if (col % 2 == 0) else col - 1

        for i in range(i, i + 2):
            for j in range(j, j + 2):
                if [row, col] != [i, j] and sudoku_board[row][col] == sudoku_board[i][j]:
                    collision = True
                    break

            if collision:
                break
                
    return collision


solver = Minisat()

file = open('testes.txt')

x = [[[0 for k in range(4)] for j in range(4)] for i in range(4)]

for i in range(4):
    for j in range(4):
        for k in range(4):
            x[i][j][k] = Variable('x_'+str(i)+str(j)+'_'+str(k))

sudoku = file.readline()
n = 1

while sudoku != '':

########################################################################################
########################################################################################
## Escreva aqui o código gerador da formula CNF que representa o Sudoku codificado na ##
## variavel 'sudoku'. Nao se esqueca que o codigo deve estar no escopo do 'while'     ##
## acima e deve guardar a formula CNF na variavel 'cnf'.                              ##
########################################################################################
########################################################################################
    sudoku_board = [[-1 for k in range(4)] for j in range(4)]

    # Precisamos instanciar as informações desse sudoku em uma matriz.
    # Note que o split serve para dividir a informação da linha em células.
    for vals in sudoku.split(";"):

        if vals == "" or vals == "\n":
            break
        
        # Casting porque os valores são lidos como string 
        row, col, val = vals.split(" ")
        sudoku_board[int(row)][int(col)] = int(val)


    cells_placed = []
    current_cell = 0
    cells_left = True

    while cells_left:
        row = current_cell // 4;
        col = current_cell % 4;

        # Já tem algo aqui, a gente não precisa fazer nada,
        # apenas avançar pra próxima célula
        if sudoku_board[row][col] != -1:
            current_cell += 1
            continue

        if can_write_cell(sudoku_board, row, col):
            cells_placed.append(current_cell)
            current_cell += 1

        # Tem que fazer backtracking se não deu pra colocar a célular
        else:
            sudoku_board[row][col] = -1
            current_cell = cells_placed.pop()



########################################################################################
########################################################################################
## Fim do código gerador da formula CNF.                                              ##
########################################################################################
########################################################################################

    sol = [[0 for j in range(4)] for i in range(4)]

    solution = solver.solve(cnf)

    if solution.success:
        print("Solution %d:\n" % n)
        for i in range(4):
            for j in range(4):
                for k in range(4):
                    if solution[x[i][j][k]]:
                        sol[i][j] = k

        print("%d %d | %d %d" % (sol[0][0], sol[0][1], sol[0][2], sol[0][3]))
        print("%d %d | %d %d" % (sol[1][0], sol[1][1], sol[1][2], sol[1][3]))
        print("---------")
        print("%d %d | %d %d" % (sol[2][0], sol[2][1], sol[2][2], sol[2][3]))
        print("%d %d | %d %d\n" % (sol[3][0], sol[3][1], sol[3][2], sol[3][3]))

    else:
        print("There is no solution.\n")

    n+=1

    sudoku = file.readline()

file.close()
