/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    A confecção desse EP necessitou, além das documentações
 *    básicas da biblioteca algs4 e da biblioteca padrão do Java,
 *    da leitura dos artigos da Wikipedia sobre:
 *     - Nearest Neighbor Searching: https://en.wikipedia.org/wiki/Nearest_neighbor_search
 *     - Range Searching: https://en.wikipedia.org/wiki/Range_searching
 *
 *  Bugs & Limitações:
 *    A complexidade dos métodos range() e nearest(), por ser
 *    linear, cresce tanto quanto o número de itens na ST. Eles
 *    não são, portanto, muito eficientes para tabela de símbolos
 *    muito grandes, visto que seria necessário percorrer toda
 *    a tabela diversas vezes, se chamadas consecutivas a esses
 *    métodos fossem realizadas.
 */

import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.RedBlackBST;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;

public class PointST<Value> {

    /**
     * A red-black binary search tree that will
     * back our ST implementation
     */
    private RedBlackBST<Point2D,Value> bst;


    /**
     * An integer that holds the current size of
     * the ST
     */
    private int size;


    /**
     * Construct an empty ST of points
     */
    public PointST() {
        this.bst = new RedBlackBST<Point2D,Value>();
        this.size = 0;
    }

    /**
     * The ST is empty?
     */
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * Number of points contained in the ST
     */
    public int size() {
        return this.size;
    }

    /**
     * Associate a point with a given value
     */
    public void put(Point2D point, Value val) {
        if (point == null || val == null) {
            throw new IllegalArgumentException();
        }
        bst.put(point, val);
        this.size++;
    }

    /**
     * Returns the value associated with a given point
     */
    public Value get(Point2D point) {
        if (point == null) {
            throw new IllegalArgumentException();
        }
        return bst.get(point);
    }

    /**
     * Returns true if the ST contains a given point
     */
    public boolean contains(Point2D point) {
        if (point == null) {
            throw new IllegalArgumentException();
        }
        return bst.contains(point);
    }

    /**
     * Returns an object iterable over all points in ST
     */
    public Iterable<Point2D> points() {
        return bst.keys();
    }

    /**
     * Returns a collection with all points in the ST that
     * are also within the argument rect
     */
    public Iterable<Point2D> range(RectHV rect) {
        ArrayList<Point2D> containedPoints = new ArrayList<Point2D>();

        if (rect == null) {
            throw new IllegalArgumentException();
        }

        for (Point2D currentPoint : points()) {
            if (rect.contains(currentPoint)) {
                containedPoints.add(currentPoint);
            }
        }
        return containedPoints;
    }

    /**
     * Iterates over the ST comparing, one by one, every point available
     * with the querying point, looking for the minimun distance. This
     * approach is naïve, since it must run through the whole ST
     */
    public Point2D nearest(Point2D point) {
        Point2D nearestPoint = null;
        double minSquaredDistance = Double.MAX_VALUE;
        double currentDistance;

        if (point == null) {
            throw new IllegalArgumentException();
        }

        for (Point2D currentPoint : points()) {
            currentDistance = point.distanceSquaredTo(currentPoint);

            if (currentDistance < minSquaredDistance) {
                nearestPoint = currentPoint;
                minSquaredDistance = currentDistance;
            }
        }
        return nearestPoint;
    }

    /**
     * Unit testing for the class
     */
    public static void main(String[] args) {
        RectHV rect;
        Point2D point;
        PointST<Point2D> pointST = new PointST<Point2D>();

        StdOut.println("\nPointST empty? " + pointST.isEmpty());
        StdOut.println("PointST size: " + pointST.size());

        StdOut.println("\nAdding 7 random points to ST");
        for (int i = 0; i < 7; i++) {
            point = new Point2D(StdRandom.uniform(0.0, 1.0), StdRandom.uniform(0.0, 1.0));
            pointST.put(point, point);
        }

        StdOut.println("\nPointST empty? " + pointST.isEmpty());
        StdOut.println("PointST size: " + pointST.size());

        StdOut.println("\nIterating through the ST: ");
        for (Point2D p : pointST.points()) {
            StdOut.println("=> " + p.toString());
        }

        StdOut.println("\nFinding the points in ST nearest to: ");
        double[][] coords = {{0.00, 1.00},
                             {0.50, 0.12},
                             {0.75, 0.25},
                             {0.33, 0.67}};
        for (double[] coord : coords) {
            point = new Point2D(coord[0], coord[1]);
            StdOut.print("(" + coord[0] + ", " + coord[1] + ")\t=> ");
            StdOut.println(pointST.nearest(point).toString());
        }

        StdOut.println("\nTesting if all points are contained in the unitary square: ");
        rect = new RectHV(0.0, 0.0, 1.0, 1.0);
        for (Point2D p : pointST.range(rect)) {
            StdOut.println(p.toString());
        }

        StdOut.println("\nTesting which points are within rect [0, 0.5] x [0, 0.75]: ");
        rect = new RectHV(0.0, 0.0, 0.5, 0.75);
        for (Point2D p : pointST.range(rect)) {
            StdOut.println(p.toString());
        }
    }
}