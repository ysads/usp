/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa.
 *
 *  Bugs & Limitações:
 *    Não foram previamente identificada quaisquer bugs ou limitações
 *    que a solução abaixo possa vir a ter.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.Stopwatch;

import java.lang.Math;
import java.lang.IllegalArgumentException;

public class PercolationStats {

    /**
     * Dimension of the grid to be testes by the
     * percolation class
     */
    private int n;

    /**
     * Number of time Percolation should be testes
     */
    private int trials;

    /**
     * An array to hold the results of each individual
     * experiment ran on the Percolation class
     */
    private int[] results;

    /**
     * Perform independent trials on an n-by-n grid
     */
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }

        this.n = n;
        this.trials = trials;
        this.results = new int[trials];

        performTests();
    }

    /**
     * Sample mean of percolation threshold
     */
    public double mean() {
        return StdStats.mean(this.results) / (this.n * this.n);
    }

    /**
     * Sample standard deviation of percolation threshold
     */
    public double stddev() {
        return StdStats.stddev(this.results) / (this.n * this.n);
    }

    /**
     * Low endpoint of 95% confidence interval
     */
    public double confidenceLow() {
        return mean() - (1.96 * stddev() / Math.sqrt(trials));
    }

    /**
     * High endpoint of 95% confidence interval
     */
    public double confidenceHigh() {
        return mean() + (1.96 * stddev() / Math.sqrt(trials));
    }

    /**
     * Unit tests for PercolationStats class
     */
    public static void main(String[] args) {
        int n      = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);

        Stopwatch watch = new Stopwatch();
        PercolationStats stats = new PercolationStats(n, trials);
        
        StdOut.println("mean()\t\t\t = " + stats.mean());
        StdOut.println("stddev()\t\t = " + stats.stddev());
        StdOut.println("confidenceLow()\t\t = " + stats.confidenceLow());
        StdOut.println("confidenceHigh()\t = " + stats.confidenceHigh());
        StdOut.println("elapsed time\t\t = " + watch.elapsedTime());
    }

    /**
     * Execute several independent percolation tests
     * to extract a more real threshold
     */
    private void performTests() {
        Percolation perc;
        int row, col;

        for (int i = 0; i < trials; i++) {
            /**
             * Keep opening sites randomly until the
             * system finally percolates
             */
            perc = new Percolation(this.n);

            do {
                /**
                 * Keep generating random sites until
                 * a blocked site is found
                 */
                do {
                    row = StdRandom.uniform(0, this.n);
                    col = StdRandom.uniform(0, this.n);
                } while (perc.isOpen(row, col));
                
                perc.open(row, col);
            } while (!perc.percolates());

            /**
             * Account this threshold into the results
             */
            this.results[i] = perc.numberOfOpenSites();
        }
    }
}