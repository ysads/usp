/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa.
 *
 *  Bugs & Limitações:
 *    Não foram previamente identificada quaisquer bugs ou limitações
 *    que a solução abaixo possa vir a ter.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

import java.lang.IllegalArgumentException;

public class Percolation {

    final int BLOCKED_SITE      = -1;
    final int OPEN_EMPTY_SITE   = 0;
    final int OPEN_FULL_SITE    = 1;

    /**
     * Represents the grid height and width
     */
    private int n;

    /**
     * Holds the current number of open sites, whether they are
     * empty or full
     */
    private int numOpenSites;

    /**
     * For the sake of simplicity, a unidimensional array is
     * used to represent the bidimensional index
     */
    private int[] grid;

    /**
     * Auxiliary array used to determine whether the i-th index
     * is reaches the bottom or not
     */
    private boolean[] reachBottom;

    /**
     * A union-find data structure used to hold the connections
     * between different sites
     */
    private WeightedQuickUnionUF unionFind;

    /**
     * Initializes the n-by-n grid with all sites blocked
     */
    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }

        this.n = n;
        this.numOpenSites = 0;

        this.unionFind = new WeightedQuickUnionUF(n * n);
        this.grid = new int[n * n];
        this.reachBottom = new boolean[n * n];

        /**
         * Converts the bidimensional representation of the
         * indexs into a unidimensional index so we can make
         * all sites blocked in the array
         */
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.grid[index1D(i, j)] = this.BLOCKED_SITE;
            }
        }
        initializeReachBottom();
    }

    /**
     * Opens the site (row, col) if it is not open already
     */
    public void open(int row, int col) {
        int index = index1D(row, col);
        int parent;
        int rowN, colN, indexN;
        boolean anyReachBottom;

        if (index < 0) {
            throw new IllegalArgumentException();
        }

        /**
         * If the site we're opening is at the grid's first
         * row, we should make it full. Otherwise, make it
         * empty
         */
        if (index < this.n) {
            this.grid[index] = this.OPEN_FULL_SITE;
        }
        else {
            this.grid[index] = this.OPEN_EMPTY_SITE;    
        }
        this.numOpenSites++;

        /**
         * These are the direct neighbours of the site we're
         * currently analyzing. We will check if one of them
         * is OPEN_FULL_SITE to decide the parent's new status
         */
        int[][] neighbours = { 
            { row - 1, col },
            { row + 1, col },
            { row, col - 1 },
            { row, col + 1 }
        };

        for (int[] neighbour : neighbours) {
            rowN   = neighbour[0];
            colN   = neighbour[1];
            indexN = index1D(rowN, colN);

            /**
             * The neighbour must be within the grid borders
             * to prevent ArrayIndexOutOfBoundsException
             */
            if (indexN < 0) {
                continue;
            }

            if (isOpen(rowN, colN)) {

                /**
                 * Before uniting the components, we save whether any
                 * of them, individually, reaches the bottom of the grid
                 */
                anyReachBottom = this.reachBottom[index] || this.reachBottom[indexN];

                /**
                 * If either the current site or one of its neighbours
                 * are an OPEN_FULL_SITE, then both of them should be
                 * OPEN_FULL_SITE and this should be propagated to all
                 * of the current site component's elements
                 */
                if (isFull(row, col) || isFull(rowN, colN)) {
                    unionFind.union(index, indexN);
                    
                    /**
                     * We should now get the new component's parent so
                     * we can update it to OPEN_FULL_SITE
                     */
                    parent = unionFind.find(index);
                    this.grid[parent] = this.OPEN_FULL_SITE;
                }
                else {
                    unionFind.union(index, indexN);
                }

                /**
                 * If any of the indexes reaches the bottom, then we
                 * should update the newly joint component's parent
                 * reference to true
                 */
                if (anyReachBottom) {
                    this.reachBottom[unionFind.find(index)] = true;
                }
            }
        }
    }

    /**
     * Is the site (row, col) open? This happens to be true
     * if and only if its parent is open
     */
    public boolean isOpen(int row, int col) {
        int index = index1D(row, col);
        int parent;

        if (index < 0) {
            throw new IllegalArgumentException();
        }

        parent = unionFind.find(index);
        return this.grid[parent] == this.OPEN_EMPTY_SITE ||
               this.grid[parent] == this.OPEN_FULL_SITE;
    }

    /**
     * Is the site (row, col) full? This happens to be true
     * if and only if its parent is full
     */
    public boolean isFull(int row, int col) {
        int index = index1D(row, col);
        int parent;

        if (index < 0) {
            throw new IllegalArgumentException();
        }

        parent = unionFind.find(index);
        return this.grid[parent] == this.OPEN_FULL_SITE;
    }

    public int getRepresentation(int row, int col) {
        return unionFind.find(index1D(row, col));
    }

    /**
     * Returns the number of open sites
     */
    public int numberOfOpenSites() {
        return this.numOpenSites;
    }

    /**
     * Does the system percolate? To find out, we simply
     * verify each element of the bottom-most row to check
     * if they are linked to any element in top-most row
     */
    public boolean percolates() {
        int index = unionFind.find(0);
        return this.reachBottom[index];
    }


    /**
     * Unit tests for Percolation class
     */
    public static void main(String[] args) {
        int n = 3;
        Percolation perc = new Percolation(n);

        StdOut.println("Examples run for n = 3");
        
        StdOut.println("\nInitial state");
        StdOut.println("Percolates? " + perc.percolates());
        StdOut.println("Number of open sites: " + perc.numberOfOpenSites());

        int[][] indexes = {{0, 0}, {2, 1}, {0, 1}, {1, 2}, {1, 1}};

        for (int[] index : indexes) {
            int row = index[0];
            int col = index[1];

            StdOut.println("\n========================\n");

            StdOut.println("(" + row + "," + col + ") ");
            StdOut.println("\nisOpen: " + perc.isOpen(row, col));
            StdOut.println("isFull: " + perc.isFull(row, col));

            StdOut.println("\nOpening...");
            perc.open(row, col);

            StdOut.println("\nisOpen: " + perc.isOpen(row, col));
            StdOut.println("isFull: " + perc.isFull(row, col));

            StdOut.println("\n--- Percolates now? " + perc.percolates());
            StdOut.println("--- Number of open sites: " + perc.numberOfOpenSites());
        }
    }


    /**
     * Initializes the auxiliary array used to represent
     * whether a given index reaches bottom or not
     */
    private void initializeReachBottom() {
        for (int i = 0; i < this.n - 1; i++) {
            for (int j = 0; j < this.n; j++) {
                this.reachBottom[index1D(i, j)] = false;
            }
        }
        /**
         * Since the last line is at the bottom, we mark
         * their references as true
         */
        for (int i = 0; i < this.n; i++) {
            this.reachBottom[index1D(n - 1, i)] = true;
        }
    }


    /**
     * Converts a bidimensional index represented by
     * row and column into an unidimensional index. No
     * Converts a bidimensional index - represented byte 
     * a negative value is return whenever an index out
     * of bounds is passed as argument
     */
    private int index1D(int row, int col) {
        if (row < 0 || row >= this.n || col < 0 || col >= this.n) {
            return -1;
        }
        return row * this.n + col;
    }
}