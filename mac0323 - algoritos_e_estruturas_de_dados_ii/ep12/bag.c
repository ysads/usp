/*
* MAC0323 Algoritmos e Estruturas de Dados II
* 
* ADT Bag implementada com lista ligada de itens. 
*  
*    https://algs4.cs.princeton.edu/13stacks/
*    https://www.ime.usp.br/~pf/estruturas-de-dados/aulas/bag.html
* 
* Os itens desta implementação são genéricos: "shallow copy" dos itens dados.
*
*----------------------------------------------------------------------------
* Object copying
* https://en.wikipedia.org/wiki/Object_copying
* 
* Understanding Deep and Shallow Copy 
* https://we-are.bookmyshow.com/understanding-deep-and-shallow-copy-in-javascript-13438bad941c
*
* Shallow copy is a bit-wise copy of an object. A new object is created that has an 
* exact copy of the values in the original object. If any of the fields of the object 
* are references to other objects, just the reference addresses are copied i.e., 
* only the memory address is copied.

* A deep copy copies all fields, and makes copies of dynamically allocated memory 
* pointed to by the fields. A deep copy occurs when an object is copied along with the 
* objects to which it refers.
*
*/

/* interface para o uso da funcao deste módulo */
#include "bag.h"  

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc() */

#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debuging */
#endif

/*----------------------------------------------------------*/
/* 
* Estrutura Básica da Bag
* 
* Implementação com listas ligada dos itens.
*/
typedef struct node {
    struct node *next;
    void *item;
    size_t itemSize;
} Node;

struct bag {
    Node *first;
    Node *iterator;
    int length;
};

/*------------------------------------------------------------*/
/* 
* Protótipos de funções administrativas: tem modificador 'static'
* 
*/
static void *
duplicate(const void *item, size_t nBytes);

static Node*
newNode(const void *item, size_t size);

static void
freeNode(Node *node);

/*-----------------------------------------------------------*/
/*
*  newBag()
*
*  RETORNA (referência/ponteiro para) uma bag vazia.
* 
*/
Bag
newBag()
{
    Bag bag;

    bag = (Bag) emalloc(sizeof(struct bag));
    bag->length = 0;
    bag->first = NULL;
    bag->iterator = NULL;

    return bag;
}

/*-----------------------------------------------------------*/
/*
*  freeBag(BAG)
*
*  RECEBE uma Bag BAG e devolve ao sistema toda a memoria 
*  utilizada.
*
*/
void  
freeBag(Bag bag)
{
    Node *node;
    Node *next;

    node = bag->first;

    /**
     * Enquanto houver um próximo nó, desaloca-os, juntamente
     * com o seu conteúdo.
     */
    while (node != NULL) {
        next = node->next;
        freeNode(node);
        node = next;
    }
    free(bag);
}    

/*------------------------------------------------------------*/
/*
* OPERAÇÕES USUAIS: add(), size(), isEmpty() e itens().
*/

/*-----------------------------------------------------------*/
/*
*  add(BAG, ITEM, NITEM)
* 
*  RECEBE uma bag BAG e um ITEM e insere o ITEM na BAG.
*  NITEM é o número de bytes de ITEM.
*
*  Para criar uma copia/clone de ITEM é usado o seu número de bytes NITEM.
*
*/
void  
add(Bag bag, const void *item, size_t nItem)
{
    Node *newFirst;

    /**
     * Algumas verificações de segurança para alertar no caso de algum
     * parâmetro ser NULL.
     */
    if (bag == NULL) {
        ERROR("[add] bag arg can't be NULL");
        return;
    }
    if (item == NULL) {
        ERROR("[add] item arg can't be NULL");
        return;
    }

    /**
     * Atualiza os ponteiros da bag, de modo que o nó recém-alocado
     * se torne o novo primeiro nó da bag.
     */
    newFirst = newNode(item, nItem);
    newFirst->next = bag->first;

    bag->first = newFirst;
    bag->length++;
}    

/*-----------------------------------------------------------*/
/* 
*  SIZE(BAG)
*
*  RECEBE uma bag BAG
* 
*  RETORNA o número de itens em BAG.
*/
int
size(Bag bag)
{
    /**
     * Alerta que o bag passada ainda não foi inicializada, mas
     * retorna 0 para manter a consistência da API.
     */
    if (bag == NULL) {
        ERROR("[size] bag can't be NULL");
        return 0;
    }
    return bag->length;
}

/*-----------------------------------------------------------*/
/* 
*  ISEMPTY(BAG)
*
*  RECEBE uma bag BAG.
* 
*  RETORNA TRUE se BAG está vazia e FALSE em caso contrário.
*
*/
Bool
isEmpty(Bag bag)
{
    return ((bag == NULL) || (bag->length == 0));
}

/*-----------------------------------------------------------*/
/* 
*  ITENS(BAG, INIT)
* 
*  RECEBE uma bag BAG e um Bool INIT.
*
*  Se INIT é TRUE,  ITENS() RETORNA uma cópia/clone do primeiro item na lista de itens na BAG.
*  Se INIT é FALSE, ITENS() RETORNA uma cópia/clone do item sucessor do último item retornado.
*  Se BAG está vazia ou não há sucessor do último item retornada, ITENS() RETORNA NULL.
*
*  Se entre duas chamadas de ITENS() a BAG é alterada, o comportamento é  indefinido. 
*  
*/
void * 
itens(Bag bag, Bool init)
{
    Node *node;

    if (bag == NULL) {
        ERROR("[itens] bag can't be NULL");
    }

    /**
     * Se init é TRUE, reseta o pointeiro node, de modo que ele volte
     * a apontar para o primeiro item na bag. Além disso, se o iterador
     * não tiver atingido o fim da bag é preciso atualizá-lo.
     */
    if (init) {
        bag->iterator = bag->first;
    }
    else if (bag->iterator) {
        bag->iterator = bag->iterator->next;
    }
    node = bag->iterator;

    /**
     * Se a bag estiver vazia ou se não houver um próximo item para
     * retornar, então node vai ser NULL, e a função deve retornar NULL.
     * Caso contrário, retorna uma cópia do item armazenado em node. 
     */
    if (node == NULL) {
        return NULL;
    }
    else {
        return duplicate(node->item, node->itemSize);
    }
}

/*------------------------------------------------------------*/
/* 
* Implementaçao de funções administrativas
*/

/**
 * Cria uma cópia de um ponteiro passado, alocando espaço para o seu
 * conteúdo e, em seguida, copiando-o para a nova posição de memória
 *
 * @param item: um ponteiro cujo conteúdo será copiado
 * @return um ponteiro para a posição de memória com o conteúdo copiado
 */
static void *
duplicate(const void *item, size_t nBytes)
{
    void *copy = emalloc(nBytes);
    memmove(copy, item, nBytes);
    return copy;
}

/**
 * Libera o espaço ocupado por um nó e seus atributos.
 * 
 * @param node: um ponteiro para o nó a ser desalocado.
 */
static void
freeNode(Node *node) {
    free(node->item);
    free(node);
}

/**
 * Instancia um novo nó cujo conteúdo vai ser uma cópia do conteúdo
 * armazenado no ponteiro item. Armazena também o tamanho, em bytes,
 * de item, de modo que ele possa ser usado depois.
 *
 * @param item: um ponteiro para o item a ser armazenado no novo nó.
 * @param size: o tamanho, em bytes, do item.
 * @return um ponteiro para o nó recém alocado.
 */
static Node*
newNode(const void *item, size_t itemSize) {
    Node *node;

    node = (Node*) emalloc(sizeof(Node));

    node->next     = NULL;
    node->item     = duplicate(item, itemSize);
    node->itemSize = itemSize;

    return node;
}
