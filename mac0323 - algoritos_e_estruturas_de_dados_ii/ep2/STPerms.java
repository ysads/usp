/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Nesse EP, foram discutidas algumas questões relativas à otimização
 *    dos tempos de execução, bem como possíveis técnicas para estimar
 *    os valores s e t para as subsequências. A solução final, no entanto,
 *    segue um caminho baseado em backtracking - que não foi discutida
 *    previamente, ainda que seja uma opção relativamente óbvia para
 *    um problema de tentativa e erro. Algumas das referências utilizadas
 *    para a costura final da solução foram:
 *    
 *      - https://docs.oracle.com/javase/8/docs/api/index.html?java/util/package-summary.html
 *        Referência de uso da classe Stack<T>
 *        
 *      - https://introcs.cs.princeton.edu/java/23recursion/Permutations.java.html
 *        Referência para geração de todas as permutações de uma String
 *
 *  Bugs & Limitações:
 *    Por ser um algoritmo baseado em uma solução exaustiva, sua performance
 *    está diretamente ligada ao número de permutações que precisam ser
 *    avaliadas. Assim, para valores de n muito grandes (> 9), o algoritmo
 *    começa a ter tempos de execução muito elevados.
 */

/**
 *  Modo de Usar
 *  Compilation:  javac-algs4 STPerms.java
 *  Execution:    java STPerms n s t opcao
 *  
 *  Enumera todas as (s,t)-permutações das n primeiras letras do alfabeto.
 *  As permutações devem ser exibidas em ordem lexicográfica.
 *  Sobre o papel da opcao, leia o enunciado do EP.
 *
 *  % java STPerms 4 2 2 0
 *  badc
 *  bdac
 *  cadb
 *  cdab
 *  % java STPerms 4 2 2 1
 *  4
 *  % java STPerms 4 2 2 2
 *  badc
 *  bdac
 *  cadb
 *  cdab
 *  4
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

import java.util.Stack;

public class STPerms {

    private static int n;
    private static int s;
    private static int t;

    private static int count;
    private static int option;


    private static int largestSequence(String permutation, boolean increasing) {
        int base = 0;
        int curr = 1;

        /**
         * Starting lenCurrSeq with 1 because subsequences always
         * have at least one character
         */
        int lenCurrSeq = 1;
        int lenLgstSeq = 1;

        Stack<Character> stackChars = new Stack<Character>();
        Stack<Integer>   stackIndex = new Stack<Integer>();

        while (base < n - lenLgstSeq ) {
            stackChars.push(permutation.charAt(base));
            stackIndex.push(base);

            curr = base + 1;
            lenCurrSeq = 1;

            while (!stackChars.empty()) {
                /**
                 * When we reach the end of the permutation array, it's
                 * time to backtrack. We update the largest sequence size
                 * if we found another bigger sequence and downgrade current
                 * sequence size
                 */
                if (curr >= n) {
                    curr = stackIndex.pop() + 1;
                    stackChars.pop();
                    
                    lenLgstSeq = Math.max(lenLgstSeq, lenCurrSeq);
                    lenCurrSeq -= 1;

                    /**
                     * We must continue to next iteration for safety reasons,
                     * because we still can be out of permutation array
                     * boundaries which would throw an exception
                     */
                    continue;
                }

                /**
                 * We must save the current stackChars's top position
                 * to know where we should backtrack, once we reach
                 * the end of the permutation array
                 */
                if ((!increasing && permutation.charAt(curr) < stackChars.peek()) ||
                    ( increasing && permutation.charAt(curr) > stackChars.peek())) {

                    stackChars.push(permutation.charAt(curr));
                    stackIndex.push(curr);

                    lenCurrSeq += 1;
                }
                curr++;
            }
            base++;
        }
        return lenLgstSeq;
    }

    private static void permutations(String fixed, String perm) {
        int len = perm.length();

        /**
         * A 0 length to perm means we already a permutation mounted and thus
         * we can evaluate it to know whether it's an (s,t)-permutation or not
         */
        if (len == 0) {
            int largeIncreasing = largestSequence(fixed, true);
            int largeDecreasing = largestSequence(fixed, false);

            if (largeIncreasing == s && largeDecreasing == t) {
                count += 1;

                /**
                 * Show information only if requested by the user, even though
                 * we always count every (s,t)-permutation found
                 */
                if (option != 1) {
                    StdOut.println(fixed);
                }
            }
        }
        else {
            for (int i = 0; i < len; i++) {
                permutations(fixed + perm.charAt(i), perm.substring(0, i) + perm.substring(i+1, len));
            }
        }
    }

    private static void print_help() {
        StdOut.println("Usage: ");
        StdOut.println("java STPerms <n> <s> <t> <option>\n");
        
        StdOut.println("n\t\t Integer smaller of equal to 26, being the number of letters to permute");
        StdOut.println("s\t\t Integer, size of largest increasing subsequence in a permutation");
        StdOut.println("t\t\t Integer, size of largest decreasing subsequence in a permutation");
        
        StdOut.println("option\t\t Integer to indicate in which mode program run. Available: ");
        StdOut.println("\t\t   0: prints all the (s,t)-permutations found");
        StdOut.println("\t\t   1: prints only the number (s,t)-permutations found");
        StdOut.println("\t\t   2: prints the number of found (s,t)-permutations and the permutations itself\n");
    }

    public static void main(String[] args) {
        /**
         * Just a tweak to guide user if s/he doesn't pass the expected number
         * of arguments. This also prevents ArrayIndexOutOfBoundsException
         */
        if (args.length != 4) {
            print_help();
            return;
        }

        n = Integer.parseInt(args[0]);
        s = Integer.parseInt(args[1]);
        t = Integer.parseInt(args[2]);
        option = Integer.parseInt(args[3]);

        /**
         * A simple way to create the base permutation. We just keep "adding"
         * letters to 'a' until we have the first n letters in the string
         */
        String string = "";
        for (int i = 0; i < n; i++) {
            string += (char) ('a' + i);
        }

        Stopwatch timer = new Stopwatch();
        permutations("", string);

        if (option != 0) {
            StdOut.println(count);
        }
    }
}
