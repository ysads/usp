import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

public class Permutation {

    private static int n;

    private static void permutations(String fixed, String perm) {
        int len = perm.length();

        if (len == 0) {
            StdOut.println(fixed);
        }
        else {
            for (int i = 0; i < len; i++) {
                permutations(fixed + perm.charAt(i), perm.substring(0, i) + perm.substring(i+1, len));
            }
        }
    }

    public static void main(String[] args) {
        String[] strs = {
            // "dbac",
            "abc"
        };

        Stopwatch timer = new Stopwatch();
        for (String str : strs) {
            // n = str.length();
            permutations("", str);
        }
        StdOut.println("elapsed time = " + timer.elapsedTime());
    }
}