/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *
 *    - https://docs.oracle.com/javase/7/docs/api/java/lang/String.html
 *    - https://docs.oracle.com/javase/7/docs/api/java/lang/Long.html
 *      Documentação oficial do Java para as classes Long e String,
 *      de onde obtive a forma correta de utilizar os métodos Long.toString(),
 *      String.charAt() e String.substring()
 *      
 *    - https://code.tutsplus.com/articles/understanding-bitwise-operators--active-11301
 *      Referência de como usar o operador | (bitwise OR)
 *
 *
 *  Bugs & Limitações:
 *    
 *    Devido ao fato de o programa depender da geração de 2^n combinações
 *    para analisar, o máximo valor teórico de n que o programa pode receber
 *    é limitado pelo número de bits que o tipo de dados long possui - 64bits.
 *    Na prática, porém, para n=64, 2^n ocupará 65 bits, o que inviabiliza
 *    tal valor. Assim
 */

/**
 *  Modo de Usar
 *  Compilation: javac-algs4 Combinacao.java
 *  Execution:   java Combinacao n k [opcao]
 *  
 *  Enumera todas as combinações dos números em {1,2,...,n} k a k.
 *  Se opcao = 0 (defaul), gera e exibe todas as permutações em ordem 
 *  lexicográfica
 *  Se opcao = 1 apenas, __gera todas__ as combinações, mas __não__ as 
 *  exibe; apenas exibe o total de combinações.
 */

import java.math.BigInteger;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

public class CombinacaoBit {
    
    /**
     * Contador de combinações
     */
    private static int count = 0;
    

    /**
     * Indica o que deve ser impresso na tela
     * 
     * 0 - Imprime o número de combinações e as combinações propriamente ditas
     * 1 - Imprime somente o número de combinações
     */
    private static int opcao = 0;


    public static void combinacao(int n, int k) {
        String strBinario;
        String strCombinacao;

        int pos;
        int numBits1;

        long totalCombinacoes = (long) Math.pow(2, n);
        long numBinNormal;
        long num = totalCombinacoes;

        while (num > 0) {
            /**
             * Esse passo é importante porque normaliza o número de digitos que estarão
             * presentes na representação binária final, de modo que todos os números
             * possuam n bits, mesmo aqueles que forem bem menores - 11, 10, por exemplo.
             */
            numBinNormal = totalCombinacoes | num;

            /**
             * O método substring aqui serve para retirar o bit 1 que aparece na frente
             * de todos os números na representação final. Isso se deve ao fato de que,
             * para representar o total de combinações de n, são necessário n+1 bits.
             */
            strBinario = Long.toString(numBinNormal, 2).substring(1);

            numBits1 = 0;
            strCombinacao = "";
            
            /**
             * É preciso contar o número de bits iguais a 1 na representação binária.
             * Assim, é possível descobrir se esse binário equivale a alguma das
             * combinações em que estamos interessados. Note que não é preciso percorrer
             * os n caracteres da string strBinario se já percebemos que o número de bits
             * excede o valor de k
             */
            for (pos = 0; pos < n && numBits1 <= k + 1; pos++) {
                if (strBinario.charAt(pos) == '1') {
                    strCombinacao += (pos + 1) + " ";
                    numBits1++;
                }
            }

            if (numBits1 == k) {
                if (opcao == 0) {
                    StdOut.println(strCombinacao);    
                }
                count++;
            }
            num--;
        }
    }

    
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int k = Integer.parseInt(args[1]);

        if (args.length == 3) {
            opcao = Integer.parseInt(args[2]);
        }

        Stopwatch timer = new Stopwatch();
        combinacao(n, k);
        StdOut.println(count);
        StdOut.println("elapsed time = " + timer.elapsedTime());
    }
}
