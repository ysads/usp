/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Nesse EP somente alguns conceitos básicos da linguagem Java
 *    e de combinatória foram discutidos com alguns colegas, mais
 *    como uma forma de rever ideias, paradigmas e questões sintáticas.
 *    A solução final, contudo, foi costurada por meio de testes
 *    empíricos seguidos de análise e correlação dos dados observados.
 *
 *  Bugs & Limitações:
 *    Não foram identificados bugs ou limitações relevantes. A única
 *    observação pertinente é que, em relação ao tempo de execução do 
 *    código, com valores grandes de n e pequenos de k, o código leva
 *    consideravelmente mais tempo para exibir as inúmeras combinações
 *    possíveis; isso, no entanto, já era esperado.
 */

/**
 *  Modo de Usar
 *  Compilation: javac-algs4 Combinacao.java
 *  Execution:   java Combinacao n k [opcao]
 *  
 *  Enumera todas as combinações dos números em {1,2,...,n} k a k.
 *  Se opcao = 0 (defaul), gera e exibe todas as permutações em ordem 
 *  lexicográfica
 *  Se opcao = 1 apenas, __gera todas__ as combinações, mas __não__ as 
 *  exibe; apenas exibe o total de combinações.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

public class Combinacao {

    /**
     * Contador de combinações
     */
    private static int count = 0;
    

    /**
     * Indica o que deve ser impresso na tela
     * 
     * 0 - Imprime o número de combinações e as combinações propriamente ditas
     * 1 - Imprime somente o número de combinações
     */
    private static int opcao = 0;


    public static void combinacao(int n, int k) {
        int pos, posAnterior, indiceAux;
        int[] numeros = new int[k];
        boolean restamCombinacoes = true;

        /**
         * Preenche o array inicial com a primeira combinação, a partir
         * da qual as demais vão ser geradas
         */
        for (pos = 0; pos < k; pos++) {
            numeros[pos] = pos + 1;
        }
        count = 1;

        /**
         * Feito à parte para evitar que, com valores grandes de n, sejam
         * avaliadas muitas condições desnecessárias
         */
        if (opcao == 0) {
            for (pos = 0; pos < k; pos++) {
                StdOut.print(numeros[pos] + " ");
            }
            StdOut.println();
        }

        /**
         * Note que começamos do último indíce do array, de modo a garantir
         * a ordem lexicográfica das combinações
         */
        pos = posAnterior = k - 1;
        while (restamCombinacoes) {
            /**
             * Se chegamos a um índice negativo então já avaliamos tudo
             * e não restam combinações. Se não der pra incrementar a
             * posição atual, então devemos retroceder pos
             */
            if (pos < 0) {
                restamCombinacoes = false;
                continue;
            }
            else if (numeros[pos] + 1 > n - (k - 1 - pos)) {
                posAnterior = pos;
                pos--;
                continue;
            }

            numeros[pos]++;
            count++;

            /**
             * É preciso "resetar" as posições posteriores no caso de
             * esta posição atual não ter se mantido. Isso é necessário
             * para forçar todas as possibilidades considerando o elemento
             * que acabamos de incrementar acima
             */
            if (posAnterior != pos) {
                indiceAux = pos + 1;

                while (indiceAux < k) {
                    numeros[indiceAux] = numeros[pos] + indiceAux - pos;
                    indiceAux++;
                }
                pos = k - 1;
            }

            /**
             * Exibe a combinação gerada, se necessário
             */
            if (opcao == 0) {
                for (int j = 0; j < k; j++) {
                    StdOut.print(numeros[j] + " ");
                }
                StdOut.println();    
            }
        }
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int k = Integer.parseInt(args[1]);

        if (args.length == 3) {
            opcao = Integer.parseInt(args[2]);
        }

        Stopwatch timer = new Stopwatch();
        combinacao(n, k);
        StdOut.println(count);
        StdOut.println("elapsed time = " + timer.elapsedTime());
    }
}
