/****************************************************************
    Nome: Ygor Sad Machado
    NUSP: 8910368

    Ao preencher esse cabeçalho com o meu nome e o meu número USP,
    declaro que todas as partes originais desse exercício programa
    (EP) foram desenvolvidas e implementadas por mim e que portanto
    não constituem desonestidade acadêmica ou plágio.
    Declaro também que sou responsável por todas as cópias desse
    programa e que não distribui ou facilitei a sua distribuição.
    Estou ciente que os casos de plágio e desonestidade acadêmica
    serão tratados segundo os critérios divulgados na página da 
    disciplina.
    Entendo que EPs sem assinatura devem receber nota zero e,
    ainda assim, poderão ser punidos por desonestidade acadêmica.

    Abaixo descreva qualquer ajuda que você recebeu para fazer este
    EP. Inclua qualquer ajuda recebida por pessoas (inclusive
    monitoras e colegas). Com exceção de material de MAC0323, caso
    você tenha utilizado alguma informação, trecho de código,...
    indique esse fato abaixo para que o seu programa não seja
    considerado plágio ou irregular.

    Se for o caso, descreva a seguir 'bugs' e limitações do seu programa:

****************************************************************/

/******************************************************************************
 *  Compilation:  javac-algs4 Combinacao.java
 *  Execution:    java Combinacao n k [opcao]
 *  
 *  Enumera todas as combinações dos números em {1,2,...,n} k a k.
 *  Se opcao = 0 (defaul), gera e exibe todas as permutações em ordem 
 *  lexicográfica
 *  Se opcao = 1 apenas, __gera todas__ as combinações, mas __não__ as 
 *  exibe; apenas exibe o total de combinações.
 *
 * % java Combinacao 5 3 1
 * 10
 * elapsed time = 0.002
 * % java Combinacao 5 3 
 * 1 2 3 
 * 1 2 4 
 * 1 2 5 
 * 1 3 4 
 * 1 3 5 
 * 1 4 5 
 * 2 3 4 
 * 2 3 5 
 * 2 4 5 
 * 3 4 5 
 * 10
 * elapsed time = 0.002
 * % java Combinacao 100 3 1
 * 161700
 * elapsed time = 0.004
 * % java Combinacao 1000 3 1
 * 166167000
 * elapsed time = 0.726
 *   
 ******************************************************************************/

import java.math.BigInteger;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.Stopwatch;

public class CombinacaoBig {
    
    /**
     * Contador de combinações
     */
    private static int count = 0;
    

    /**
     * Indica o que deve ser impresso na tela
     * 
     * 0 - Imprime o número de combinações e as combinações propriamente ditas
     * 1 - Imprime somente o número de combinações
     */
    private static int opcao = 0;


    public static void combinacao(int n, int k) {
        String strBinario;
        String strCombinacao;

        int pos;
        int numBits1;

        BigInteger totalCombinacoes = new BigInteger(Integer.toString(2));
        BigInteger numBinNormal;
        BigInteger num;

        totalCombinacoes = totalCombinacoes.pow(n);
        num = totalCombinacoes;

        while (!num.equals(BigInteger.ZERO)) {
            // for (num = totalCombinacoes; num >= 1; num.subtract(BigInteger.ONE)) {
            /**
             * Esse passo é importante porque normaliza o número de digitos que estarão
             * presentes na representação binária final, de modo que todos os números
             * possuam n bits, mesmo aqueles que forem bem menores - 11, 10, por exemplo
             */
            numBinNormal = totalCombinacoes.or(num);

            /**
             * O método substring aqui serve para retirar o bit 1 que aparece na frente
             * de todos os números na representação final. Isso se deve ao fato de que,
             * para representar o total de combinações de n, são necessário n+1 bits
             */
            strBinario = numBinNormal.toString(2).substring(1);

            numBits1 = 0;
            strCombinacao = "";
            
            for (pos = 0; pos <= k + 1; pos++) {
                if (strBinario.charAt(pos) == '1') {
                    strCombinacao += (pos + 1) + " ";
                    numBits1++;
                }
            }

            // StdOut.println("n bits: " + numBits1);
            if (numBits1 == k) {
                if (opcao == 0) {
                    StdOut.println(strCombinacao);    
                }
                count++;
            }

            num = num.subtract(BigInteger.ONE);
        }
    }

    
    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int k = Integer.parseInt(args[1]);

        if (args.length == 3) {
            opcao = Integer.parseInt(args[2]);
        }

        Stopwatch timer = new Stopwatch();
        combinacao(n, k);
        StdOut.println(count);
        StdOut.println("elapsed time = " + timer.elapsedTime());
    }
}
