/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Nesse EP a implementação da tabela de símbolos foi baseada
 *    naquela constante na biblioteca algs4.
 *     - https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/BinarySearchST.java.html
 *
 *  Bugs & Limitações:
 *    Não foram identificados bugs ou limitações relevantes.
 */

/*
* MAC0323 Estruturas de Dados e Algoritmo II
* 
* Tabela de simbolos implementada atraves de vetores ordenados 
* redeminsionaveis 
*
* https://algs4.cs.princeton.edu/31elementary/BinarySearchST.java.html
* 
* As chaves e valores desta implementação são mais ou menos
* genéricos
*/

/* interface para o uso da funcao deste módulo */
#include "binarysearchst.h"  

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc(), ecalloc() */

#define DEBUG
#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debug */
#endif

/*----------------------------------------------------------
 * CONSTANTES 
 */
#define INITIAL_CAPACITY 4

/*----------------------------------------------------------*/
/* 
 * Estrutura Básica da Tabela de Símbolos: 
 * 
 * implementação com vetores ordenados
 */

struct pair {
    void *key;
    void *val;
    size_t keySize;
    size_t valSize;
};

typedef struct pair* Pair;

struct binarySearchST {
    int size;
    int capacity;
    int (*comparator)(const void*, const void*);
    Pair *pairs;
};

/*------------------------------------------------------------*/
/* 
 * Funções administrativas
 */
void resize(BinarySearchST st, int newCapacity);
void freePair(Pair pair);
void *duplicate(void *item, size_t nBytes);
int compareKeyToPairAt(BinarySearchST st, const void *key, int pos);

/*-----------------------------------------------------------*/
/*
*  initST(COMPAR)
*
*  RECEBE uma função COMPAR() para comparar chaves.
*  RETORNA (referência/ponteiro para) uma tabela de símbolos vazia.
*
*  É esperado que COMPAR() tenha o seguinte comportamento:
*
*      COMPAR(key1, key2) retorna um inteiro < 0 se key1 <  key2
*      COMPAR(key1, key2) retorna 0              se key1 == key2
*      COMPAR(key1, key2) retorna um inteiro > 0 se key1 >  key2
* 
*  TODAS OS OPERAÇÕES da ST criada utilizam a COMPAR() para comparar
*  chaves.
* 
*/
BinarySearchST
initST(int (*compar)(const void *key1, const void *key2))
{
    BinarySearchST st = emalloc(sizeof(struct binarySearchST));

    st->comparator = compar;
    st->size       = 0;
    st->capacity   = INITIAL_CAPACITY;
    st->pairs      = ecalloc(INITIAL_CAPACITY, sizeof(Pair));

    return st;
}

/*-----------------------------------------------------------*/
/*
*  freeST(ST)
*
*  RECEBE uma BinarySearchST ST e devolve ao sistema toda a memoria 
*  utilizada por ST.
*
*/
void  
freeST(BinarySearchST st)
{
    int pos;

    if (st != NULL) {
        /**
         * Desaloca cada um dos itens, antes de mexer com os arrays
         * em si. Isso é necessário porque os itens são alocados
         * durante sua inserção na ST
         */
        for (pos = 0; pos < size(st); pos++) {
            freePair(st->pairs[pos]);
        }

        /**
         * Desaloca os demais ponteiros da ST, incluindo a própria ST
         */
        free(st->pairs);
        free(st);
    }
}    

/*------------------------------------------------------------*/
/*
* OPERAÇÕES USUAIS: put(), get(), contains(), delete(),
* size() e isEmpty().
*/

/*-----------------------------------------------------------*/
/*
*  put(ST, KEY, NKEY, VAL, NVAL)
* 
*  RECEBE a tabela de símbolos ST e um par KEY-VAL e procura a KEY na ST.
*
*     - se VAL é NULL, a entrada da chave KEY é removida da ST  
*  
*     - se KEY nao e' encontrada: o par KEY-VAL é inserido na ST
*
*     - se KEY e' encontra: o valor correspondente é atualizado
*
*  NKEY é o número de bytes de KEY e NVAL é o número de bytes de VAL.
*
*  Para criar uma copia/clone de KEY é usado o seu número de bytes NKEY.
*  Para criar uma copia/clode de VAL é usado o seu número de bytes NVAL.
*/
void  
put(BinarySearchST st, const void *key, size_t nKey, const void *val, size_t nVal)
{
    Pair pair;
    int pos, j;

    /**
     * Algumas validações de segurança
     */
    if (st == NULL || key == NULL) {
        ERROR("[put] neither st or key can be NULL");
        return;
    }

    pos = rank(st, key);

    /**
     * Inserir NULL na ST equivale a delete a chave em questão
     */
    if (val == NULL) {
        delete(st, key);
        return;
    }

    /**
     * A ST já contém a chave em questão, ou seja, devemos apenas
     * atualizar o seu valor
     */
    if ((pos < size(st)) && (compareKeyToPairAt(st, key, pos) == 0)) {
        memmove(st->pairs[pos]->val, val, nVal);
        return;
    }

    /**
     * Se a ST chegou ao limite de sua capacidade, então é preciso
     * aumentar o tamanho dos seus arrays de chaves e valores
     */
    if (st->capacity == st->size) {
        resize(st, st->capacity * 2);
    }

    /**
     * Copia, um a um, todos os items do vetor que estão à frente
     * da posição localizada, de modo a abrir espaço para o novo item
     */
    for (j = size(st); j > pos; j--) {
        st->pairs[j] = st->pairs[j - 1];
    }

    pair = emalloc(sizeof(struct pair));
    pair->key = emalloc(nKey);
    pair->val = emalloc(nVal);

    pair->keySize = nKey;
    pair->valSize = nVal;

    memcpy(pair->key, key, nKey);
    memcpy(pair->val, val, nVal);

    /**
     * Insere o par chave-valor na ST, alocando para ele o espaço
     * necessário e depois copiando o valor passado para este espaço
     */
    st->pairs[pos] = pair;
    st->size++;
}

/*-----------------------------------------------------------*/
/*
*  get(ST, KEY)
*
*  RECEBE uma tabela de símbolos ST e uma chave KEY.
*
*     - se KEY está em ST, RETORNA uma cópia/clone do valor
*       associado a KEY;
*
*     - se KEY não está em ST, RETORNA NULL.
* 
*/
void *
get(BinarySearchST st, const void *key)
{
    int pos;

    /**
     * Algumas validações de segurança
     */
    if (st == NULL || key == NULL) {
        ERROR("[get] neither st or key can be NULL");
        return NULL;
    }

    /**
     * Se rank identificou exatamente a chave buscada e ela está dentro
     * da ST, então pode-se retornar o valor associado a essa chave. Note
     * que é importante verificar se as chaves são iguais pois rank()
     * retorna 0, quando a chave não foi encontrada na ST
     */
    pos = rank(st, key);
    
    if ((pos < size(st)) && (compareKeyToPairAt(st, key, pos) == 0)) {
        return duplicate(st->pairs[pos]->val, st->pairs[pos]->valSize);
    }
    else {
        return NULL;
    }
}

/*-----------------------------------------------------------*/
/* 
*  CONTAINS(ST, KEY)
*
*  RECEBE uma tabela de símbolos ST e uma chave KEY.
* 
*  RETORNA TRUE se KEY está na ST e FALSE em caso contrário.
*
*/
Bool
contains(BinarySearchST st, const void *key)
{
    if ((st == NULL) || (key == NULL)) {
        ERROR("[contains] neither st or key can be NULL");
        return FALSE;
    }
    return (get(st, key) != NULL);
}

/*-----------------------------------------------------------*/
/* 
*  DELETE(ST, KEY)
*
*  RECEBE uma tabela de símbolos ST e uma chave KEY.
* 
*  Se KEY está em ST, remove a entrada correspondente a KEY.
*  Se KEY não está em ST, faz nada.
*
*/
void
delete(BinarySearchST st, const void *key)
{
    int pos, j;

    if ((st == NULL) || (key == NULL)) {
        ERROR("[delete] neither st or key can be NULL");
        return;
    }

    pos = rank(st, key);
    
    /**
     * Se a chave não está na tabela ou se seu rank não
     * corresponde à chave que deve ser removida, aborta
     * a execução
     */
    if ((pos >= size(st)) || (compareKeyToPairAt(st, key, pos) != 0)) {
        return;
    }

    /**
     * Libera o espaço usado pelo item a ser removido,
     * garantindo que não haja memory leaks
     */
    freePair(st->pairs[pos]);

    /**
     * Move todos os items do vetor uma posição à frente,
     * sobrescrevendo, pois, o item a ser removido
     */
    for (j = pos; j < size(st) - 1; j++) {
        st->pairs[j] = st->pairs[j + 1];
    }

    /**
     * É necessário "esvaziar" as últimas posições dos arrays
     * pois elas contêm dados duplicados
     */
    st->pairs[j] = NULL;
    st->size--;

    if (size(st) && (size(st) == st->capacity / 4)) {
        resize(st, st->capacity / 2);
    }
}

/*-----------------------------------------------------------*/
/* 
*  SIZE(ST)
*
*  RECEBE uma tabela de símbolos ST.
* 
*  RETORNA o número de itens (= pares chave-valor) na ST.
*
*/
int
size(BinarySearchST st)
{
    if (st == NULL) {
        return 0;
    }
    else {
        return st->size;
    }
}

/*-----------------------------------------------------------*/
/* 
*  ISEMPTY(ST, KEY)
*
*  RECEBE uma tabela de símbolos ST.
* 
*  RETORNA TRUE se ST está vazia e FALSE em caso contrário.
*
*/
Bool
isEmpty(BinarySearchST st)
{
    return ((st == NULL) || (st->size == 0));
}


/*------------------------------------------------------------*/
/*
* OPERAÇÕES PARA TABELAS DE SÍMBOLOS ORDENADAS: 
* min(), max(), rank(), select(), deleteMin() e deleteMax().
*/

/*-----------------------------------------------------------*/
/*
*  MIN(ST)
* 
*  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
*  da menor chave na tabela.
*
*  Se ST está vazia RETORNA NULL.
*
*/
void *
min(BinarySearchST st)
{
    Pair min;

    if (isEmpty(st)) {
        return NULL;
    }
    /**
     * Identifica o primeiro item da ST e cria uma cópia dele
     * para ser retornada
     */
    min = st->pairs[0];
    return duplicate(min->key, min->keySize);
}

/*-----------------------------------------------------------*/
/*
*  MAX(ST)
* 
*  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
*  da maior chave na tabela.
*
*  Se ST está vazia RETORNA NULL.
*
*/
void *
max(BinarySearchST st)
{
    Pair max;

    if (isEmpty(st)) {
        return NULL;
    }
    /**
     * Identifica o último item da ST e cria uma cópia dele
     * para ser retornada
     */
    max = st->pairs[size(st) - 1];
    return duplicate(max->key, max->keySize);
}

/*-----------------------------------------------------------*/
/*
*  RANK(ST, KEY)
* 
*  RECEBE uma tabela de símbolos ST e uma chave KEY.
*  RETORNA o número de chaves em ST menores que KEY.
*
*  Se ST está vazia RETORNA NULL.
*
*/
int
rank(BinarySearchST st, const void *key)
{
    int lft, rgt;
    int mid, cmp;

    /**
     * Algumas verificações úteis para manter a segurança dos
     * métodos e evitar falhas de segmentação
     */
    if ((st == NULL) || (key == NULL)) {
        ERROR("[rank] neither st or key can be NULL");
        return EXIT_FAILURE;
    }

    /**
     * Variáveis usadas para definir os limites da busca binária
     * a ser executada sobre o array de keys
     */
    lft = 0;
    rgt = size(st) - 1;

    while (lft <= rgt) {
        mid = lft + (rgt - lft) / 2;
        cmp = compareKeyToPairAt(st, key, mid);

        /**
         * O cerne da busca binária. É preciso saber se a chave
         * foi localizada - situação esta em que a não é necessário
         * continuar a execução da função -, ou, caso contrário, em
         * qual novo intervalo a função deve buscar a chave
         */
        if (cmp > 0) {
            lft = mid + 1;
        }
        else if (cmp < 0) {
            rgt = mid - 1;
        }
        else {
            return mid;
        }
    }
    return lft;
} 

/*-----------------------------------------------------------*/
/*
*  SELECT(ST, K)
* 
*  RECEBE uma tabela de símbolos ST e um inteiro K >= 0.
*  RETORNA a (K+1)-ésima menor chave da tabela ST.
*
*  Se ST não tem K+1 elementos RETORNA NULL.
*
*/
void *
select(BinarySearchST st, int k)
{
    /**
     * Verificações de segurança. Não é possível retornar a (k+1)-ésima
     * menor chave se não há k itens na ST
     */
    if ((st == NULL) || (size(st) < k)) {
        return NULL;
    }
    return st->pairs[k]->key;
}

/*-----------------------------------------------------------*/
/*
*  deleteMIN(ST)
* 
*  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
*  à menor chave.
*
*  Se ST está vazia, faz nada.
*
*/
void
deleteMin(BinarySearchST st)
{
    if ((st != NULL) && (!isEmpty(st))) {
        /**
         * Busca a chave na primeira posição da ST, usando select, e a
         * usa para remover o item da ST
         */
        delete(st, select(st, 0));
    }
}

/*-----------------------------------------------------------*/
/*
*  deleteMAX(ST)
* 
*  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
*  à maior chave.
*
*  Se ST está vazia, faz nada.
*
*/
void
deleteMax(BinarySearchST st)
{
    if ((st != NULL) && (!isEmpty(st))) {
        /**
         * Busca a chave na última posição da ST, usando select, e a
         * usa para remover o item da ST
         */
        delete(st, select(st, st->size - 1));
    }
}

/*-----------------------------------------------------------*/
/* 
 * KEYS(ST, INIT)
 *
 * RECEBE uma tabela de símbolos ST e um Bool INIT.
 * 
 * Se INIT é TRUE, KEYS() RETORNA uma cópia/clone da menor chave na ST.
 * Se INIT é FALSE, KEYS() RETORNA a chave sucessora da última chave retornada.
 * Se ST está vazia ou não há sucessora da última chave retornada, KEYS() RETORNA NULL.
 * Se entre duas chamadas de KEYS() a ST é alterada, o comportamento é 
 * indefinido. 
 * 
 */
void * 
keys(BinarySearchST st, Bool init)
{
    Pair pair;
    static int index;

    /**
     * A variável index é reiniciada sempre que init for TRUE. Do contrário,
     * é preciso tentar incrementar o seu valor para visitar a próxima chave.
     * Como index é estática, ela retém seu valor ao longo de diversas chamadas.
     */
    if (init) {
        index = 0;
    }
    else {
        index++;
    }
    
    /**
     * No caso de já terem sido percorridas todas as chaves, ou de a ST estar
     * vazia, não há mais o que fazer e a execução deve ser encerrada.
     */
    if ((st == NULL) || (isEmpty(st)) || (index == size(st))) {
        return NULL;
    }
    pair = st->pairs[index];
    return duplicate(pair->key, pair->keySize);
}

/*-----------------------------------------------------------*/
/*
 * Visit each entry on the ST.
 *
 * The VISIT function is called, in-order, with each pair key-value in the ST.
 * If the VISIT function returns zero, then the iteration stops.
 * 
 * visitST returns zero if the iteration was stopped by the visit function,
 * nonzero otherwise.
 */
int
visitST(BinarySearchST st, int (*visit)(const void *key, const void *val))
{
    Pair pair;
    int pos;

    if (st == NULL) {
        ERROR("[visitST] st can't be NULL");
        return EXIT_FAILURE;
    }
    
    pos = 0;
    while (pos < size(st)) {
        pair = st->pairs[pos];

        /**
         * Tenta executar a função visit usando o par chave-valor como
         * parâmetro. Se ela retornar 0, aborta a execução de visitST.
         * Senão, continua a execução do laço principal
         */
        if (visit(pair->key, pair->val) == 0) {
            return EXIT_FAILURE;
        }
        pos++;
    }
    return EXIT_SUCCESS;
}
    

/*------------------------------------------------------------*/
/* 
 * Funções administrativas
 */

/**
 * Função cujo objetivo é redimensionar os arrays internos da ST de modo
 * a comprimir ou expandir sua capacidade. Para isso, ela aloca dois novos
 * arrays, copiando os itens dos arrays antigos para os novos
 * 
 * @param st: a ST a ser redimensionada
 * @param newCapacity: a nova capacidade desejada para a ST
 */
void
resize(BinarySearchST st, int newCapacity)
{
    Pair *pairsTemp;
    int i;

    if (newCapacity <= size(st)) {
        ERROR("[resize] new capacity must be larger than symbol table size");
        return;
    }
    
    /**
     * Aloca um novo array com a capacidade solicitada
     */
    pairsTemp = ecalloc(newCapacity, sizeof(Pair));

    /**
     * Copia, item por item, os itens contidos na ST para os arrays redimensionados
     */
    for (i = 0; i < st->size; i++) {
        pairsTemp[i] = st->pairs[i];
    }

    /**
     * Como o conteúdo do array atual já foi copiado para o novo array,
     * deve-se liberar o espaço deste primeiro
     */
    free(st->pairs);

    /**
     * Atualiza o array de pairs da ST para que aponte para o novo array
     */
    st->pairs = pairsTemp;
    st->capacity = newCapacity;
}

/**
 * Uma função auxiliar para tornar a execução de comparações mais semântica.
 * Tudo o que ela faz é chamar a função de comparação armazenada na ST para
 * a chave passada e a chave localizada no (pos+1)-ésimo par da ST
 *
 * @param st: a ST
 * @param key: a chave referência para a comparação
 * @param pos: o item da ST cuja chave vai ser comparada com key
 * @return 0 se as chaves são iguais, um valor negativo se key é menor do chave
 * do item em pos, e um valor positivo se key é maior do que a chave em pos
 */
int
compareKeyToPairAt(BinarySearchST st, const void *key, int pos)
{
    return st->comparator(key, st->pairs[pos]->key);
}

/**
 * Desaloca o espaço ocupado por um objeto Pair, desalocando inclusive
 * o espaço tomado pelos seus membros - key e val
 * 
 * @param pair: uma referência para uma struct pair alocada dinamicamente
 */
void
freePair(Pair pair)
{
    free(pair->key);
    free(pair->val);
    free(pair);
}

/**
 * Cria uma cópia de um ponteiro passado, alocando espaço para o seu
 * conteúdo e, em seguida, copiando-o para a nova posição de memória
 *
 * @param item: um ponteiro cujo conteúdo será copiado
 * @return um ponteiro para a posição de memória com o conteúdo copiado
 */
void *
duplicate(void *item, size_t nBytes)
{
    void *copy = emalloc(nBytes);
    memcpy(copy, item, nBytes);
    return copy;
}
