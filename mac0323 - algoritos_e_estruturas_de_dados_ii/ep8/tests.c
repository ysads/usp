#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "binarysearchst.h"
#include "util.h"

void inspect(BinarySearchST st) {
    printf("$\n");
    printf("size:\t%d / %d\n", size(st), st->capacity);
    printf("empty?\t%s\n",    (isEmpty(st) == 1) ? "TRUE" : "FALSE");
    
    printf("pairs:\n");
    // for (int i = 0; i < size(st); i++) {
    //     printf(" - %s\t%d\t<%zu, %zu>\n",
    //         ((char*) st->pairs[i]->key),
    //        *((int*)  st->pairs[i]->val),
    //         (        st->pairs[i]->keySize),
    //         (        st->pairs[i]->valSize));
    // }
    printf("\n");
}

void insert(BinarySearchST st, char* key, int val) {
    Integer st_val = emalloc(sizeof(Integer));
    String  st_key = emalloc(sizeof(key) + 1);

    /**
     * We need to make copies of the args passed, since they
     * may not be pointers and put() function deals only with
     * pointers
     */
    if (val == -1) {
        st_val = NULL;
    }
    else {
        *st_val = val;    
    }
    strncpy(st_key, key, strlen(key));

    // printf("\n=> %d - %s", *st_val, st_key);

    /**
     * Insert the item into the tree, making sure to pass the
     * correct datatypes size
     */
    put(st, st_key, strlen(st_key) + 1, st_val, sizeof(Integer));

    /**
     * Free the auxiliary pointers allocated. We don't need
     * them anymore, since the tree has its own copy of them
     */
    free(st_key);
    free(st_val);

    printf("$\n");
    printf("> inserted %s\n\n", key);
}

void search(BinarySearchST st, char *key) {
    Integer val = get(st, key);

    printf("$\n");
    if (val == NULL) {
        printf("> %s not found\n\n", key);
    }
    else {
        printf("> %s\t%d\n\n", key, *val);
    }
}

void is_present(BinarySearchST st, char *key) {
    Bool present = contains(st, key);
    printf("$\n");
    printf("> %s %s\n\n", (present ? "contains" : "don't contain"), key);
}

void get_boundary(BinarySearchST st, Bool isMax) {
    String key = isMax ? max(st) : min(st);

    printf("$\n");
    printf("> %s is %s\n\n", (isMax ? "max" : "min"), key);

    /**
     * Disalloc the String since it's been allocated within
     * the max function
     */
    free(key);
}

void pull_out(BinarySearchST st, char *key) {
    printf("$\n");
    printf("> deleting %s\n\n", key);
    delete(st, key);
}

void pull_out_boundary(BinarySearchST st, Bool isMax) {
    printf("$\n");
    if (isMax) {
        printf("> removing max\n\n");
        deleteMax(st);
    }
    else {
        printf("> removing min\n\n");
        deleteMin(st);
    }
}

void iterate_keys(BinarySearchST st, int max) {
    String key;
    int ite;

    key = keys(st, TRUE);
    ite = 0;

    printf("$\n keys: \n");
    while ((key != NULL) && (ite < max)) {
        printf(" - (%s)\n", ((char*) key));
        ite++;
        free(key);
        key = keys(st, FALSE);
    }
}

int say_hi(const void *key, const void *val) {
    Integer st_val = (Integer) val;

    printf("~!~ Hi, %s! You have %d occurrences!\n", ((char*) key), *((int*) val));

    return (*st_val != 1);
}

int main() {
    
    BinarySearchST st;
    String key;
    int count;

    /**
     * Creating tree
     */
    st = initST(strCmp);
    
    printf("------------------------------------\n");

    inspect(st);

    printf("\n$ testing get edge cases: \n");
    search(NULL, "morango");
    search(st, NULL);

    printf("\n$ testing rank edge cases: \n");
    rank(NULL, "morango");
    rank(st, NULL);

    printf("\n$ testing contains edge cases: \n");
    contains(NULL, "morango");
    contains(st, NULL);

    printf("\n$ testing put edge cases: \n");
    put(NULL, "morango", 8, NULL, 4);
    put(st, NULL, 8, NULL, 4);

    printf("------------------------------------\n");

    get_boundary(st, TRUE);
    get_boundary(st, FALSE);

    search(st, "espresso");
    insert(st, "espresso", 10);
    inspect(st);
    search(st, "macchiato");

    is_present(st, "espresso");
    is_present(st, "capuccino");

    insert(st, "macchiato", 3);
    inspect(st);

    insert(st, "affogato", 0);
    inspect(st);

    get_boundary(st, TRUE);
    get_boundary(st, FALSE);

    insert(st, "espresso", 1);
    insert(st, "capuccino", 2);
    inspect(st);

    iterate_keys(st, size(st));

    is_present(st, "affogato");
    pull_out(st, "affogato");
    is_present(st, "affogato");

    is_present(st, "capuccino");

    insert(st, "capuccino", 100);
    search(st, "capuccino");
    insert(st, "american", 3);
    inspect(st);

    visitST(st, say_hi);

    iterate_keys(st, 3);
    iterate_keys(st, 2);
    iterate_keys(st, size(st));

    get_boundary(st, TRUE);
    get_boundary(st, FALSE);

    search(st, "espresso");
    search(st, "banana");
    search(st, "macchiato");
    search(st, "shake");

    inspect(st);

    is_present(st, "macchiato");
    insert(st, "macchiato", -1);
    is_present(st, "macchiato");

    inspect(st);

    pull_out_boundary(st, FALSE);
    pull_out_boundary(st, TRUE);

    inspect(st);

    /**
     * Free the tree pointers
     */
    freeST(st);
}