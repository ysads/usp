/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Nesse EP a implementação da tabela de símbolos foi baseada
 *    naquela constante na biblioteca algs4.
 *     - https://algs4.cs.princeton.edu/33balanced/RedBlackBST.java.html
 *
 *  Bugs & Limitações:
 *    Não foram identificados bugs ou limitações relevantes.
 */

/*
* MAC0323 Estruturas de Dados e Algoritmo II
* 
* Tabela de simbolos implementada por meio de árvores rubro-negras
* 
*/
/* interface para o uso da funcao deste módulo */
#include "redblackst.h"  

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc(), ecalloc() */

// #undef DEBUG
// #ifdef DEBUG
#include <stdio.h>   /* printf(): para debug */
// #endif

/*
 * CONSTANTES 
 */
#define RED   TRUE
#define BLACK FALSE 

/*----------------------------------------------------------*/
/* 
 * Estrutura Básica da Tabela de Símbolos: 
 * 
 * implementação com árvore rubro-negra
 */
typedef struct node Node;

struct redBlackST {
    Node* root;
    int (*comparator)(const void*, const void*);
};

/*----------------------------------------------------------*/
/* 
 * Estrutura de um nó da árvore
 *
 */
struct node {
    void* key;
    void* val;
    
    size_t sizeKey;
    size_t sizeVal;
    
    Node* lft;
    Node* rgt;

    Bool color;
    int size;
};

/*------------------------------------------------------------*/
/* 
 *  Protótipos de funções administrativas.
 * 
 *  Entre essa funções estão isRed(), rotateLeft(), rotateRight(),
 *  flipColors(), moveRedLeft(), moveRedRight() e balance().
 * 
 *  Não deixe de implementar as funções chamadas pela função 
 *  check(): isBST(), isSizeConsistent(), isRankConsistent(),
 *  is23(), isBalanced().
 *
 */

/**
 * ---------------------------------------------------------------
 *
 * Funções auxiliares para a verificação do balanceamento da ST
 * 
 */
static Bool
isBST(RedBlackST);

static Bool
isSizeConsistent(RedBlackST);

static Bool
isRankConsistent(RedBlackST);

static Bool
is23(RedBlackST);

static Bool
isBalanced(RedBlackST);


/**
 * ---------------------------------------------------------------
 *
 *  Funções auxiliares que corrigem o balanceamento da ST
 *  
 */
static Bool
isRed(Node*);

static Node*
rotateLeft(Node *node);

static Node*
rotateRight(Node *node);

static Node*
moveRedLeft(Node *node);

static Node*
moveRedRight(Node *node);

static void
flipColors(Node*);

static Node*
balance(Node* node);


/**
 * ---------------------------------------------------------------
 * 
 * Funções auxiliares para a manipulação de struct node
 * 
 */
Node*
initNode(const void*, size_t, const void*, size_t, Bool, int);

void
freeNode(Node *node);

Node*
putNode(RedBlackST st, Node *node, const void *key, size_t sizeKey, const void *val, size_t sizeVal);

Node*
getNode(RedBlackST, Node*, const void*);

Node*
deleteNode(RedBlackST st, Node *node, const void *key);

int
sizeNode(Node *node);

Node*
deleteMinNode(Node *node);

Node*
deleteMaxNode(Node *node);

Node*
boundaryNode(Node *node, Bool isMax);

int
rankNode(RedBlackST st, Node *node, const void *key);

Node*
selectNode(Node *node, int k);

int
heightNode(Node *node);


/**
 * ---------------------------------------------------------------
 *
 * Funções auxiliares para gestão de memória
 * 
 */
int
compareToNode(RedBlackST, const void*, Node*);

void
*duplicate(const void*, size_t);


/*---------------------------------------------------------------*/
/*
 *  initST(COMPAR)
 *
 *  RECEBE uma função COMPAR() para comparar chaves.
 *  RETORNA (referência/ponteiro para) uma tabela de símbolos vazia.
 *
 *  É esperado que COMPAR() tenha o seguinte comportamento:
 *
 *      COMPAR(key1, key2) retorna um inteiro < 0 se key1 <  key2
 *      COMPAR(key1, key2) retorna 0              se key1 == key2
 *      COMPAR(key1, key2) retorna um inteiro > 0 se key1 >  key2
 * 
 *  TODAS OS OPERAÇÕES da ST criada utilizam a COMPAR() para comparar
 *  chaves.
 * 
 */
RedBlackST
initST(int (*compar)(const void *key1, const void *key2))
{
    RedBlackST tree = (RedBlackST) emalloc(sizeof(struct redBlackST));

    tree->root = NULL;
    tree->comparator = compar;
    return tree;
}


/*-----------------------------------------------------------*/
/*
 *  freeST(ST)
 *
 *  RECEBE uma RedBlackST  ST e devolve ao sistema toda a
 *  memoria utilizada por ST.
 *
 */
void  
freeST(RedBlackST st)
{
    freeNode(st->root);
    free(st);
}

/*------------------------------------------------------------*/
/*
 * OPERAÇÕES USUAIS: put(), get(), contains(), delete(),
 * size() e isEmpty().
 */
/*-----------------------------------------------------------*/
/*
 *  put(ST, KEY, NKEY, VAL, NVAL)
 * 
 *  RECEBE a tabela de símbolos ST e um par KEY-VAL e procura a KEY na ST.
 *
 *     - se VAL é NULL, a entrada da chave KEY é removida da ST  
 *  
 *     - se KEY nao e' encontrada: o par KEY-VAL é inserido na ST
 *
 *     - se KEY e' encontra: o valor correspondente é atualizado
 *
 *  NKEY é o número de bytes de KEY e NVAL é o número de bytes de NVAL.
 *
 *  Para criar uma copia/clone de KEY é usado o seu número de bytes NKEY.
 *  Para criar uma copia/clode de VAL é usado o seu número de bytes NVAL.
 *
 */
void  
put(RedBlackST st, const void *key, size_t sizeKey, const void *val, size_t sizeVal)
{
    if ((st == NULL) || (key == NULL)) {
        ERROR("[put] neither st or key can be null");
    }
    
    /**
     * Inserir NULL em uma ST equivale a remover o item em questão
     */
    if (val == NULL) {
        delete(st, key);
        return;
    }

    st->root = putNode(st, st->root, key, sizeKey, val, sizeVal);
    st->root->color = BLACK;
}


/*-----------------------------------------------------------*/
/*
 *  get(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *
 *     - se KEY não está em ST, RETORNA NULL;
 *
 *     - se KEY está em ST, RETORNA uma cópia/clone do valor
 *       associado a KEY.
 * 
 */
void *
get(RedBlackST st, const void *key)
{
    Node *node;
    
    if ((st == NULL) || (key == NULL)) {
        ERROR("[get] neither st or key can be null");
    }

    /**
     * Realiza uma busca binária para localizar o nó cuja
     * chave equivale àquela que se busca
     */
    node = getNode(st, st->root, key);

    /**
     * Se o nó foi localizado, retorna uma cópia do seu valor
     */
    if (node) {
        return duplicate(node->val, node->sizeVal);
    }
    else {
        return NULL;
    }
}


/*-----------------------------------------------------------*/
/* 
 *  CONTAINS(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 * 
 *  RETORNA TRUE se KEY está na ST e FALSE em caso contrário.
 *
 */
Bool
contains(RedBlackST st, const void *key)
{
    if ((st == NULL) || (key == NULL)) {
        ERROR("[contains] neither st or key can be NULL");
    }

    /**
     * Se houver algum nó na ST com a chave dada, ele será encontrado
     * pela função getNode. Portanto, basta utilizá-la para saber se
     * a ST contém a chave em questão
     */
    return (getNode(st, st->root, key) != NULL);
}

/*-----------------------------------------------------------*/
/* 
 *  DELETE(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 * 
 *  Se KEY está em ST, remove a entrada correspondente a KEY.
 *  Se KEY não está em ST, faz nada.
 *
 */
void
delete(RedBlackST st, const void *key)
{
    if ((st == NULL) || (key == NULL)) {
        ERROR("[delete] neither st or key can be NULL");
        return;
    }

    /**
     * Não faz sentido deletar um nó se ele não está presente na ST
     */
    if (!contains(st, key)) {
        return;
    }

    /**
     * Se ambos os filhos da raiz forem negros, torna a raiz rubra
     */
    if (!isRed(st->root->lft) && !isRed(st->root->rgt)) {
        st->root->color = RED;   
    }

    st->root = deleteNode(st, st->root, key);

    /**
     * Se, após a deleção, a árvore não estiver vazia, é preciso
     * fazer com que o nó raiz fique negro
     */
    if (!isEmpty(st)) {
        st->root->color = BLACK;
    }
}


/*-----------------------------------------------------------*/
/* 
 *  SIZE(ST)
 *
 *  RECEBE uma tabela de símbolos ST.
 * 
 *  RETORNA o número de itens (= pares chave-valor) na ST.
 *
 */
int
size(RedBlackST st)
{
    if (st == NULL) {
        return 0;
    }
    else {
        return sizeNode(st->root); 
    }
}


/*-----------------------------------------------------------*/
/* 
 *  ISEMPTY(ST, KEY)
 *
 *  RECEBE uma tabela de símbolos ST.
 * 
 *  RETORNA TRUE se ST está vazia e FALSE em caso contrário.
 *
 */
Bool
isEmpty(RedBlackST st)
{
    return ((st == NULL) && (st->root == NULL));
}

/*------------------------------------------------------------*/
/*
 * OPERAÇÕES PARA TABELAS DE SÍMBOLOS ORDENADAS: 
 * min(), max(), rank(), select(), deleteMin() e deleteMax().
 */

/*-----------------------------------------------------------*/
/*
 *  MIN(ST)
 * 
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da menor chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
min(RedBlackST st)
{
    Node *min;

    if (isEmpty(st)) {
        ERROR("[min] st can't be empty");
        return NULL;
    }

    min = boundaryNode(st->root, FALSE);
    return duplicate(min->key, min->sizeKey);
}


/*-----------------------------------------------------------*/
/*
 *  MAX(ST)
 * 
 *  RECEBE uma tabela de símbolos ST e RETORNA uma cópia/clone
 *  da maior chave na tabela.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
void *
max(RedBlackST st)
{
    Node *max;

    if (isEmpty(st)) {
        ERROR("[max] st can't be empty");
        return NULL;
    }

    max = boundaryNode(st->root, TRUE);
    return duplicate(max->key, max->sizeKey);
}


/*-----------------------------------------------------------*/
/*
 *  RANK(ST, KEY)
 * 
 *  RECEBE uma tabela de símbolos ST e uma chave KEY.
 *  RETORNA o número de chaves em ST menores que KEY.
 *
 *  Se ST está vazia RETORNA NULL.
 *
 */
int
rank(RedBlackST st, const void *key)
{
    if ((st == NULL) || (key == NULL)) {
        ERROR("[rank] neither st or key can be NULL");
        return EXIT_FAILURE;
    }

    return rankNode(st, st->root, key);
} 


/*-----------------------------------------------------------*/
/*
 *  SELECT(ST, K)
 * 
 *  RECEBE uma tabela de símbolos ST e um inteiro K >= 0.
 *  RETORNA a (K+1)-ésima menor chave da tabela ST.
 *
 *  Se ST não tem K+1 elementos RETORNA NULL.
 *
 */
void *
select(RedBlackST st, int k)
{
    Node *node;

    if ((k < 0) || (k >= size(st))) {
        ERROR("[select] k out of range!");
    }

    /**
     * Busca pelo item com a (k+1)-ésima menor chave a partir
     * da raiz, retornando uma cópia dele
     */
    node = selectNode(st->root, k);
    return duplicate(node->key, node->sizeKey);
}


/*-----------------------------------------------------------*/
/*
 *  deleteMIN(ST)
 * 
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à menor chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMin(RedBlackST st)
{
    if (isEmpty(st)) {
        ERROR("[deleteMin] can't delete from empty ST");
        return;
    }

    /**
     * Torna a raiz da ST rubra, caso seus dois filhos sejam negros 
     */
    if (!isRed(st->root->lft) && !isRed(st->root->rgt)) {
        st->root->color = RED;
    }

    st->root = deleteMinNode(st->root);

    /**
     * Após a remoção da menor chave, torna a raiz negra novamente
     * de modo a não quebrar o invariante das árvores rubro-negras
     */
    if (!isEmpty(st)) {
        st->root->color = BLACK;
    }
}


/*-----------------------------------------------------------*/
/*
 *  deleteMAX(ST)
 * 
 *  RECEBE uma tabela de símbolos ST e remove a entrada correspondente
 *  à maior chave.
 *
 *  Se ST está vazia, faz nada.
 *
 */
void
deleteMax(RedBlackST st)
{
    if (isEmpty(st)) {
        ERROR("[deleteMax] can't delete from empty ST");
        return;
    }

    /**
     * Torna a raiz da ST rubra, caso seus dois filhos sejam negros 
     */
    if (!isRed(st->root->lft) && !isRed(st->root->rgt)) {
        st->root->color = RED;
    }

    st->root = deleteMaxNode(st->root);

    /**
     * Após a remoção da menor chave, torna a raiz negra novamente
     * de modo a não quebrar o invariante das árvores rubro-negras
     */
    if (!isEmpty(st)) {
        st->root->color = BLACK;
    }
}


/*-----------------------------------------------------------*/
/* 
 *  KEYS(ST, INIT)
 * 
 *  RECEBE uma tabela de símbolos ST e um Bool INIT.
 *
 *  Se INIT é TRUE, KEYS() RETORNA uma cópia/clone da menor chave na ST.
 *  Se INIT é FALSE, KEYS() RETORNA a chave sucessora da última chave retornada.
 *  Se ST está vazia ou não há sucessora da última chave retornada, KEYS() retorna NULL.
 *
 *  Se entre duas chamadas de KEYS() a ST é alterada, o comportamento é 
 *  indefinido. 
 *  
 */
void * 
keys(RedBlackST st, Bool init)
{
    static int iterator;
    Node *node;

    /**
     * Se init é TRUE, é necessário reiniciar o contador de iterações
     */
    if (init) {
        iterator = 0;
    }
    else {
        iterator++;
    }

    /**
     * Se a ST está vazia ou o iterador alcançou o limite de itens na
     * ST, não há o que retornar
     */
    if (isEmpty(st) || (iterator == size(st))) {
        return NULL;
    }

    /**
     * Usa a função selectNode para encontrar o (k+1)-ésimo menor item
     * da ST, ainda não visitado, retornando uma cópia dele
     */
    node = selectNode(st->root, iterator);
    return duplicate(node->key, node->sizeKey);
}


/*------------------------------------------------------------*/
/* 
 * Funções administrativas
 */

/***************************************************************************
 *  Utility functions.
 ***************************************************************************/

/*
 * HEIGHT(ST)
 * 
 * RECEBE uma RedBlackST e RETORNA a sua altura. 
 * Uma BST com apenas um nó tem altura zero.
 * 
 */
int
height(RedBlackST st)
{
    if (st == NULL) {
        ERROR("[height] st can't be NULL");
        return EXIT_FAILURE;
    }
    return heightNode(st->root);
}


/**
 * Cria uma cópia de um ponteiro passado, alocando espaço para o seu
 * conteúdo e, em seguida, copiando-o para a nova posição de memória
 *
 * @param item: um ponteiro cujo conteúdo será copiado
 * @return um ponteiro para a posição de memória com o conteúdo copiado
 */
void *
duplicate(const void *item, size_t nBytes)
{
    void *copy = emalloc(nBytes);
    memcpy(copy, item, nBytes);
    return copy;
}


/**
 * Uma função auxiliar cujo objetivo é alocar um novo nó para a árvore
 * com base nos parâmetros recebidos
 * 
 * @param key: a chave do novo nó
 * @param sizeKey: o número de bytes da chave do novo nó
 * @param val: o valor do novo nó
 * @param sizeVal: o número de bytes do valor do novo nó
 * @param color: um booleano que diz se o nó é rubro ou negro
 * @param size: a altura da subárvore cuja raiz é o nó criado
 * @return um nó alocado e com os campos preenchidos
 */
Node*
initNode(const void *key, size_t sizeKey, const void *val, size_t sizeVal, Bool color, int size)
{
    Node *node = emalloc(sizeof(Node));

    /**
     * Cria cópias dos valores do par chave-valor, movendo seu
     * conteúdo para os ponteiros recém-alocados
     */
    node->key = duplicate(key, sizeKey);
    node->val = duplicate(val, sizeVal);

    memcpy(node->key, key, sizeKey);
    memcpy(node->val, val, sizeVal);
    

    /**
     * Inicializa os demais valores de node, de acordo com
     * os argumentos recebidos pela função
     */
    node->sizeVal = sizeVal;
    node->sizeKey = sizeKey;
    node->color   = color;
    node->size    = size;
    node->lft     = NULL;
    node->rgt     = NULL;

    return node;
}


/**
 * Libera a subárvore cujo nó raiz é apontado pelo argumento
 * 
 * @param node: um ponteiro para a raiz da subárvore a ser desalocada
 */
void
freeNode(Node *node)
{
    if (node == NULL) {
        return;
    }

    freeNode(node->lft);
    freeNode(node->rgt);
    
    /**
     * Uma vez que as subárvores esquerda e direita estejam
     * liberadas, o nó em si é liberado
     */
    free(node->key);
    free(node->val);
    free(node);
}


/**
 * Função que insere um novo nó dentro da ST. Para tanto, executa uma busca
 * binária de modo a localizar a posição do valor a ser inserido, e, após a
 * inserção, acerta o balanceamento da ST por meio de rotações e flips
 *
 * @param st: a ST na qual o par chave-valor deve ser inserido
 * @param node: o nó a ser examinado em relação ao par chave-valor
 * @param key: a chave que identifica o par
 * @param sizeKey: o tamanho da chave a inserir, em bytes
 * @param val: o valor da chave passada
 * @param sizeVal: o tamanho do valor a inserir, em bytes
 * @return um ponteiro para o nó inserido
 */
Node*
putNode(RedBlackST st, Node *node, const void *key, size_t sizeKey, const void *val, size_t sizeVal)
{
    int cmp;

    /**
     * Se o nó recebido é NULL, então encontramos a posição certa do
     * par chave-valor e podemos simplesmente alocar o novo nó
     */
    if (node == NULL) {
        return initNode(key, sizeKey, val, sizeVal, RED, 1);
    }

    cmp = compareToNode(st, key, node);
    if (cmp < 0) {
        node->lft = putNode(st, node->lft, key, sizeKey, val, sizeVal);
    }
    else if (cmp > 0) {
        node->rgt = putNode(st, node->rgt, key, sizeKey, val, sizeVal);
    }
    else {
        /**
         * Se o nó associado à chave passada já existe, então basta
         * atualizar o seu valor. Para tanto, desaloca da memória o
         * antigo valor e duplica o conteúdo passado no argumento val
         */
        free(node->val);
        node->val = duplicate(val, sizeVal);
    }

    /**
     * Se a subárvore cuja raiz é node está pendendo para a direita,
     * resolve isso com rotações e flips de cores
     */
    if (isRed(node->rgt) && !isRed(node->lft)) {
        node = rotateLeft(node);
    }
    if (isRed(node->lft) && isRed(node->lft->lft)) {
        node = rotateRight(node);
    }
    if (isRed(node->lft) && isRed(node->rgt)) {
        flipColors(node);
    }

    /**
     * Atualiza o tamanho da subárvore apontada por este nó para que ele
     * leve em conta o tamanho das subárvores esquerda e direita
     */
    node->size = sizeNode(node->lft) + sizeNode(node->rgt) +  1;

    return node;
}


/**
 * Busca, dentro da ST, pelo nó cuja chave seja equivalente àquela
 * passada como parâmetro. Para tanto, efetua uma busca binária.
 * 
 * @param st: a ST na qual deve ser feita a busca
 * @param node: o nó atual em análise
 * @param key: a chave buscada
 * @return um ponteiro para o nó buscado, caso ele seja localizado,
 * ou NULL, caso contrário
 */
Node*
getNode(RedBlackST st, Node *node, const void *key)
{
    int cmp;

    if (node == NULL) {
        return NULL;
    }

    cmp = compareToNode(st, key, node);
    if (cmp == 0) {
        return node;
    }
    else if (cmp < 0) {
        return getNode(st, node->lft, key);
    }
    else {
        return getNode(st, node->rgt, key);
    }
}


Node*
deleteNode(RedBlackST st, Node *node, const void *key)
{
    Node *minNode;

    /**
     * Se o nó a ser removido possui chave inferior ao nó em análise, direciona
     * a remoção para a subárvore esquerda
     */
    if (compareToNode(st, key, node) < 0) {
        if (!isRed(node->lft) && !isRed(node->lft->lft)) {
            node = moveRedLeft(node);
        }
        node->lft = deleteNode(st, node->lft, key);
    }
    else {
        if (isRed(node->lft)) {
            node = rotateRight(node);
        }
        if ((compareToNode(st, key, node) == 0) && (node->rgt == NULL)) {
            return NULL;
        }
        if (!isRed(node->rgt) && !isRed(node->rgt->lft)) {
            node = moveRedRight(node);
        }

        /**
         * Se o nó a ser removido foi encontrado, é preciso identificar o nós capaz
         * de substituí-lo na árvore, isto é, o menor dos nós de sua subárvore direita.
         */
        if (compareToNode(st, key, node) == 0) {
            minNode = boundaryNode(node->rgt, FALSE);

            /**
             * Desaloca da memória os itens alocados dinamicamente
             */
            free(node->key);
            free(node->val);

            /**
             * Substitui os ponteiros contidos no node atual para que apontem para o
             * seu substituto, bem como os tamanhos dos atributos alocados dinamicamente
             */
            node->key = emalloc(minNode->sizeKey);
            node->val = emalloc(minNode->sizeVal);

            memcpy(node->key, minNode->key, minNode->sizeKey);
            memcpy(node->val, minNode->val, minNode->sizeVal);

            node->sizeKey = minNode->sizeKey;
            node->sizeVal = minNode->sizeVal;

            /**
             * Finaliza a remoção fazendo a remoção do minNode de sua antiga posição
             */
            node->rgt = deleteMinNode(node->rgt);
        }
        else {
            /**
             * Caso ainda não tenha sido localizado o item a ser removido, a busca deve
             * ser continuada, porém, dentro da subárvore direita do nó atual
             */
            node->rgt = deleteNode(st, node->rgt, key);
        }
    }
    return balance(node);
}


/**
 * Avalia o tamanho de uma subárvore cuja raiz é node
 * 
 * @param node: o nó cujo size será avaliado
 * @return um inteiro com o tamanho do nó
 */
int
sizeNode(Node *node)
{
    if (node == NULL) {
        return 0;
    }
    else {
        return node->size;
    }
}


/**
 * Percorre a ST em busca da menor chave armazenada nela, de modo a
 * removê-la da ST. Essa função também avalia o balanceamento da ST
 * de modo a não quebrá-lo.
 *
 * @param node: o nó da ST para se checar se é o de menor chave.
 * @return um ponteiro para a ST atualizada sem o antigo nó de menor chave.
 */
Node*
deleteMinNode(Node *node)
{
    /**
     * Se não há outro nó à esquerda do nó atual, então ele é o menor
     * nó da ST. Desaloca o espaço ocupado por ele e retorna NULL
     */
    if (node->lft == NULL) {
        freeNode(node);
        return NULL;
    }

    if (!isRed(node->lft) && !isRed(node->lft->lft)) {
        node = moveRedLeft(node);
    }

    /**
     * Continua a buscar o menor nó dentro da subárvore esquerda
     * de node, retornando-o balanceado
     */
    node->lft = deleteMinNode(node->lft);
    return balance(node);
}


/**
 * Percorre a ST em busca da maior chave armazenada nela, de modo a
 * removê-la da ST. Essa função também avalia o balanceamento da ST
 * de modo a não quebrá-lo.
 *
 * @param node: o nó da ST para se checar se é o de maior chave.
 * @return um ponteiro para a ST atualizada sem o antigo nó de maior chave.
 */
Node*
deleteMaxNode(Node *node)
{
    if (isRed(node->lft)) {
        node = rotateRight(node);
    }

    /**
     * Se não há outro nó à direita do nó atual, então ele é o maior
     * nó da ST. Desaloca o espaço ocupado por ele e retorna NULL
     */
    if (node->rgt == NULL) {
        freeNode(node);
        return NULL;
    }

    if (!isRed(node->rgt) && !isRed(node->rgt->lft)) {
        node = moveRedRight(node);
    }

    /**
     * Continua a buscar o maior nó dentro da subárvore direita
     * de node, retornando-o balanceado
     */
    node->rgt = deleteMaxNode(node->rgt);
    return balance(node);
}


/**
 * Uma função que retorna um nó fronteiriço da ST, isto é o máximo ou o mínimo
 * nó presentes na estrutura. O tipo de nó a ser retornado é definido pelo
 * argumento isMax. Se essa função é chamada, é garantido que existe um nó
 * fronteiriço.
 *
 * @param node: um ponteiro para um dado nó, cujo caráter fronteiriço vai ser analisado.
 * @param isMax: um booleano que indica se o nó buscado é o máximo ou o mínimo.
 * @return um ponteiro para o nó fronteiriço em questão.
 */
Node*
boundaryNode(Node *node, Bool isMax)
{
    Node *nextNode;

    /**
     * Se buscamos o máximo nó da ST, o próximo nó a ser analisado
     * com certeza vai estar à direita. Caso contrário, buscamos à
     * esquerda
     */
    if (isMax) {
        nextNode = node->rgt;
    }
    else {
        nextNode = node->lft;
    }
    
    /**
     * Se não houver um próximo nó para analisar, então achamos o
     * nó de interesse. Caso contrário, continua a busca
     */
    if (nextNode == NULL) {
        return node;
    }
    else {
        return boundaryNode(nextNode, isMax);
    }
}


/**
 * Função que retorna o número de nós estritamente menores que aquele associado
 * à chave passada como argumento.
 *
 * @param st: a ST.
 * @param node: o nó cujo rank vai ser avaliado.
 * @param key: a chave do nó cujo rank deseja-se conhecer.
 * @return um inteiro representando o número de chaves em questão.
 */
int
rankNode(RedBlackST st, Node *node, const void *key)
{
    int cmp;

    /**
     * Nós nulos, por convenção, não possuem nenhum nó estritamente menor do
     * que si próprios
     */
    if (node == NULL) {
        return 0;
    }

    cmp = compareToNode(st, key, node);
    
    if (cmp < 0) {
        /**
         * Se a chave buscada é menor do que a chave do nó em questão, então
         * este último não entra no rank do nó buscado. Continuamos a busca
         * pela subárvore esquerda do nó atual
         */
        return rankNode(st, node->lft, key);
    }
    else if (cmp > 0) {
        /**
         * Se a chave buscada é maior do que a chave do nó em questão, então
         * este último entra no rank do nó buscado, que deve portanto incluir,
         * além do nó atual, o número de nós na subárvore esquerda do nó atual
         * - o size do nó à esquerda -, e o número de nós à direita do atual que
         * são menores que o nó buscado
         */
        return 1 + sizeNode(node->lft) + rankNode(st, node->rgt, key);
    }
    else {
        /**
         * Se a chave buscada é a do nó em questão, então seu rank é o tamanho
         * da sua subárvore esquerda
         */
        return sizeNode(node->lft);
    }
}


/**
 * Função cujo objetivo é retornar o item portador da menor
 * (k+1)-ésima chave na ST. Essa função faz isso comparando 
 * o tamanho das subárvores à esquerda de um dado nó.
 *
 * @param node: um ponteiro para um nó que suspeita-se ser o (k+1)-ésimo menor.
 * @param k: a posição do nó buscado dentro da ST.
 * @return um ponteiro para o (k+1)-ésimo item de menor chave.
 */
Node*
selectNode(Node *node, int k)
{
    int sizeLeft = sizeNode(node->lft);

    /**
     * Se for encontrado um nó com exatamente k itens à sua
     * esquerda, então esse é o (k+1)-ésimo menor item da ST.
     * Retorna-o, portanto.
     */
    if (sizeLeft == k) {
        return node;
    }
    else if (sizeLeft > k) {
        return selectNode(node->lft, k);
    }
    else {
        return selectNode(node->rgt, k - sizeLeft - 1);
    }
}


/**
 * Função que computa a altura de um dado nó. O conceito de altura
 * é tal que uma árvore com 1 nó possui altura 0.
 *
 * @param node: o nó cuja altura deve ser calculada.
 * @return um inteiro indicando a altura do nó.
 */
int
heightNode(Node *node) {
    int lftHeight;
    int rgtHeight;

    if (node == NULL) {
        return -1;
    }

    /**
     * Computa as alturas das subárvores esquerda e direita,
     * retornando a maior delas
     */
    lftHeight = heightNode(node->lft);
    rgtHeight = heightNode(node->rgt);

    if (lftHeight > rgtHeight) {
        return 1 + lftHeight;
    }
    else {
        return 1 + rgtHeight;
    }
}


/**
 * Uma função auxiliar para tornar a execução de comparações mais semântica.
 * Tudo o que ela faz é chamar a função de comparação armazenada na ST para
 * a chave passada e a chave localizada no parâmetro node.
 *
 * @param st: a ST.
 * @param key: a chave referência para a comparação.
 * @param node: o nó com o qual a key deve ser comparada.
 * @return 0 se as chaves são iguais, um valor negativo se key é menor do chave.
 * do item em pos, e um valor positivo se key é maior do que a chave em pos.
 */
int
compareToNode(RedBlackST st, const void *key, Node *node)
{
    return st->comparator(key, node->key);
}


/**
 * Verifica se um dado nó é vermelho. Por convenção, nós-folha 
 * são sempre negros.
 *
 * @param node: o nó a ser verificado.
 * @return TRUE se o nó é vermelho, e FALSE, caso contrário.
 */
static Bool
isRed(Node *node) {
    if (node == NULL) {
        return FALSE;
    }
    else {
        return (node->color == RED);    
    }
}


/**
 * Inverte as cores de um nó e de seus filhos
 *
 * @param node: o nó sobre o qual vai ser efetuada a troca
 * @return um ponteiro para node com sua própria cor e a cor de
 * seus filhos atualizada
 */
static
void flipColors(Node *node) {
    node->color = !node->color;
    node->lft->color = !node->lft->color;
    node->rgt->color = !node->rgt->color;
}


/**
 * Rotaciona um nó pendente para a esquerda de modo que ele
 * penda para a direita
 *
 * @param node: o nó a ser rotacionado
 * @return um ponteiro com o nó rotacionado
 */
static Node*
rotateRight(Node *node)
{
    Node *rotatedNode = node->lft;
    
    /**
     * Atualiza os ponteiros do nó rotacionado, de modo que 
     * o filho direito dele fique vermelho
     */
    node->lft = rotatedNode->rgt;

    rotatedNode->rgt = node;
    rotatedNode->color = rotatedNode->rgt->color;
    rotatedNode->rgt->color = RED;
    rotatedNode->size = sizeNode(node);

    /**
     * Atualiza o tamanho do antigo nó
     */
    node->size = sizeNode(node->lft) + sizeNode(node->rgt) + 1;

    return rotatedNode;
}


/**
 * Rotaciona um nó pendente para a direita de modo que ele
 * penda para a esquerda
 *
 * @param node: o nó a ser rotacionado
 * @return um ponteiro com o nó rotacionado
 */
static Node*
rotateLeft(Node *node)
{
    Node *rotatedNode = node->rgt;

    /**
     * Atualiza os ponteiros do nó rotacionado, de modo que 
     * o filho esquerdo dele fique vermelho
     */
    node->rgt = rotatedNode->lft;

    rotatedNode->lft = node;
    rotatedNode->color = rotatedNode->lft->color;
    rotatedNode->lft->color = RED;
    rotatedNode->size = sizeNode(node);

    /**
     * Atualiza o tamanho do antigo nó
     */
    node->size = sizeNode(node->lft) + sizeNode(node->rgt) + 1;

    return rotatedNode;
}


/**
 * Função auxiliar que objetiva a criação de um nó rubro na
 * subárvore esquerda do argumento node. Para isso, assume que
 * node é rubro, e ambos node->lft e node->lft->lft são negros.
 *
 * @param node: um ponteiro para o nó que deve ter um nó rubro à direita.
 * @return um ponteiro para o nó node com um nó rubro à direita.
 */
static Node*
moveRedLeft(Node *node)
{
    flipColors(node);
    
    if (isRed(node->rgt->lft)) {
        node->rgt = rotateRight(node->rgt);
        node = rotateLeft(node);
        flipColors(node);
    }

    return node;
}


/**
 * Função auxiliar que objetiva a criação de um nó rubro na
 * subárvore direita do argumento node. Para isso, assume que
 * node é rubro, e ambos node->rgt e node->rgt->lft são negros.
 *
 * @param node: um ponteiro para o nó que deve ter um nó rubro à direita.
 * @return um ponteiro para o nó node com um nó rubro à direita.
 */
static Node*
moveRedRight(Node *node)
{
    flipColors(node);

    /**
     * Se após a inversão de cores existirem dois nós vermelhos
     * consecutivos, usa rotação para consertar o balanceamento
     */
    if (isRed(node->lft->lft)) {
        node = rotateRight(node);
        flipColors(node);
    }
    return node;
}


/**
 * Função auxiliar que verifica potenciais situações em que
 * a ST possa não estar balanceada. Se algum delas for verificada,
 * aplica a correção apropriada - flip ou rotação.
 *
 * @param node: o nó cujo balanceamento deve ser verificado.
 * @return um ponteiro para o nó devidamente balanceado.
 */
static Node*
balance(Node* node)
{
    if (isRed(node->rgt)) {
        node = rotateLeft(node);
    }
    if (isRed(node->lft) && isRed(node->lft->lft)) {
        node = rotateRight(node);
    }
    if (isRed(node->lft) && isRed(node->rgt)) {
        flipColors(node);
    }

    /**
     * Ao final das correções, é preciso atualizar o tamanho do
     * nó, para que leve em conta eventuais mudanças na estrutura
     * da ST
     */
    node->size = sizeNode(node->lft) + sizeNode(node->rgt) + 1;

    return node;
}


/***************************************************************************
 *  Check integrity of red-black tree data structure.
 ***************************************************************************/

/*
 * CHECK(ST)
 *
 * RECEBE uma RedBlackST ST e RETORNA TRUE se não encontrar algum
 * problema de ordem ou estrutural. Em caso contrário RETORNA 
 * FALSE.
 * 
 */
Bool
check(RedBlackST st)
{
    if (!isBST(st))            ERROR("check(): not in symmetric order");
    if (!isSizeConsistent(st)) ERROR("check(): subtree counts not consistent");
    if (!isRankConsistent(st)) ERROR("check(): ranks not consistent");
    if (!is23(st))             ERROR("check(): not a 2-3 tree");
    if (!isBalanced(st))       ERROR("check(): not balanced");
    return isBST(st) && isSizeConsistent(st) && isRankConsistent(st) && is23(st) && isBalanced(st);
}

/**
 * Verifica se um dado nó está de acordo com o princípio de ordem de BSTs.
 * Isso equivale a perguntar se a chave à esquerda de um nó é menor do que a dele,
 * e, a chave à direita, é maior.
 *
 * @param st: a ST à qual o nó em análise pertence.
 * @param node: o nó cuja ordem deve ser avaliada.
 * @param minKey: uma chave limitante inferior; pode ser NULL.
 * @param maxKey: uma chave limitante superior; pode ser NULL.
 * @return TRUE se o nó possui ordem consistente.
 */
static Bool
isNodeBST(RedBlackST st, Node *node, const void *minKey, const void *maxKey)
{
    /**
     * Nós-folha são uma BST por convenção
     */
    if (node == NULL) {
        return TRUE;
    }

    /**
     * Se há uma chave limitante inferior e ela é maior do que a chave do item
     * atual, então o princípio de ordem de uma BST foi violado
     */
    if ((minKey != NULL) && (compareToNode(st, minKey, node) >= 0)) {
        return FALSE;
    }

    /**
     * Se há uma chave limitante superior e ela é menor do que a chave do item
     * atual, então o princípio de ordem de uma BST foi violado
     */
    if ((maxKey != NULL) && (compareToNode(st, maxKey, node) <= 0)) {
        return FALSE;
    }

    return isNodeBST(st, node->lft, minKey, node->key) && isNodeBST(st, node->rgt, node->key, maxKey);
}


/* 
 * ISBST(ST)
 * 
 * RECEBE uma RedBlackST ST.
 * RETORNA TRUE se a árvore é uma BST.
 * 
 */
static Bool
isBST(RedBlackST st)
{
    return isNodeBST(st, st->root, NULL, NULL);
}


/**
 * Função que analisa se um dado nó possui tamanho consistente.
 * Isto é, ela verifica se o tamanho de um nó é uma unidade maior
 * do que a soma dos tamanhos de suas subárvores esquerda e direita.
 *
 * @param node: o nó cujo tamanho deve ser avaliado.
 * @return TRUE se o nó possui tamanho consistente.
 */
static Bool
isNodeSizeConsistent(Node *node)
{
    /**
     * Folhas estão sempre com o tamanho consistente por convenção
     */
    if (node == NULL) {
        return TRUE;
    }

    /**
     * Se o tamanho de um nó não for consistente com a soma dos tamanhos
     * de seus filhos, então o a ST não tem tamanho consistente
     */
    if (sizeNode(node) != sizeNode(node->lft) + sizeNode(node->rgt) + 1) {
        return FALSE;
    }
    return isNodeSizeConsistent(node->lft) && isNodeSizeConsistent(node->rgt);
}

/* 
 *  ISSIZECONSISTENT(ST) 
 *
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se para cada nó h
 *  vale que size(h) = 1 + size(h->lft) + size(h->rgt) e 
 *  FALSE em caso contrário.
 */
static Bool
isSizeConsistent(RedBlackST st)
{
    return isNodeSizeConsistent(st->root);
}


/* 
 *  ISRANKCONSISTENT(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se seus rank() e
 *  select() são consistentes.
 */
static Bool
isRankConsistent(RedBlackST st)
{
    Node *node;
    void *key;
    
    int pos;
    int ranking;

    for (pos = 0; pos < size(st); pos++) {
        node = selectNode(st->root, pos);

        /**
         * Se o rank do pos-ésimo menor item da ST não corresponder
         * a pos, então os itens não estão ordernados corretamente 
         */
        if (pos != rank(st, node->key)) {
            return FALSE;
        }
    }

    key = keys(st, TRUE);
    while (key != NULL) {
        /**
         * Obtém o rank da chave atual e o nó associado a esse
         * ranking
         */
        ranking = rank(st, key);
        node = selectNode(st->root, ranking);

        /**
         * Se o nó associado ao rank da chave atual não corresponder
         * ao nó que possui tal chave, então a ST não está ordernada
         * corretamente
         */
        if (compareToNode(st, key, node) != 0) {
            return FALSE;
        }

        /**
         * Libera o espaço alocado para a chave atual e requere
         * a próxima chave da ST
         */
        free(key);
        key = keys(st, FALSE);
    }
    return TRUE;
}


/**
 * Função auxiliar que verifica se um nó é válido dentro dos
 * invariantes de uma árvore 2-3. Para isso, verifica se todos
 * os caminhos da árvore são negros ou se possuem nós de cores
 * intercaladas.
 * 
 * @param st: a ST a verificar.
 * @param node: o nó cuja validade será verificada.
 * @return se o nó obedece ao invariante das árvores 2-3.
 */
static Bool
isNode23(RedBlackST st, Node *node)
{
    /**
     * Nós-folha são sempre 2-3, por convenção
     */
    if (node == NULL) {
        return TRUE;
    }

    /**
     * Não pode haver nós vermelhos em caminhos à direita
     */
    if (isRed(node->rgt)) {
        return FALSE;
    }

    /**
     * Dois nós consecutivos não podem ser rubros
     */
    if ((node != st->root) && isRed(node) && isRed(node->lft)) {
        return FALSE;
    }
    return isNode23(st, node->lft) && isNode23(st, node->rgt);
}


/* 
 *  IS23(ST)
 *
 *  RECEBE uma RedBlackST ST e RETORNA FALSE se há algum link RED
 *  para a direta ou se ha dois links para esquerda seguidos RED 
 *  Em caso contrário RETORNA TRUE (= a ST representa uma árvore 2-3). 
 */
static Bool
is23(RedBlackST st)
{
    return isNode23(st, st->root);
}


/**
 * Função que verifica se todos os caminhos da árvore possuem
 * o mesmo número de nós negros entre a raiz e uma folha qualquer.
 * 
 * @param node: nó a partir do qual os caminhos são analisados.
 * @param numBlackNodes: o número de nós negros necessário.
 * @return TRUE se todos os caminhos possuem o mesmo número de nós negros.
 */
static Bool
isEveryPathBalanced(Node *node, int numBlackNodes)
{
    /**
     * Nós-folha devem possuir zero nós negros em seu
     * caminho até um nó-folha
     */
    if (node == NULL) {
        return (numBlackNodes == 0);
    }

    /**
     * Se o nó em questão for negro, diminui o número de nós
     * negros ainda necessários até encontrar uma folha e 
     * segue avaliando os próximos caminhos
     */
    if (!isRed(node)) {
        numBlackNodes--;
    }
    return isEveryPathBalanced(node->lft, numBlackNodes) && isEveryPathBalanced(node->rgt, numBlackNodes);
}


/* 
 *  ISBALANCED(ST) 
 * 
 *  RECEBE uma RedBlackST ST e RETORNA TRUE se st satisfaz
 *  balanceamento negro perfeiro.
 */ 
static Bool
isBalanced(RedBlackST st)
{
    int numBlackNodes;
    Node *node;
    
    numBlackNodes = 0;
    node = st->root;

    /**
     * Inicialmente descobre o número de nós negros que um
     * caminho arbitrário da ST possui
     */
    while (node != NULL) {
        if (!isRed(node)) {
            numBlackNodes++;
        }
        node = node->lft;
    }

    /**
     * Verifica se todos os caminhos da árvore possuem o mesmo
     * número de nós negros que aquele averiguado no caminho
     * arbitrário
     */
    return isEveryPathBalanced(st->root, numBlackNodes);
}
