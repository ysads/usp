/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    A confecção desse EP necessitou, além de conhecimentos prévios na
 *    linguagem Java, de consulta às seguintes APIs da biblioteca algs4:
 *
 *      - https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/Picture.java.html
 *      - https://algs4.cs.princeton.edu/24pq/IndexMinPQ.java.html
 *      - https://algs4.cs.princeton.edu/44sp/DijkstraSP.java.html
 *
 *  Bugs & Limitações:
 *    A adaptação do algoritmo de Dijkstra implicou em perdas de performance.
 *    Para grandes reduções ou para imagens de grande dimensão, o tempo para
 *    localizar e remover os seams é relativamente alto. Nos testes executados,
 *    remover 200 colunas e 100 linhas de uma imagem 768x432 pixels levou cerca
 *    de 18 minutos.
 */

import edu.princeton.cs.algs4.IndexMinPQ;
import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

import java.awt.Color;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;

public class SeamCarver {

    /**
    * A copy of the last processed image.
    */
    private Picture picture;

    /**
    * The width of the picture.
    */
    private int width;

    /**
    * The height of the picture.
    */
    private int height;

    /**
     * A matrix of Pixel objects representing the pixels in the
     * picture.
     */
    private Pixel[][] pixels;

    /**
     * Helper constants to make code more semantic.
     */
    private final boolean X = false;
    private final boolean Y = true;

    /**
    * Create a seam carver object based on the given picture.
    * @param picture: a Picture object representing the image to work on.
    */
    public SeamCarver(Picture picture) {
        this.width   = picture.width();
        this.height  = picture.height();
        this.picture = new Picture(picture);
        this.pixels  = new Pixel[this.width][this.height];
        buildPixels();
    }

    /**
     * Curren picture
     * @return the current picture.
     */
    public Picture picture() {
        return this.picture;
    }

    /**
    * Width of the picture.
    * @return the width of the last processed image.
    */
    public int width() {
        return this.width;
    }

    /**
     * Height of the picture.
     * @return the height of the last processed image.
     */
    public int height() {
        return this.height;
    }

    /**
     * Returns the energy of the pixel in the given coordinates.
     * @param  x: the column of the pixel.
     * @param  y: the row of the pixel.
     * @return a double representing its energy.
     */
    public double energy(int x, int y) {
        if ((x >= this.width) || (y >= this.height)) {
            throw new IllegalArgumentException();   
        }
        return this.pixels[x][y].energy;
    }

    /**
     * Sequence of indices for horizontal seam.
     * @return a sequence of indexes describing the lowest energy path.
     */
    public int[] findHorizontalSeam() {
        double minEnergyToSource;
        Pixel pixel;
        Pixel lastPixelMinPath;

        for (int x = 0; x < this.width(); x++) {
            for (int y = 0; y < this.height(); y++) {
                this.pixels[x][y].energyToSource = Double.POSITIVE_INFINITY;
            }
        }

        /**
         * For each row, take its first pixel as source to a Dijkstra-like
         * algorithm, who will determine the minimun-energy path possible
         * starting at this pixel. Note the information about minimal energy
         * is not reset across iterations, which means we're always open
         * to eventual better paths. 
         */
        for (int y = 0; y < this.height(); y++) {
            findMinEnergyFromSource(this.pixels[0][y], X);
        }

        minEnergyToSource = Double.POSITIVE_INFINITY;
        lastPixelMinPath  = null;

        for (int y = 0; y < this.height(); y++) {
            pixel = this.pixels[this.width() - 1][y];

            if (pixel.energyToSource < minEnergyToSource) {
                lastPixelMinPath  = pixel;
                minEnergyToSource = pixel.energyToSource;
            }
        }
        return lastPixelMinPath.rebuildPath(X);
    }

    /**
     * Sequence of indices for vertical seam.
     * @return a sequence of indexes describing the lowest energy path.
     */
    public int[] findVerticalSeam() {
        double minEnergyToSource;
        Pixel pixel;
        Pixel lastPixelMinPath;

        for (int x = 0; x < this.width(); x++) {
            for (int y = 0; y < this.height(); y++) {
                this.pixels[x][y].energyToSource = Double.POSITIVE_INFINITY;
            }
        }

        /**
         * For each column, take its first pixel as source to a Dijkstra-like
         * algorithm, who will determine the minimun-energy path possible
         * starting at this pixel. Note the information about minimal energy
         * is not reset across iterations, which means we're always open
         * to eventual better paths. 
         */
        for (int x = 0; x < this.width(); x++) {
            findMinEnergyFromSource(this.pixels[x][0], Y);
        }

        minEnergyToSource = Double.POSITIVE_INFINITY;
        lastPixelMinPath  = null;

        for (int x = 0; x < this.width(); x++) {
            pixel = this.pixels[x][this.height() - 1];

            if (pixel.energyToSource < minEnergyToSource) {
                lastPixelMinPath  = pixel;
                minEnergyToSource = pixel.energyToSource;
            }
        }
        return lastPixelMinPath.rebuildPath(Y);
    }

    /**
     * Removes the given horizontal seam from current picture.
     * @param seam: the y indices of the Pixels to remove.
     */
    public void removeHorizontalSeam(int[] seam) {
        if (this.height() == 1) {
            throw new IllegalArgumentException();
        }
        this.validateSeam(seam, X);

        Picture smallerPicture = new Picture(this.width(), this.height() - 1);
        Pixel pixel;

        /**
         * For each column, copies the non-altered pixels to a new image.
         * Note this copy is splitted in two phases: first we copy pixels
         * until the pixel in the seam, and then we copy the pixels after
         * the seam - taking the stance that each subsequent pixel in the
         * original picture should be on row above its original position.
         */
        for (int x = 0; x < this.width(); x++) {
            for (int y = 0; y < seam[x]; y++) {
                smallerPicture.setRGB(x, y, this.picture.getRGB(x, y));
            }
            for (int y = seam[x] + 1; y < this.height(); y++) {
                smallerPicture.setRGB(x, y - 1, this.picture.getRGB(x, y));
            }
        }

        /**
         * Finally, update the pointers in the object, rebuilding the
         * pixels matrix and the energy matrix, since the original picture
         * has been mutated.
         */
        this.picture = smallerPicture;
        this.height--;
        this.buildPixels();
    }

    /**
     * Removes the given vertical seam from current picture.
     * @param seam: the x indices of the Pixels to remove.
     */
    public void removeVerticalSeam(int[] seam) {
        if (this.width() == 1) {
            throw new IllegalArgumentException();
        }
        this.validateSeam(seam, Y);

        Picture smallerPicture = new Picture(this.width() - 1, this.height());
        Pixel pixel;

        /**
         * For each row, copies the non-altered pixels to a new image.
         * Note this copy is splitted in two phases: first we copy pixels
         * until the pixel in the seam, and then we copy the pixels after
         * the seam - taking the stance that each subsequent pixel in the
         * original picture should be on column before its original position.
         */
        for (int y = 0; y < this.height(); y++) {
            for (int x = 0; x < seam[y]; x++) {
                smallerPicture.setRGB(x, y, this.picture.getRGB(x, y));
            }
            for (int x = seam[y] + 1; x < this.width(); x++) {
                smallerPicture.setRGB(x - 1, y, this.picture.getRGB(x, y));
            }
        }

        /**
         * Finally, update the pointers in the object, rebuilding the
         * pixels matrix and the energy matrix, since the original picture
         * has been mutated.
         */
        this.picture = smallerPicture;
        this.width--;
        this.buildPixels();
    }

    /**
     * Unit testing (required)
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("You must give a filepath in order to run SeamCarver unit tests.");
        }
        Picture picture = new Picture(args[0]);
        SeamCarver carver = new SeamCarver(picture);
        int[] verticalSeam, horizontalSeam;

        StdOut.printf("Image %s loaded.\n", args[0]);
        StdOut.printf("Image size: %d-by-%d\n\n", carver.width(), carver.height());

        StdOut.printf("Printing the associated energy matrix: \n");
        for (int row = 0; row < carver.height(); row++) {
            for (int col = 0; col < carver.width(); col++) {
                StdOut.printf("%9.2f ", carver.energy(col, row));
            }
            StdOut.println();
        }

        StdOut.println();
        StdOut.printf("Looking up for vertical seam...\n");
        verticalSeam = carver.findVerticalSeam();
        StdOut.printf("Vertical seam found: [ ");
        for (int x : verticalSeam) {
            StdOut.print(x + " ");
        }
        StdOut.printf("]\n\n");

        StdOut.printf("Removing vertical seam...\n");
        carver.removeVerticalSeam(verticalSeam);
        StdOut.printf("Seam removed. New image size: %d-by-%d\n\n", carver.width(), carver.height());

        StdOut.printf("Looking up for horizontal seam...\n");
        horizontalSeam = carver.findHorizontalSeam();
        StdOut.printf("Vertical seam found: [ ");
        for (int y : horizontalSeam) {
            StdOut.print(y + " ");
        }
        StdOut.printf("]\n\n");

        StdOut.printf("Removing horizontal seam...\n");
        carver.removeHorizontalSeam(horizontalSeam);
        StdOut.printf("Seam removed. New image size: %d-by-%d\n\n", carver.width(), carver.height());

        StdOut.printf("Showing the final picture...\n");
        carver.picture().show();
    }

    /**
     * Builds a 2D array of Pixel objects, representing each pixel in the
     * Picture object. After that, also evaluates the energy of each Pixel.
     */
    private void buildPixels() {
        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {
                this.pixels[x][y] = new Pixel(x, y);
            }
        }
        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {
                this.pixels[x][y].updateEnergy();
            }
        }
    }

    /**
     * Converts the single dimension index into a bidimensional index
     * and returns the Pixel at this given position.
     * @param  index: the index of the Pixel we want to get.
     * @return the Pixel object at the given index.
     */
    private Pixel getPixelAtIndex(int index) {
        int y = index / this.width();
        int x = index - (this.width() * y);
        
        return this.pixels[x][y];
    }

    /**
     * Performs a Dijkstra-like algorithm whose source is the given Pixel.
     * @param source: the Pixel in which the Dijkstra must start.
     * @param axis: the axis in which Dijkstra will be performed, being X or Y.
     */
    private void findMinEnergyFromSource(Pixel source, boolean axis) {
        IndexMinPQ<Double> queue = new IndexMinPQ<Double>(this.width() * this.height());
        Pixel currPixel;

        /**
         * The first pixel to visit is the source, which is given as arg.
         * Also, since it's a source, its energyToSource is zero.
         */
        source.energyToSource = source.energy;
        queue.insert(source.toIndex(), source.energyToSource);

        while (!queue.isEmpty()) {
            currPixel = getPixelAtIndex(queue.delMin());

            /**
             * Iterate over the neighbouring pixels, checking if there's
             * a shorter path to them.
             */
            for (Pixel neighbour : currPixel.neighbours(axis)) {
                if (currPixel.energyToSource + neighbour.energy < neighbour.energyToSource) {
                    /**
                     * Since we found a path to this neighbour Pixel with
                     * lower energy, we must update it with this new data.
                     */
                    neighbour.updateMinEnergyPath(currPixel);

                    /**
                     * If this pixel is already contained in the queue,
                     * we just decrease its key. Otherwise, insert it
                     * so we can perform the checks on it in the future.
                     */
                    if (queue.contains(neighbour.toIndex())) {
                        queue.decreaseKey(neighbour.toIndex(), neighbour.energyToSource);
                    }
                    else {
                        queue.insert(neighbour.toIndex(), neighbour.energyToSource);
                    }
                }
            }
        }
    }

    /**
     * Validates a given seam according to its orientation.
     * @param seam: the seam to validate.
     * @param axis: the axis on which the seam is based.
     */
    private void validateSeam(int[] seam, boolean axis) {
        int maxDimension, oppositeDimension;
        int prev;

        /**
         * Initializes helper variables depending on whether a given
         * seam is horizontal or vertical.
         */
        if (axis == X) {
            maxDimension = this.width();
            oppositeDimension = this.height();
        }
        else {
            maxDimension = this.height();
            oppositeDimension = this.width();
        }

        /**
         * If the seam is null or if it has a weird length, throws
         * an exception.
         */
        if (seam == null) {
            throw new IllegalArgumentException("Seam is null.");
        }
        if (seam.length != maxDimension) {
            throw new IllegalArgumentException("Seam have " + seam.length + " size. It should have " + maxDimension);
        }

        /**
         * If we have two consecutive seams that diverge by more than
         * one unit, then throws an exception.
         */
        prev = seam[0] - 1;
        for (int i = 0; i < maxDimension; i++) {
            if (seam[i] < 0 || seam[i] >= oppositeDimension) {
                throw new IllegalArgumentException(seam[i] + " not in interval [" + 0 + "," + oppositeDimension + "[");
            }
            if (Math.abs(seam[i] - prev) > 1) {
                throw new IllegalArgumentException(seam[i] + " and " + prev  + " differ by more than 1.");
            }
            prev = seam[i];
        }
    }

    private class Pixel {
        public int r;
        public int g;
        public int b;

        private int x;
        private int y;

        public double energy;
        public double energyToSource;

        public Pixel parent;

        /**
         * Constructor.
         * @param  x: the column of the pixel (0 <= x < width).
         * @param  y: the row of the pixel (0 <= y < height).
         */
        public Pixel(int x, int y) {
            this.x = x;
            this.y = y;
            this.parent = null;
            splitRGB();
        }

        /**
         * Updates the constraints of the minimun energy path for this
         * Pixel, which means updating the pointer to the parent Pixel
         * and the amount in the path to this Pixel.
         * @param parent: the Pixel imediately before this.
         */
        public void updateMinEnergyPath(Pixel parent) {
            this.parent = parent;
            this.energyToSource = parent.energyToSource + this.energy;
        }

        /**
         * Since the pixels are laid on a bidimensional array, this
         * method returns a single-dimensional representation of a
         * pixel position on the matrix.
         * @return a single int representing the pixel position.
         */
        public int toIndex() {
            return x + y * width;
        }

        /**
         * Return the list of neighbour pixels on the desired axis.
         * @param  onAxis: the axis to find the neighbours on, being X or Y.
         * @return an iterable collection of Pixel.
         */
        public Iterable<Pixel> neighbours(boolean onAxis) {
            ArrayList<Pixel> list = new ArrayList<Pixel>();

            if (onAxis == X) {
                /**
                 * There is no neighbour if we reach the last pixel
                 * on the row. This also guarantee to us that we have
                 * a pixel ahead in the same row.
                 */
                if (this.x < width() - 1) {
                    /**
                     * If we're not on the first row, there is a pixel
                     * ahead on the previous row.
                     */
                    if (this.y > 0) {
                        list.add(pixels[this.x + 1][this.y - 1]);
                    }
                    list.add(pixels[this.x + 1][this.y]);
                    
                    /**
                     * If we're not on the last row, there is also a
                     * pixel ahead on the next row.
                     */
                    if (this.y < height() - 1) {
                        list.add(pixels[this.x + 1][this.y + 1]);
                    }
                }
            }
            else {
                /**
                 * There is no neighbour if we reach the last pixel on the
                 * column. This also guarantee to us that we have a pixel
                 * below in the same column.
                 */
                if (this.y < height() - 1) {
                    /**
                     * If we're not on the first column, there is a pixel
                     * below on the previous column.
                     */
                    if (this.x > 0) {
                        list.add(pixels[this.x - 1][this.y + 1]);
                    }
                    list.add(pixels[this.x][this.y + 1]);
                    
                    /**
                     * If we're not on the last column, there is also a
                     * pixel below on the next column.
                     */
                    if (this.x < width() - 1) {
                        list.add(pixels[this.x + 1][this.y + 1]);
                    }
                }
            }
            return list;
        }

        /**
         * Starting from the current pixel, it rebuilds its minimun energy
         * path, always going through the parent of each visited pixel.
         * @param  onAxis: the axis in which the path is, being X or Y.
         * @return an iterable collection of indices.
         */
        public int[] rebuildPath(boolean onAxis) {
            int[] path;
            Pixel currPixel = this;

            /**
             * Depending on whether the path is horizontal or vertical,
             * the path to return may vary from containing x or y indices. 
             */
            if (onAxis == X) {
                path = new int[width()];
                for (int i = width() - 1; i >= 0; i--) {
                    path[i] = currPixel.y;
                    currPixel = currPixel.parent;
                }
            }
            else {
                path = new int[height()];
                for (int i = height() - 1; i >= 0; i--) {
                    path[i] = currPixel.x;
                    currPixel = currPixel.parent;
                }
            }
            return path;
        }

        /**
         * Helper method that splits a 32-bit encoded RGB color into
         * three, 8-bit, independent components.
         */
        private void splitRGB() {
            int rgb = picture.getRGB(this.x, this.y);

            this.r = (rgb >> 16) & 0xFF;
            this.g = (rgb >>  8) & 0xFF;
            this.b = (rgb >>  0) & 0xFF;
        }

        /**
         * Updates the energy of this pixel based on its current circular
         * neighbours.
         * @return the updated energy of the pixel.
         */
        private double updateEnergy() {
            this.energy = Math.sqrt(delta(X) + delta(Y));
            return this.energy;
        }

        /**
         * Evaluates the difference of squares of each RGB component between
         * the circular neighbour pixels. Circular here means that pixels
         * should be interpreted as if they were laid in a torus. That is,
         * a pixel in column 0 is neighbour to one in column width - 1.
         * @param  isX: if true, returns the horizontal delta; otherwise,
         *               returns the vertical delta.
         * @return a double representing the square of difference.
         */
        private double delta(boolean isX) {
            Pixel nextPixel, prevPixel;
            double difference;
            int prevX, nextX;
            int prevY, nextY;

            /**
             * Notice that % operator in Java corresponds to remainder, not
             * modulus. So, to perform a mod b, it's necessary to do:
             *                     (a % b + b) % b
             * This will give the circular neighbor of a given coordinate,
             * not matter if x or y.
             */
            if (isX) {
                nextX = ((this.x + 1) % width + width) % width;
                prevX = ((this.x - 1) % width + width) % width;
                prevY = nextY = this.y;
            }
            else {
                nextY = ((this.y + 1) % height + height) % height;
                prevY = ((this.y - 1) % height + height) % height;
                prevX = nextX = this.x;
            }

            nextPixel = pixels[nextX][nextY];
            prevPixel = pixels[prevX][prevY];

            /**
             * Finally, evaluate the squares of difference between each
             * color component.
             */
            difference = Math.pow(nextPixel.r - prevPixel.r, 2.0) +
                         Math.pow(nextPixel.g - prevPixel.g, 2.0) +
                         Math.pow(nextPixel.b - prevPixel.b, 2.0);
  
            return difference;
        }
    }
}
