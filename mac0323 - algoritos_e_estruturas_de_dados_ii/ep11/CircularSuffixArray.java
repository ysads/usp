/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP foram consultadas as fontes:
 *    - https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html
 *      Referência de uso da classe Arrays
 *
 *    - https://docs.oracle.com/javase/8/docs/api/java/util/Comparator.html
 *      Referência de uso da classe Comparator
 *
 *  Bugs & Limitações:
 *    Não foram previamente identificada quaisquer bugs ou limitações
 *    que a solução abaixo possa vir a ter.
 */
import edu.princeton.cs.algs4.StdOut;

import java.lang.IllegalArgumentException;
import java.util.Arrays;
import java.util.Comparator;

class CircularSuffixArray {

    private int length;
    
    /**
     * An array to hold the indices of the suffixes in what would
     * have been the original, unsorted, array of suffixes,
     * generated procedurally.
     */
    private Integer[] indices;

    /**
     * The string on top of which the suffixes will be constructed.
     */
    private String str;

    public CircularSuffixArray(String s) {
        if (s == null) {
            throw new IllegalArgumentException();
        }

        this.str = s;
        this.length = s.length();

        /**
         * Populate the array of indices, initially, with consecutive values.
         */
        this.indices = new Integer[this.length];
        for (int i = 0; i < this.length; i++) {
            this.indices[i] = i;
        }

        sortSuffixes();
    }

    /**
     * A method whose purpose is to guide how two suffixes are compared.
     * This approach intends to make cheaper comparing two suffixes, since we
     * don't have to generate and store them in a separate array. The entire
     * process is done on top of the base suffix, by taking two different
     * suffixes starting at particular positions of the original string. From
     * these positions, the algorithm walks through the entire string length,
     * comparing characters at equivalent offsets - from the original index.
     */
    private void sortSuffixes() {
        Arrays.sort(this.indices, new Comparator<Integer> () {
            @Override
            public int compare(Integer a, Integer b) {
                /**
                 * These are the indices in which the suffixes begin.
                 */
                int indexA = a;
                int indexB = b;

                for (int i = 0; i < length; i++) {
                    /**
                     * Reset indexA or indexB if one of them goes beyond the
                     * borders of the original base string.
                     */
                    if (indexA >= length) {
                        indexA = 0;
                    }
                    if (indexB >= length) {
                        indexB = 0;
                    }

                    /**
                     * If the chars at equivalent offsets don't match, we already
                     * know which suffix is lexicographically smaller.
                     */
                    if (str.charAt(indexA) < str.charAt(indexB)) {
                        return -1;
                    }
                    else if (str.charAt(indexA) > str.charAt(indexB)) {
                        return 1;
                    }

                    /**
                     * The chars at equivalent offsets are equal, then we increase
                     * the offset by moving to next position;
                     */
                    indexA++;
                    indexB++;
                }

                /**
                 * Both the suffixes are equal if we ran through the entire
                 * suffixes and didn't found a single divergent char.
                 */
                return 0;
            }
        });
    }

    /**
     * @return Returns the length of s.
     */
    public int length() {
        return this.length;
    }

    /**
     * Returns the index of i-th sorted suffix.
     * @param int: the element whose index will be returned.
     * @return a integer corresponding to the index.
     */
    public int index(int i) {
        return this.indices[i];
    }

    /**
     * Unit testing for the class' public methods
     * @param args: the command-line arguments passed to the class.
     */
    public static void main(String[] args) {
        CircularSuffixArray csa = new CircularSuffixArray(args[0]);
        
        StdOut.println("len > " + csa.length());
        StdOut.println("-----------------------------");
        
        for (int i = 0; i< csa.length(); i++) {
            StdOut.println("index " + i + " > " + csa.index(i));
        }        
    }
}