/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa.
 *
 *  Bugs & Limitações:
 *    Não foram previamente identificada quaisquer bugs ou limitações
 *    que a solução abaixo possa vir a ter.
 */

public class BurrowsWheeler {

    public static void transform() {
    }

    public static void inverseTransform() {
    }

    public static void main(String[] args) {
    }
}