/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP foram consultadas as fontes:
 *    
 *    - https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html
 *      Referência de uso da classe LinkedList
 *
 *  Bugs & Limitações:
 *    Não foram previamente identificada quaisquer bugs ou limitações
 *    que a solução abaixo possa vir a ter.
 */
import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

import java.util.LinkedList;
import java.util.NoSuchElementException;

class MoveToFront {

    private static LinkedList<Character> characters;

    /**
     * Initializes a linked list with the first 256 chars from ASCII
     * extended. It is initialized in such a way that the i-th element
     * of the list represents the i-th first ASCII char.
     */
    private static void initList() {
        characters = new LinkedList<Character>();
        for (int i = 0; i < 256; i++) {
            characters.addLast((char) i);
        }
    }

    /**
     * Searches for a particular char within the linked list and return
     * the position in which it has been found.
     * @param  charIn: the char to search for.
     * @return an integer with the position of the char.
     */
    private static int getIndex(char charIn) {
        int index = 0;

        for (char c : characters) {
            if (c == charIn) {
                return index;
            }
            index++;
        }
        throw new NoSuchElementException();
    }

    /**
     * Moves char from a given index to the front of list. That is, the
     * item at position index is removed and reinserted in the front of
     * the linked list.
     * @param c: the char to reinsert at the front of the list.
     * @param index: the index whose element will be erased.
     */
    private static void moveToFront(char c, int index) {
        characters.remove(index);
        characters.addFirst(c);
    }

    /**
     * Reads buffer from stdin as binary data, 8 bits at a time, and then
     * encodes it, using "Move To Front" technique.
     */
    public static void encode() {
        char c;
        int index;

        initList();

        while (!BinaryStdIn.isEmpty()) {
            c = BinaryStdIn.readChar();
            
            /**
             * Get the current index of the char read and output it, as
             * binary info, to stdout, moving it to the front of the list
             * after that.
             */
            index = getIndex(c);
            BinaryStdOut.write(index, 8);
            moveToFront(c, index);
        }
        BinaryStdOut.close();
    }

    public static void decode() {
        int index;
        char c;

        initList();

        while (!BinaryStdIn.isEmpty()) {
            index = (int) BinaryStdIn.readInt(8);

            /**
             * Get the current index of the char read and output it, as
             * binary info, to stdout, moving it to the front of the list
             * after that.
             */
            c = characters.get(index);
            BinaryStdOut.write(c, 8);
            moveToFront(c, index);
        }
        BinaryStdOut.close();
    }

    /**
     * Sample client that calls encode() if the command-line
     * argument is "-" an decode() if it is "+".
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        if (args[0].equals("-")) {
            encode();
        }
        else if (args[0].equals("+")) {
            decode();
        }
        else {
            throw new IllegalArgumentException("Illegal command line argument");
        }
    }
}