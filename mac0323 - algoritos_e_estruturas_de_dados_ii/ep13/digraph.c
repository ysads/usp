/*
 * MAC0323 Algoritmos e Estruturas de Dados II
 * 
 * ADT Digraph implementada atrevés de vetor de listas de adjacência.
 * As listas de adjacência são bag de ints que são mais restritos 
 * que as bags genéricas do EP12. Veja a api bag.h e simplifique 
 * o EP12 de acordo. 
 *  
 * Busque inspiração em: 
 *
 *    https://algs4.cs.princeton.edu/42digraph/ (Graph representation)
 *    https://algs4.cs.princeton.edu/42digraph/Digraph.java.html
 * 
 * DIGRAPH
 *
 * Digraph representa um grafo orientado de vértices inteiros de 0 a V-1. 
 * 
 * As principais operações são: add() que insere um arco no digrafo, e
 * adj() que itera sobre todos os vértices adjacentes a um dado vértice.
 * 
 * Arcos paralelos e laços são permitidos.
 * 
 * Esta implementação usa uma representação de _vetor de listas de adjacência_,
 * que é uma vetor de objetos Bag indexado por vértices. 

 * ATENÇÃO: Por simplicidade esses Bag podem ser int's e não de Integer's.
 *
 * Todas as operações consomen no pior caso tempo constante, exceto
 * iterar sobre os vértices adjacentes a um determinado vértice, cujo 
 * consumo de tempo é proporcional ao número de tais vértices.
 * 
 * Para documentação adicional, ver 
 * https://algs4.cs.princeton.edu/42digraph, Seção 4.2 de
 * Algorithms, 4th Edition por Robert Sedgewick e Kevin Wayne.
 *
 */

/* interface para o uso da funcao deste módulo */
#include "digraph.h"


#include "bag.h"     /* add() e itens() */
#include <stdio.h>   /* fopen(), fclose(), fscanf(), ... */
#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc(), ecalloc() */

#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debuging */
#endif

/*----------------------------------------------------------*/
/* 
 * Estrutura básica de um Digraph
 * 
 * Implementação com vetor de listas de adjacência.
 */
struct digraph {
    Bag *adj;
    int *inDegree;
    int numVertex;
    int numEdge;
    vertex iterator;
};

/*------------------------------------------------------------*/
/* 
 * Protótipos de funções administrativas: tem modificador 'static'
 * 
 */

/*-----------------------------------------------------------*/
/*
 *  newDigraph(V)
 *
 *  RECEBE um inteiro V.
 *  RETORNA um digrafo com V vértices e 0 arcos.
 * 
 */
Digraph
newDigraph(int V)
{
    Digraph graph;
    vertex i;

    graph = (Digraph) emalloc(sizeof(struct digraph));

    /**
     * Inicializa o vetor que armazena o grau de entrada de
     * cada um dos vértices
     */
    graph->inDegree = (int*) ecalloc(sizeof(int), V);

    /**
     * Initializa a lista de adjacências de cada um dos
     * vértices
     */
    graph->adj = (Bag*) ecalloc(sizeof(Bag), V);
    for (i = 0; i < V; i++) {
        graph->adj[i] = newBag();
    }

    graph->numVertex = V;
    graph->numEdge   = 0;

    return graph;
}

/*-----------------------------------------------------------*/
/*
 *  cloneDigraph(G)
 *
 *  RECEBE um digrafo G.
 *  RETORNA um clone de G.
 * 
 */
Digraph
cloneDigraph(Digraph G)
{
    Digraph clone; 
    vertex v, w;
    int nVertex;

    if (G == NULL) {
        ERROR("[cloneDigraph] arg G cant be NULL");
        return NULL;
    }

    nVertex = vDigraph(G);
    clone = newDigraph(nVertex);

    /**
     * Itera por cada um dos vértices de G, de modo a percorrer
     * suas listas de adjacência, clonando assim suas arestas.
     */
    for (v = 0; v < nVertex; v++) {
        w = adj(G, v, TRUE);

        while (w != -1) {
            addEdge(clone, v, w);
            w = adj(G, v, FALSE);
        }
    }
    return clone;
}

/*-----------------------------------------------------------*/
/*
 *  reverseDigraph(G)
 *
 *  RECEBE um digrafo G.
 *  RETORNA o digrafo R que é o reverso de G: 
 *
 *      v-w é arco de G <=> w-v é arco de R.
 * 
 */
Digraph
reverseDigraph(Digraph G)
{
    Digraph R;
    vertex v, w;
    int nVertex;

    if (G == NULL) {
        ERROR("[reverseDigraph] arg G cant be NULL");
        return NULL;
    }

    nVertex = vDigraph(G);
    R = newDigraph(nVertex);

    /**
     * Itera por cada um dos vértices de G, de modo a percorrer
     * suas listas de adjacência, inserido arestas do tipo w->v
     * em R.
     */
    for (v = 0; v < nVertex; v++) {
        /**
         * Inicia a iteração pela lista de adjacências de v.
         * O arg TRUE aqui força a iteração a começar pelo
         * primeiro item da lista.
         */
        w = adj(G, v, TRUE);

        while (w != -1) {
            addEdge(R, w, v);
            w = adj(G, v, FALSE);
        }
    }
    return R;
}

/*-----------------------------------------------------------*/
/*
 *  readDigraph(NOMEARQ)
 *
 *  RECEBE uma stringa NOMEARQ.
 *  RETORNA o digrafo cuja representação está no arquivo de nome NOMEARQ.
 *  O arquivo contém o número de vértices V, seguido pelo número de arestas E,
 *  seguidos de E pares de vértices, com cada entrada separada por espaços.
 *
 *  Veja os arquivos  tinyDG.txt, mediumDG.txt e largeDG.txt na página do 
 *  EP e que foram copiados do algs4, 
 * 
 */
Digraph
readDigraph(String nomeArq)
{
    FILE *file;
    Digraph graph;

    int nVertex, nEdge;
    int v, w, e;

    if (nomeArq == NULL) {
        ERROR("[readDigraph] nomeArq cant be NULL");
    }

    /**
     * Abre o arquivo com a descrição do digrafo e lê dele o número
     * de vértices e arestas.
     */
    file = fopen(nomeArq, "r");
    fscanf(file, "%d %d", &nVertex, &nEdge);

    /**
     * Aloca um novo grafo com o número de vértices lido do arquivo.
     */
    graph = newDigraph(nVertex);

    /**
     * Lê todas as arestas do digrafo, inserindo-as nele.
     */
    for (e = 0; e < nEdge; e++) {
        fscanf(file, "%d %d", &v, &w);
        addEdge(graph, v, w);
    }
    return graph;
}


/*-----------------------------------------------------------*/
/*
 *  freeDigraph(G)
 *
 *  RECEBE um digrafo G e retorna ao sistema toda a memória 
 *  usada por G.
 *
 */
void  
freeDigraph(Digraph G)
{
    vertex v;

    if (G == NULL) {
        ERROR("[freeDigraph] arg G cant be NULL");
        return;
    }

    /**
     * Desaloca, uma a uma, todas as listas de adjacências contidas
     * no digrafo G.
     */
    for (v = 0; v < vDigraph(G); v++) {
        freeBag(G->adj[v]);
    }

    /**
     * Por fim, desaloca os atributos internos de G.
     */
    free(G->inDegree);
    free(G->adj);
    free(G);
}

/*------------------------------------------------------------*/
/*
 * OPERAÇÕES USUAIS: 
 *
 *     - vDigraph(), eDigraph(): número de vértices e arcos
 *     - addEdge(): insere um arco
 *     - adj(): itera sobre os vizinhos de um dado vértice
 *     - outDegree(), inDegree(): grau de saída e de entrada
 *     - toString(): usada para exibir o digrafo 
 */

/*-----------------------------------------------------------*/
/* 
 *  VDIGRAPH(G)
 *
 *  RECEBE um digrafo G e RETORNA seu número de vertices.
 *
 */
int
vDigraph(Digraph G)
{
    if (G == NULL) {
        return 0;
    }
    return G->numVertex;
}

/*-----------------------------------------------------------*/
/* 
 *  EDIGRAPH(G)
 *
 *  RECEBE um digrafo G e RETORNA seu número de arcos (edges).
 *
 */
int
eDigraph(Digraph G)
{
    if (G == NULL) {
        return 0;
    }
    return G->numEdge;
}

/*-----------------------------------------------------------*/
/*
 *  addEdge(G, V, W)
 * 
 *  RECEBE um digrafo G e vértice V e W e INSERE o arco V-W  
 *  em G.
 *
 */
void  
addEdge(Digraph G, vertex v, vertex w)
{
    if (G == NULL) {
        ERROR("[addEdge] arg G cannot be NULL");
        return;
    }
    
    /**
     * Adiciona w à lista de adjacências de v.
     */
    add(G->adj[v], w);

    /**
     * Incrementa o número de arestas no digrafo e o grau de 
     * entrada do vértice w.
     */
    G->inDegree[w]++;
    G->numEdge++;
}    


/*-----------------------------------------------------------*/
/* 
 *  ADJ(G, V, INIT)
 * 
 *  RECEBE um digrafo G, um vértice v de G e um Bool INIT.
 *
 *  Se INIT é TRUE,  ADJ() RETORNA o primeiro vértice na lista de adjacência de V.
 *  Se INIT é FALSE, ADJ() RETORNA o sucessor na lista de adjacência de V do 
 *                   último vértice retornado.
 *  Se a lista de adjacência de V é vazia ou não há sucessor do último vértice 
 *  retornada, ADJ() RETORNA -1.
 *
 *  Se entre duas chamadas de ADJ() a lista de adjacência de V é alterada, 
 *  o comportamento é  indefinido. 
 *  
 */
int 
adj(Digraph G, vertex v, Bool init)
{
    if (G == NULL) {
        ERROR("[adj] arg G cant be NULL");
    }
    else if (v > vDigraph(G)) {
        ERROR("[adj] arg v is not in G");
    }

    return itens(G->adj[v], init);
}

/*-----------------------------------------------------------*/
/*
 *  outDegree(G, V)
 * 
 *  RECEBE um digrafo G e vértice V.
 *  RETORNA o número de arcos saindo de V.
 *
 */
int
outDegree(Digraph G, vertex v)
{
    if (G == NULL) {
        ERROR("[outDegree] arg G cant be NULL");
    }
    else if (v > vDigraph(G)) {
        ERROR("[outDegree] arg v is not in G");
    }

    return size(G->adj[v]);
}

/*-----------------------------------------------------------*/
/*
 *  inDegree(G, V)
 * 
 *  RECEBE um digrafo G e vértice V.
 *  RETORNA o número de arcos entrando em V.
 *
 */
int
inDegree(Digraph G, vertex v)
{
    if (G == NULL) {
        ERROR("[inDegree] arg G cant be NULL");
        return -1;
    }
    else if (v > vDigraph(G)) {
        ERROR("[outDegree] arg is not in G");
    }

    return G->inDegree[v];
}


/*-----------------------------------------------------------*/
/*
 *  toString(G)
 * 
 *  RECEBE um digrafo G.
 *  RETORNA uma string que representa G. Essa string será usada
 *  para exibir o digrafo: printf("%s", toString(G)); 
 *    
 *  Sugestão: para fazer esta função inspire-se no método 
 *  toString() da classe Digraph do algs4.
 */
String
toString(Digraph G)
{
    String strGraph;
    String strHead;
    String strAux;
    vertex v, w;

    int graphSize, vertexAdjSize;

    /**
     * Descobre o tamanho do grafo, antes de, de fato, imprimi-lo
     * nas strings. Isso é necessário para saber o tamanho da string
     * a ser alocada para conter a representação do grafo.
     */
    graphSize = sizeof(int) * 2 + 10;
    for (v = 0; v < vDigraph(G); v++) {
        /**
         * O tamanho a ser usado para imprimir essa lista de adjacências
         * é o número de itens nela contidos, mais o número de espaços entre
         * esses itens - incluindo, também, o caracter newline ao final.
         */
        vertexAdjSize = sizeof(int) * size(G->adj[v]) + size(G->adj[v]);
        graphSize    += vertexAdjSize;
    }

    /**
     * Aloca uma string com o tamanho total necessário para armazenar o grafo.
     */
    strGraph = (String) emalloc(graphSize);
        
    /**
     * Imprime o cabeçalho do grafo, isto é, quantos vértices e quantas arestas
     * ele possui.
     */
    strHead = (String) emalloc(graphSize);
    sprintf(strHead, "v: %d, e: %d\n", vDigraph(G), eDigraph(G));
    strcat(strGraph, strHead);

    /**
     * Em seguida, itera por cada vértice, adicionando, à string que representa
     * o grafo inteiro, o número associado ao vértice em análise.
     */
    for (v = 0; v < vDigraph(G); v++) {
        vertexAdjSize = sizeof(int) * size(G->adj[v]) + size(G->adj[v]);
        strAux = (String) emalloc(vertexAdjSize);

        sprintf(strAux, "%d: ", v);
        strcat(strGraph, strAux);

        /**
         * Por fim, itera por sobre todos os elementos na lista de adjacências
         * do vértice v, de modo a incluir na string final os números associados
         * aos vértices aos quais v está conectado.
         */
        w = adj(G, v, TRUE);
        
        while (w != -1) {
            sprintf(strAux, "%d ", w);
            strcat(strGraph, strAux);

            /**
             * Busca o próximo vértice na lista de adjacências de v.
             */
            w = adj(G, v, FALSE);
        }
        strcat(strGraph, "\n");
    }
    return strGraph;
}

/*------------------------------------------------------------*/
/* 
 * Implementaçao de funções administrativas: têm o modificador 
 * static.
 */

