/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para esse EP foram discutidas, com alguns colegas, boas referências
 *    acerca do objeto de trabalho: permutações em ordem lexicográfica.
 *    Isso foi importante para ventilar ideias sobre como gerar tais
 *    permutações de maneira eficiente sem o uso outras estruturas de
 *    dados auxiliares potencialmente custosas - tanto em termos de
 *    processamento quanto em termos de espaço. Algumas das referências
 *    consultadas foram:
 *    
 *      - https://en.wikipedia.org/wiki/Lexicographical_order
 *        Referência sobre o conceito de ordem lexicográfica
 *
 *      - https://www.nayuki.io/page/next-lexicographical-permutation-algorithm
 *        Referência sobre a geração de sequências lexicograficamente ordenadas
 *        usando baixa complexidade de espaço
 *
 *      - https://www.devmedia.com.br/padrao-de-projeto-iterator-em-java/26733
 *        Referência sobre o padrão de projeto Iterator em Java
 *
 *  Bugs & Limitações:
 *    Um detalhe importante que poderia melhorar um pouco a velocidade de execução
 *    do algoritmo seria fazer a ordenação da string base diretamente na classe
 *    Arrangements, uma única vez. Desse modo, sempre que um novo ArrangementsIterator
 *    fosse instanciado, tal objeto já receberia a primeira permutação pronta. No
 *    entanto isso não chega a ser um problema crítico, já que o tempo de ordenação
 *    de uma string de tamanho N usando heap sort tem complexidade O(log N).
 *    
 *    Ressalte-se que tal abordagem -- usando pré-ordenação -- foi desconsiderada
 *    para não alterar demais o esqueleto do código. Além disso, a string original
 *    base precisaria ser alterada para que isso fosse possível sem dobrar o consumo
 *    de memória, o que poderia ser um problema em termos de complexidade de espaço,
 *    ou ainda poderia inviabilizar outras hipotéticas operações na classe Arrangements.
 */

import java.lang.IllegalArgumentException;
import java.lang.UnsupportedOperationException;

import java.util.NoSuchElementException;
import java.util.Arrays;
import java.util.Iterator;

import edu.princeton.cs.algs4.StdOut;


public class Arrangements implements Iterable<String> {

    
    /**
     * An attribute to hold the base string passed to class
     * at constructor and that will be used to generate the
     * iterators
     */
    private String base;


    /**
     * The public class constructor. Just throws an exception
     * in case its string argument is invalid 
     */
    public Arrangements(String s) {
        if (s == null || s.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.base = s;
    }


    /**
     * This method simply creates a new ArrangementsIterator object
     */
    public Iterator<String> iterator() {
        return new ArrangementsIterator(this.base);
    }


    /**
     * A private subclass used to implement the iterator's logic
     */
    private class ArrangementsIterator implements Iterator<String> {

        /**
         * Holds the current permutation on the iterator, which
         * it's used as a base to generate the next one 
         */
        private char[] currPerm;


        /**
         * Says whether a the currPem has passed through any
         * sorting process or not. This is used to know if
         * the first permutation has already been requested
         */
        private boolean beenSorted;


        /**
         * Constructor of the subclass
         */
        public ArrangementsIterator(String str) {
            this.currPerm = str.toCharArray();
            this.beenSorted = false;
        }


        /**
         * We won't implement this method for this moment
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }


        /**
         * To discover whether we have a next permutation
         * or not we'll go through a naive logic: checking
         * if all string positions are strictly increasing 
         */
        public boolean hasNext() {
            int i = 0;

            /**
             * This is necessary to prevent iterations whose
             * base string is already sorted - aaa, 321, etc -
             * from not being recognized as possible to be
             * returned by method next()
             */
            if (!this.beenSorted) {
                return true;
            }

            while (i < currPerm.length - 1) {
                if (currPerm[i] < currPerm[i+1]) {
                    return true;
                }
                i++;
            }
            return false;
        }


        /**
         * This method generates the next permutation on-demand, which
         * reduces its space complexity, since we don't store large
         * amounts of data
         */
        public String next() {
            int i, p, n, k;

            /**
             * The first item of the iterator is simply the currPerm string
             * sorted. So we sort currPerm and mark the according attribute
             * so we don't re-sort the string later 
             */
            if (!this.beenSorted) {
                sortCurrPerm();
                return new String(currPerm);
            }

            n = currPerm.length;
            i = n - 2;

            /**
             * Here we find the rightmost element whose right neighbour
             * is bigger that it, that is, the biggest decreasing sequence
             */
            while (i >= 0 && currPerm[i+1] <= currPerm[i]) {
                i--;
            }

            /**
             * If the first index is reach, then the whole string is
             * ordered and there is not another permutation to generate.
             * Throws an exception then
             */
            if (i == -1) {
                throw new NoSuchElementException();
            }

            p = n - 1;
            while (p >= i && currPerm[i] >= currPerm[p]) {
                p--;
            }

            /**
             * We must swap the two items so that we can readjust the
             * remaining items between i and n
             */
            swap(i, p);

            /**
             * We now revert the final positions of the string to generate
             * the next permutation. This is like trying to find the next
             * available suffix for a fixed prefix -- that is, maintaining
             * positions 0..i fixed 
             */
            k = 0;
            while ((i + 1 + k) <= (n - 1 - k)) {
                swap(i + 1 + k, n - 1 - k);
                k++;
            }

            return new String(currPerm);
        }


        /**
         * This sort method creates our first permutation. It uses the
         * classic sorting algorithm known as heap sort
         */
        private void sortCurrPerm() {
            int length = currPerm.length;

            /**
             * Builds a max-heap structure, so we have all parent nodes
             * greater than its chidren, and also the largest one at 0
             */
            buildMaxHeap();

            while (length > 0) {
                /**
                 * Swaps the first and the last item in the heap. Now
                 * we have the biggest element in the end of the string
                 */
                swap(0, length - 1);

                /**
                 * Reduces the length, so we can take our just-sorted
                 * item out of our heap 
                 */
                length--;

                /**
                 * Then, heapifies the rest of our now messed heap.
                 * This is needed because it's very unlikely that,
                 * after swap, the new first item is in its right index
                 */
                heapify(length, 0);
            }

            /**
             * Mark the current permutation as been-sorted, which means
             * it has passed through the sorting process
             */
            this.beenSorted = true;
        }


        private void swap(int pos1, int pos2) {
            char aux = this.currPerm[pos2];
            this.currPerm[pos2] = this.currPerm[pos1];
            this.currPerm[pos1] = aux;
        }


        private void heapify(int length, int pos) {
            int index, lftSon, rgtSon;

            /**
             * Keeps iterating until the end of the currPerm, fixing
             * troubled subtrees
             */
            while (pos < length) {
                index = pos;

                lftSon = 2 * index + 1;
                rgtSon = lftSon + 1;

                /**
                 * Left son is bigger than its parent? If so, save the
                 * son's index so we can swap it with its parent later
                 */
                if (lftSon < length && currPerm[lftSon] > currPerm[index]) {
                    index = lftSon;
                }

                /**
                 * Right son is bigger than its parent? If so, save the
                 * son's index so we can swap it with its parent later
                 */
                if (rgtSon < length && currPerm[rgtSon] > currPerm[index]) {
                    index = rgtSon;
                }

                /**
                 * We don't have to do anything, since the siblings are actually
                 * smaller than its parents. Just end the functions execution
                 */
                if (index == pos) {
                    return;
                }

                /**
                 * Otherwise, we have found our siblings to be bigger 
                 * than its parents, which means they should be swaped
                 */
                swap(pos, index);
                
                pos = index;
            }
        }


        private void buildMaxHeap() {
            int i = currPerm.length / 2 - 1;

            /**
             * We start from the middle of the string because this is sufficient
             * to sort the whole string
             */
            while (i >= 0) {
                heapify(currPerm.length, i);
                i--;
            }
        }
    }


    /**
     * Unit tests
     */
    public static void main(String[] args) {
        String s = (args.length == 0) ? "" : args[0];

        Arrangements arr = new Arrangements(s);
        
        StdOut.println("Teste 1: imprime no máximo os 10 primeiros arranjos");
        Iterator<String> it = arr.iterator();
        for (int i = 0; it.hasNext() && i < 10; i++) {
            StdOut.println(i + " : " + it.next());
        }
        
        StdOut.println("Teste 2: imprime todos os arranjos");
        int i = 0;
        for (String arranjo: arr) {
            StdOut.println(i + " : " + arranjo);
            i++;
        }
    }
}
