/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa. Por basear-se no conceito de listas
 *    ligadas, visto em semestres anteriores, a solução pode ser
 *    construída sem grandes dúvidas ou inseguranças.
 *
 *  Bugs & Limitações:
 *    Uma possível limitação encontrada dá-se pelo uso do tipo de
 *    dados `int` para armazenar o número de itens no Deque. Como
 *    tal tipo de dados fazia parte da API exigida, ele não foi
 *    substituído por um outro tipo, maior, capaz de armazenar
 *    grandes números. Sendo assim, podem ser armazenados 2^31
 *    elementos no Deque antes de haver overflow em `length`.
 *    No entanto, por ser um valor relativamente alto, isso não
 *    configura-se mais como uma limitação teórica do que prática. 
 */


import edu.princeton.cs.algs4.StdOut;

import java.lang.IllegalArgumentException;
import java.lang.UnsupportedOperationException;

import java.util.NoSuchElementException;
import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

    /**
     * Pointer to Deque's first item
     */
    private DequeNode head;

    /**
     * Pointer to Deque's last item
     */
    private DequeNode tail;

    /**
     * The current size of the structure
     */
    private int length;

    /**
     * Initializes an empty Deque
     */
    public Deque() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    /**
     * Is the Deque empty?
     */
    public boolean isEmpty() {
        return this.length == 0;
    }

    /**
     * Return the number of items on the Deque
     */
    public int size() {
        return this.length;
    }

    /**
     * Add an item to structure's front
     */
    public void addFirst(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }

        /**
         * Instantiates a new object whose `next` pointer
         * points to the structure current head
         */
        @SuppressWarnings("unchecked")
        DequeNode nextHead = new DequeNode(null, this.head, (Item) item);

        /**
         * A null head means this is the first item. That is,
         * we should also update the structure's tail
         */
        if (this.head == null) {
            this.tail = nextHead;
        }

        /**
         * Updates the structure head, so that it points to the
         * newly-instantiated object
         */
        this.head = nextHead;
        this.length++;
    }

    /**
     * Add an item to structure's back
     */
    public void addLast(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }

        /**
         * Instantiates a new object whose `prev` pointer 
         * points to the structure current tail
         */
        @SuppressWarnings("unchecked")
        DequeNode nextTail = new DequeNode(this.tail, null, item);

        /**
         * A null tail means this is the first item. That is,
         * we should also update the structure's head
         */
        if (this.tail == null) {
            this.head = nextTail;
        }
        
        /**
         * Updates the structure tail, so that it points to the
         * newly-instantiated object
         */
        this.tail = nextTail;
        this.length++;
    }

    /**
     * Remove and return the item at structure's front
     */
    public Item removeFirst() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }
        DequeNode staged = this.head;

        /**
         * This item is staged to be destroyed, which
         * means we should update its neighbours pointers
         * so they no more refer to `staged`
         */
        staged.linkNeighbours();

        @SuppressWarnings("unchecked")
        Item item = (Item) staged.getItem();

        this.head = staged.getNext();
        this.length--;

        /**
         * If the structure gets empty after removing
         * item, we should also update the tail pointer
         */
        if (this.isEmpty()) {
            this.tail = null;
        }
        return item;
    }

    /**
     * Remove and return the item at structure's back
     */
    public Item removeLast() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }
        DequeNode staged = this.tail;

        /**
         * This item is staged to be destroyed, which
         * means we should update its neighbours pointers
         * so they no more refer to `staged`
         */
        staged.linkNeighbours();

        @SuppressWarnings("unchecked")
        Item item = (Item) staged.getItem();

        this.tail = staged.getPrev();
        this.length--;

        /**
         * If the structure gets empty after removing
         * item, we should also update the head pointer
         */
        if (this.isEmpty()) {
            this.head = null;
        }

        return item;
    }

    /**
     * Return an iterator over items ordered from front to back
     */
    public Iterator<Item> iterator() {
        return new DequeIterator(this.head);
    }

    /**
     * Unit testing to validate class's public API
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int n = 14;
        Deque deque = new Deque();

        StdOut.println("\nDeque empty? " + deque.isEmpty());
        StdOut.println("Deque size: " + deque.size());

        StdOut.println("\nAdd [0-" + n / 2 + ") to deque's back");
        for (int i = 0; i < n / 2; i++) {
            deque.addLast(i);
        }

        StdOut.println("\nIterating test: ");
        for (Object item : deque) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nDeque empty? " + deque.isEmpty());
        StdOut.println("Deque size: " + deque.size());

        StdOut.println("\nRemoving from front: ");
        StdOut.println("- front: " + deque.removeFirst());
        StdOut.println("- front: " + deque.removeFirst());
        StdOut.println("- front: " + deque.removeFirst());


        StdOut.println("\nDeque now: ");
        for (Object item : deque) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nAdd [" + n / 2 +  "-" + n + ") to deque's front");
        for (int i = (n / 2); i < n; i++) {
            deque.addFirst(i);
        }

        StdOut.println("\nDeque now: ");
        for (Object item : deque) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nDeque empty? " + deque.isEmpty());
        StdOut.println("Deque size: " + deque.size());

        StdOut.println("\nRemoving from back: ");
        StdOut.println("- back: " + deque.removeLast());
        StdOut.println("- back: " + deque.removeLast());
        StdOut.println("- back: " + deque.removeLast());
       
        StdOut.println("\nFinal deque: ");
        for (Object item : deque) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nDeque empty? " + deque.isEmpty());
        StdOut.println("Deque size: " + deque.size());
    }

    /**
     * Private class to represent a Deque's Unit, that is, a single
     * information that can be stored under a Deque structure. It's
     * implemented as a double linked list, which means each node
     * has a pointer to both its left and right neighbours
     */
    private class DequeNode<Item> {
        
        private DequeNode prev = null;
        private DequeNode next = null;
        private Item item = null;

        public DequeNode() {
        }

        public DequeNode(DequeNode prev, DequeNode next, Item item) {
            this.prev = prev;
            this.next = next;
            this.item = item;

            this.updateNeighbours();
        }

        /**
         * Setter methods
         */
        public void setItem(Item item) {
            this.item = item;
        }

        public void setPrev(DequeNode prev) {
            this.prev = prev;
        }

        public void setNext(DequeNode next) {
            this.next = next;
        }

        /**
         * Getter methods
         */
        public Item getItem() {
            return this.item;
        }

        public DequeNode getPrev() {
            return this.prev;
        }

        public DequeNode getNext() {
            return this.next;
        }

        /**
         * This method allows a bypass around this particular node.
         * It connect's the previous element with next one and vice
         * versa, so that the get directly connected
         */
        public void linkNeighbours() {
            if (this.prev != null) {
                this.prev.setNext(this.next);
            }
            if (this.next != null) {
                this.next.setPrev(this.prev);
            }
        }

        /**
         * If the node has neighbours at either its left or right
         * we should update them so that they get aware this particular
         * node exists
         */
        private void updateNeighbours() {
            if (this.next != null) {
                this.next.setPrev(this);
            }

            if (this.prev != null) {
                this.prev.setNext(this);
            }
        }
    }

    /**
     * The iterator class used to run through the Deque contents
     */
    private class DequeIterator implements Iterator<Item> {

        /**
         * Pointer to Deque's item current under iteration.
         * Initially points to Deque's head
         */
        DequeNode current;

        public DequeIterator(DequeNode head) {
            this.current = head;
        }

        public boolean hasNext() {
            return this.current != null;
        }

        public Item next() {
            /**
             * Throws an exception if there are no more items
             * to iterate through
             */
            if (!hasNext()) {
                throw new java.util.NoSuchElementException();
            }

            /**
             * Save the current item so it can be returned later.
             * Also moves the current pointer to the next item
             */
            @SuppressWarnings("unchecked")
            Item item = (Item) current.getItem();
            this.current = current.getNext();
            
            return item;
        }

        /**
         * We won't implement this method for this moment
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}