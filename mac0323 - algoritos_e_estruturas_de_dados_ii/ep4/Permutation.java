/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa. Por basear-se no conceito de listas
 *    ligadas, visto em semestres anteriores, a solução pode ser
 *    construída sem grandes dúvidas ou inseguranças.
 *
 *  Bugs & Limitações:
 *    Não foram encontradas limitações relevantes para o funcionamento da
 *    classe `Permutation`.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;

import java.util.Iterator;

public class Permutation {

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int n = args.length;
        int k = Integer.parseInt(args[0]);

        String input;
        RandomizedQueue queue = new RandomizedQueue();

        while (!StdIn.isEmpty()) {
            input = StdIn.readString();
            queue.enqueue(input);
        }

        Iterator<String> it = queue.iterator();
        for (int i = 0; i < k && it.hasNext(); i++) {
            StdOut.println(it.next());
        }
    }
}