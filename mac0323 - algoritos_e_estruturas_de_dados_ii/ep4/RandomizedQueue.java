/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    Para a confecção deste EP, não foi necessária a consulta a
 *    nenhuma fonte externa. Por basear-se no conceito de listas
 *    ligadas, visto em semestres anteriores, a solução pode ser
 *    construída sem grandes dúvidas ou inseguranças.
 *
 *  Bugs & Limitações:
 *    Uma limitação da solução aqui apresentada é o tempo gasto pelas
 *    funções `enlargeCollection` e `reduceCollection` para redimensionar
 *    o array usado internamente para armazenar os dados da estrutura.
 *    Para estruturas de grande porte, o tempo de cópia dos itens para o
 *    novo array pode ser sensivelmente maior, o que não reduz o mérito
 *    dos métodos `enqueue` e `dequeue` terem tempo amortizado constante.
 */

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.lang.IllegalArgumentException;
import java.lang.UnsupportedOperationException;

import java.util.NoSuchElementException;
import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {

    /**
     * A constant describing the minimum size the internal
     * collection array should have
     */
    private final int MIN_SIZE = 2;

    /**
     * A internal array used to hold contents of the structure
     */
    private Object[] collection;

    /**
     * The current size of the structure
     */
    private int length;

    /**
     * The total capacity of the collection for the moment
     */
    private int capacity;

    /**
     * Builds an empty Randomized Queue
     */
    public RandomizedQueue() {
        this.collection = new Object[MIN_SIZE];
        this.length = 0;
        this.capacity = MIN_SIZE;
    }

    /**
     * Is the Randomized Queue empty?
     */
    public boolean isEmpty() {
        return this.length == 0;
    }

    /**
     * Return the number of items on the Randomized Queue
     */
    public int size() {
        return this.length;
    }

    /**
     * Add an item to the Randomized Queue
     */
    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException();
        }

        /**
         * If the structure is out of space, then we grow
         * its capacity 
         */
        if (this.length == this.capacity) {
            enlargeCollection(this.capacity + 1);
        }
        this.collection[this.length] = item;
        this.length++;
    }

    /**
     * Removes an item from Randomized Queue and returns it
     */
    public Item dequeue() {
        if (this.isEmpty()) {
            throw new NoSuchElementException();
        }

        int index = StdRandom.uniform(0, this.length);

        @SuppressWarnings("unchecked")
        Item item = (Item) this.collection[index];

        /**
         * Because a random item is removed, we can't let blank
         * spaces in the middle of the collection. Thus, we
         * substitute the removed item with the one in the last
         * position of the collection
         */
        this.collection[index] = this.collection[this.length - 1];
        this.collection[this.length - 1] = null;
        this.length--;

        /**
         * We always call this method to ensure we reduce space
         * consumption whenever we can 
         */
        reduceCollection();

        return item;
    }

    /**
     * Returns a random item at the structure
     */
    
    public Item sample() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        int index = StdRandom.uniform(0, this.length);

        @SuppressWarnings("unchecked")
        Item item = (Item) this.collection[index];
        
        return item;
    }

    /**
     * Return an independent iterator over items in random order
     */
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    /**
     * Unit testing to validate class's public API
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        int n = 14;
        RandomizedQueue queue = new RandomizedQueue();

        StdOut.println("\nQueue empty? " + queue.isEmpty());
        StdOut.println("Queue size: " + queue.size());

        StdOut.println("\nAdd [0-" + n / 2 + ") to queue");
        for (int i = 0; i < n / 2; i++) {
            queue.enqueue(i);
        }

        StdOut.println("\nIterating test: ");
        for (Object item : queue) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nQueue empty? " + queue.isEmpty());
        StdOut.println("Queue size: " + queue.size());

        StdOut.println("\nSome samples: ");
        for (int i = 0; i < 3; i++) {
            StdOut.println(queue.sample());
        }

        StdOut.println("\nDequeueing some itens: ");
        for (int i = 0; i < 3; i++) {
            StdOut.println(queue.dequeue());
        }

        StdOut.println("\nQueue now: ");
        for (Object item : queue) {
            StdOut.print(item + " ");
        }
        StdOut.println();

        StdOut.println("\nQueue empty? " + queue.isEmpty());
        StdOut.println("Queue size: " + queue.size());

        StdOut.println("\nMultiple iterators test: ");
        for (Object j : queue) {
            for (Object k : queue) {
                StdOut.print(j + " " + k + "\t");
            }
            StdOut.println();
        }
    }

    /**
     * Enlarges the internal collection used to hold elements.
     * Every time the collection is enlarged, its capacity doubles
     */
    private void enlargeCollection(int minCapacity) {
        int doubledCapacity = this.capacity << 1;

        /**
         * Copies the content of the current collection into another
         * bigger array, whose capacity equals to `doubledCapacity`
         */
        Object[] staging = new Object[doubledCapacity];
        for (int i = 0; i < this.length; i++) {
            staging[i] = this.collection[i];
        }

        /**
         * Updates the queue's internal capacity and collection
         */
        this.collection = staging;
        this.capacity = doubledCapacity;
    }

    /**
     * Reduces the collection array size if it has a lot of
     * empty spaces. This allows us to free some memory we're
     * not actually in need of
     */
    private void reduceCollection() {
        if (this.length <= this.capacity / 4 && this.capacity > MIN_SIZE) {
            
            int reducedCapacity = Math.max(this.capacity / 2, MIN_SIZE);

            /**
             * Copies the existing array contents into a smaller, more
             * economic array, whose capacity equals to `reducedCapacity`
             */
            Object[] staging = new Object[reducedCapacity];
            for (int i = 0; i < this.length; i++) {
                staging[i] = this.collection[i];
            }

            /**
             * Updates the queue's internal capacity and collection
             */
            this.collection = staging;
            this.capacity = reducedCapacity;
        }
    }

    /**
     * The iterator class used to run through the Randomized Queue contents
     */
    private class RandomizedQueueIterator implements Iterator<Item> {

        private boolean[] occurrences;
        private int thrownCount;
        
        public RandomizedQueueIterator() {
            this.occurrences = new boolean[length];
            this.thrownCount = 0;

            for (int i = 0; i < length; i++) {
                this.occurrences[i] = false;
            }
        }

        public boolean hasNext() {
            return this.thrownCount != length;
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            /**
             * Keep trying random indexes until we find one
             * that has not been thrown yet
             */
            int index;

            do {
                index = StdRandom.uniform(0, length);
            } while (this.occurrences[index]);

            this.occurrences[index] = true;
            this.thrownCount++;

            @SuppressWarnings("unchecked")
            Item item = (Item) collection[index];

            return item;
        }

        /**
         * We won't implement this method for this moment
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
