/**
 *  Nome: Ygor Sad Machado
 *  NUSP: 8910368

 *  Ao preencher esse cabeçalho com o meu nome e o meu número USP,
 *  declaro que todas as partes originais desse exercício programa
 *  (EP) foram desenvolvidas e implementadas por mim e que portanto
 *  não constituem desonestidade acadêmica ou plágio.
 *  Declaro também que sou responsável por todas as cópias desse
 *  programa e que não distribui ou facilitei a sua distribuição.
 *  Estou ciente que os casos de plágio e desonestidade acadêmica
 *  serão tratados segundo os critérios divulgados na página da 
 *  disciplina.
 *  Entendo que EPs sem assinatura devem receber nota zero e,
 *  ainda assim, poderão ser punidos por desonestidade acadêmica.
 *
 *  Fontes & Referências:
 *    A confecção desse EP necessitou, além de conhecimentos prévios na
 *    linguagem C, de consulta às seguintes APIs da biblioteca algs4:
 *
 *      - https://algs4.cs.princeton.edu/42digraph/
 *      - https://algs4.cs.princeton.edu/42digraph/DepthFirstOrder.java
 *      - https://algs4.cs.princeton.edu/42digraph/Topological.java
 *      - https://algs4.cs.princeton.edu/42digraph/DirectedCycle.java
 *
 *  Bugs & Limitações:
 *    Não foram encontradas limitações relevantes na implementação do arquivo
 *    topological.c. No entanto, é valido afirmar que, por depender das
 *    implementações dos arquivos bag.c e digraph.c, se tais bibliotecas não
 *    possuírem desempenho satisfatório, naturalmente, a presente biblioteca
 *    também não o possuirá.
 */

/*
 * MAC0323 Algoritmos e Estruturas de Dados II
 * 
 * ADT Topological é uma "representação topológica" de digrafo.
 * Esta implementação usa ADT Digraph do EP13.
 *  
 * Busque inspiração em: 
 *
 *   https://algs4.cs.princeton.edu/42digraph/
 *   https://algs4.cs.princeton.edu/42digraph/DepthFirstOrder.java
 *   https://algs4.cs.princeton.edu/42digraph/Topological.java
 *   https://algs4.cs.princeton.edu/42digraph/DirectedCycle.java
 * 
 * TOPOLOGICAL
 *
 * Topological é uma ¨representação topológica" de um dado digrafo.
 * 
 * As principais operações são: 
 *
 *      - hasCycle(): indica se o digrafo tem um ciclo (DirectedCycle.java)
 *      - isDag(): indica se o digrafo é acyclico (Topological.java)
 *
 *      - pre(): retorna a numeração pré-ordem de um vértice em relação a uma dfs 
 *               (DepthFirstOrder.java)
 *      - pos(): retorna a numareção pós-ordem de um vértice em relação a uma dfs
 *               (DepthFirstOrder.java)
 *      - rank(): retorna a numeração topológica de um vértice (Topological.java)
 * 
 *      - preorder(): itera sobre todos os vértices do digrafo em pré-ordem
 *                    (em relação a uma dfs, DepthFirstOrder.java)
 *      - postorder(): itera sobre todos os vértices do digrafo em pós-ordem
 *                    (em relação a uma dfs, ordenação topologica reversa, 
 *                     DepthFirstOrder.java)
 *      - order(): itera sobre todos os vértices do digrafo em ordem  
 *                 topologica (Topological.java)
 *      - cycle(): itera sobre os vértices de um ciclo (DirectedCycle.java)
 *
 * O construtor e "destrutor" da classe consomem tempo linear..
 *
 * Cada chama das demais operações consome tempo constante.
 *
 * O espaço gasto por esta ADT é proporcional ao número de vértices V do digrafo.
 * 
 * Para documentação adicional, ver 
 * https://algs4.cs.princeton.edu/42digraph, Seção 4.2 de
 * Algorithms, 4th Edition por Robert Sedgewick e Kevin Wayne.
 */

/* interface para o uso da funcao deste módulo */
#include "topological.h"

#include "digraph.h"
#include "bag.h"
#include "util.h"

#include <stdlib.h>
#undef DEBUG
#ifdef DEBUG
#include <stdio.h>
#endif

/**
 * ----------------------------------------------------------
 * 
 * Tipos de dados auxiliares
 * 
 */

/**
 * Uma enum que representa as estratégias para construir um
 * caminho baseado em estruturas de dados conhecidas.
 *
 * - QUEUE: a inserção é sempre feita ao final do array, começando
 *          a partir da posição 0.
 * - STACK: a inserção é feita na primeira posição livre do array,
 *          começando da posição mais à direita possível.
 */
enum path_type
{
    QUEUE,
    STACK
};
typedef enum path_type PathType;

/**
 * Uma struct cuja função é abstrair a ideia de um caminho,
 * de modo a representar tanto um caminho pré quanto um pós.
 */
struct path
{
    int length;
    int totalVertices;
    int* index;
    vertex iterator;
    vertex* vertices;
    PathType type;
};
typedef struct path* Path;


/*----------------------------------------------------------*/
/* 
 * Estrutura básica de um Topological
 * 
 */
struct topological 
{
    Digraph digraph;
    Path pre;
    Path pos;
    Path topological;
    Path cycle;
    vertex cycleSource;
};

/*------------------------------------------------------------*/
/* 
 * Protótipos de funções administrativas: tem modificador 'static'
 * 
 */
static Path
newPath(Digraph G, PathType type);

static void
freePath(Path path);

static void
visitVertices(Topological ts, Digraph G);

static void
visitVertex(Topological ts, Digraph G, Bool *visited, vertex v);

static void
addVertexToPath(Path path, vertex v);

static vertex
nextVertexOnPath(Path path, Bool init);

static int
firstVertexOnPath(Path path);

static void
findTopologicalOrder(Topological ts);

static void
findCycleOnVertex(Topological ts, Bool *onPath, Bool *visited, vertex *edgeTo, vertex v);

static void
fillCyclePath(Topological ts, vertex *edgeTo);

/*-----------------------------------------------------------*/
/*
 *  newTopologica(G)
 *
 *  RECEBE um digrafo G.
 *  RETORNA uma representação topológica de G.
 * 
 */
Topological
newTopological(Digraph G)
{
    Topological ts;

    /**
     * Aloca espaço para a estrutura de Topological e, em seguida,
     * associa a ela o digrafo G que ela representa.
     */
    ts = (Topological) emalloc(sizeof(struct topological));
    ts->digraph = G;
    ts->cycleSource = -1;

    /**
     * Aloca espaço para a estruturas que armazenam os caminhos pré
     * e pós ordem, e, em seguida, chama a função que visita todos os
     * vértices de G, de modo a preencher tais estruturas.
     */
    ts->pre = newPath(G, QUEUE);
    ts->pos = newPath(G, QUEUE);
    visitVertices(ts, G);

    /**
     * Redefine o array auxiliar de vértices visitados. Também aloca
     * espaço para armazenar a ordenação topológica, se ela existir,
     * 
     */
    ts->topological = newPath(G, STACK);
    ts->cycle = newPath(G, STACK);
    findTopologicalOrder(ts);

    return ts;
}

/*-----------------------------------------------------------*/
/*
 *  freeTopological(TS)
 *
 *  RECEBE uma representação topologica TS.
 *  DEVOLVE ao sistema toda a memória usada por TS.
 *
 */
void  
freeTopological(Topological ts)
{
    freePath(ts->pre);
    freePath(ts->pos);
    freePath(ts->topological);
    freePath(ts->cycle);
    free(ts);
}    

/*------------------------------------------------------------*/
/*
 *  OPERAÇÕES: 
 *
 */

/*-----------------------------------------------------------*/
/* 
 *  HASCYCLE(TS)
 *
 *  RECEBE uma representação topológica TS de um digrafo;
 *  RETORNA TRUE seu o digrafo possui um ciclo e FALSE em caso 
 *  contrário.
 *
 */
Bool
hasCycle(Topological ts)
{
    return (ts && ts->cycleSource != -1);
}

/*-----------------------------------------------------------*/
/* 
 *  ISDAG(TS)
 *
 *  RECEBE um representação topológica TS de um digrafo.
 *  RETORNA TRUE se o digrafo for um DAG e FALSE em caso 
 *  contrário.
 *
 */
Bool
isDag(Topological ts)
{
    return (ts && ts->cycleSource == -1);
}

/*-----------------------------------------------------------*/
/* 
 *  PRE(TS, V)
 *
 *  RECEBE uma representação topológica TS de um digrafo e um 
 *  vértice V.
 *  RETORNA a numeração pré-ordem de V em TS.
 *
 */
int
pre(Topological ts, vertex v)
{
    if (ts == NULL) {
        ERROR("[post] ts can't be NULL");
        return -1;
    }
    return ts->pre->index[v];
}

/*-----------------------------------------------------------*/
/* 
 *  POST(TS, V)
 *
 *  RECEBE uma representação topológica TS de um digrafo e um 
 *  vértice V.
 *  RETORNA a numeração pós-ordem de V em TS.
 *
 */
int
post(Topological ts, vertex v)
{
    if (ts == NULL) {
        ERROR("[post] ts can't be NULL");
        return -1;
    }
    return ts->pos->index[v];
}

/*-----------------------------------------------------------*/
/* 
 *  RANK(TS, V)
 *
 *  RECEBE uma representação topológica TS de um digrafo e um 
 *  vértice V.
 *  RETORNA a posição de V na ordenação topológica em TS;
 *  retorna -1 se o digrafo não for um DAG.
 *
 */
int
rank(Topological ts, vertex v)
{
    if (ts == NULL) {
        ERROR("[rank] ts can't be NULL");
        return -1;
    }

    /**
     * Só faz sentido retornar o ranking de um vértice se o digrafo
     * for um DAG. Caso contrário, há um ciclo, e portanto o digrafo
     * não aceita ordenação topológica.
     */
    if (isDag(ts)) {
        return ts->topological->index[v];
    }
    else {
        return -1;
    }
}

/*-----------------------------------------------------------*/
/* 
 *  PREORDER(TS, INIT)
 * 
 *  RECEBE uma representação topológica TS de um digrafo e um 
 *  Bool INIT.
 *
 *  Se INIT é TRUE,  PREORDER() RETORNA o primeiro vértice na ordenação pré-ordem de TS.
 *  Se INIT é FALSE, PREORDER() RETORNA o vértice sucessor do último vértice retornado
 *                   na ordenação pré-ordem de TS; se todos os vértices já foram retornados, 
 *                   a função retorna -1.
 */
vertex
preorder(Topological ts, Bool init)
{
    if (ts == NULL) {
        ERROR("[preorder] ts cant be NULL");
        return -1;
    }
    return nextVertexOnPath(ts->pre, init);
}

/*-----------------------------------------------------------*/
/* 
 *  POSTORDER(TS, INIT)
 * 
 *  RECEBE uma representação topológica TS de um digrafo e um 
 *  Bool INIT.
 *
 *  Se INIT é TRUE,  POSTORDER() RETORNA o primeiro vértice na ordenação pós-ordem de TS.
 *  Se INIT é FALSE, POSTORDER() RETORNA o vértice sucessor do último vértice retornado
 *                   na ordenação pós-ordem de TS; se todos os vértices já foram retornados, 
 *                   a função retorna -1.
 */
vertex
postorder(Topological ts, Bool init)
{
    if (ts == NULL) {
        ERROR("[posorder] ts cant be NULL");
        return -1;
    }
    return nextVertexOnPath(ts->pos, init);
}

/*-----------------------------------------------------------*/
/* 
 *  ORDER(TS, INIT)
 * 
 *  RECEBE uma representação topológica TS de um digrafo e um Bool INIT.
 *
 *  Se INIT é TRUE,  ORDER() RETORNA o primeiro vértice na ordenação topológica 
 *                   de TS.
 *  Se INIT é FALSE, ORDER() RETORNA o vértice sucessor do último vértice retornado
 *                   na ordenação topológica de TS; se todos os vértices já foram 
 *                   retornados, a função retorna -1.
 *
 *  Se o digrafo _não_ é um DAG, ORDER() RETORNA -1.
 */
vertex
order(Topological ts, Bool init)
{
    if (ts == NULL) {
        ERROR("[order] ts cant be NULL");
        return -1;
    }

    /**
     * Só faz sentido retornar uma ordenaçõa topológica do digrafo se
     * ele for um DAG. Caso contrário, ela não existe.
     */
    if (isDag(ts)) {
        return nextVertexOnPath(ts->topological, init);
    }
    else {
        return -1;
    }
}

/*-----------------------------------------------------------*/
/* 
 *  CYCLE(TS, INIT)
 * 
 *  RECEBE uma representação topológica TS de um digrafo e um Bool INIT.
 *
 *  Se INIT é TRUE,  CYCLE() RETORNA um vértice em um ciclo do digrafo.
 *  Se INIT é FALSE, CYCLE() RETORNA o vértice  no ciclo que é sucessor do 
 *                   último vértice retornado; se todos os vértices no ciclo já 
 *                   foram retornados, a função retorna -1.
 *
 *  Se o digrafo é um DAG, CYCLE() RETORNA -1.
 *
 */
vertex
cycle(Topological ts, Bool init)
{
    if (ts == NULL) {
        ERROR("[cycle] ts cant be NULL");
        return -1;
    }

    /**
     * Só faz sentido retornar um ciclo se houver um.
     */
    if (hasCycle(ts)) {
        return nextVertexOnPath(ts->cycle, init);
    }
    else {
        return -1;
    }
}


/*------------------------------------------------------------*/
/* 
 * Implementaçao de funções administrativas: têm o modificador 
 * static.
 */

/**
 * Aloca espaço para um novo path com o número de vertices do
 * Digraph G recebido como parâmetro. A forma como o caminho vai
 * ser construído/lido depende do parâmetro type.
 * 
 * @param G: uma referência para um Digraph.
 * @param type: o tipo do caminho em construção, podendo ser QUEUE ou STACK.
 * @return uma referência para um Path.
 */
static Path
newPath(Digraph G, PathType type)
{
    Path path;

    path = (Path) emalloc(sizeof(struct path));

    path->totalVertices = vDigraph(G);
    path->length = 0;
    path->index = (int*) ecalloc(vDigraph(G), sizeof(int));
    path->vertices = (vertex*) ecalloc(vDigraph(G), sizeof(vertex));
    path->iterator = -1;
    path->type = type;

    return path;
}

/**
 * Retorna ao sistema a memória ocupada pelo objeto Path cuja
 * referência é recebida via parâmetro.
 * 
 * @param path: o objeto a ser desalocado.
 */
static void
freePath(Path path)
{
    if (path != NULL) {
        free(path->index);
        free(path->vertices);
        free(path);
    }
}

/**
 * Visita todos os vértices de um dígrafo, preenchendo seus
 * caminhos pré e pós-ordem, contidos no objeto ts.
 *
 * @param ts: referência para Topological em que os caminhos serão montados.
 * @param G: referência para o Digraph cujos vértices serão visitados.
 */
static void
visitVertices(Topological ts, Digraph G)
{
    vertex v;
    Bool *visited;
    
    /**
     * Aloca um array para auxiliar na marcação dos vértices já visitados.
     */
    visited = (Bool*) ecalloc(vDigraph(ts->digraph), sizeof(Bool));

    /**
     * Itera por cada um dos vértices de G, de modo a percorrer
     * suas listas de adjacência.
     */
    for (v = 0; v < vDigraph(G); v++) {
        if (!visited[v]) {
            visitVertex(ts, G, visited, v);
        }   
    }

    /**
     * Libera a memória utilizada pelo array auxiliar
     */
    free(visited);
}

/**
 * Visita um dado vértice v, adicionando aos caminhos pré- e pós-ordem
 * contidos em ts. Além disso, visita, recursivamente, todos os vértices
 * na lista de adjacências de v.
 *
 * @param ts: referência para Topological que contém os caminhos.
 * @param G: referência para o Digraph que contém v.
 * @param v: vértice a ser visitado.
 */
static void
visitVertex(Topological ts, Digraph G, Bool *visited, vertex v)
{
    vertex w;

    /**
     * Marca o vértice como visitado e adiciona-o ao caminho pré-ordem
     * em ts.
     */
    visited[v] = TRUE;
    addVertexToPath(ts->pre, v);

    /**
     * Inicia a iteração pela lista de adjacências de v.
     * O arg TRUE aqui força a iteração a começar pelo
     * primeiro item da lista.
     */
    w = adj(G, v, TRUE);

    while (w != -1) {
        if (!visited[w]) {
            visitVertex(ts, G, visited, w);
        }
        w = adj(G, v, FALSE);
    }

    /**
     * Por fim, adiciona v ao caminho pós-ordem em ts.
     */
    addVertexToPath(ts->pos, v);
}

/**
 * Adiciona um vértice v a um caminho referenciado por path. Note que o
 * vértice vai ser adicionado ao caminho de acordo com seu atributo type.
 * 
 * @param ts: referência para o Path que armazena o caminho.
 * @param v: vértice a ser adicionado ao caminho.
 */
static void
addVertexToPath(Path path, vertex v)
{
    int position;

    /**
     * Para adicionar um vértice ao começo do caminho, é preciso subtrair um
     * do total de vértices esperado nesse caminho, já que os arrays são
     * indexados a partir do 0.
     */
    if (path->type == QUEUE) {
        position = path->length;
    }
    else {
        position = path->totalVertices - path->length - 1;
    }

    path->vertices[position] = v;
    path->index[v] = position;
    path->length++;
}

/**
 * Itera por um dado caminho recebido como parâmetro. O parâmetro init
 * determina se a iteração deve ser reiniciada.
 *
 * @param  path: referência para Path a ser percorrida.
 * @param  init: bool que, se for TRUE, indica que a iteração deve ser reiniciada.
 * @return o próximo vértice no caminho, ou -1 se não houver um próximo.
 */
static vertex
nextVertexOnPath(Path path, Bool init)
{
    int index;

     /**
     * Se init for TRUE, deve-se reiniciar o iterador interno do caminho,
     * de modo a recomeçar a sequência em seu primeiro vértice.
     */
    if (init) {
        path->iterator = firstVertexOnPath(path);
    }
    else {
        path->iterator++;
    }
    index = path->iterator;

    /**
     * Caso todos os vértices no caminho já tenham sido visitados, retorna -1.
     * Caso contrário, retorna o próximo vértice nele contido.
     */
    if (index == path->totalVertices) {
        return -1;
    }
    else {
        return path->vertices[index];
    }
}

/**
 * Retorna o índice do primeiro vértice na referência a Path. Essa função
 * é necessária porque em caminhos do tipo STACK, o preenchimento do array
 * de vértices é feito de trás para frente, e portanto o primeiro vértice
 * do caminho encontra-se no meio do array.
 * 
 * @param path: referência para o Path.
 * @return o índice do primeiro vértice se o caminho for do tipo STACK, ou 0 caso contrário.
 */
static int
firstVertexOnPath(Path path)
{
    if (path->type == STACK) {
        return path->totalVertices - path->length;
    }
    else {
        return 0;
    }
}

/**
 * Determina se um dado digrafo, contido em ts, possui ordernação topológica.
 * Para isso, itera por todos os vértices de v em busca de algum ciclo. Se
 * algum for localizado, o digrafo não possui ordenação topológica.
 * 
 * @param ts: referência para o Topological associado ao digrafo que se deseja estudar.
 */
static void
findTopologicalOrder(Topological ts)
{
    vertex v;
    vertex *edgeTo;
    Bool   *onPath;
    Bool   *visited;
    
    /**
     * Instancia dois arrays para auxiliar na detecção e posterior
     * armazenamento de um eventual ciclo.
     */
    edgeTo  = (vertex*) ecalloc(vDigraph(ts->digraph), sizeof(vertex));
    onPath  = (Bool*)   ecalloc(vDigraph(ts->digraph), sizeof(Bool));
    visited = (Bool*)   ecalloc(vDigraph(ts->digraph), sizeof(Bool));

    for (v = 0; v < vDigraph(ts->digraph); v++) {
        if (!visited[v] && !hasCycle(ts)) {
            findCycleOnVertex(ts, onPath, visited, edgeTo, v);
        }
    }

    fillCyclePath(ts, edgeTo);

    /**
     * Libera a memória utilizada pelos arrays auxiliares
     */
    free(edgeTo);
    free(onPath);
    free(visited);
}

/**
 * Analisa se um dado vértice fecha um ciclo dentro do digrafo representado por ts.
 * 
 * @param ts: referência para o Topological que contém o digrafo a analisar.
 * @param onPath: array de Bool que indica se um dado vértice está contido num caminho local.
 * @param visited: array de Bool que indica se um dado vértice já foi visitado.
 * @param edgeTo: array que representa as arestas percorridas até o vértice v.
 * @param v: o vertex a ser analisado.
 */
static void
findCycleOnVertex(Topological ts, Bool *onPath, Bool *visited, vertex *edgeTo, vertex v)
{
    vertex w;

    visited[v] = TRUE;
    onPath[v]      = TRUE;

    /**
     * Inicia a iteração pela lista de adjacências de v. O argumento
     * TRUE aqui força a iteração a começar pelo primeiro item da lista.
     */
    w = adj(ts->digraph, v, TRUE);

    while (w != -1) {
        /**
         * Na hipótese de um ciclo ter sido localizado, não há mais
         * o que fazer. Encerra a execução, portanto.
         */
        if (hasCycle(ts)) {
            return;
        }

        if (!visited[w]) {
            /**
             * Para cada vértice na lista de adjacências de v, caso ele
             * ainda não tenha sido visitado, é preciso estender a busca
             * do ciclo para ele.
             */
            edgeTo[w] = v;
            findCycleOnVertex(ts, onPath, visited, edgeTo, w);
        }
        else if (onPath[w]) {
            /**
             * Se um vértice for visitado duas vezes em um mesmo
             * caminho local, então um ciclo foi detectado e é
             * preciso salvar sua origem para posterior iteração.
             */
            ts->cycleSource = v;
            edgeTo[w] = v;

        }

        w = adj(ts->digraph, v, FALSE);
    }

    /**
     * Por fim, adiciona o vértice v ao caminho em ordem
     * topológica, e remove-o do caminho local.
     */
    addVertexToPath(ts->topological, v);
    onPath[v] = FALSE;
}

/**
 * Caso ts não represente um DAG, essa função constroi e armazena o ciclo encontrado
 * no digrafo, a partir do vértice cycleSource, que é um atributo de ts.
 * 
 * @param ts: referência para Topological que contém o digrafo.
 * @param edgeTo: array que representa as arestas percorridas durante o percurso do digrafo.
 */
static void
fillCyclePath(Topological ts, vertex *edgeTo)
{
    vertex v;

    /**
     * Se ts representar um DAG, então não há ciclo a ser construído. 
     */
    if (isDag(ts)) {
        return;
    }

    /**
     * Por fim, é preciso adicionar a raiz do ciclo ao caminho, já que ele
     * é usado como condição de parada do loop.
     */
    addVertexToPath(ts->cycle, ts->cycleSource);

    /**
     * A partir do vértice-raiz do ciclo, é preciso construi-lo de trás
     * para frente, preenchendo o caminho com os vértices contidos no
     * ciclo, obtidos a partir das arestas visitadas.
     */
    for (v = edgeTo[ts->cycleSource]; v != ts->cycleSource; v = edgeTo[v]) {
        addVertexToPath(ts->cycle, v);
    }
}
