/*
* MAC0323 Algoritmos e Estruturas de Dados II
* 
* ADT Bag implementada com lista ligada de itens. 
*  
*    https://algs4.cs.princeton.edu/13stacks/
*    https://www.ime.usp.br/~pf/estruturas-de-dados/aulas/bag.html
* 
* ATENÇÃO: por simplicidade Bag contém apenas inteiros (int) não 
* negativos (>=0) que são 'nomes' de vértices (vertex) de um 
* digrafo.
*/

/* interface para o uso da funcao deste módulo */
#include "bag.h"  

#include <stdlib.h>  /* free() */
#include <string.h>  /* memcpy() */
#include "util.h"    /* emalloc() */

#undef DEBUG
#ifdef DEBUG
#include <stdio.h>   /* printf(): para debuging */
#endif

/*----------------------------------------------------------*/
/* 
* Estrutura Básica da Bag
* 
* Implementação com listas ligada dos itens.
*/

typedef struct node_bag {
    struct node_bag *next;
    int item;
} NodeBag;


struct bag {
    NodeBag *first;
    NodeBag *iterator;
    int length;
};

/*------------------------------------------------------------*/
/* 
* Protótipos de funções administrativas: tem modificador 'static'
* 
*/
static NodeBag*
newNode(int item);

static void
freeNode(NodeBag *node);

/*-----------------------------------------------------------*/
/*
*  newBag()
*
*  RETORNA (referência/ponteiro para) uma bag vazia.
* 
*/
Bag
newBag()
{
    Bag bag = (Bag) emalloc(sizeof(struct bag));

    bag->first = NULL;
    bag->iterator = NULL;
    bag->length = 0;

    return bag;
}

/*-----------------------------------------------------------*/
/*
*  freeBag(BAG)
*
*  RECEBE uma Bag BAG e devolve ao sistema toda a memoria 
*  utilizada.
*
*/
void  
freeBag(Bag bag)
{
    NodeBag *node;
    NodeBag *next;

    node = bag->first;

    /**
     * Enquanto houver um próximo nó, desaloca-os, juntamente
     * com o seu conteúdo.
     */
    while (node != NULL) {
        next = node->next;
        freeNode(node);
        node = next;
    }
    free(bag);
}

/*------------------------------------------------------------*/
/*
* OPERAÇÕES USUAIS: add(), size(), isEmpty() e itens().
*/

/*-----------------------------------------------------------*/
/*
*  add(BAG, ITEM, NITEM)
* 
*  RECEBE uma bag BAG e um ITEM e insere o ITEM na BAG.
*  NITEM é o número de bytes de ITEM.
*
*  Para criar uma copia/clone de ITEM é usado o seu número de bytes NITEM.
*
*/
void  
add(Bag bag, vertex item)
{
    NodeBag *newFirst;

    /**
     * Algumas verificações de segurança para alertar no caso de algum
     * parâmetro ser NULL.
     */
    if (bag == NULL) {
        ERROR("[add] bag arg can't be NULL");
        return;
    }
    if (item < 0) {
        ERROR("[add] item arg can't be NULL");
        return;
    }

    /**
     * Atualiza os ponteiros da bag, de modo que o nó recém-alocado
     * se torne o novo primeiro nó da bag.
     */
    newFirst = newNode(item);
    newFirst->next = bag->first;

    bag->first = newFirst;
    bag->length++;
}    

/*-----------------------------------------------------------*/
/* 
*  SIZE(BAG)
*
*  RECEBE uma bag BAG
* 
*  RETORNA o número de itens em BAG.
*/
int
size(Bag bag)
{
    /**
     * Alerta que o bag passada ainda não foi inicializada, mas
     * retorna 0 para manter a consistência da API.
     */
    if (bag == NULL) {
        ERROR("[size] bag can't be NULL");
        return 0;
    }
    return bag->length;
}

/*-----------------------------------------------------------*/
/* 
*  ISEMPTY(BAG)
*
*  RECEBE uma bag BAG.
* 
*  RETORNA TRUE se BAG está vazia e FALSE em caso contrário.
*
*/
Bool
isEmpty(Bag bag)
{
    return ((bag == NULL) || (bag->length == 0));
}

/*-----------------------------------------------------------*/
/* 
*  ITENS(BAG, INIT)
* 
*  RECEBE uma bag BAG e um Bool INIT.
*
*  Se INIT é TRUE,  ITENS() RETORNA uma cópia/clone do primeiro item na lista de itens na BAG.
*  Se INIT é FALSE, ITENS() RETORNA uma cópia/clone do item sucessor do último item retornado.
*  Se BAG está vazia ou não há sucessor do último item retornado, ITENS() RETORNA -1.
*
*  Se entre duas chamadas de ITENS() a BAG é alterada, o comportamento é  indefinido. 
*  
*/
vertex 
itens(Bag bag, Bool init)
{
    NodeBag *node;

    if (bag == NULL) {
        ERROR("[itens] bag can't be NULL");
        return -1;
    }

    /**
     * Se init é TRUE, reseta o pointeiro node, de modo que ele volte
     * a apontar para o primeiro item na bag. Além disso, se o iterador
     * não tiver atingido o fim da bag é preciso atualizá-lo.
     */
    if (init) {
        bag->iterator = bag->first;
    }
    else if (bag->iterator) {
        bag->iterator = bag->iterator->next;
    }
    node = bag->iterator;

    /**
     * Se a bag estiver vazia ou se não houver um próximo item para
     * retornar, então node vai ser NULL, e a função deve retornar NULL.
     * Caso contrário, retorna uma cópia do item armazenado em node. 
     */
    if (node == NULL) {
        return -1;
    }
    else {
        return node->item;
    }
}

/*------------------------------------------------------------*/
/* 
* Implementaçao de funções administrativas
*/
/**
 * Libera o espaço ocupado por um nó e seus atributos.
 * 
 * @param node: um ponteiro para o nó a ser desalocado.
 */
static void
freeNode(NodeBag *node) {
    free(node);
}

/**
 * Instancia um novo nó cujo conteúdo vai ser uma cópia do conteúdo
 * armazenado no ponteiro item. Armazena também o tamanho, em bytes,
 * de item, de modo que ele possa ser usado depois.
 *
 * @param item: um ponteiro para o item a ser armazenado no novo nó.
 * @param size: o tamanho, em bytes, do item.
 * @return um ponteiro para o nó recém alocado.
 */
static NodeBag*
newNode(int item) {
    NodeBag *node;

    node = (NodeBag*) emalloc(sizeof(NodeBag));

    node->next = NULL;
    node->item = item;

    return node;
}
