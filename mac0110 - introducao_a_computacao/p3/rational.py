class Rational:

  def __init__(proprio, num, den = 1):
    if hasattr(num, 'num') and hasattr(num, 'den'):
      proprio.num = num.num
      proprio.den = num.den

    elif type(num) == int and type(den) == int:
      proprio.num = num
      proprio.den = den

  def __str__(proprio):
    return "%d / %d" % (proprio.num, proprio.den)

  def __mul__(this, other):
    return Rational(this.num * other.num, this.den * other.den)

  def __add__(this, other):
    n = this.num * other.den + this.den * other.num
    d = this.den * other.den
    return Rational(n, d)


a = Rational(3, 4)
b = Rational(11, 127)
c = Rational(a)
d = Rational(6)

print(a)
print(b)
print(c)
print(d)

print(a * b)
print(Rational(1, 2) + Rational(3, 2))