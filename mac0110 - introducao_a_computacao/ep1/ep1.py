"""
  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
  DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
  OU PLÁGIO.  
  DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
  DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
  DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
  DIVULGADOS NA PÁGINA DA DISCIPLINA.
  ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
  AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

  Nome : Ygor Sad Machado
  NUSP : 8910368
  Turma: BCC-2018
  Prof.: Marcelo Queiroz

"""
from random import randint
import pdb


"""
  Descrição:
    Escolhe um jogador para a rodada

  Parâmetros:
    Não há.

  Retorno:
    Uma string contendo o nome do jogador sorteado
"""
def escolher_jogador():
  if randint(0, 1):
    return "computador"
  else:
    return "humano"


"""
  Descrição
    Calcula a soma de todos os pesos p_n para n em [0,N-1]. Para tanto,
    usa a distribuição de probabilidade dist, separando cada um dos
    dígitos que a compõem e somando-os. Note que o n-ésimo dígito sempre
    corresponde ao peso de n na roleta.

  Parâmetros
    N    : número de pesos contidos em dist
    dist : número representando a distribuição de probabilidade dos pesos

  Retorno
    Um inteiro representando a soma de todos os pesos p_n contidos na
    distribuição dist.
"""
def soma_total_pesos(N, dist):
  soma = 0

  for i in range(N):
    digito = dist % 10
    dist   = dist // 10
    soma  += digito

  return soma


"""
  Descrição
    Devolve um inteiro dist onde cada dígito n=0,...N-1 representa um peso
    relativo em uma distribuição de probabilidade, verificando que dist
    esteja entre 0 e 10 ** (N-1).

  Parâmetros
    N : limite superior de números para calcular a distribuição enviesada

  Retorno
    Um inteiro cujo i-ésimo dígito representa o valor da distribuição para
    o p i-ésimo número, sendo p um número possível de ser sorteado dentro
    do intervalo [0,N], inclusive.
"""
def distribuicao(N):
  dist = 0
  ordem_digito = 1

  for i in range(N):
    dist += randint(0, 9) * ordem_digito
    ordem_digito *= 10

  assert dist in range(10 ** N)
  return dist


"""
  Descrição:
    * calcula S
    * gera um randint entre 0 e S-1
    * acha o menor n para o qual s < p0+p1+...+pn

  Parâmetros:

  Retorno:
"""
def sorteia(N, dist):
  soma_total = soma_total_pesos(N, dist)
  s = randint(0, soma_total-1)

  # Note que n começa em -1 pois n=0 já é um peso válido. Logo, para
  # permitir que já o primeiro n possa ser sorteado, é preciso que seu
  # valor de inicialização seja inferior ao menor n possível
  # 
  soma_pesos = 0
  n = -1

  # Itera pelos pesos enquanto não encontrar um n para o qual a soma
  # de todos os pesos anteriores é estritamente maior do que s
  #
  while (s >= soma_pesos):
    digito = dist % 10
    dist   = dist // 10

    # Acrescenta o peso do elemento atual à soma e adiciona uma unidade
    # ao valor de para indicar qual é o n mínimo para que a soma dos
    # pesos supere o valor de s
    # 
    n = n + 1
    soma_pesos += digito
    
  return n


"""
  Descrição:
    Realiza uma jogada para o jogador especificado, garantindo
    que o lance dado está dentro do limite permitido [0,N-1] e que
    é diferente do lance_anterior.

  Parâmetro:
    N              : inteiro que representa o número máximo de itens da roleta
    jogador        : string contendo o nome do atual jogador - 'humano' ou 'computador'
    lance_anterior : inteiro representando o lance dado pelo último jogador, seja ele
                     humano ou computador, que é usado para garantir que a jogada tenha
                     um lance distinto.

  Retorno:
    Não há.
"""
def jogada(N, jogador, lance_anterior):

  # Se for a vez do computador, simplesmente escolhe um número em [0,N]
  #
  if (jogador == "computador"):
    print("É a vez da mesa jogar.")
    
    lance = lance_anterior

    # Continua a gerar novos números aleatórios enquanto o número gerado
    # não for diferente do lance anterior
    while (lance == lance_anterior):
      lance = randint(0, N-1)
  else:
    lance = int(input("É a sua vez de jogar: "))

    # Se o lance dado estiver fora do intervalo [0,N] ou se ele for igual
    # ao lance anterior, continua pedindo ao usuário um novo número
    #
    while (lance == lance_anterior or lance >= N):
      lance = int(input("Por favor digite outro número: "))

      if lance == lance_anterior:
        print("A mesa já escolheu esse número.")

  assert lance in range(N) and lance != lance_anterior
  return lance


"""
  Descrição:
    Computa a distância entre dois números a e b relativos a um terceiro
    número N - aqui, a quantidade de itens na roleta do jogo
  
  Parâmetros:
    N : inteiro que representa a quantidade máxima de números na roleta
    a : um número inteiro
    b : um número inteiro

  Retorno:
    Um inteiro contendo a menor distância entre a e b
"""
def computar_distancia(N, a, b):
  return min((a - b) % N, (b - a) % N)


"""
  Descrição:
    Determina qual deve ser o valor a ser modificado na pontuação. Para tanto,
    verifica se ambas as distâncias são diferentes de 0. Caso sejam, retorna 10
    pontos. Do contrário, então uma dos jogadores acertou o lance com exatidão, e,
    portanto, devem ser alterados 100 pontos.

  Parâmetros:
    distancia_humano     : inteiro que indica a diferença entre o lance do jogador
                           e o número sorteado
    distancia_computador : inteiro que indica a diferença entre o lance do computador
                           e o número sorteado

  Retorno:
    Um inteiro indicando qual deve ser a diferença de pontos, podendo ser 10 ou 100.
"""
def determinar_pontuacao(distancia_humano, distancia_computador):
  if (distancia_humano and distancia_computador):
    return 10
  else:
    return 100


"""
  Descrição:
    Faz uma análise da estrutura interna da roleta, explicitando o seu
    enviesamento ao exibir o peso de cada rótulo, a probabilidade de seu
    sorteio, e a frequência com que ele apareceria em um sorteio

  Parâmetros:
    N    : inteiro que representa o número de rótulos na roleta
    dist : inteiro que representa a distribuição de pesos dos rótulos na roleta

  Retorno:
    Não há 
"""
def desmonta_roleta(N, dist):
  print("\nEssa é a estrutura da roleta maluca")
  print("x\tPeso\tProb(x)\tFreq(x)")

  soma_pesos    = soma_total_pesos(N, dist)
  dist_original = dist

  for x in range(N):
    peso = dist % 10
    dist = dist // 10
    frequencia = 0

    # Computa a frequência de aparecimento do número x em uma
    # sequência de 1000 sorteios, incrementando 
    # 
    for i in range(1000):
      numero = sorteia(N, dist_original)

      if (numero == x):
        frequencia += 1

    # Faz a tabulação dos dados acerca desse rótulo de modo a
    # exibi-los na tabela
    # 
    print(x, peso, "{0:2.3f}".format(peso/soma_pesos), "{0:2.3f}".format(frequencia/1000), sep="\t")


"""
  Descrição:
    Contém a estruturação básica para a lógica do programa

  Parâmetros:
    Não há.

  Retorno:
    Não há.
"""
def main():
  print("Bem-vind@ à roleta maluca!")

  N = int(input("Por favor digite a quantidade de elementos da roleta (entre 2 e 100): "))

  # Cria a distribuição dos pesos, enviesando a roleta
  #
  print("A roleta possui os números 0...", N, sep="")
  print("Aguarde enquanto envieso a roleta...")
  dist = distribuicao(N)


  # Cria o estado inicial do jogo, definindo as variáveis que irão
  # controlar a sua execução - ou término
  # 
  pontuacao_humano = 0
  pontuacao_computador = 0
  rodada = 0
  continuar_jogo = "S"


  # Laço que controla se o jogo deve continuar ou ser encerrado
  # 
  while (continuar_jogo == "S"):
    rodada += 1

    print("\nRodada", rodada)
    print("Escolhendo jogador inicial...")
    jogador = escolher_jogador()

    # Verifica se quem começa a rodada é o computador ou o usuário,
    # alterando a ordem das chamadas de acordo com o caso
    # 
    if (jogador == "computador"):     
      
      lance_computador = jogada(N, jogador, -1)
      print("A mesa escolhe o número", lance_computador, end=".\n")
      lance_humano = jogada(N, "humano", lance_computador)
    else:

      lance_humano = jogada(N, jogador, -1)
      lance_computador = jogada(N, "computador", lance_humano)
      print("A mesa escolhe o número", lance_computador, end=".\n")


    # Sorteia um novo número enviesado
    # 
    sorteio = sorteia(N, dist)
    print("Sorteio =", sorteio, end=".\n")


    # Verifica qual jogador mais se aproximou do número sorteado, definindo,
    # assim, quem ganhou o jogo
    # 
    distancia_computador = computar_distancia(N, lance_computador, sorteio)
    distancia_humano     = computar_distancia(N, lance_humano, sorteio)


    # Determina qual deve ser a diferença de pontos a ser aplicada na pontuação
    # dos jogadores com base na proximidade do acerto
    # 
    valor_pontuacao = determinar_pontuacao(distancia_humano, distancia_computador)


    if (distancia_humano <= distancia_computador):
      print("Você ganhou!")

      pontuacao_humano += valor_pontuacao
      pontuacao_computador -= valor_pontuacao
    else:
      print("A mesa ganhou!")

      pontuacao_humano -= valor_pontuacao
      pontuacao_computador += valor_pontuacao


    # Exibe a pontuação e pergunta ao jogador se ele gostaria de continuar
    # 
    print("Pontuação: Jogador = ", pontuacao_humano, ", Mesa = ", pontuacao_computador, sep="")
    continuar_jogo = input("Deseja continuar jogando (S/N): ")


  # Caso o usuário deseje, exibe a estrutura enviesada da roleta
  # 
  if continuar_jogo == "Abra o jogo!":
    desmonta_roleta(N, dist) 
    

  # Finaliza o jogo mostrando quanto o jogador ganhou ou perdeu - isto é,
  # sua pontuação final
  # 
  if (pontuacao_humano > 0):
    print("\nVocê deve receber", pontuacao_humano, "da mesa!")
  else:
    print("\nVocê deve pagar", pontuacao_humano, "para a mesa!")
  
  print("Obrigado por jogar a roleta maluca!")


# Chama o código principal contido na função main
main()
