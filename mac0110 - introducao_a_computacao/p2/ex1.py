#coding: utf8

# abordagem recursiva
def metodo_horner_recursivo(coeficientes, x):
  print(coeficientes)

  if len(coeficientes) == 1:
    return coeficientes[0]

  return coeficientes[0] + x * (metodo_horner_recursivo(coeficientes[1:], x))


def metodo_horner_iterativo(coeficientes, x):
  tam = len(coeficientes)
  p_x = coeficientes[-1]

  print(coeficientes[-1])
  for i in range(tam-1, 0, -1):
    # print (str(coeficientes[i-1]) + "\n")
    print("p_x: " + str(p_x))
    print (str(x) + " * " + str(p_x) + " + " + str(coeficientes[i-1]) + "\n")
    p_x = x * p_x + coeficientes[i-1]

  return p_x

coef = [1, 2, 1, 2, 3]
print("----------\n")
print(metodo_horner_recursivo(coef, 2))
print("----------\n")
print(metodo_horner_iterativo(coef, 2))
