#coding: utf8

def cont_identicamente_nulos(a):
  m = len(a)
  n = len(a[0])

  cont_col_nulas = 0
  cont_lin_nulas = 0

  for i in range(m):
    lin_nula = True
    col_nula = True

    for j in range(n):
      if a[i][j] != 0:
        lin_nula = False

      if a[j][i] != 0:
        col_nula = False

    if lin_nula:
      cont_lin_nulas += 1

    if col_nula:
      cont_col_nulas += 1

  return cont_lin_nulas, cont_lin_nulas



m1 = [[1, 0, 2, 3], [4, 0, 5, 6], [0, 0, 0, 0], [0, 0, 0, 0]]
print(cont_identicamente_nulos(m1))