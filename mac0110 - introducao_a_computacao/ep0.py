"""
    AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
    DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
    TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
    DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
    DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
    OU PLÁGIO.  
    DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
    DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
    SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
    DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
    DIVULGADOS NA PÁGINA DA DISCIPLINA.
    ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
    AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

    Nome : Ygor Sad Machado
    NUSP : 8910368
    Turma: BCC-2018
    Prof.: Marcelo Queiroz

"""

# Inicialmente, faz o número valer 0, de modo que
# fatoração do número em termos de potência de base 2
# possa ser feita
number = 0

# ---------------
# Pergunta no: 1
# ---------------
print('Pense em um número entre 0 e 10.')
choice = int(input('O número é par (digite 0) ou ímpar (digite 1)? '))

if (choice == 1):
    print('Subtraia 1 do número.')

# O número é determinado por meio de sua representação
# binária, de maneira tal que a entrada do usuário (0 ou 1)
# representa a presença do bit para a respectiva potência
# de base dois
# 
# E.g.: A primeira entrada do usuário representa o 0-ésimo
# bit do número (isto é, o bit 2 ** 0). Portanto, se o usuário
# digitar 1, tal bit deve ser somado a number. Do contrário, ele
# não é levado em consideração, justamente por que a representação
# binária do número pensado não contém este bit (2 ** 0)
# 
print('Divida o resultado por 2.')
number = number + (choice * (2 ** 0))


# ---------------
# Pergunta no: 2
# ---------------
choice = int(input('O número resultante é par (digite 0) ou ímpar (digite 1)? '))

if (choice == 1):
    print('Subtraia 1 do número.')

print('Divida o resultado por 2.')

# Segundo bit menos significativo
# (bit correspondente a 2 ** 1)
number = number + (choice * (2 ** 1))


# ---------------
# Pergunta no: 3
# ---------------
choice = int(input('O número resultante é par (digite 0) ou ímpar (digite 1)? '))

if (choice == 1):
    print('Subtraia 1 do número.')

print('Divida o resultado por 2.')

# Terceiro bit menos significativo
# (bit correspondente a 2 ** 2)
number = number + (choice * (2 ** 2))


# ---------------
# Pergunta no: 4
# ---------------
choice = int(input('O número resultante é par (digite 0) ou ímpar (digite 1)? '))

if (choice == 1):
    print('Subtraia 1 do número.')

print('Divida o resultado por 2.')

# Quarto bit menos significativo
# (bit correspondente a 2 ** 3)
number = number + (choice * (2 ** 3))


# Exibe o número final montado a partir
# das entradas do usuário
print('O número que você pensou é ', number, '?')
choice = int(input('Digite 1 para sim, 0 para não: '))


# Determina, com base na entrada do usuário,
# qual mensagem deve ser exibida
if (choice == 1):
    print('Já consigo ler mentes... agora só me falta dominar o mundo! huahuahua')
else:
    print('Até parece! Você que errou nas contas... kkkk')