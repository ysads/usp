# coding=utf-8
"""
  AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP, 
  DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA. 
  TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO PROGRAMA (EP) FORAM 
  DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES
  DESSE EP E QUE PORTANTO NÃO CONSTITUEM DESONESTIDADE ACADÊMICA
  OU PLÁGIO.  
  DECLARO TAMBÉM QUE SOU RESPONSÁVEL POR TODAS AS CÓPIAS
  DESSE PROGRAMA E QUE EU NÃO DISTRIBUI OU FACILITEI A
  SUA DISTRIBUIÇÃO. ESTOU CIENTE QUE OS CASOS DE PLÁGIO E
  DESONESTIDADE ACADÊMICA SERÃO TRATADOS SEGUNDO OS CRITÉRIOS
  DIVULGADOS NA PÁGINA DA DISCIPLINA.
  ENTENDO QUE EPS SEM ASSINATURA NÃO SERÃO CORRIGIDOS E,
  AINDA ASSIM, PODERÃO SER PUNIDOS POR DESONESTIDADE ACADÊMICA.

  Nome : Ygor Sad Machado
  NUSP : 8910368
  Turma: BCC-2018
  Prof.: Marcelo Queiroz

  Referências externas:
    - Single line loop (linhas 355 e 356): http://blog.teamtreehouse.com/python-single-line-loops
    - Join em strings (linha 176): https://www.geeksforgeeks.org/join-function-python/
"""
from random import randint


"""
  Descrição:
    Extrai o núcleo do sujeito ou núcleo verbal da string passada. Em outras
    palavras, retira preposições e artigos, deixando apenas a palavra mais
    relevante na expressão.

  Parâmetros:
    expressao: uma string a ser limpa

  Retorno:
    Uma string contendo o núcleo semântico da expressão
"""
def extrair_nucleo_semantico(expressao):
  if not expressao:
    return ""

  artigos = ["a", "as", "o", "os"]
  palavras = expressao.split()

  # Por definição, artigos estão sempre à frente do sujeito e do objeto, por isso
  # inicialmente testa os artigos para saber se a expressão é desse tipo
  #
  for artigo in artigos:
    if artigo is palavras[0]:
      return palavras[-1]

  # Se não achar um artigo, então a expressão é um verbo e portanto seu núcleo está
  # antes de uma eventual preposição
  # 
  return palavras[0]


"""
  Descrição:
    Verifica se dadas duas palavras rimam. Para isso, simplesmente compara os dois
    últimos caracteres das palavras, dizendo se são iguais. Por conveniência, tome
    que uma string vazia sempre rima com qualquer outra. Note que é necessário extrair
    o núcleo semântico das palavras para impedir que se compare uma preposição com
    um verbo ou um substantivo, no caso de frases em ordem inversa

  Parâmetros
    palavra1 : string que contém a primeira palavra
    palavra2 : string que contém a segunda palavra

  Retorno:
    Um booleano que indica se as duas palavras rimam ou não
"""
def rimam(palavra1, palavra2):
  palavra1 = extrair_nucleo_semantico(palavra1)
  palavra2 = extrair_nucleo_semantico(palavra2)

  if palavra1 and palavra2:
    return palavra1[-2:] == palavra2[-2:]
  else:
    return True


"""
  Descrição:
    Sorteia um item aleatório de 'lista', obedecendo à condição de que deve-se repetir o
    sorteio até que se ache um item ainda não sorteado. A marcação de quais items já
    foram sorteados é feita pela variável 'lista_controle', sendo que as posições cujo
    valor é False já foram sorteadas. Note que caso o sorteio seja realizado max_sorteios
    vezes e ainda assim não seja achado um item não-sorteado, então deve-se retornar o
    último item sorteado
    
  Parâmetros:
    lista            : uma lista de strings de onde sairá o item sorteado
    lista_controle   : uma lista de controle, com o mesmo tamanho de 'lista', cujo propósito
                       é marcar se um dado item de 'lista' já foi usado
    max_sorteios     : inteiro que indica o máximo número de sorteios que podem ser feitos
    rima             : string que contém uma palavra com a qual a sorteada deve rimar

  Retorno:
    Uma string contida em lista
"""
def sortear_palavra(lista, lista_controle, max_sorteios, rima):
  
  # Inicializa o contador de sorteios
  # 
  num_sorteios = 0
  
  # Realiza o primeiro sorteio fora do loop
  # 
  index = randint(0, len(lista) - 1)

  # Somente é necessário realizar mais de um sorteio se for preciso controlar a ocorrência
  # dos sorteios. Ou seja, caso não tenha sido passada uma 'lista_controle', simplesmente
  # retorna o primeiro item sorteado
  # 
  if lista_controle:

    # Realiza novos sorteios até achar um item válido (não usado e que obedeça à rima)
    # ou atingir o num máximo de sorteios
    #
    while not(rimam(lista[index], rima) and lista_controle[index]) and (num_sorteios < max_sorteios):
      index = randint(0, len(lista) - 1)
      num_sorteios += 1

    # Sinaliza na lista de controle que o item nessa posição já foi utilizado ao menos uma vez
    # 
    lista_controle[index] = False

  return lista[index]


"""
  Descrição:
    Conserta erros de regência em frases na ordem inversa. Isto é, nas frases
    em ordem inversa, move a preposição do fim da frase para junto do artigo
    que acompanha o objeto. Assim, frases do tipo "a vida o mar passa por" ficam
    com a construção "a vida por o mar passa", que declinável com mais facilidade

  Parâmetros:
    frase : uma string contendo uma frase
    ordem : um inteiro que indica a ordem das palavras na frase passada

  Retorno:
    Uma string contendo uma frase sem erros de regência
"""
def ajustar_regencia(frase, ordem):
  # Somente é necessário ajustar a regência de frases em ordem inversa
  # 
  if ordem == 0:
    palavras = frase.split(" ")

    # Copia para a string o sujeito e move a preposição para o meio
    # da frase
    # 
    frase = palavras[:2]
    frase.append(palavras[-1])

    # Em seguida, copia os demais itens do meio da string até o fim, sem
    # contar com a preposição, que já foi copiada
    # 
    for palavra in palavras[-4:-1]:
      frase.append(palavra)

    # Transforma o vetor de palavras em string, juntando as palavras com
    # espaços em branco
    # 
    frase = " ".join(frase)

  return frase


"""
  Descrição:
    Verifica se é necessário adicionar uma conjunção à frase passada
    como parâmetro. Note que existe uma chance em 3 de haver conjunção
    ou não.

  Parâmetro:
    frase : string que representa a frase na qual uma eventual conjunção
            será adicionada

  Retorno:
    Uma string contendo uma frase – com ou sem conjunção.
"""
def tratar_conjuncao(frase):
  conjuncoes = ["como", "e", "enquanto", "mesmo quando", "porque", "quando", "se", "toda vez que"]

  adicionar_conjuncao = randint(0, 2)

  # Como 'adicionar_conjuncao' pode ter três valores distintos - 0, 1, 2 -, somente adiciona
  # a conjunção se a variável tiver valor 0. Dessa forma garante-se probabilidade de 1/3
  # para a ocorrência de uma conjunção no início de uma frase
  # 
  if adicionar_conjuncao == 0:
    
    # Como aqui não há restrição para repetições de conjunção, passa a lista de controle vazia
    # e define um máximo de um sorteio
    # 
    conjuncao = sortear_palavra(conjuncoes, [], 1, "")
    frase = conjuncao + " " + frase

  return frase


"""
  Descrição:
    Função que ajusta a declinação de uma frase, isto é, busca por preposições e
    artigos adjacentes e faz a sua junção corretamente

  Parâmetros:
    frase: string contendo uma frase cuja declinação deve ser ajustada

  Retorno:
    Uma string contendo uma frase ajustada
"""
def ajustar_declinacao(frase):

  # Retira os hífens que marcam a ausência de preposição nos verbos intransitivos
  # 
  frase = frase.replace(" -", "")
  
  preposicoes = [["a", "a"], ["de", "d"], ["em", "n"], ["por", "pel"]]
  artigos = ["a", "as", "o", "os"]

  # Verifica se alguma das composicoes preposicao + artigo foi encontrada
  # na frase. Em caso positivo, faz a declinação, justapondo o artigo encontrado
  # ao radical característico da preposição em uso
  # 
  for preposicao in preposicoes:
    for artigo in artigos:
      declinacao_errada = " " + preposicao[0] + " " + artigo + " "
      
      if declinacao_errada in frase:
        frase = frase.replace(declinacao_errada, " " + preposicao[1] + artigo + " ")

  # Por não seguir o padrão de declinação tradicional do português, a crase deve
  # ser ajustada novamente ao final
  #
  if " aa " in frase:
    frase = frase.replace("aa", "à")

  return frase


"""
  Descrição:
    Função que monta uma frase com base em uma lista de substantivos e um lista de verbos,
    determinando se é necessário que ela possua uma rima

  Parâmetros:
    substantivos      : uma lista de strings contendo substantivos
    cont_substantivos : uma lista de booleanos que indica quais substantivos não foram usados
    verbos            : uma lista de strings contendo verbos
    cont_verbos       : uma lista de booleanos que indica quais verbos não foram usados
    rima              : uma string contendo a palavra com a qual a frase a ser montada deve rimar 

  Retorno:
    Uma string contendo uma frase completa
"""
def montar_frase(substantivos, cont_substantivos, verbos, cont_verbos, rima):

  # Define como máximo número de sorteios a soma entre quantidade de verbos e substantivos
  # 
  max_sorteios = len(substantivos) + len(verbos)

  # Sorteia as palavras que irão compor as frases de acordo com uma das ordem abaixo:
  # 
  # 0 - sujeito + objeto + verbo
  # 1 - sujeito + verbo + objeto
  # 2 - verbo + objeto + sujeito
  # 
  ordem = randint(0, 2)
  if ordem == 0:
    palavra1 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, "")
    palavra2 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, "")
    palavra3 = sortear_palavra(verbos, cont_verbos, max_sorteios, rima)

  elif ordem == 1:
    palavra1 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, "")
    palavra2 = sortear_palavra(verbos, cont_verbos, max_sorteios, "")
    palavra3 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, rima)

  else:
    palavra1 = sortear_palavra(verbos, cont_verbos, max_sorteios, "")
    palavra2 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, "")
    palavra3 = sortear_palavra(substantivos, cont_substantivos, max_sorteios, rima)

  # Aglutina as três palavras sorteadas em uma única string para que possa ser processada
  # 
  frase = palavra1 + " " + palavra2 + " " + palavra3

  # Verifica, por fim, se é necessário adicionar uma conjunção à frase, ajustar a declinação
  # de verbos que exigem preposição, ou mudar o lugar da preposição, caso seja uma frase em
  # ordem inversa
  # 
  frase = ajustar_regencia(frase, ordem)
  frase = tratar_conjuncao(frase)
  frase = ajustar_declinacao(frase)

  return frase


"""
  Descrição:
    Estiliza o verso para exibição na tela. As regras para estilização são: versos pares e o
    último verso do poema tem ponto final; versos ímpares possuem a primeira letra maiúscula;
    a cada 4 versos deve-se pular uma linha

  Parâmetros:
    verso          : uma string contendo o texto do verso
    posicao_versos : um inteiro indicando qual a ordem do verso no poema
    versos_totais  : um inteiro que indica quantos versos tem o poema no total

  Retorno:
    Uma string contendo o verso estilizado
"""
def estilizar_verso(verso, posicao_verso, versos_totais):
  verso = verso.capitalize() if (posicao_verso % 2 == 0) else verso
  verso = verso + "." if ((posicao_verso + 1) % 2 == 0 or posicao_verso == (versos_totais - 1)) else verso
  verso = "\n" + verso if ((posicao_verso % 4 == 0) and posicao_verso > 0) else verso

  return verso


"""
  Descrição:
    Função que gera uma combinação de versos de acordo com os parâmetros que lhe são passados,
    podendo ou não ter rimas e depois exibe-o na tela

  Parâmetros:
    substantivos  : lista de strings contendo os substantivos que podem ser usados no programa
    verbos        : lista de strings contendo os verbos que podem ser usados no programa
    numero_versos : inteiro que indica quantos versos devem ser gerados
    deve_rimar    : booleano que indica se deve ou não haver rimas no versos

  Retorno:
    Saída na tela contendo o poema
"""
def produz_versos(substantivos, verbos, numero_versos, deve_rimar):
  rima = ""

  # Define as funções indicadoras que irão controlar a frequência de sorteio dos substantivos
  # e verbos, de modo a haver a menor quantidade de repetições possível. As posições com True
  # são aquelas ainda vagas, isto é, que ainda não foram sorteadas
  # 
  cont_verbos       = [True for i in range(len(verbos))]
  cont_substantivos = [True for i in range(len(substantivos))]
  
  for i in range(numero_versos):
    frase = montar_frase(substantivos, cont_substantivos, verbos, cont_verbos, rima)

    # Verifica a necessidade de armazenar a última palavra da frase atual para que a próxima
    # frase a ser construída rime com ela
    # 
    if (i % 2 == 0) and deve_rimar:
      rima = frase.split(" ")[-1]
    else:
      rima = ""

    print(estilizar_verso(frase, i, numero_versos))


"""
  Descrição:
    Função inicial do projeto

  Parâmetros:
    Não há

  Retorno:
    Não há
"""
def main():
  # Inicializa as listas que vão armazenar os verbos e substantivos
  # 
  substantivos = []
  verbos = [] 


  # Identifica a quantidade de substantivos a serem recebidos, recebendo-os em seguida
  #
  print("Quantos substantivos voce deseja utilizar?")
  num = int(input())


  print("Digite um substantivo (com artigo) por linha:")
  for i in range(num):
    item = input()
    substantivos.append(item)


  # Identifica a quantidade de verbos a serem recebidos, recebendo-os em seguida
  #
  print("Quantos verbos voce deseja utilizar?")
  num = int(input())

  print("Digite um verbo (com preposicao) por linha:")
  for i in range(num):
    item = input()
    verbos.append(item)


  # Recebe como input se o poema deverá ter rimas e a quantidade de versos deste
  # 
  print("Voce deseja uma poesia com rima? Responda sim ou nao:")
  deve_rimar = input()
  deve_rimar = True if deve_rimar == "sim" else False
  
  print("Quantos versos voce deseja que a poesia tenha?")
  numero_versos = int(input())


  # Chama a função responsável por produzir o poema
  # 
  produz_versos(substantivos, verbos, numero_versos, deve_rimar)
  print("\n")


main()
