/*
    Aluno: Ygor Sad Machado
    N.USP: 8910368
    Curso: Engenharia de Computação
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
    Define algumas constantes que são úteis ao longo da execução do programa.
*/
#define MAX_FILENAME_LENGTH 51
#define MAX_SEQUENCE_LENGTH 11
#define MAX_STRING_LENGTH 151
#define AVG_STRING_LENGTH 21


/*
    Define uma estrutura cuja função é representar com constantes numéricas
    os tipos de dados lidos do arquivo de metadados.
*/
typedef enum tiposDados {
    inteiro = 0,
    decimal = 1,
    duploDecimal = 2,
    caracter = 3,
    string = 4    
} TiposDados;


/*
    Define uma estrutura que representa um metadado lido do arquivo de metadados.
    Ela armazena tanto um inteiro indicando qual o tipo de dados desse metadado,
    bem como o seu tamanho medido em bytes.
*/
typedef struct metadado {
    int tipo;
    int tamanho;    
} Metadado;


/*
    Define uma estrutura que representa a estrutura de dados descrita no arquivo
    de metadados. Sua função é armazenar a sequência com que serão passados os
    parâmetros para a função de inserção, o nome do arquivo que armazenará todos
    os registros, e uma matriz de índices que contém a chave e o offset de todos
    os registros contidos no arquivo de registros. Essa matriz é crucial para a
    realização da busca binária, na função de pesquisa.
*/
typedef struct desc {
    Metadado sequenciaLeitura[MAX_SEQUENCE_LENGTH];
    char nomeArquivoRegistros[MAX_FILENAME_LENGTH];
    int** indice;
} Descritor;


/*
    Descrição: 
        Função que, dados um nome de arquivo e um modo de abertura, encapsula o processo
        de abertura desse arquivo.

    Parâmetros:
        char* nomeArquivo : string contendo o nome do arquivo a ser aberto
        char* modoAbertura: string representando o modo com que o arquivo deve ser aberto

    Retorno:
        Um ponteiro para o arquivo aberto, caso a abertura tenha obtido sucesso. Se o arquivo
        não puder ser aberto, finaliza a execução do programa.

*/
FILE* abrirArquivo(char* nomeArquivo, char* modoAbertura) {
    FILE* arquivo;
    arquivo = fopen(nomeArquivo, modoAbertura);
    if (arquivo == NULL) {
        printf("O arquivo %s nao pode ser aberto!", nomeArquivo);
        exit(1);
    }
    return arquivo;
}


/*
    Descrição:
        Função que aloca uma string com a quantidade de posições especificada no parâmetro
        passado.

    Parâmetros:
        int tamanho: tamanho da string a ser alocada.

    Retorno:
        Um ponteiro para a string alocada, caso a alocação tenha obtido sucesso. Se, contudo,
        ela não obtiver sucesso, finaliza a execução do programa e nada é retornado.

*/
char* alocarString(int tamanho) {
    char* string;
    
    string = (char*) malloc(tamanho * sizeof(char));
    if (string == NULL) {
        printf("Nao ha memoria suficiente para executar o programa!\n");
        exit(1);
    }
    return string;
}


/*
    Descrição:
        Função que aloca uma matriz com a quantidade de linhas e colunas especificada nos
        parâmetros passados. Caso a matriz não possa ser alocada, finaliza a execução do
        programa.

    Parâmetros:
        int linhas  : quantidade de linhas da matriz a ser alocada.
        int colunas : quantidade de colunas da matriz a ser alocada.
        int*** vetor: ponteiro que receberá o endereço da matriz a ser alocada.

    Retorno:
        Não há retorno.

*/
void alocarMatriz(int*** vetor, int linhas, int colunas) {
    int i;
    
    *vetor = (int**) malloc(linhas * sizeof(int*));
    if (*vetor == NULL) {
        printf("Nao ha memoria suficiente para executar o programa!\n");
        exit(1);
    }
    
    for (i = 0; i < linhas; i++) {
        (*vetor)[i] = (int*) malloc(colunas * sizeof(int));
        if ((*vetor)[i] == NULL) {
            printf("Nao ha memoria suficiente para executar o programa!\n");
            exit(1);
        }
    }
}


/*
    Descrição:
        Função que desaloca uma matriz com a quantidade de linhas no parâmetro passado.

    Parâmetros:
        int linhas   : quantidade de linhas da matriz a ser desalocada.
        int*** matriz: ponteiro para o endereço da matriz a ser desalocada.

    Retorno:
        Não há retorno.

*/
void desalocarMatriz(int*** matriz, int linhas) {
    int i;
    for (i = 0; i < linhas; i++) {
        free((*matriz)[i]);
    }
    free(*matriz);
}


/*
    Descrição:
        Função que determina qual operação - insert, index, search, exit - deve ser
        executada pelo programa. Essa operação é determinada a partir de uma string
        que contém o comando passado ao programa, e corresponde, sempre, à primeira
        palavra da cadeia de caracteres. Assim, basta, à função, copiar para uma string
        auxiliar todos os caracteres lidos até que um espaço seja encontrado.
        Por medida de segurança, também é feita a verificação para o fim de string ou
        o caracter de quebra de linhas.

    Parâmetros:
        char* comando: string contendo o comando passado ao programa.

    Retorno:
        Um ponteiro para a string que contém a operação que deve ser executada.


*/
char* identificarOperacao(char* comando) {
    int i = 0;
    char* operacao;
    
    operacao = alocarString(MAX_STRING_LENGTH);
    while (((comando[i] != ' ') && (comando[i] != '\n')) && (comando[i] != '\0')) {
        operacao[i] = comando[i];
        i++;
    }
    operacao[i] = '\0';
    return operacao;
}


/*
    Descrição:
        Limpa um vetor, atribuindo, a todas as suas posições, o caracter de espaço - ' '.

    Parâmetros:
        char* vetor: ponteiro para a string que deve ser limpa.
        int tamanho: tamanho da string que deve ser lida.

    Retorno:
        Não há.
*/
void limparVetor(char* vetor, int tamanho) {
    int i;
    for (i = 0; i < tamanho; i++) {
        vetor[i] = ' ';
    }
}


/*
    Descrição:
        Função que, dada uma string contendo um comando que deve ser interpretado pelo
        programa, identifica quais são os parâmetros desse comando. Isto é, no caso dos
        comandos de inserção, define quais são os dados que efetivamente devem ser
        inseridos no arquivo de registros; enquanto que, no caso dos comandos de busca,
        determina qual a chave do registro a ser buscado.
        O que essa função faz é, basicamente, localizar o primeiro espaço em branco após
        a última posição analisada, e, a partir dela, copiar, para a string info, todos
        os caracteres até que uma vírgula - caracter ',' - seja encontrada. Nesse ponto,
        se os dados copiados representarem uma string, isto é, estiverem entre aspas,
        estas são removidas, de modo que apenas dados realmente úteis estejam contidos
        em info.
        Um ponto importante acerca de sua implementação reside no fato de que a função
        armazena, estaticamente, a última posição da string que foi analisada. Assim,
        chamadas sucessivas a ela sempre continuam a análise dos caracteres a partir
        do ponto em que ela parou em sua última execução.

    Parâmetros:
        char* comando: string contendo o comando cujos parâmetros devem ser identificados. 
        char* info   : string que conterá os dados identificados na string de comando.

    Retorno:
        Um inteiro que indica se o fim da string de comando foi atingido.
*/
int identificarDados(char* comando, char* info) {
    static int i = 0;
    int k, j = 0;
    
    /*
        Limpa todas as posições do vetor, de modo a garantir que informações anteriores
        não interfiram na obtenção dos novos dados.
    */
    limparVetor(info, MAX_STRING_LENGTH);
    
    /*
        Caso localize o caracter de quebra de linha ou de terminação de string, faz info
        valer NULL, retorna a variável de posição para 0, e retorna um, indicando que o fim
        da string de comando foi atingido.
    */
    if (comando[i] == '\0' || comando[i] == '\n') {
        i = 0;
        info = NULL;
        return 1;
    }
    
    /*
        Itera pela string até localizar um espaço, posição a partir de onde iniciar-se-á
        a operação de cópia das informações para a string info.
    */
    while (comando[i] != ' ') {
        i++;
    }
    i++;
    j = 0;

    /*
        Inicia, efetivamente, a cópia dos caracteres da string de comando para a string info,
        até que uma vírgula, um caracter de quebra de linha ou um terminador de string seja
        encontrado.
    */
    while (comando[i] != ',' && comando[i] != '\0' && comando[i] != '\n') {
        info[j] = comando[i];
        j++;
        i++;
    }

    /*
        Adiciona o terminador de string a info, e verifica se é necessário eliminar as aspas,
        para o caso de strings que estejam sendo copiadas. Se positivo, o loop interno é
        responsável por essa alteração.
    */
    info[j] = '\0';
    if (info[0] == '\"') {
        info[j-1] = '\0';
        for (k = 1; k < j-1; k++) {
            info[k-1] = info[k];
        }
        info[k-1] = '\0';
    }
    i++;
    return 0;
}


/*
    Descrição:
        Esta função tem por objetivo a determinação do tamanho de uma string definida
        no arquivo de metadados. Em termos simples, ela recebe uma cadeia de caracteres
        contendo a declaração de uma string no arquivo de metadados - algo como 'char[100]'
        ou char[50] -, e identifica o número que está contido entre os colchetes, isto é,
        o tamanho que a string defina por essa declaração possui.

    Parâmetros:
        char* identificador: cadeia de caracteres contendo a definição de uma string
                             contida no arquivo de metadados.

    Retorno:
        Um inteiro que indica o tamanho da string descrita no arquivo de metadados.
*/
int determinarTamanhoString(char* identificador) {
    int tamanhoString = 0;
    char* c;
    
    /*
        Identifica o posição do primeiro colchete e avança para a posição seguinte, onde
        o número, que descreve o tamanho da string declarada, começa.
    */
    c = strchr(identificador, '[');
    c++;
    
    /*
        Itera pelos dígitos que estão entre os colchetes, convertendo-os para inteiro
        e avaliando-os juntamente com os outros já lidos.
    */
    while (*c != ']') {
        tamanhoString = (tamanhoString * 10) + ((int) (*c - '0'));
        c++;
    }
    return tamanhoString;
}


/*
    Descrição:
        Função delegada à determinação do tamanho da estrutura definida no arquivo
        de metadados.

    Parâmetros:
        Descritor descritorMetadados : struct que descreve a estrutura definida no
                                       arquivo de metadados.

    Retorno:
        Um inteiro que representa o tamanho de um registro descrito pelo arquivo de
        metadados.

*/
int determinarTamanhoRegistro(Descritor descritorMetadados) {
    int i;
    int tamanho = 0;
    
    /*
        Itera pela sequência de leitura dos parâmetros da estrutura, somando todos os
        seus tamanhos em bytes.
    */
    for (i = 0; i < MAX_SEQUENCE_LENGTH; i++) {
        if (descritorMetadados.sequenciaLeitura[i].tipo != -1) {
            tamanho += descritorMetadados.sequenciaLeitura[i].tamanho;
        }
    }
    return tamanho;
}


/*
    Descrição:
        Função que objetiva a determinação de quantos registros estão atualmente inseridos
        no arquivo de registros.

    Parâmetros:
        FILE* arquivoRegistros: ponteiro para o arquivo que contém os registros já
                                inseridos.
        int tamanhoRegistro   : inteiro que representa o tamanho de cada um dos registros
                                inseridos.

    Retorno:
        Um inteiro que indica a quantidade de registros contida no arquivo de registros.
*/
int determinarQuantidadeRegistros(FILE* arquivoRegistros, int tamanhoRegistro) {
    int numBytesArquivo;
    int numRegistros;

	/*
        Move o ponteiro para o fim do arquivo em que estão os dados. SEEK_END é a constante
        que representa a posição final do arquivo.
    */
    fseek(arquivoRegistros, 0, SEEK_END);
    
    /*
        ftell é uma função que diz em que posição do arquivo o ponteiro está. Seu retorno
        corresponde, na prática, a quantos bytes foram percorridos da posição inicial até
        a posição atual. Assim, se o  ponteiro estiver no final do arquivo, ela retorna a
        quantidade de bytes que o arquivo possui no total, isto é, seu tamanho.
    */
    numBytesArquivo = ftell(arquivoRegistros);
    
    /*
        Nesse ponto, o cast para inteiro e a multiplicação por 1.0 são necessários para
        impedir a ocorrência da divisão inteira, que pode resultar em zero.
    */
    numRegistros = (int) ((1.0 * numBytesArquivo) / tamanhoRegistro);
    
    /*
        Retorna o ponteiro para o início do arquivo. Aqui, SEEK_SET é uma constante que
        indica o início do arquivo.
    */
    fseek(arquivoRegistros, 0, SEEK_SET);
    return numRegistros;
}


/*
    Descrição:
        Função que interpreta o arquivo de metadados, determinando quais os tipos de dados
        que compõem os campos de cada registro, bem como o nome do arquivo que armazenará
        os registros a serem inseridos.

    Parâmetros:
        FILE* arquivo: ponteiro para o arquivo de metadados que contém a descrição de cada
                       registro.

    Retorno:
        Uma struct do tipo Descritor que contém as informações acerca do nome do arquivo de
        registros e da sequência com que os dados de cada registro devem ser inseridos nesse
        arquivo.

*/
Descritor determinarTiposdeDados(FILE* arquivo) {
    int i;
    char aux[MAX_STRING_LENGTH];
    Descritor descritorMetadados;
    
    /*
        Inicia o vetor que armazena a sequência de leitura dos campos componentes de cada
        registro. O valor de inicialização é -1, para que não se confunda com nenhuma das
        constantes que representam os tipos de dados.
    */
    for (i = 0; i < MAX_SEQUENCE_LENGTH; i++) {
        descritorMetadados.sequenciaLeitura[i].tipo = -1;
    }
    
    /*
        Os primeiros dois fscanfs são responsáveis pela detecção do nome do arquivo que
        armazenará os eventuais registros a serem inseridos.
    */
    i = 0;
    fscanf(arquivo, "%s", aux);
    fscanf(arquivo, "%s", descritorMetadados.nomeArquivoRegistros);
    
    /*
        Em seguida, são lidos trechos do arquivo de metadados, separados por espaço, de
        modo que aqueles que representam tipos de dados sejam identificados e categorizados
        adequadamente.
        A primeira leitura é feita fora do laço de modo a evitar leitura redundante do
        último trecho de caracteres contido no arquivo.
    */
    fscanf(arquivo, "%s", aux);
    do {

        /*
            Efetua a categorização dos metadados, computando o seu tamanho e indicando a
            sua ordem de leitura no vetor apropriado.
            No caso das strings, para avaliar o tamanho que efetivamente ocuparão no arquivo
            de registros, é utilizada a função determinarTamanhoString();
        */
        if (strcmp(aux, "int") == 0) {
            descritorMetadados.sequenciaLeitura[i].tipo = inteiro;
            descritorMetadados.sequenciaLeitura[i].tamanho = sizeof(int);
            i++;
        }
        else if (strcmp(aux, "char") == 0) {
            descritorMetadados.sequenciaLeitura[i].tipo = caracter;
            descritorMetadados.sequenciaLeitura[i].tamanho = sizeof(char);
            i++;
        }
        else if (strcmp(aux, "double") == 0) {
            descritorMetadados.sequenciaLeitura[i].tipo = duploDecimal;
            descritorMetadados.sequenciaLeitura[i].tamanho = sizeof(double);
            i++;
        }
        else if (strcmp(aux, "float") == 0) {
            descritorMetadados.sequenciaLeitura[i].tipo = decimal;
            descritorMetadados.sequenciaLeitura[i].tamanho = sizeof(float);
            i++;
        }
        else if (strstr(aux, "char[") != NULL) {
            descritorMetadados.sequenciaLeitura[i].tipo = string;
            descritorMetadados.sequenciaLeitura[i].tamanho = sizeof(char) * determinarTamanhoString(aux);
            i++;
        }
        fscanf(arquivo, "%s", aux);
    } while (!feof(arquivo));
    
    return descritorMetadados;
}


/*
    Descrição:
        Função cujo objetivo é, a partir do nome do arquivo de registros, alterá-lo para
        gerar o nome do arquivo de índices.

    Parâmetros:
        char* nomeArquivoIndices   : string que armazenará o nome do arquivo de índices.
        char* nomeArquivoRegistros : string que contém o nome do arquivo de registros.

    Retorno:
        Não há.

*/
void alterarNomeArquivoIndices(char* nomeArquivoIndices, char* nomeArquivoRegistros) {
    int i;
    int tamNomeArquivo;
    
    /*
        Inicialmente, o nome do arquivo de registros é copiado para uma nova string. Isso pode
        ser feito porque somente os últimos caracteres, que indicam a extensão do arquivo, é
        que variam de um nome de arquivo para o outro.
    */
    strcpy(nomeArquivoIndices, nomeArquivoRegistros);
    
    /*
        Substitui a extensão 'reg' com a extensão 'idx', letra por letra
    */
    tamNomeArquivo = strlen(nomeArquivoRegistros);
    i = tamNomeArquivo - 1;
    nomeArquivoIndices[i--] = 'x';
    nomeArquivoIndices[i--] = 'd';
    nomeArquivoIndices[i--] = 'i';
}


/*
    Descrição:
        Função que, com base em seu tipo de dados, escreve determinada informação no arquivo
        de registros.

    Parâmetros:
        char* informacao  : string que contém o dado a ser inserido no arquivo de saída.
        int tipoDado      : constante inteira que indica qual o tipo de dados da informação
                            que deve ser inserida no arquivo de registros.
        int tamanhoDado   : inteiro que indica o tamanho do dado que deve ser inserido no
                            arquivo.
        FILE* arquivoSaida: ponteiro para o arquivo de registros em que a informação deve
                            ser inserida.

    Retorno:
        Não há.

*/
void escreverArquivoRegistros(char* informacao, int tipoDado, int tamanhoDado, FILE* arquivoSaida) {
    int auxi;
    float auxf;
    double auxd;
    char auxc;
    
    /*
        Determina, com base na constante inteira recebida, a qual tipo de dados o dado recebido
        pertence. Após, converte-a de string para o seu próprio tipo de dados, se necessário, e
        a insere no arquivo de registros.
    */
    switch (tipoDado) {
        case inteiro:
            auxi = atoi(informacao);
            fwrite(&auxi, tamanhoDado, 1, arquivoSaida);
            break;
            
        case decimal:
            auxf = (float) atof(informacao);
            fwrite(&auxf, tamanhoDado, 1, arquivoSaida);
            break;
            
        case duploDecimal:
            auxd = atof(informacao);
            fwrite(&auxd, tamanhoDado, 1, arquivoSaida);
            break;
            
        case caracter:
            auxc = informacao[0];
            fwrite(&auxc, tamanhoDado, 1, arquivoSaida);
            break;
            
        case string:
            fwrite(informacao, tamanhoDado, 1, arquivoSaida);
            break;
    }
}


/*
    Descrição:
        Função que adiciona um registro ao índice ao adicionar o valor do seu campo chave e
        o seu offset, dentro do arquivo de registros, ao arquivo de índices. O offset indica
        quantos bytes à frente do início do arquivo está o primeiro dado do registro.

    Parâmetros:
        int chave           : valor do campo chave do registro que está sendo adicionado
                              ao índice.
        int offset          : valor do offset do registro no arquivo.
        FILE* arquivoIndices: ponteiro para o arquivo de índices.

    Retorno:
        Não há.

*/
void escreverArquivoIndice(int chave, int offset, FILE* arquivoIndices) {
    fwrite(&chave, sizeof(int), 1, arquivoIndices);
    fwrite(&offset, sizeof(int), 1, arquivoIndices);
}


/*
    Descrição:
        Função que dada uma string contendo uma série de dados, identifica-os e insere-os,
        na ordem correta, no arquivo de registros.

    Parâmetros:
        char* dados         : string que contém os dados do registro que devem ser inseridos
                              no arquivo de registros.
        Descritor descritor : struct que descreve a estrutura definida no arquivo de metadados.
        FILE* arquivoSaida  : ponteiro para o arquivo em que será inserido o novo registro.

    Retorno:
        Não há.

*/
void inserirRegistro(char* dados, Descritor descritor, FILE* arquivoSaida) {
    int i = 0;
    int tipoDado;
    int tamanhoDado;
    int terminouTokenizar = 0;
    char info[MAX_STRING_LENGTH];
    
    /*
        Este laço itera por toda a string que contém os dados que devem ser inseridos, obtendo-os,
        um a um, e passando-os para a função de escrita. Note que a função responsável pela
        identificação dos dados é chamada sucessivamente para os mesmos argumentos.
    */
    terminouTokenizar = identificarDados(dados, info);
    while (!terminouTokenizar) {
        tipoDado = descritor.sequenciaLeitura[i].tipo;
        tamanhoDado = descritor.sequenciaLeitura[i].tamanho;
        escreverArquivoRegistros(info, tipoDado, tamanhoDado, arquivoSaida);
        terminouTokenizar = identificarDados(dados, info);
        i++;
    }
}


/*
    Descrição:
        Função que percorre todo o arquivo de registros, identificando as suas chaves e os
        seus offsets, e inserindo esses valores numa matriz de índices, de modo que,
        posteriormente, possam ser ordenados e inseridos no arquivo de índices.

    Parâmetros:
        FILE* arquivoRegistros  : ponteiro para o arquivo que contém os registros já inseridos.
        int*** vetorIndices     : ponteiro para a matriz que armazenará a chave e offset de
                                  cada registro.
        int tamanhoRegistro     : tamanho, em bytes, de cada registro.
        int quantidadeRegistros : quantidade de registros contidos no arquivo de registros.
        int tamanhoTipoChave    : tamanho do tipo de dados da chave de cada registro.

    Retorno:
        Inteiro que indica se a matriz de índices foi gerada ou não.

*/
int gerarMatrizIndices(FILE* arquivoRegistros, int*** vetorIndices, int tamanhoRegistro, int quantidadeRegistros, int tamanhoTipoChave) {
    int i = 0;
    int chave;
    int offset;
    
    /*
        Verifica se existem registros inseridos no arquivo. Se não, simplesmente encerra a
        execuçao da função.
    */
    if (quantidadeRegistros == 0) {
        return 1;
    }

    /*
        Aloca a memória necessária pra o armazenamento dos valores das chaves e dos offsets.
    */
    alocarMatriz(vetorIndices, quantidadeRegistros, 2);
    
    /*
        Retorna o ponteiro para o início do arquivo.
    */
    fseek(arquivoRegistros, 0, SEEK_SET);
    
    /*
        Itera por todos os registros contidos no arquivo, obtendo sua chave - valor único que
        os identifica - e seu offset - a partir de quantos bytes à frente do início do arquivo
        seus dados estão. Esses valores são armazenados na matriz.
        Observe que, dentro do loop, o ponteiro de arquivo é reposicionado certa quantidade de
        bytes sempre à frente da posição em que se encontra. É importante salientar que essa
        quantidade de bytes sempre desconta o tamanho da chave, de modo a manter a integrida dos
        dados, uma vez que, ao lê-la, a função sempre reposiciona o cursor, o que poderia causar
        erros de leitura.
    */
    for (i = 0; i < quantidadeRegistros; i++) {
        offset = ftell(arquivoRegistros);
        fread(&chave, tamanhoTipoChave, 1, arquivoRegistros);

        (*vetorIndices)[i][0] = chave;
        (*vetorIndices)[i][1] = offset;
    
        fseek(arquivoRegistros, tamanhoRegistro-tamanhoTipoChave, SEEK_CUR);
    }
    return 0;
}


/*
    Descrição:
        Função que, recursivamente, ordena uma matriz. O algoritmo implementado corresponde
        ao QuickSort. O algoritmo funciona com base na divisão do vetor em partições, que
        são criadas através da escolha de um pivô. À direita dele, devem ficar todos os itens
        maiores do que ele; e, à sua esquerda, todos os menores.
        Essa estratégia se baseia no fato de que uma partição com um único item sempre está
        ordenada. Assim, o que a função faz e ir criando e organizando as partições, de modo
        recursivo, até que as partições possuam apenas um item, o que garante a sua ordenação.
    
    Parâmetros:
        float* v : partição da matriz que deve ser ordenada. Inicialmente, corresponde à
                   matriz inteira.
        int ini  : linha inicial da matriz da partição que deve ser ordenada.
        int fim  : linha final da matriz da partição que deve ser ordenada.
    
    Retorno:
        Nao há.

*/
void ordenarMatrizIndices(int** v, int ini, int fim) {
    int pivo;
    int i = ini;
    int j = fim;
    int aux;
    
    /*
        Determina o pivô, que, nesse caso, corresponde ao elemento central da partição.
    */
    pivo = v[(ini + fim)/2][0];
    
    /*
        Enquanto os elementos à esquerda do pivô não forem todos menores do que ele, e,
        aqueles à direita, não forem maiores do que ele, continua iterando pela partição,
        organizando-a segundo essa lógica.
    */
    while (i <= j) {
        /*
            Itera pela partição, em busca de elementos mal-posicionados, isto é, elementos
            à direita do pivô que sejam menores do que ele, ou elementos à esquerda do pivo
            que sejam maiores do que ele.
        */
        while (v[i][0] < pivo && i < fim) {
            i++;
        }
        while (v[j][0] > pivo && j > ini) {
            j--;
        }
        /*
            Se dois elementos mal-posicionados forem localizados, eles são trocados de posição.
        */
        if (i <= j) {
            aux = v[i][0];
            v[i][0] = v[j][0];
            v[j][0] = aux;
            
            aux = v[i][1];
            v[i][1] = v[j][1];
            v[j][1] = aux;
            
            i++;
            j--;
        }
    }
    /*
        Ao fim do loop acima, a partição estará organizada segundo o critério supracitado. Assim,
        são feitas chamadas recursivas à função de ordenacao, passando, como partição, a lista de
        itens à direita e à esquerda do pivô, para que sejam organizadas segundo o mesmo critério.
    */
    if (j > ini) {
        ordenarMatrizIndices(v, ini, j);
    }
    if (i < fim) {
        ordenarMatrizIndices(v, i, fim);
    }
}


/*
    Descrição:
        Função que lê todos os registros contidos no arquivo de registros, criando um
        índice com os seus dados.

    Parâmetros:
        Descritor descritorMetadados: struct que descreve a estrutura descrita no arquivo
                                      de metadados.
        FILE* arquivoRegistros      : ponteiro para o arquivo que contém os registros.
        FILE* arquivoIndices        : ponteiro para o arquivo que conterá o índice.

    Retorno:
        Ponteiro para a matriz, ordenada, que contém os dados de indexação dos registros.

*/
int** indexarRegistro(Descritor descritorMetadados, FILE* arquivoRegistros, FILE* arquivoIndices) {
    int i;
    int tamanhoTipoChave;
    int tamNomeArquivo;
    int tamanhoRegistro;
    int quantidadeRegistros;
    int** matrizIndices;
    
    char nomeArquivoIndices[MAX_FILENAME_LENGTH];
    
    /*
        Inicializa o ponteiro para o arquivo de índices, de modo a prevenir falhas de
        segmentação
    */
    arquivoIndices = NULL;
    
    /*
        Esvazia o buffer do arquivo de registros, escrevendo, nele, os dados pendentes.
    */
    fflush(arquivoRegistros);
    
    /*
        Altera o nome do arquivo de registros para criar um arquivo de índices
    */
    alterarNomeArquivoIndices(nomeArquivoIndices, descritorMetadados.nomeArquivoRegistros);
    
    /*
        Abre o arquivo de índices para leitura
    */
    if (descritorMetadados.indice == NULL) {
        arquivoIndices = abrirArquivo(nomeArquivoIndices, "wb+");
    }
    
    /*
        Obtém o tamanho de cada registro e a quantidade de registros que estão contidos no arquivo
    */
    tamanhoRegistro = determinarTamanhoRegistro(descritorMetadados);
    quantidadeRegistros = determinarQuantidadeRegistros(arquivoRegistros, tamanhoRegistro);

    /*
        Determina o tamanho do tipo de dados da chave. Isso será usado para a criação do índice
    */
    tamanhoTipoChave = descritorMetadados.sequenciaLeitura[0].tamanho;
    
    /*
        Gera uma matriz com os dados das chaves e dos offsets, de modo a permitir a
        ordenação do arquivo de índices.
    */
    gerarMatrizIndices(arquivoRegistros, &matrizIndices, tamanhoRegistro, quantidadeRegistros, tamanhoTipoChave);
    
    /*
        Ordena a matriz que contém os dados dos índices.
    */
    ordenarMatrizIndices(matrizIndices, 0, quantidadeRegistros-1);
    
    /*
        Escreve os índices no arquivo de índices.
    */
    for (i = 0; i < quantidadeRegistros; i++) {
        escreverArquivoIndice(matrizIndices[i][0], matrizIndices[i][1], arquivoIndices);
    }
    
    /*
        Esvazia o buffer do arquivo de índices, escrevendo, nele, os dados dos índices.
    */
    fflush(arquivoIndices);
    
    if (arquivoIndices != NULL) {
        fclose(arquivoIndices);
    }
    
    return matrizIndices;
}


/*
    Descrição:
        Função delegada à execução de um algoritmo de busca binária para localizar
        determinado registro com base em sua chave.
        O processo funciona da seguinte maneira: a matriz é sempre divida ao meio,
        e o elemento central é analisado. Se ele corresponder ao que está sendo
        buscado, retorna o seu offset. SPor outro lado, se  esse elemento central
        for maior do que o elemento procurado, é analisada a metade superior da matriz
        segundo essa lógica; do contrário, a metade inferior é processada, também,
        com o mesmo raciocínio.

    Parâmetros:
        int** indice     : matriz que contém o índice dos registros inseridos no arquivo.
        int chaveBuscada : chave do registro que está sendo buscado.
        int numRegistros : número de registros contidos no arquivo de registros.

    Retorno:
        Um inteiro indicando o offset do registro buscado, ou -1, caso o registro não
        seja encontrado.
*/
int realizarBuscaBinaria(int** indice, int chaveBuscada, int numRegistros) {
    int i;
    int inicio = 0;
    int fim = numRegistros-1;
    int meio = (int) ((inicio + fim) / 2);
    
    /*
        O laço continua a executar até que os delimitadores superior e inferior sejam
        iguais, isto é, até que a partição da matriz que está em análise contenha apenas
        um elemento.
    */
    while (inicio <= fim) {
        meio = (int) ((inicio + fim) / 2);  
        /*
            Se o elemento central corresponde àquele buscado, retorna a segunda coluna
            da matriz, que corresponde ao offset desse registro. Se não, analisa, com
            a mesma lógica, a metade superior ou a metade inferior da matriz, conforme
            for mais adequado.
        */
        if (indice[meio][0] == chaveBuscada) {
            return indice[meio][1];
        }
        else if (indice[meio][0] < chaveBuscada) {
            inicio = meio+1;
        }
        else if (indice[meio][0] > chaveBuscada) {
            fim = meio-1;
        }
    }
    return -1;
}


/*
    Descrição:
        Função que, dado um offset, imprime os dados do registro localizado nessa posição
        do arquivo.

    Parâmetros:
        Descritor descritorMetadados : struct que descreve a estrutura descrita no arquivo
                                       de metadados.
        int offsetRegistro           : offset do registro a ser impresso.
        FILE* arquivoRegistros       : ponteiro para o arquivo que contém os registros.

    Retorno:
        Não há.

*/
void exibirRegistro(Descritor descritorMetadados, int offsetRegistro, FILE* arquivoRegistros) {
    int i;
    int auxi;
    char auxc;
    float auxf;
    double auxd;
    char auxs[MAX_STRING_LENGTH];
    
    /*
        Altera o ponteiro do arquivo para a posição em que o registro está localizado.
    */
    fseek(arquivoRegistros, offsetRegistro, SEEK_SET);
    
    /*
        Itera pelo vetor que armazena a sequência de armazenagem dos dados no registro, de
        modo a saber a ordem com qual devem ser lidos tais dados a partir do arquivo de
        registro.
    */
    for (i = 0; i < MAX_SEQUENCE_LENGTH; i++) {
        switch (descritorMetadados.sequenciaLeitura[i].tipo) {
            case inteiro:
                fread(&auxi, descritorMetadados.sequenciaLeitura[i].tamanho, 1, arquivoRegistros);
                printf("%d\n", auxi);
                break;
                
            case decimal:
                fread(&auxf, descritorMetadados.sequenciaLeitura[i].tamanho, 1, arquivoRegistros);
                printf("%g\n", auxf);
                break;
            
            case duploDecimal:
                fread(&auxd, descritorMetadados.sequenciaLeitura[i].tamanho, 1, arquivoRegistros);
                printf("%.02lf\n", auxd);
                break;
            
            case caracter:
                fread(&auxc, descritorMetadados.sequenciaLeitura[i].tamanho, 1, arquivoRegistros);
                printf("%c\n", auxc);
                break;
            
            case string:
                fread(auxs, descritorMetadados.sequenciaLeitura[i].tamanho, 1, arquivoRegistros);
                printf("%s\n", auxs);
                break;
        }
    }
}


/*
    Descrição:
        Função que recebe um comando de busca, na forma de uma string, identifca a chave do
        registro que deve ser buscado, e efetua todos os pré-procedimentos necessários à busca,
        antes de, efetivamente, chamar a função a executa. Dentre esses pré-procedimentos, podem
        ser citados a geração do índice, caso ele ainda não exista, dentre outros.

    Parâmetro:
        Descritor descritorMetadados : struct que descreve a estrutura descrita no arquivo
                                       de metadados.
        FILE* arquivoRegistros       : ponteiro para o arquivo que contém os registros.
        char* dados                  : string que contém os dados do comando passado ao programa.

    Retorno:
        Ponteiro para a matriz que contém o índice dos registros inseridos.

*/
int** buscarRegistro(char* dados, Descritor descritorMetadados, FILE* arquivoRegistros) {
    int i;
    int chaveBuscada;
    int numRegistros;
    int tamanhoRegistro;
    int offsetRegistro;
    char* info;
    char* nomeArquivoIndices;
    
    /*
        Verifica se a matriz de índices já existe. Se não existir, chama a função
        delegada à sua criação. O parâmetro NULL na chamada a esta função se deve
        ao fato de que não há necessidade de se passar um ponteiro para o arquivo
        de índices, uma vez que a própria função criá-lo-á.
    */
    if (descritorMetadados.indice == NULL) {
        descritorMetadados.indice = indexarRegistro(descritorMetadados, arquivoRegistros, NULL);
    }
    
    /*
        Inicia a tokenização da string de comando. Nessa primeira chamada, a
        função strtok retornará o nome da operação que deve ser executada -
        insert, search, index -, informação esta que não possui utilidade nesse
        ponto do programa. Portanto, essa chamada é necessária somente para que
        função atinja a parte da string que possui os dados realmente úteis.
    */
    info = strtok(dados, " ,\"");
    
    /*
        Já esta chamada, por outro lado, retorna o primeiro dado útil que deve
        ser escrito no arquivo de registros. Ela é executada fora do laço para
        evitar leituras redundantes decorrentes de características de implementação
        da função strtok.
    */
    info = strtok(NULL, " ,\"");
    
    /*
        Converte a chave a ser buscada de string para inteiro
    */
    chaveBuscada = atoi(info);
    
    tamanhoRegistro = determinarTamanhoRegistro(descritorMetadados);
    numRegistros = determinarQuantidadeRegistros(arquivoRegistros, tamanhoRegistro);    
    
    /*
        Realiza busca binária da chave desejada no índice criado
    */
    offsetRegistro = realizarBuscaBinaria(descritorMetadados.indice, chaveBuscada, numRegistros);
    
    /*
        Exibe o registro localizado
    */
    if (offsetRegistro != -1) {
        exibirRegistro(descritorMetadados, offsetRegistro, arquivoRegistros);
    }
    
    return descritorMetadados.indice;
}


int main() {

    FILE* arquivoIndices;
    FILE* arquivoRegistros;
    FILE* arquivoMetadados;
    
    Descritor descritorMetadados;
    
    char nomeArquivoMetadados[MAX_FILENAME_LENGTH];
    char comando[MAX_STRING_LENGTH];
    char* operacao;
    
    /*
        Recebe o nome do arquivo de metadados.
    */
    scanf("%s", nomeArquivoMetadados);

    /*
        Abre o arquivo de metadados para leitura.
    */
    arquivoMetadados = abrirArquivo(nomeArquivoMetadados, "r");

    /*
        Determina quais sao os tipos de dados da estrutura de dados descrita
        no arquivo de metadados.
    */
    descritorMetadados = determinarTiposdeDados(arquivoMetadados);
    
    /*
        Inicializa a matriz que armazenará o índice.
    */
    descritorMetadados.indice = NULL;

    /*
        Abre - ou cria, se ainda nao existir - o arquivo que sera usado para
        armazenar os registros inseridos.
    */
    arquivoRegistros = abrirArquivo(descritorMetadados.nomeArquivoRegistros, "ab+");
    
    /*
        Recebe os comandos e os executa.
    */
    do {
        
        /*
            Armazena em uma string o comando a ser executado, retirando, quando
            necessário, o caracter de retorno de carro - '\r' - do seu final.
        */
        fgets(comando, MAX_STRING_LENGTH, stdin);

        int ultimaPosicao = strlen(comando)-1;
        if (comando[ultimaPosicao] == '\r') {
            comando[ultimaPosicao] = '\0';
        }
        
        /*
            Identifica, com o auxílio da função, qual operação deve ser executada,
            chamando a função delegada ao tratamento dessa operação.
        */
        operacao = identificarOperacao(comando);
        
        if (strcmp(operacao, "insert") == 0) {
            inserirRegistro(comando, descritorMetadados, arquivoRegistros);
        }
        else if (strcmp(operacao, "index") == 0) {
            descritorMetadados.indice = indexarRegistro(descritorMetadados, arquivoRegistros, arquivoIndices);
        }
        else if (strcmp(operacao, "search") == 0) {
            descritorMetadados.indice = buscarRegistro(comando, descritorMetadados, arquivoRegistros);
        }
    } while (strcmp(operacao, "exit") != 0);
    
    
    /*
        Fecha os arquivos e desaloca as matrizes e vetores alocados dinamicamente
        ao longo da execução do programa.
    */
    free(operacao);
    desalocarMatriz(&descritorMetadados.indice, determinarQuantidadeRegistros(arquivoRegistros, determinarTamanhoRegistro(descritorMetadados)));
    fclose(arquivoRegistros);
    fclose(arquivoMetadados);

    return 0;
}
