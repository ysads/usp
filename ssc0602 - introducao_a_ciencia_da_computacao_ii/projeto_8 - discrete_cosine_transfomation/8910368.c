#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>


/*
	Descricao:
	 	Funcao que exibe uma determinada quantidade de itens de um vetor de modo invertido,
        isto e, do fim para o inicio. Os itens sao impressos com duas casas decimais de
        precisao.
	
	Parametros:
		float* vetor          : ponteiro para o vetor que sera exibido.
		int tamanho           : tamanho do vetor que sera exibido.
        int itensParaImprimir : quantidade de itens que devem ser exibidos.
		
	Retorno:
		Nao ha.
  
*/
void exibirVetorInvertido(float* vetor, int tamanho, int itensParaImprimir) {
    int i = 0;
    while (i < itensParaImprimir) {
        printf("%0.2f\n", vetor[tamanho-1-i]);
        i++;
    }
}


/*
	Descricao:
		Funcao responsavel por receber uma string, contendo um nome de arquivo, e usa-la
		para abrir este arquivo. No caso de o arquivo nao poder ser aberto, o programa se
		encerra.
		
	Parametros:
		char* nomeArquivo: string que contem o nome do arquivo que sera aberto.
		
	Retorno:
		Um ponteiro FILE* para o arquivo, caso este tenha sido aberto com sucesso. Se nao,
		nada e retornado.
		
*/
FILE* abrirArquivo(char* nomeArquivo) {
	FILE* arquivo;
	arquivo = fopen(nomeArquivo, "rb");
	if (arquivo == NULL) {
		printf("O arquivo nao pode ser lido!!\n");
		exit(1);
	}
	return arquivo;
}


/*
	Descricao:
		Funcao delegada a instanciacao de um vetor de floats, baseando-se no parametro que
		recebe e que define quantas posicoes ele deve ter. Caso a funcao, por algum motivo,
		nao consiga instanciar o vetor, o program se encerra.
	
	Parametros:
		int numPosicoes: tamanho que o vetor instanciado deve possuir.
	
	Retorno:
		Um ponteiro para o vetor instanciado, caso isso tenha ocorrido. Se a instanciacao
		nao puder ser realizada, nada e retornado.

*/
float* instanciarVetor(int numPosicoes) {
	float* vetor;
	vetor = (float*) malloc(numPosicoes * sizeof(float));
	if (vetor == NULL) {
        printf("O vetor nao pode ser alocado!\n");
		exit(1);
	}
	return vetor;
}


/*
	Descricao:
		Funcao que, com base no tamanho de um arquivo, calcula a quantidade de amplitudes
		sonoras que ele possui. Esse valor e obtido atraves da verificacao do tamanho total
		do arquivo, que, logo apos, e dividido pelo tamanho de um inteiro de 8 bits. Dessa
		forma, obtem-se quantos desses inteiros de 8 bits, isto e, quantas amplitudes, estao
		contidas no arquivo.
	
	Parametros:
		FILE* arquivo: ponteiro para o arquivo que contem os valores das amplitudes.
	
	Retorno:
		Inteiro que indica a quantidade de amplitudes que o arquivo lido possui.

*/
int determinarNumeroAmplitudes(FILE* arquivo) {
    int numBytesArquivo;
    int numAmplitudes;

	/*
        Move o ponteiro para o fim do arquivo em que estao os dados. SEEK_END e a constante que representa
        a posicao final do arquivo
    */
    fseek(arquivo, 0, SEEK_END);
    
    /*
        ftell e uma funcao que diz em que posicao do arquivo o ponteiro esta. Seu retorno corresponde, na
        pratica, a quantos bytes foram percorridos da posicao inicial ate a posicao atual. Assim, se o 
        ponteiro estiver no final do arquivo, ela retorna a quantidade de bytes que o arquivo possui no
        total, isto e, seu tamanho.    
    */
    numBytesArquivo = ftell(arquivo);
    
    /*
        Nesse ponto, o cast para inteiro e a multiplicacao por 1.0 sao necessarios para impedir a
        ocorrencia da divisao inteira, que pode resultar em zero
    */
    numAmplitudes = (int) (1.0 * numBytesArquivo) / sizeof(uint8_t);
    
    /*
        Retorna o ponteiro para o inicio do arquivo. Aqui, SEEK_SET e uma constante que indica o inicio
        do arquivo
    */
    fseek(arquivo, 0, SEEK_SET);

    return numAmplitudes;
}


/*
	Descricao:
		Funcao que desaloca o vetor recebido.
		
	Parametro:
		float** vetor: ponteiro para o vetor que deve ser desalocado.
		
	Retorno:
		Nao ha.

*/
void desalocarVetor(float** vetor) {
	free(*vetor);
}


/*
	Descricao:
		Procedimento que le, uma a uma, todas as amplitudes do audio armazenado no arquivo de
		som. Note que cada uma das amplitudes equivale a um inteiro de 8 bits, por isso,
		durante a leitura, e utilizada uma variavel auxiliar do tipo uint8_t, que possui este
		tamanho e que impede a leitura de dados extras ou incompletos.
	
	Parametros:
		FILE* arquivo	  : ponteiro para o arquivo de som que contem os dados das amplitudes
							sonoras do audio.
		float* amplitudes : vetor que armazenara as amplitudes que serao lidas do arquivo.
		int numAmplitudes : inteiro que indica quantas amplitudes devem ser lidas do arquivo.
	
	Retorno:
		Nao ha.
		
*/
void identificarAmplitudes(FILE* arquivo, float* amplitudes, int numAmplitudes) {
    int i;
    uint8_t aux;
    for (i = 0; i < numAmplitudes; i++) {
        fread(&aux, sizeof(uint8_t), 1, arquivo);
        amplitudes[i] = aux;
    }
}


/*
    Descricao:
        Procedimento delegado a aplicacao da transformada discreta do cosseno. Ele, basicamente,
        traduz a expressao matematica da transformacao em linguagem estruturada. Note que, por
        questoes de precisao, todas as variaveis inteiras sao convertidas em pontos flutuantes
        atraves da multiplicacao por 1.0. Alem disso, a constante M_PI, definida em math.h, e
        utilizada para representar, com precisao satisfatoria, o valor da constante matematica pi.
    
    Parametros:
        float* amplitudes              : vetor que contem os valores de amplitudes lidos do arquivo.
        float* amplitudesTransformadas : vetor que armazenara os valores de amplitudes finais, transformados.
        int numAmplitudes              : inteiro que indica a quantidade de amplitudes lidas.
    
    Retorno:
        Nao ha.

*/
void aplicarTransformadaDiscretaDoCosseno(float* amplitudes, float* amplitudesTransformadas, int numAmplitudes) {
    int k, n;
    float valorTransformado;
    
    for (k = 0; k < numAmplitudes; k++) {
        valorTransformado = 0.0;
        for (n = 0; n < numAmplitudes; n++) {
            valorTransformado += amplitudes[n] * cos((M_PI/(numAmplitudes * 1.0)) * (1.0 * n + 0.5) * (k * 1.0));
        }
        amplitudesTransformadas[k] = valorTransformado;
    }
}


/*
    Descricao:
        Funcao que, recursivamente, ordena um vetor. O algoritmo implementado corresponde ao
        QuickSort. O algoritmo funciona com base na divisao do vetor em particoes, que sao
        criadas atraves da escolha de um pivo. A direita dele, devem ficar todos os itens
        maiores do que ele; e, a sua esquerda, todos os menores.
        Essa estrategia se baseia no fato de que uma particao com um unico item sempre esta
        ordenada. Assim, o que a funcao faz e ir criando e organizando as particoes, de modo
        recursivo, ate que as particoes possuam apenas um item, o que garante a sua ordenacao.
    
    Parametros:
        float* v : particao do vetor que deve ser ordenada. Inicialmente, corresponde ao vetor
                   inteiro.
        int ini  : posicao inicial, no vetor, da particao que deve ser ordenada.
        int fim  : posicao final, no vetor, da particao que deve ser ordenada.
    
    Retorno:
        Nao ha.

*/
void ordenarVetor(float* v, int ini, int fim) {
    float pivo;
    int i = ini;
    int j = fim;
    float aux;
    
    /*
        Determina o pivo, que, nesse caso, corresponde ao elemento central da particao
    */
    pivo = v[(ini + fim)/2];
    
    /*
        Enquanto os elementos a esquerda do pivo nao forem todos menores do que ele, e,
        aqueles a direita, nao forem maiores do que ele, continua iterando pela particao,
        organizando-a segundo essa logica
    */
    while (i <= j) {
        /*
            Itera pela particao, em busca de elementos mal-posicionados, isto e, elementos
            a direita do pivo que sejam menores do que ele, ou elementos a esquerda do pivo
            que sejam maiores do que ele
        */
        while (v[i] < pivo && i < fim) {
            i++;
        }
        while (v[j] > pivo && j > ini) {
            j--;
        }
        /*
            Se dois elementos mal-posicionados forem localizados, eles sao trocados de posicao
        */
        if (i <= j) {
            aux = v[i];
            v[i] = v[j];
            v[j] = aux;
            i++;
            j--;
        }
    }
    /*
        Ao fim do loop acima, a particao estara organizada segundo o criteiro supracitado. Assim,
        sao feitas chamadas recursivas a funcao de ordenacao, passando, como particao, a lista de
        itens a direita e a esquerda do pivo, para que sejam organizadas segundo o mesmo criterio.
    */
    if (j > ini) {
        ordenarVetor(v, ini, j);
    }
    if (i < fim) {
        ordenarVetor(v, i, fim);
    }
}


int main() {

    FILE* arquivo;

    char nomeArquivo[50];
    int numAmplitudes;
    int numValoresParaImprimir;
    float *amplitudes;
    float *amplitudesTransformadas;
    
    scanf("%s", nomeArquivo);
    scanf("%d", &numValoresParaImprimir);
    
    /*
        Tenta abrir o arquivo sonoro com base no nome de arquivo passado.
    */
    arquivo = abrirArquivo(nomeArquivo);
    
    /*
        Determina o numero de amplitudes contidas no arquivo sonoro.
    */
    numAmplitudes = determinarNumeroAmplitudes(arquivo);
    
    /*
        Instancia os vetores que armazenarao as amplitudes lidas do arquivo e os seus valores
        apos a transformacao discreta do cosseno.
    */
    amplitudes = instanciarVetor(numAmplitudes);
    amplitudesTransformadas = instanciarVetor(numAmplitudes);
    
    /*
        Le as amplitudes do arquivo sonoro, armazenando-as em um dos vetores.
    */
    identificarAmplitudes(arquivo, amplitudes, numAmplitudes);

    /*
        Submete o vetor com as amplitudes lidas ao processo de transformacao discreta do cosseno,
        de modo que, apos a execucacao do algoritmo, o vetor 'amplitudesTransformadas' contera os
        valores das amplitudes transformadas.
    */
    aplicarTransformadaDiscretaDoCosseno(amplitudes, amplitudesTransformadas, numAmplitudes);
    
    /*
        Ordena o vetor que contem as amplitudes transformadas.
    */
    ordenarVetor(amplitudesTransformadas, 0, numAmplitudes-1);
    
    /*
        Exibe determinada quantidade de itens do vetor, a partir do seu fim. Isso se deve ao fato
        de que, apos a ordenacao, os maiores itens sempre estarao no fim do vetor. A quantidade de
        itens que deve ser impressa esta armazenada na variavel 'numValoresParaImprimir'.
    */
    exibirVetorInvertido(amplitudesTransformadas, numAmplitudes, numValoresParaImprimir);
    
    /*
        Desaloca os vetores utilizados, liberando espaco na memoria RAM.
    */
    desalocarVetor(&amplitudes);
    desalocarVetor(&amplitudesTransformadas);

}
