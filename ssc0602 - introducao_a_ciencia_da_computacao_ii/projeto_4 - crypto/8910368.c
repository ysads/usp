#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define NUM_CHAR 92


typedef int t_matriz;


/*
    Vetor responsavel pela correspondencia dos caracteres com os seus respectivos codigos. Note que
    a posicao de cada letra no vetor representa o seu correspondente codigo numerico
*/
char vetorConversao[NUM_CHAR] = {' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
   'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
   'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2',
   '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', '!', '\"', '#', '$', '%', '&', '\'',
   '(', ')', '*', '+', ',', '-', '.', '/', '[', '\\', ']', '_', '{', '|', '}'};


/*
    Descricao:
        Funcao delegada a alocar espaco na memoria heap para uma matriz com base nas suas dimensoes
        dadas pelos parametros linhas e colunas

    Parametros:
        t_matriz*** matriz: endereco do ponteiro que referencia a matriz a ser alocada
        int linhas        : numero de linhas da matriz a ser alocada
        int colunas       : numero de colunas da matriz a ser alocada

*/
int instanciarMatriz(t_matriz*** matriz, int linhas, int colunas) {
    int i;

    *matriz = (t_matriz**) malloc(sizeof(t_matriz*) * linhas);
    if (*matriz == NULL) {
        return 0;
    }

    for (i = 0; i < linhas; i++) {
        (*matriz)[i] = (t_matriz*) malloc(sizeof(t_matriz) * colunas);

        if ((*matriz)[i] == NULL) {
            return 0;
        }
    }
    return 1;
}


/*
    Descricao:
       Funcao responsavel por desalocar as matrizes alocadas na heap

    Parametros:
       t_matriz*** matriz: matriz a ser desalocada
       int linhas: quantidade de linhas da referida matriz  

*/
void desalocarMatriz(t_matriz*** matriz, int linhas) {
    int i;

    for (i = 0; i < linhas; i++) {
        free((*matriz)[i]);
    }
    free(*matriz);
}


/*
    Descricao:
        Funcao que escalona, individualmente, cada linha da matriz, encontrando o valor
        de cada variavel do sistema linear

    Parametros:
        t_matriz*** matriz: matriz de coeficientes do sistema linear
        int linhaAtual    : linha que sera escalonada
        int linhas        : numero de linhas que o sistema linear possui
*/
void ajustarLinha(t_matriz** matriz, int linhas, int linhaAtual) {
    int i, j, colunas;
    t_matriz aux;

    /*
        Define o numero de colunas do sistema para ser igual ao dobro de linhas
    */
    colunas = 2 * linhas;

    /*
        Divide todos os termos da linha pelo seu primeiro elemento nao-nulo. Dessa forma, o pivo
        torna-se um
    */
    aux = matriz[linhaAtual][linhaAtual];
    for (j = 0; j < colunas; j++) {
        matriz[linhaAtual][j] = matriz[linhaAtual][j] / aux;
    }

    /*
        Zera os outros elementos da coluna do pivo.
    */
    for (i = 0; i < linhas; i++) {
        if (i != linhaAtual) {
            aux = matriz[i][linhaAtual] * -1;
            for (j = 0; j < colunas; j++) {
                matriz[i][j] = matriz[i][j] + matriz[linhaAtual][j] * aux;
            }
        }
    }
}



/*
    Descricao:
        Funcao responsavel por resolver o sistema linear atraves do metodo de Gauss-Jordan

    Parametros:
        t_matriz*** matriz : matriz que contem os coeficientes das equacoes que compoem o sistema linear
        t_matriz*** solucao: matriz unidimensional em que sera armazenada a solucao do sistema linear
        int linhas         : numero de linhas do sistema

*/
void resolverSistemaLinear(t_matriz** matriz, int linhas, t_matriz** solucao) {
    int i, j, linhaAtual;
    int colunas = 2 * linhas;

    for (linhaAtual = 0; linhaAtual < linhas; linhaAtual++) {

        /*
            Verifica se o pivo desta linha ja e, naturalmente, nao-nulo. Se for, simplesmente ajusta a
            linha; do contrario, busca algum elemento nao-nulo, na mesma coluna, porem em outras linhas.
            Se algum for localizado, a linha e ajustada, se nao, o programa se encerra, pois nao e possivel
            resolver o sistema atraves do metodo de Gauss-Jordan
        */
        if (matriz[linhaAtual][linhaAtual] != 0) {
            ajustarLinha(matriz, linhas, linhaAtual);
        }
        else {

            /*
                Busca o primeiro elemento não-nulo da coluna desejada
            */
            i = linhaAtual + 1;
            while ((matriz[i][linhaAtual] == 0) && (i < linhas)) {
                i++;
            }

            if (i == linhas) {
                /*
                    Caso em que não há elementos não nulos na coluna. Assim, este sistema
                    torna-se impossível ou possível e indeterminado
                */
                exit(1);
            }
            else {
                t_matriz aux;
                for (j = 0; j < colunas; j++) {
                    aux = matriz[i][j];
                    matriz[i][j] = matriz[linhaAtual][j];
                    matriz[linhaAtual][j] = aux;
                }
                ajustarLinha(matriz, linhas, linhaAtual);
            }
        }        
    }

    /*
        Atribui a matriz de solucoes o valor da matriz inversa. Note que a matriz inversa
        esta localizada nas colunas da segunda metade da matriz que armazena o sistema linear.
        Por isso, sao copiados os itens localizados nas colunas 'colunas+j' da matriz principal
    */
    colunas = linhas;
    for (i = 0; i < linhas; i++) {
        for (j = 0; j < colunas; j++) {
            solucao[i][j] = matriz[i][colunas+j];
        }
    }
}


/*
    Descricao: 
        Funcao que recebe um caracter e o codifica, retornando o seu respectivo codigo
        associado
    
    Parametros:
        char caracter: caracter a ser codificado

*/
int codificarCaracter(char caracter) {
    int i;

    /*
        Itera por todos os itens do vetor de conversao buscando qual e a posicao do caracter que
        sera codificado. Essa posicao e retornada, assim que e localizada
    */
    for (i = 0; i < NUM_CHAR; i++) {
        if (vetorConversao[i] == caracter) {
            return i;
        }
    }
}


/*
    Descricao:
        Funcao que recebe o codigo de um caracter e retorna qual e o caracter associado a este
        codigo. Basicamente, essa funcao acessa, diretamente, no vetor de conversao, a posicao
        'codigo' e retorna qual e o caracter la contido

    Parametros:
        int codigo: codigo do caracter que deve ser decodificado
*/
char decodificarCaracter(int codigo) {
    return vetorConversao[codigo];
}


/*
    Descricao:
        Funcao delegada a calcular a dimensao das matrizes que sao utilizadas durante o processo de
        descriptografia dos dados. Para tanto, verifica-se o tamanho do arquivo bytes e, em seguida,
        divide-se esse valor pelo tamanho de um unico caracter, o que da a quantidade de caracteres no
        arquivo. Por fim, e obtida e retornada a raiz quadrada dessa quantidade, que, numericamente,
        equivale a dimensao buscada
        

    Parametros:
        FILE* arquivo: ponteiro para o arquivo que possui a mensagem a ser descriptografada

*/
int calcularDimensaoMatrizes(FILE* arquivo) {
    int numCaracteres;
    int tamanhoArquivo;

    /*
        Move o ponteiro para o fim do arquivo em que estao os dados. SEEK_END e a constante que representa
        a posicao final do arquivo
    */
    fseek(arquivo, 0, SEEK_END);

    /*
        ftell e uma funcao que diz em que posicao do arquivo o ponteiro esta. Seu retorno corresponde, na
        pratica, a quantos bytes foram percorridos da posicao inicial ate a posicao atual. Assim, se o 
        ponteiro estiver no final do arquivo, ela retorna a quantidade de bytes que o arquivo possui no
        total, isto e, seu tamanho.    
    */
    tamanhoArquivo = ftell(arquivo);

    /*
        Nesse ponto, o cast para inteiro e a multiplicacao por 1.0 sao necessarios para impedir a
        ocorrencia da divisao inteira, que pode resultar em zero
    */
    numCaracteres = (int) (1.0 * tamanhoArquivo / sizeof(char));

    /*
        Retorna o ponteiro para o inicio do arquivo. Aqui, SEEK_SET e uma constante que indica o inicio
        do arquivo
    */
    fseek(arquivo, 0, SEEK_SET);

    return (int) (sqrt(numCaracteres));
}


/*
    Descricao:
        Funcao que recebe os caracteres contidos no arquivo, converte-os para valores numericos e
        armazena-os numa matriz no arranjo correto para que sejam decodificados

    Parametros:
        t_matriz** matriz: ponteiro para a matriz em que ficarao armazenados os caracteres convertidos
                           para os seus equivalentes numericos
        int dimensao     : inteiro correspondente a dimensao da matriz que sera preenchida com os
                           caracteres convertidos
        FILE* arquivo    : ponteiro para o arquivo em que esta a mensagem codificada
*/
void receberCaracteres(FILE* arquivo, t_matriz** matriz, int dimensao) {
    int i, j;

    for (j = 0; j < dimensao; j++) {
        for (i = 0; i < dimensao; i++) {
            matriz[i][j] = codificarCaracter(fgetc(arquivo));
        }
    }
}


/*
    Descricao:
        Funcao responsavel por receber a matriz chave, isto e, a matriz de numeros que sera usada como
        chave para a descriptografia da mensagem

    Parametros:
        t_matriz** matriz: ponteiro para a matriz que armazena a chave de descriptografia
        int dimensao     : inteiro correspondente a dimensao da matriz que armazena a chave de
                           descriptografia

*/
void receberMatrizChave(t_matriz** matriz, int dimensao) {
    int i, j;
    
    for (i = 0; i < dimensao; i++) {
        for (j = 0; j < dimensao; j++) {
            scanf("%d", &matriz[i][j]);
        }
    }
}


/*
    Descricao:
        Funcao que calcula a matriz inversa de uma dada funcao, atraves da resolucao de um sistema
        linear. O que ela faz e criar uma matriz - chamada de matriz aumentada - que contem o mesmo
        numero de linhas que a matriz que sera invertida, porem, que possui o dobro de colunas. Assim,
        na primeira metade das suas colunas sao postos os valores da matriz que sera invertida, e, na
        sua segunda metade, os valores da matriz identidade de mesma ordem que a aquela que sera
        invertida.
        Por fim, chama-se a funcao que escalona essa matriz aumentada, de modo que a identidade se forme
        em sua primeira metade. Esse processo, gera a matriz inversa na segunda metade da matriz aumentada.
        Essa matriz inversa e, no fim, copiada para a matriz que originalmente armazenava a chave de
        descriptografia, de modo que ela, agora, armazene a sua inversa.

    Parametros:
        t_matriz** matrizChave: ponteiro para a matriz que armazena a chave de descriptografia, e que
                                deve ser invertida
        int linhas            : numero de linhas da matriz-chave
*/
void inverterMatrizChave(t_matriz** matrizChave, int linhas) {
    int i, j;
    int colunas;
    t_matriz** matrizAumentada;

    /*
        'colunas' vale o dobro de 'linhas' porque a matriz aumentada deve armazenar, simultaneamente,
        a matriz-chave e a matriz identidade de mesma ordem.
    */
    colunas = 2 * linhas;

    instanciarMatriz(&matrizAumentada, linhas, colunas);

    for (i = 0; i < linhas; i++) {

        /*
            Copia, para a primeira metade da matriz aumentada, os valores da matriz-chave
        */
        for (j = 0; j < linhas; j++) {
            matrizAumentada[i][j] = matrizChave[i][j];
        }

        /*
            Copia, para a segunda metade da matriz aumentada, os valores da matriz identidade que possui
            a mesma ordem da matriz-chave. Observe que a matriz identidade possui sua diagonal principal
            igual a 1 e todos os seus restantes elementos iguais a 0; por isso, so e adicionado 1 a matriz
            aumentada quando 'j' for igual a 'linhas+i', que o mesmo que dizer que o elemento 'matriz[i][j]'
            pertence a diagonal principal
        */
        for (j = linhas; j < colunas; j++) {
            if (j == linhas+i) {
                matrizAumentada[i][j] = 1;
            }
            else {
                matrizAumentada[i][j] = 0;
            }
        }
    }

    /*
        Chama a funcao responsavel por escalonar o sistema linear, o que copia a matriz inversa da
        matriz-chave para a variavel 'matrizChave', e, em seguida, desaloca a matriz aumentada
    */
    resolverSistemaLinear(matrizAumentada, linhas, matrizChave);
    desalocarMatriz(&matrizAumentada, linhas);
}


/*
    Descricao:
        Funcao que efetivamente decodifica a mensagem. Isso e feito atraves de uma multiplicacao matricial,
        em que a matriz-chave e multiplicada pela matriz codificada, sendo o resultado dessa operacao
        armazenado na variavel 'matrizDecodificada'.

    Parametros:
        t_matriz** matrizDecodificada: matriz, inicialmente vazia, que armazenara o resultado da multiplicacao
                                       matricial
        t_matriz** matrizCodificada  : matriz que armazena os codigos correspondentes a mensagem codificada
        t_matriz** matrizChave       : matriz que armazena a chave de descriptografia
        int dimensao                 : inteiro que indica a dimensao das matrizes que serao multiplicadas

*/
void resolverEquacaoMatricial(t_matriz** matrizDecodificada, t_matriz** matrizCodificada, t_matriz** matrizChave, int dimensao) {
    int i, j, k;
    int elementoNovo;

    /*
        Os dois loops mais externos controlam qual elemento da matriz decodificada sera calculado
    */
    for (i = 0; i < dimensao; i++) {
        for (j = 0; j < dimensao; j++) {
            elementoNovo = 0;

            /*
                Cada novo elemento da matriz decodificada e o resultado do somatorio do produto de cada
                elemento da coluna 'k' da matriz-chave pelo seu corresponde na linha 'k' da matriz codificada
            */
            for (k = 0; k < dimensao; k++) {
                elementoNovo += matrizChave[i][k] * matrizCodificada[k][j];
            }
            matrizDecodificada[i][j] = elementoNovo;
        }
    }
}


/*
    Descricao:
        Funcao que exibe a mensagem decodificada a partir da matriz decodificada

    Parametros:
        t_matriz** matrizDecodificada: matriz com os codigos da letras que compoem a mensagem decodificada
        int dimensao                 : inteiro que corresponde a dimensao da matriz que armazena os codigos
                                       da mensagem decodificada

*/
void exibirMensagemDecodificada(t_matriz** matrizDecodificada, int dimensao) {
    int i, j;

    for (j = 0; j < dimensao; j++) {
        for (i = 0; i < dimensao; i++) {
            /*
                Imprime, na tela, o caracter correspondente ao codigo armazenado na posicao [i][j] da matriz
                decodificada.
            */
            printf("%c", decodificarCaracter(matrizDecodificada[i][j]));
        }
    }
}


int main() {

    FILE* arquivo;
    t_matriz** matrizCodificada;
    t_matriz** matrizDecodificada;
    t_matriz** matrizChave;

    char nomeArquivo[50];
    int dimensao, i;

    scanf("%s", nomeArquivo);
    arquivo = fopen(nomeArquivo, "r");

    /*
        Verifica se o arquivo que contem a mensagem codificada foi aberto de maneira correta.
        Se nao o foi, a execucao do codigo e abortada.
    */
    if (arquivo == NULL) {
        printf("o arquivo não pode ser lido");
        exit(1);
    }

    /*
        Calcula a dimensao da matrizes a serem usadas durante a descriptografia
    */
    dimensao = calcularDimensaoMatrizes(arquivo);

    /*
        Instancia todas as matrizes principais que serao usadas durante a descriptografia
    */
    instanciarMatriz(&matrizCodificada, dimensao, dimensao);
    instanciarMatriz(&matrizDecodificada, dimensao, dimensao);
    instanciarMatriz(&matrizChave, dimensao, dimensao);

    /*
        Le, do arquivo, a mensagem criptografada, e, da entrada padrao do sistema, a
        matriz-chave de criptografia
    */
    receberCaracteres(arquivo, matrizCodificada, dimensao);
    receberMatrizChave(matrizChave, dimensao);

    /*
        Inverte a matriz que contem a chave de criptografia, de modo que ela, apos o processo,
        armazene a chave de descriptografia
    */
    inverterMatrizChave(matrizChave, dimensao);

    /*
        De posse da chave de descriptografia e da mensagem criptografada, resolve a equacao matricial
        que determina qual e a matriz que representa a mensagem descriptografada
    */
    resolverEquacaoMatricial(matrizDecodificada, matrizCodificada, matrizChave, dimensao);

    /*
        Exibe a mensagem descriptografada
    */
    exibirMensagemDecodificada(matrizDecodificada, dimensao);

    /*
        Desaloca as matrizes utilizadas durante a descriptografia
    */
    desalocarMatriz(&matrizCodificada, dimensao);
    desalocarMatriz(&matrizDecodificada, dimensao);
    desalocarMatriz(&matrizChave, dimensao);

    return 0;
}