/*
 * Criado por: Ygor Sad Machado (8910368)
 * Engenharia de Computacao
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main() {
  int num_linhas, i;
  unsigned long int linha_atual, linha_anterior, soma_parcial_linha, soma_total_linha;
  unsigned long int num_termos;
  unsigned long int* numeros;

  scanf("%d", &num_linhas);

  //Garante que o programa somente funcionara triangulos com numero
  //positivo de linhas
  if (num_linhas < 1) {
    exit(1);
  }

  //Calcula a quantidade de termos que deverao ser alocados no vetor
  //usando soma de termos de uma progressao aritmetica
  num_termos = ((1 + num_linhas) * num_linhas) / 2;

  
  numeros = (unsigned long int*) malloc(num_termos * sizeof(unsigned long int));
  
  //Inicializa as primeiras posicoes do vetor e as variaveis que farao
  //o controle de povoamento do vetor
  numeros[0] = 1;
  numeros[1] = 0;
  numeros[2] = 0;
  linha_atual = 1;
  linha_anterior = 0;

  for (i = 1; i < num_linhas; i++) {

    //Verifica qual e a soma dos elementos da linha atual. Esse valor
    //e importante para se saber quando a proxima linha comeca
    soma_total_linha = pow(2, i);
    soma_parcial_linha = 0;
    
    while (soma_parcial_linha < soma_total_linha) {

      //Essa condicao verifica se o elemento atual precisa ser calculado atraves
      //da soma dos dois outros imediatamente acima dele ou se ele vale 1
      if (soma_parcial_linha == 0 || soma_parcial_linha == soma_total_linha - 1) {
        numeros[linha_atual] = 1;
        linha_atual++;
        soma_parcial_linha++;
      }
      else {
        numeros[linha_atual] = numeros[linha_anterior] + numeros[linha_anterior+1];
        soma_parcial_linha += numeros[linha_atual];
        linha_anterior++;
        linha_atual++;
      }
    }
    linha_anterior++;
  }

  for (i = 0; i < num_termos; i++) {
    printf("%ld\n", numeros[i]);
  }

  return 0;
}
