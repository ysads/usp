/*
	# Método de Gauss-Jordan para Resolução #
		# de Sistemas Lineares #

	Nome: Ygor Sad Machado
	N° USP: 8910368
	Entrega: 03/09/2014
	Curso: Engenharia de Computação
*/


#include <stdio.h>
#include <stdlib.h>


void ajustarLinha(double*** matriz, int dimensao, int linhaAtual) {
    int i, j;
    double aux;

    //Faz o pivô valer 1
    aux = (*matriz)[linhaAtual][linhaAtual];
    for (j = 0; j <= dimensao; j++) {
        (*matriz)[linhaAtual][j] = (*matriz)[linhaAtual][j] / aux;
    }

    //Zera os outros elementos da coluna do pivô
    for (i = 0; i < dimensao; i++) {
        if (i != linhaAtual) {
            aux = (*matriz)[i][linhaAtual] * -1;
            for (j = 0; j <= dimensao; j++) {
                (*matriz)[i][j] = (*matriz)[i][j] + (*matriz)[linhaAtual][j] * aux;
            }
        }
    }
}


void ajustarMatriz(double*** matriz, int dimensao, int linhaAtual) {
    int i, j;

    if ((*matriz)[linhaAtual][linhaAtual] != 0) {
        ajustarLinha(matriz, dimensao, linhaAtual);
    }
    else {

        //Busca o primeiro elemento não-nulo da coluna desejada
        i = linhaAtual + 1;
        while (((*matriz)[i][linhaAtual] == 0) && (i < dimensao)) {
            i++;
        }

        if (i == dimensao) {
            exit(1);
        }
        else {
            double aux;
            for (j = 0; j <= dimensao; j++) {
                aux = (*matriz)[i][j];
                (*matriz)[i][j] = (*matriz)[linhaAtual][j];
                (*matriz)[linhaAtual][j] = aux;
            }
            ajustarLinha(matriz, dimensao, linhaAtual);
        }
    }
}


int main() {

    int i, j;
    int dimensao;
    double** matriz;

    scanf("%d", &dimensao);

    //Aloca a matriz de doubles
    matriz = (double**) malloc(sizeof(double*) * dimensao);
    if (matriz == NULL) {
        exit(1);
    }
    for (i = 0; i < dimensao; i++) {
        matriz[i] = (double*) malloc(sizeof(double) * (dimensao+1));
        if (matriz[i] == NULL) {
            exit(1);
        }
    }

    //Recebe os valores da matriz
    for (i = 0; i < dimensao; i++) {
        for (j = 0; j < dimensao; j++) {
            scanf("%lf", &matriz[i][j]);
        }
    }
    for (i = 0; i < dimensao; i++) {
        scanf("%lf", &matriz[i][dimensao]);
    }

    //Identifica cada solução do sistema linear
    for (i = 0; i < dimensao; i++) {
        ajustarMatriz(&matriz, dimensao, i);
    }

    //Exibe as soluções do sistema
    for (i = 0; i < dimensao; i++) {
        printf("%lf\n", matriz[i][dimensao]);
    }

    //Desaloca a matriz
    for (i = 0; i < dimensao; i++) {
        free(matriz[i]);
    }
    free(matriz);

    return 0;
}
