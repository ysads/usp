#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Estrutura que representa as matrizes. Ela possui três ponteiros que,
//dependendo da escolha do usuário, podem ou não ser alocados.
typedef struct matriz {
    int** i;
    char** c;
    double** d;
} Matriz;


//Aloca a matriz de acordo com a opção passada
//Para tanto, esta função recebe a struct com os ponteiros para os
//três tipos válidos de matrizes, alocando apenas aquele correspondente
//à opção selecionada pelo usuário, deixando os outros como NULL
void alocarMatriz(Matriz *mat, int opcao, int linhas, int colunas) {
    int i;
    switch (opcao) {
        case 1:
            mat->i = (int**) malloc(sizeof(int*) * linhas);
            if (mat->i == NULL) exit(1);
            for (i = 0; i < linhas; i++) {
                mat->i[i] = (int*) malloc(sizeof(int) * colunas);
                if (mat->i[i] == NULL) exit(1);
            }
            mat->c = NULL;
            mat->d = NULL;
            break;

        case 2:
            mat->c = (char**) malloc(sizeof(char*) * linhas);
            if (mat->c == NULL) exit(1);
            for (i = 0; i < linhas; i++) {
                mat->c[i] = (char*) malloc(sizeof(char) * colunas);
                if (mat->c[i] == NULL) exit(1);
            }
            mat->i = NULL;
            mat->d = NULL;
            break;

        case 3:
            mat->d = (double**) malloc(sizeof(double*) * linhas);
            if (mat->d == NULL) exit(1);
            for (i = 0; i < linhas; i++) {
                mat->d[i] = (double*) malloc(sizeof(double) * colunas);
                if (mat->d[i] == NULL) exit(1);
            }
            mat->c = NULL;
            mat->i = NULL;
            break;
    }
}


//Função delegada ao preenchimento da matriz. Ela usa a função rand() do C
//para gerar números aleatórios, permitindo que as matrizes inteiras recebam
//valores no intervalo [1..100]; que as matrizes de caracteres armazenem
//caracteres entre 'a' e 'z'; e que as matrizes de double possuam valores
//correspondentes à divisão entre números nos intervalos [0..99] e [1..100]
void preencherMatriz(Matriz *mat, int opcao, int linhas, int colunas) {
    int i, j;

    for (i = 0; i < linhas; i++) {
        for (j = 0; j < colunas; j++) {
            switch (opcao) {
                case 1:
                    mat->i[i][j] = rand() % 100 + 1;
                    break;

                case 2:
                    mat->c[i][j] = ((char) 'a' + (rand() % 26));
                    break;

                case 3:
                    mat->d[i][j] = ((double) (rand() % 100) / (rand() % 100 + 1));
                    break;
            }
        }
    }
}


//Função que mostra uma determinada matriz, de acordo com seu tipo, cuidando
//para que ela seja formatada de maneira amigável aos olhos
void apresentarMatriz(Matriz *mat, int opcao, int linhas, int colunas) {
    int i, j;

    for (i = 0; i < linhas; i++) {
        for (j = 0; j < colunas; j++) {
            switch (opcao) {
                case 1:
                    printf("%4d ", mat->i[i][j]);
                    break;

                case 2:
                    printf("%3c ", mat->c[i][j]);
                    break;

                case 3:
                    printf("%7.4lf ", mat->d[i][j]);
                    break;
            }
        }
        printf("\n");
    }
}


//Função responsável por desalocar a matriz
//Note que ela desaloca apenas o ponteiro que foi inicialmente alocado
void desalocarMatriz(Matriz *mat, int opcao) {
    switch (opcao) {
        case 1:
            free(mat->i);
            break;

        case 2:
            free(mat->c);
            break;

        case 3:
            free(mat->d);
            break;
    }
}


int main() {

    int opcao, colunas, linhas;
    Matriz mat;
    scanf("%d %d %d", &opcao, &linhas, &colunas);

    srand(time(NULL));

    alocarMatriz(&mat, opcao, linhas, colunas);
    preencherMatriz(&mat, opcao, linhas, colunas);
    apresentarMatriz(&mat, opcao, linhas, colunas);
    desalocarMatriz(&mat, opcao);

    return 0;
}

