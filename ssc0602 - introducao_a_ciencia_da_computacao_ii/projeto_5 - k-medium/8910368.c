#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <float.h>


/*
	Descricao:
	 	Funcao que exibe, na forma de um inteiro de 8 bits, um vetor de float recebido
	 	como parametro. Os dados sao escritos na saida padrao do sistema, identificada
	 	por stdout.
	
	Parametros:
		float* vetor: ponteiro para o vetor que sera exibido.
		int tamanho : tamanho do vetor que sera exibido.
		
	Retorno:
		Nao ha.
  
*/
void exibirVetor(float* vetor, int tamanho) {
    int i;
    uint8_t temp;
    for (i = 0; i < tamanho; i++) {
        temp = (uint8_t) vetor[i];
        fputc(temp, stdout);
    }
}


/*
	Descricao:
		Funcao responsavel por receber uma string, contendo um nome de arquivo, e usa-la
		para abrir este arquivo. No caso de o arquivo nao poder ser aberto, o programa se
		encerra.
		
	Parametros:
		char* nomeArquivo: string que contem o nome do arquivo que sera aberto.
		
	Retorno:
		Um ponteiro FILE* para o arquivo, caso este tenha sido aberto com sucesso. Se nao,
		nada e retornado.
		
*/
FILE* abrirArquivo(char* nomeArquivo) {
	FILE* arquivo;
	arquivo = fopen(nomeArquivo, "rb");
	if (arquivo == NULL) {
		printf("O arquivo nao pode ser lido!!\n");
		exit(1);
	}
	return arquivo;
}


/*
	Descricao:
		Funcao delegada a instanciacao de um vetor de floats, baseando-se no parametro que
		recebe e que define quantas posicoes ele deve ter. Caso a funcao, por algum motivo,
		nao consiga instanciar o vetor, o program se encerra.
	
	Parametros:
		int numPosicoes: tamanho que o vetor instanciado deve possuir.
	
	Retorno:
		Um ponteiro para o vetor instanciado, caso isso tenha ocorrido. Se a instanciacao
		nao puder ser realizada, nada e retornado.

*/
float* instanciarVetor(int numPosicoes) {
	float* vetor;
	vetor = (float*) malloc(numPosicoes * sizeof(float));
	if (vetor == NULL) {
        printf("O vetor nao pode ser alocado!\n");
		exit(1);
	}
	return vetor;
}


/*
	Descricao:
		Funcao que desaloca o vetor recebido.
		
	Parametro:
		float** vetor: ponteiro para o vetor que deve ser desalocado.
		
	Retorno:
		Nao ha.

*/
void desalocarVetor(float** vetor) {
	free(*vetor);
}


/*
	Descricao:
		Funcao que, com base no tamanho de um arquivo, calcula a quantidade de amplitudes
		sonoras que ele possui. Essa valor e obtido atraves da verificacao do tamanho total
		do arquivo, que, logo apos, e dividido pelo tamanho de um inteiro de 8 bits. Dessa
		forma, obtem-se quantos desses inteiros de 8 bits, isto e, quantas amplitudes, estao
		contidas no arquivo.
	
	Parametros:
		FILE* arquivo: ponteiro para o arquivo que contem os valores das amplitudes.
	
	Retorno:
		Inteiro que indica a quantidade de amplitudes que o arquivo lido possui.

*/
int determinarNumeroAmplitudes(FILE* arquivo) {
    int numBytesArquivo;
    int numAmplitudes;

	/*
        Move o ponteiro para o fim do arquivo em que estao os dados. SEEK_END e a constante que representa
        a posicao final do arquivo
    */
    fseek(arquivo, 0, SEEK_END);
    
    /*
        ftell e uma funcao que diz em que posicao do arquivo o ponteiro esta. Seu retorno corresponde, na
        pratica, a quantos bytes foram percorridos da posicao inicial ate a posicao atual. Assim, se o 
        ponteiro estiver no final do arquivo, ela retorna a quantidade de bytes que o arquivo possui no
        total, isto e, seu tamanho.    
    */
    numBytesArquivo = ftell(arquivo);
    
    /*
        Nesse ponto, o cast para inteiro e a multiplicacao por 1.0 sao necessarios para impedir a
        ocorrencia da divisao inteira, que pode resultar em zero
    */
    numAmplitudes = (int) (1.0 * numBytesArquivo) / sizeof(uint8_t);
    
    /*
        Retorna o ponteiro para o inicio do arquivo. Aqui, SEEK_SET e uma constante que indica o inicio
        do arquivo
    */
    fseek(arquivo, 0, SEEK_SET);

    return numAmplitudes;
}


/*
	Descricao:
		Esta funcao define a qual grupo uma dada amplitude esta associada. Isso e feito
		verificando-se qual e o grupo que esta mais proximo de cada amplitude. Caso mais
		de um grupo esteja a menor distancia da amplitude, o primeiro deles sera o escolhido.
		Note que 'menorDistancia' inicialmente vale FLT_MAX, que e uma constante que representa
		o maior valor que pode ser armazenado num float. Isso se faz necessario para que
		a menor distancia da amplitude a algum grupo seja obtida com certeza.
		
	Parametro:
		float* grupos   : ponteiro para o vetor que contem os valores de cada grupo.
		int numGrupos   : inteiro que representa quantos grupos existem.
		float amplitude : valor da amplitude que sera classificada.
		
	Retorno:
		Um float contem o numero do grupo ao qual a amplitude esta associada.
*/
float determinarGrupoAssociado(float* grupos, int numGrupos, float amplitude) {
    int i;
    int grupoMaisProximo = 0;
    float menorDistancia = FLT_MAX;
    
    /*
		Itera por todos os grupos, verificando, individualmente, a distancia de cada um
		deles a amplitude dada.
    */
    for (i = 0; i < numGrupos; i++) {
		if (abs(amplitude - grupos[i]) < menorDistancia) {
			menorDistancia = abs(amplitude - grupos[i]);
			grupoMaisProximo = i;
		}
    }
    return grupoMaisProximo;
}


/*
	Descricao: 
		Funcao que identifica a qual grupo cada uma das amplitudes armazenadas no vetor
		'amplitudes' pertence, armazenando estes valores no vetor 'gruposAmplitudes'. Note
		que essa informacao e obtida atraves de consultas sucessivas a funcao
		'determinarNumeroAmplitudes'.
		
	Parametros:
		float* gruposAmplitudes : vetor que armazena a qual grupo cada amplitude esta associada.
		float* amplitudes		: vetor que armazena os valores de cada amplitude lida.
		float* grupos			: vetor que contem os valores de cada um dos grupos.
		int numGrupos			: inteiro que indica quantos grupos existem.
		int numAmplitudes		: inteiro, representando a quantidade de amplitudes lidas.
		
	Retorno:
		Nao ha.

*/
void associarGruposAsAmplitudes(float* gruposAmplitudes, float* amplitudes, float* grupos, int numGrupos, int numAmplitudes) {
    int i;

	/*
		Itera por todos os elementos do vetor 'amplitudes', obtendo a qual grupo o i-esimo
		elemento desse vetor estara associado, e armazenando-o na i-esima posicao do vetor
		'gruposAmplitudes'
	*/
    for (i = 0; i < numAmplitudes; i++) {
        gruposAmplitudes[i] = determinarGrupoAssociado(grupos, numGrupos, amplitudes[i]);
    }
}


/*
	Descricao:
		Funcao que calcula os novos valores para os grupos com base na informacao armazenada
		no vetor 'gruposAmplitudes', que, por sua vez, indica a qual grupo esta relacionada
		cada amplitude. Matematicamente, o novo valor do i-esimo grupo corresponde a media
		das amplitudes associadas a este grupo.
	
	Parametros:
		float* gruposAmplitudes : vetor que indica a que grupo cada amplitude esta associada.
		float* amplitudes		: vetor que armazena os valores das amplitudes.
		float* novosGrupos		: vetor que armazenara os valores dos novos grupos.
		int numGrupos			: inteiro que indica quantos grupos existem.
		int numAmplitudes		: inteiro que representa quantas amplitudes foram lidas do arquivo
								  de entrada.
		
	Retorno:
		Nao ha.
	
*/
void calcularNovosValoresGrupos(float* gruposAmplitudes, float* amplitudes, float* novosGrupos, int numGrupos, int numAmplitudes) {
    int i, j;
    int qtdAmplitudes;
    float somaAmplitudes;

	/*
		Itera por cada um dos grupos, de modo a calcular o novo valor de cada um deles.
	*/
    for (i = 0; i < numGrupos; i++) {
        somaAmplitudes = 0;
        qtdAmplitudes = 0;

		/*
			Itera por todas amplitudes, verificando quais delas estao associadas ao grupo
			atualmente em analise. As que estiverem sao somadas.
		*/
        for (j = 0; j < numAmplitudes; j++) {
            if (gruposAmplitudes[j] == i) {
                somaAmplitudes += amplitudes[j];
                qtdAmplitudes++;
            }
        }
        
        /*
			Calcula, efetivamente, o novo valor de cada grupo, divindindo a soma das amplitudes
			pela quantidade de amplitudes que foram somadas, atribuindo esse valor a
			i-esima posicao do vetor 'novosGrupos'.
        */
        novosGrupos[i] = somaAmplitudes / qtdAmplitudes;
    }
}


/*
	Descricao:
		Funcao que recebe os novos valores de cada grupo e os atribui ao vetor 'grupos'.
		Alem disso, essa funcao tambem calcula a media da variacao dos valores de cada
		grupo, isto e, qual foi a diferenca media entre o valor novo e o valor antigo.
		de cada grupo.
	
	Parametros:
		float* novosGrupos : vetor que contem o novo valor calculado para cada grupo.
		int numGrupos	   : inteiro que representa quantos grupos existem.
		float* grupos	   : vetor que contem os valores atuais de cada grupo, e que,
							 findado a execucao da funcao, possuira os novos valores
							 dos grupos.
	
	Retorno:
		Um float que indica a variacao media dos valores dos grupos.

*/
float atualizarValoresGrupos(float* novosGrupos, float* grupos, int numGrupos) {
    int i;
    float somaVariacaoValoresGrupos = 0;

	/*
		Substitui o i-esimo valor de cada grupo pelo seu correspondente no vetor 'novosGrupos',
		alem de somar o modulo da diferenca entre esses valores.		
	*/
    for (i = 0; i < numGrupos; i++) {		
        somaVariacaoValoresGrupos += abs(novosGrupos[i] - grupos[i]);
        grupos[i] = novosGrupos[i];
    }
    
    /*
		Calcula a media de variacao dos valores dos grupos e a retorna.
    */
    return (somaVariacaoValoresGrupos / numGrupos);
}


/*
	Descricao:
		Procedimento que atualiza os valores das amplitudes com base no valor final do
		grupo ao qual cada uma delas esta associada.
	
	Parametros:
		float* amplitudes		: vetor que armazena o valor atual de cada amplitude, e
								  que, ao termino da execucao da funcao, contera seus
								  novos valores.
		float* gruposAmplitudes : vetor que armazena o grupo a que cada amplitude esta
								  associada.
		float* grupos			: vetor que armazena os valores finais dos grupos.
		int numAmplitudes		: inteiro que indica quantas amplitudes foram lidas.
	
	Retorno:
		Nao ha.
*/
void atualizarAmplitudes(float* amplitudes, float* gruposAmplitudes, float* grupos, int numAmplitudes) {
    int i;
    int posicaoGrupoAssociadoAmplitude;

	/*
		Itera por todas amplitudes, verificando a qual grupo cada uma esta associada,
		e substituindo seu atual valor pelo valor final desse grupo.
	*/
    for (i = 0; i < numAmplitudes; i++) {
        posicaoGrupoAssociadoAmplitude = gruposAmplitudes[i];
        amplitudes[i] = floor(grupos[posicaoGrupoAssociadoAmplitude]);
    }
}


/*
	Descricao:
		Funcao encumbida de executar o algoritmo k-medias. O que ela faz e associar cada
		amplitude a um grupo, calcular os novos valores dos grupos, verificar qual foi a
		variacao media do valor de cada grupo, e, com base neste ultimo valor, determinar
		se deve repetir, mais uma vez, o processo acima. O processo deixara de ser executado
		quando a variacao  media dos valores dos grupos for menor do que o valor armazenado
		em 'maxMediaAlteracaoGrupos'.
	
	Parametros:
		float* amplitudes			  : vetor que armazena as amplitudes lidas do arquivo de som.
		float* grupos				  : vetor que armazena, inicialmente, o valor inicial de cada
									    grupo.
		int numGrupos				  : inteiro que representa quantos grupos existem.
		int numAmplitudes			  : inteiro que representa quantas amplitudes foram identificadas
									    no arquivo de som.
		float maxMediaAlteracaoGrupos : float que representa a maxima media de variacao dos valores
										dos grupos.
		
	Retorno:
		Nao ha.

*/
void processarAudio(float* amplitudes, float* grupos, int numAmplitudes, int numGrupos, int maxMediaAlteracaoGrupos) {
    float* gruposAmplitudes;
    float* novosGrupos;

    float mediaAlteracaoGrupos;

	/*
		Instancia os vetores auxiliares que serao utilizados
	*/
    gruposAmplitudes = instanciarVetor(numAmplitudes);
    novosGrupos = instanciarVetor(numGrupos);

    do {
		/*
			Associa cada amplitude a um dos grupos
		*/
        associarGruposAsAmplitudes(gruposAmplitudes, amplitudes, grupos, numGrupos, numAmplitudes);
        
        /*
			Calcula o novo valor de cada um dos grupos
        */
        calcularNovosValoresGrupos(gruposAmplitudes, amplitudes, novosGrupos, numGrupos, numAmplitudes);
        
        /*
			Substitui os valores de cada grupo, calculando a variacao media deles
        */
        mediaAlteracaoGrupos = atualizarValoresGrupos(novosGrupos, grupos, numGrupos);        
    } while (mediaAlteracaoGrupos > maxMediaAlteracaoGrupos);

    atualizarAmplitudes(amplitudes, gruposAmplitudes, grupos, numAmplitudes);

    desalocarVetor(&gruposAmplitudes);
    desalocarVetor(&novosGrupos);
}


/*
	Descricao:
		Procedimento que le, uma a uma, todas as amplitudes do audio armazenado no arquivo de
		som. Note que cada uma das amplitudes equivale a um inteiro de 8 bits, por isso,
		durante a leitura, e utilizada uma variavel auxiliar do tipo uint8_t, que possui este
		tamanho e que impede a leitura de dados extras ou incompletos.
	
	Parametros:
		FILE* arquivo	  : ponteiro para o arquivo de som que contem os dados das amplitudes
							sonoras do audio.
		float* amplitudes : vetor que armazenara as amplitudes que serao lidas do arquivo.
		int numAmplitudes : inteiro que indica quantas amplitudes devem ser lidas do arquivo.
	
	Retorno:
		Nao ha.
		
*/
void identificarAmplitudes(FILE* arquivo, float* amplitudes, int numAmplitudes) {
    int i;
    uint8_t aux;
    for (i = 0; i < numAmplitudes; i++) {
        fread(&aux, sizeof(uint8_t), 1, arquivo);
        amplitudes[i] = aux;
    }
}


int main() {

    FILE* arquivo;

    float* amplitudes;
    float* grupos;

    float maxMediaAlteracaoGrupos;
    char nomeArquivo[60];
    int numAmplitudes;
    int numGrupos;
    int i;

	/*
		Recebe o nome do arquivo e tenta abri-lo.
	*/
    scanf("%s", nomeArquivo);
    arquivo = abrirArquivo(nomeArquivo);

	/*
		Determina quantas amplitudes estao contidas no arquivo recem-aberto, e instancia
		um vetor com essa quantidade de itens.
	*/
    numAmplitudes = determinarNumeroAmplitudes(arquivo);
    amplitudes = instanciarVetor(numAmplitudes);

	/*
		Le o numero de grupos existentes, instancia o vetor 'grupos' para armazena-los, e,
		apos, le, um a um, todos eles.	
	*/
    scanf("%d", &numGrupos);
    grupos = instanciarVetor(numGrupos);
    for (i = 0; i < numGrupos; i++) {
        scanf("%f", &grupos[i]);
    }
    
    /*
		Le o valor da maxima media de variacao dos valores dos grupos.
    */
    scanf("%f", &maxMediaAlteracaoGrupos);

	/*
		Le todas as amplitudes contidas no arquivo de som e as armazena no vetor correspondente.
	*/
    identificarAmplitudes(arquivo, amplitudes, numAmplitudes);
    
    /*
		Executa o algoritmo de k-medias sobre o vetor de amplitudes com base nos valores de grupos
		obtidos.
    */
    processarAudio(amplitudes, grupos, numAmplitudes, numGrupos, maxMediaAlteracaoGrupos);
    
    /*
		Exibe o vetor de amplitudes final.
    */
    exibirVetor(amplitudes, numAmplitudes);

	/*
		Desaloca todos os vetores instanciados e, depois, fecha o arquivo de audio.
	*/
    desalocarVetor(&amplitudes);
    desalocarVetor(&grupos);
    fclose(arquivo);

    return 0;

}