#include <stdio.h>
#include <stdlib.h>
#include <math.h>


typedef double t_matriz;


/*
    Descricao:
        Funcao que escalona, individualmente, cada linha da matriz, encontrando o valor
        de cada variavel do sistema linear, isto e, cada coeficiente do polinomio que sera
        interpolado

    Parametros:
        t_matriz*** matriz: matriz de coeficientes do sistema linear
        int linhaAtual    : linha do sistema linear que sera escalonada
        int dimensao      : numero de coeficientes do polinomio; coincide, tambem, como o numero de
                            equacoes do sistema linear
*/
void ajustarLinha(t_matriz** matriz, int dimensao, int linhaAtual) {
    int i, j;
    t_matriz aux;

    /*
        Dividem todos os termos da linha pelo primeiro elemento nao-nulo desta linha. Dessa forma, o
		pivo torna-se 1
    */
    aux = matriz[linhaAtual][linhaAtual];
    for (j = 0; j <= dimensao; j++) {
        matriz[linhaAtual][j] = matriz[linhaAtual][j] / aux;
    }

    /*
        Zera os outros elementos da coluna do pivo, através de escalonamentos
    */
    for (i = 0; i < dimensao; i++) {
        if (i != linhaAtual) {
            aux = matriz[i][linhaAtual] * -1;
            for (j = 0; j <= dimensao; j++) {
                matriz[i][j] = matriz[i][j] + matriz[linhaAtual][j] * aux;
            }
        }
    }
}



/*
    Descricao:
        Funcao responsavel por resolver o sistema linear atraves do metodo de Gauss-Jordan. Para isso,
		faz sucessivas chamadas a funcao ajustarLinha()

    Parametros:
        t_matriz*** matriz : matriz que contem os coeficientes das equacoes que compoem o sistema linear
        t_matriz*** solucao: matriz unidimensional em que sera armazenada a solucao do sistema linear
        int dimensao       : numero de coeficientes do polinomio; coincide, tambem, como o numero de
                             equacoes do sistema linear

*/
void resolverSistema(t_matriz** matriz, int dimensao, t_matriz** solucao) {
    int i, j, linhaAtual;

    for (linhaAtual = 0; linhaAtual < dimensao; linhaAtual++) {

        /*
            Verifica se o pivo desta linha ja e, naturalmente, nao-nulo. Se for, simplesmente ajusta a
            linha; do contrario, busca algum elemento nao-nulo, na mesma coluna, porem em outras linhas.
            Se algum for localizado, a linha e ajustada; se nao, o programa se encerra, pois nao e possivel
            resolver o sistema atraves do metodo de Gauss-Jordan
        */
        if (matriz[linhaAtual][linhaAtual] != 0) {
            ajustarLinha(matriz, dimensao, linhaAtual);
        }
        else {

            /*
                Busca o primeiro elemento não-nulo da coluna em questao
            */
            i = linhaAtual + 1;
            while ((matriz[i][linhaAtual] == 0) && (i < dimensao)) {
                i++;
            }

            if (i == dimensao) {
                /*
                    Caso em que nao ha elementos não nulos nessa coluna. Assim, este sistema
                    torna-se impossivel ou possível e indeterminado, e, portanto, nao
					se pode aplicar, nele, o metodo de Gauss-Jordan
                */
                exit(1);
            }
            else {
				/*
					Nessa situacao, foi localizado algum elemento nao-nulo em outra coluna. Por
					isso, substitui-se a linha que esta sendo analisada - e que possui o pivo
					nulo - por aquela que contem um elemento nao-nulo
				*/
                t_matriz aux;
                for (j = 0; j <= dimensao; j++) {
                    aux = matriz[i][j];
                    matriz[i][j] = matriz[linhaAtual][j];
                    matriz[linhaAtual][j] = aux;
                }
                ajustarLinha(matriz, dimensao, linhaAtual);
            }
        }        
    }

    /*
        Atribui ao vetor de solucoes a solucao do sistema linear, isto e, o valor de cada coeficiente
		do polinomio interpolado
    */
    for (linhaAtual = 0; linhaAtual < dimensao; linhaAtual++) {
        solucao[linhaAtual][0] = matriz[linhaAtual][dimensao];
    }
}



/*
    Descricao:
       Dada uma lista de pontos no plano cartesiano e o numero de coeficientes de um polinomio,
       essa funcao e capaz de gerar um sistema linear de equacoes que representam esse dado
       polinomio

    Parametros:
       t_matriz*** equacoes: representa a matriz na qual sera armazenado o sistema linear
       t_matriz*** pontos  : matriz de pontos no plano cartesiano
       int coeficientes    : numero de coeficientes do polinomio que deve ser interpolado

*/
void montarEquacoes(t_matriz** equacoes, t_matriz** pontos, int coeficientes) {
    int i, j;
    t_matriz abscissa;
    
    for (i = 0; i < coeficientes; i++) {
        abscissa = pontos[i][0];
		
        /*
            Calcula os coeficientes de cada equacao do sistema linear. Note que o j-esimo
            coeficiente deve corresponder a abscissa elevada a sua respectiva potencia.
			Dessa forma, o parametro da funcao pow() e 'coeficientes-j-1' para que os
			primeiros coeficientes estejam elevados as maiores potencias, ao passo que,
			os ultimos, as menores
        */
        for (j = 0; j < coeficientes; j++) {
            equacoes[i][j] = pow(abscissa, coeficientes-j-1);
        }

        /*
            Adiciona a matriz de coeficientes do sistema linear o valor da ordenada do ponto,
			isto e, o valor da funcao equivalente ao polinomio que se deseja interpolar para
			a abscissa considerada
        */
        equacoes[i][coeficientes] = pontos[i][1];
    }
}



/*
    Descricao:
        Funcao delegada a alocar espaco na memoria heap para uma matriz, baseando-se nas suas
		dimensoes, dadas pelos parametros linhas e colunas

    Parametros:
        t_matriz*** matriz: endereco do ponteiro que referencia a matriz a ser alocada
        int linhas        : numero de linhas da matriz a ser alocada
        int colunas       : numero de colunas da matriz a ser alocada

*/
int instanciarMatriz(t_matriz*** matriz, int linhas, int colunas) {
    int i;

	/*
		Aloca o espaco para todas as linhas da matriz, verificando se esta alocacao foi possivel
	*/
    *matriz = (t_matriz**) malloc(sizeof(t_matriz*) * linhas);
    if (*matriz == NULL) {
        return 0;
    }

    for (i = 0; i < linhas; i++) {
		/*
			Aloca, dentro de cada linha, o espaco correspondente as colunas da matriz
		*/
        (*matriz)[i] = (t_matriz*) malloc(sizeof(t_matriz) * colunas);

        if ((*matriz)[i] == NULL) {
            return 0;
        }
    }
    return 1;
}



/*
    Descricao:
       Funcao responsavel por desalocar as matrizes alocadas na heap

    Parametros:
       t_matriz*** matriz: matriz a ser desalocada
       int linhas		 : quantidade de linhas da referida matriz  

*/
void desalocarMatriz(t_matriz*** matriz, int linhas) {
    int i;

	/*
		Desaloca, individualmente, cada uma das linhas da matriz
	*/
    for (i = 0; i < linhas; i++) {
        free((*matriz)[i]);
    }
    free(*matriz);
	*matriz = NULL;
}



int main() {

    int i, j, numCoeficientes;
    t_matriz** pontos;
    t_matriz** equacoes;
    t_matriz** solucoes;

    scanf("%d", &numCoeficientes);
	

    /*
        Instancia as matrizes que armazenarão os dados 
    */
    if (!instanciarMatriz(&equacoes, numCoeficientes, numCoeficientes+1)) {
        exit(1);
    }
    if (!instanciarMatriz(&pontos, numCoeficientes, 2)) {
        exit(1);
    }
    if (!instanciarMatriz(&solucoes, numCoeficientes, 1)) {
        exit(1);
    }

    for (i = 0; i < numCoeficientes; i++) {        
        scanf("%lf", &pontos[i][0]);
        scanf("%lf", &pontos[i][1]);
    }


    /*
        Identifica e resolve o sistema linear que representa os coeficientes do polinomio
    */
    montarEquacoes(equacoes, pontos, numCoeficientes);
    resolverSistema(equacoes, numCoeficientes, solucoes);


    /*
        Exibe a solucao do sistema linear, isto e, quais sao os coeficientes do polinomio
    */
    for (i = 0; i < numCoeficientes; i++) {
        printf("%lf\n", solucoes[i][0]);
    }

    
    /*
        Desaloca todas as matrizes anteriormente instanciadas
    */
    desalocarMatriz(&equacoes, numCoeficientes);
    desalocarMatriz(&solucoes, numCoeficientes);
    desalocarMatriz(&pontos, numCoeficientes);

    return 0;
}