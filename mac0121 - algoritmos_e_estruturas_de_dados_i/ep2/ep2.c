#include <stdlib.h>
#include <stdio.h>

#define MAX_DIM 100
#define BLOCKED_CELL '!'
#define HORIZONTAL 0
#define VERTICAL 1


/**
 * This is a custom datatype used to indicate the words that could be
 * correctly placed within the board. To understand it, think of them
 * like math vectors, that start in a given position and have an
 * orientation. Thus if we have a word_vector(initial_cell: 10, orientation: 1),
 * we know the board has a vertical word starting at the cell 10.
 *
 * 0 - Horizontal
 * 1 - Vertical
 */
typedef struct {
    int initial_cell;
    int orientation;
} word_vector;


/**
  =====================================================
       STACK SPECIFIC FUNCTIONS AND DECLARATIONS     
  =====================================================
 */
#define stack_type word_vector

typedef struct st_stack_item {
    stack_type value;
    struct st_stack_item *next;
} stack_item;

typedef struct st_stack {
    stack_item *top;
    int size;
} stack;

/**
 * Creates a new stack, allocating memory to it and making
 * its size to start in 0. This is needed to prevent memory
 * errors coming from variables instatiated with dump.
 * 
 * @return stack* : a pointer to the stack just created
 */
stack* stack_create() {
    stack *s = (stack*) malloc(sizeof(stack));
    
    s->size = 0;
    s->top = NULL;
    return s;
}

/**
 * Responds whether a stack has something attached or not. Note that 'having
 * something attached' is equivalent to say that stack has a non-nullable top
 * 
 * @param stack *s  : a stack pointer
 * @return int      : a booleany representing whether stack has something
 */
int stack_is_empty(stack *s) {
    return (s->top == NULL);
}

/**
 * Returns how many items are chained in given stack
 * 
 * @param stack*s   : a pointer to a stack
 * @return int      : an int representing the number items chained in stack
 */
int stack_height(stack *s) {
    return s->size;
}

/**
 * Inserts a new value in given stack. Note it instantiates a new
 * stack_item and make it the stack's new top, chaining it at the
 * beginning of the stack 
 * 
 * @param stack *s          : a pointer to the stack that will receive value
 * @param stack_type value  : the value we'll insert into stack
 * @return stack*           : the stack that received the value
 */
stack* stack_push(stack *s, stack_type value) {
    stack_item *item = (stack_item*) malloc(sizeof(stack_item));

    item->value = value;
    item->next = s->top;
    
    s->top = item;
    s->size++;
    
    return s;
}

/**
 * Extracts the topmost value stored in stack. Note this will
 * return NULL if stack is empty
 * 
 * @param stack *s      : stack from which we will get the top
 * @return stack_type   : the item at the top of stack
 */
stack_type stack_top(stack *s) {
    stack_type null_vector = { -1, -1 };

    if (stack_is_empty(s)) {
        return null_vector;
    } else {
        return s->top->value;
    }
}

/**
 * Removes the topmost item of stack, returning it. This
 * function also disallocates the memory once occupied by 
 * the then topmost item, guaranteeing no memory is leaked
 * 
 * @param stack *s      : a pointer to the stack to be popped
 * @return stack_type   : the then topmost value in stack
 */
stack_type stack_pop(stack *s) {
    stack_item* popped_item = s->top;
    stack_type null_vector = { -1, -1 };
    stack_type popped_value;

    if (stack_is_empty(s)) {
        return null_vector;
    } else {
        popped_value = popped_item->value;
        
        s->top = popped_item->next;
        s->size--;

        free(popped_item);
        return popped_value;
    }
}

/**
 * A function that desallocates memory used by a stack
 * data structure
 * 
 * @param stack* s : a pointer representing a stack
 * @return void
 */
void stack_destroy(stack *s) {
    stack_item *current_item = s->top;
    stack_item *aux;

    while (current_item != NULL) {
        aux = current_item->next;
        free(current_item);
        current_item = aux;
    }
    free(s);
}
/**
  ===========================================================
        END OF STACK SPECIFIC FUNCTIONS AND DECLARATIONS     
  ===========================================================
 */


/**
 * Allocates memory for an array with m lines and n columns
 * 
 * @param  m: number of lines in array
 * @param  n: number of columns in array
 * @return a pointer to the just-allocated array
 */
char** create_array(int m, int n) {
    int i;
    char** array;

    array = (char **) malloc(m * sizeof(char*));
    for (i = 0; i < m; i++) {
        array[i] = (char*) malloc(n * sizeof(char));
    }
    return array;
}

/**
 * Destroy a bidimensional array, disallocating memory for each of its
 * lines, and then disallocating memory for the array pointer itself
 * 
 * @param array : the game array
 * @param m     : integer representing number of lines in array
 */
void destroy_array(char** array, int m) {
    int i;
    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
}


/**
 * Prints the board according to the display rules, that is
 * if an empty cell is found, prints * instead of BLOCKED_CELL
 * character
 * 
 * @param board : the board matrix
 * @param m     : number of lines in the board
 * @param n     : number of columns in the board
 */
void print_board(char** board, int m, int n) {
    int i, j;

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            if (board[i][j] == BLOCKED_CELL) {
                printf("* ");
            }
            else {
                printf("%c ", board[i][j]);
            }
        }
        printf("\n");
    }
}


/**
 * Checks if a given cell is available to be filled. This implies
 * such cell to be either empty or equals to the char we're trying
 * to put into the board
 *
 * @param cell      : the cell we are examinating 
 * @param word_char : the char we are trying to put into the cell
 * @return if this cell is fillable by this char
 */
int available_cell(char cell, char word_char) {
    return ((word_char != '\0') && (cell == '0' || cell == word_char));
}


/**
 * This is an auxiliary function that checks if the word is maximal.
 * Id est, if there will be free space in front of word when we write
 * it in the board
 * 
 * @param  board     : the current board
 * @param  word_char : the last word's char
 * @param  row       : the row of the position under analysis
 * @param  col       : the column of the position under analysis
 * @param  m         : number of rows in the board
 * @param  n         : number of columns in the board
 * @return if the word's termination char would be on top of a blocked
 *         cell, if we were to write it on the board
 */
int is_word_maximal(char** board, char word_char, int row, int col, int m, int n) {
    
    if (word_char == '\0') {
        /**
         * A word is maximal when it perfectly fits all of column or line
         * or if it fits all available spaces between two blocked cells
         */
        return (row == m || col == n || board[row][col] == BLOCKED_CELL);
    }
    return 0;
}


/**
 * Checks wether the current word can be written horizontally starting
 * at a given position, expressed by row and col params. To do that,
 * keeps writing the word until it ends or the cell becomes
 * unfillable for a given char 
 * 
 * @param  board        : the current crossword board
 * @param  word         : the word we want to know if it fits on board
 * @param  m            : number of rows in the board
 * @param  n            : number of columns in the board
 * @param  initial_cell : the cell from which the word should be written
 * @param  orientation  : the orientation the word should have
 * @return whether the word fits the available space or not
 */
int is_there_space_for_word(char** board, char* word, int m, int n, int initial_cell, int orientation) {
    int row, col, i;

    /**
     * Converts the 1-dimensional index into a 2-dimensional
     * (row, col) pair, so we can access position in matrix
     */
    row = initial_cell / n;
    col = initial_cell % n;
    i = 0;

    /**
     * Decides whether the should run through matrix's rows
     * or columns, based on desired orientation
     */
    if (orientation == HORIZONTAL) {
        while (col < n && available_cell(board[row][col], word[i])) {
            col++;
            i++;
        }    
    }
    else {
        while (row < m && available_cell(board[row][col], word[i])) {
            row++;
            i++;
        }
    }
    return is_word_maximal(board, word[i], row, col, m, n);
}


/**
 * Checks whether a given word can be placed in any of the available spaces on board.
 * To do that, simply runs through the boarding cells until it finds an available
 * space for the given word
 * 
 * @param  placed_words : a stack containing the positions in which the words were placed
 * @param  board        : the current board
 * @param  word         : the word we want to check
 * @param  m            : number of rows in the board
 * @param  n            : number of columns in the board
 * @param  initial_cell : 1-dimen index of the cell from which we should start our checking
 * @param  board_limit  : 1-dimen index of the last possible cell to check
 * @return whether the word can be written starting from a given position
 */
int can_write_word(stack* placed_words, char** board, char* word, int m, int n, int initial_cell, int board_limit) {
    int row, col, i;

    while (initial_cell <= board_limit) {

        /**
         * If there's space for this word from the current initial cell, then we push
         * a word_vector {initial_state, orientation} representing the writing
         * possibility found for this word and then we stop function execution
         */
        if (is_there_space_for_word(board, word, m, n, initial_cell, HORIZONTAL)) {
            word_vector word_position = { initial_cell, HORIZONTAL };

            stack_push(placed_words, word_position);
            return 1;
        }
        else if (is_there_space_for_word(board, word, m, n, initial_cell, VERTICAL)) {
            word_vector word_position = { initial_cell, VERTICAL };

            stack_push(placed_words, word_position);
            return 1;
        }
        initial_cell++;
    }

    /**
     * If this point is reached, it means we ran through all of the board without
     * finding a space for this word to be written, which means in this current
     * boarding setting, we can't write this word
     */
    return 0;
}


/**
 * Writes a given word in the board according to a given word_vector.
 * That is, from a specified cell starts to write a word horizontally
 * or vertically
 * 
 * @param board    : the current board
 * @param word     : the word to be written
 * @param position : the vector specification, saying from which cell
 *                   word should be written and in which orientation
 * @param  m       : the number of rows in the board
 * @param  n       : the number of columns in the board
 */
void write_word(char** board, char* word, word_vector position, int m, int n) {
    int row, col, i;

    /**
     * Converts the 1-dimensional index into a 2-dimensional
     * (row, col) pair, so we can access position in matrix
     */
    row = position.initial_cell / n;
    col = position.initial_cell % n;
    i = 0;
    
    if (position.orientation == HORIZONTAL) {
        
        while (word[i] != '\0') {
            board[row][col] = word[i];
            col++;
            i++;
        }
    }
    else {
        while (word[i] != '\0') {
            board[row][col] = word[i];
            row++;
            i++;
        }
    }
}


/**
 * Checks if a given character is alphabetic. To do this, simply
 * verifiy if it's neither an empty cell or an empty space
 * 
 * @param  c : the character we're testing
 * @return whether the given char is alphabetic or not
 */
int is_alphabetic(char c) {
    return (c != BLOCKED_CELL && c != '0');
}


/**
 * Erases a given word from the board. Note this function takes into
 * account if word shares characters with another word. If so, simples
 * hops to next char
 * 
 * @param board    : the current board
 * @param word     : word that should be erased
 * @param position : a vector describing the word writing position
 * @param m        : number of lines in the board
 * @param n        : number of columns in the board
 */
void erase_word(char** board, char* word, word_vector position, int m, int n) {
    int row, col, i;

    /**
     * Converts the 1-dimensional index into a 2-dimensional
     * (row, col) pair, so we can access position in matrix
     */
    row = position.initial_cell / n;
    col = position.initial_cell % n;
    i = 0;

    /**
     * Note we use the word as guide to erase the matching
     * character from board
     */
    if (position.orientation == HORIZONTAL) {

        while (word[i] != '\0') {
            board[row][col] = '0';
            col++;
            i++;
        }
    }
    else {
        while (word[i] != '\0') {
            board[row][col] = '0';
            row++;
            i++;
        }
    }
}


/**
 * Based on a list of words and the structure of board, decides whether this
 * board can be arranged as a crossword game using the given words. To do that
 * iterates over the words, trying to understand if they can be written in any
 * of the available places in the board. If it can, goes to the next word. 
 * Otherwise, tries to backtrack until it reaches the first word. In case it
 * reached the first word, it means there's no solution for this setting, and
 * no rearrange can solve the puzzle.
 * 
 * @param  board     : the board structure with only empty or blocked cells
 * @param  words     : list of the words to try to place on top of board
 * @param  m         : number of lines in the board
 * @param  n         : number of columns in the board
 * @param  num_words : number of words to place
 * @return whether this scenario can be solved or not
 */
int is_solvable(char** board, char** words, int m, int n, int num_words) {
    int curr_word;
    int words_left, initial_cell, board_limit, solvable;

    word_vector position;

    /**
     * Instantiates an auxiliary stack that will hold the 1-dimensional
     * start position of the successfully-placed-into-board words 
     */
    stack *placed_words = stack_create();

    board_limit = m * n - 1;
    initial_cell = 0;
    curr_word = 0;

    /**
     * Control variables
     */
    words_left = 1;
    solvable = 1;

    while (words_left && solvable) {

        /**
         * It makes sense to try to write a word only if we haven't
         * reached the end of the matrix yet
         */
        if (initial_cell <= board_limit) {

            while (can_write_word(placed_words, board, words[curr_word], m, n, initial_cell, board_limit)) { 
                /**
                 * Effectively writes the word in the available position.
                 * Note the function above inserts this position into the
                 * stack, in such a way we only have to send its top to
                 * write_word function
                 */
                
                write_word(board, words[curr_word], stack_top(placed_words), m, n);

                /**
                 * Updates the word and resets the initial_cell counter,
                 * so we start to look for word position in the beginning
                 */
                curr_word++;
                initial_cell = 0;

                /**
                 * If we already wrote all of the available words, then
                 * the work is over and we can set the flag to stop the loop
                 */
                if (curr_word == num_words) {
                    words_left = 0;
                    break;
                }
            }
        }

        /**
         * If this is true, then we could not write current_word in
         * given the board's current situation. Either we should
         * backtrack and retry to write the last written word or
         * we have no solution for this crossword
         */
        if (words_left) {
            
            /**
             * If neither a single word has been written or, after
             * trying to write other words we got back to the first
             * word in the list, we can surely say this is unsolvable
             */
            if (curr_word == 0) {
                solvable = 0;
            }
            else {
                /**
                 * Backtracks to the last placed_word and pops its
                 * placed position from stack because we know that
                 * the place it was written in is bad
                 */
                curr_word--;
                position = stack_pop(placed_words);

                /**
                 * After, we should erase the word from board, so
                 * that it's back to its previous state, and increment
                 * the initial_cell to the cell right after the one
                 * it erroneously inserted in our last attempt
                 */
                erase_word(board, words[curr_word], position, m, n);
                initial_cell = position.initial_cell + 1;
            }
        }
    }

    /**
     * Destroy the stack used to store the correctly placed
     * words, so that no memory leaks
     */
    stack_destroy(placed_words);

    return solvable;
}


int main() {

    int cell;
    int m, n, p;
    int i, j, k;
    char** board;
    char** words;

    k = 0;

    scanf("%d %d", &m, &n);
    
    /**
     * Iterates over all given inputs until it receives a 0-dimen matrix
     */
    while (m != 0 && n != 0) {
        board = create_array(m, n);

        /**
         * Receives each value composing the board. Note that instead
         * of relying in -1 as the marker character for blocked cell,
         * we define BLOCKED_CELL. By doing so, we can safely mantain
         * the whole board with the char datatype, avoiding problems
         * with -1 being a composite character
         */
        for (i = 0; i < m; i++) {
            for (j = 0; j < n; j++) {
                scanf("%d", &cell);

                if (cell == -1) {
                    board[i][j] = BLOCKED_CELL;
                }
                else {
                    board[i][j] = '0';
                }
            }
        }

        scanf("%d", &p);
        words = create_array(p, MAX_DIM);

        /**
         * Required to avoid the "\n" inputed after `p` value
         * to be read as the first word
         */
        cell = getchar();

        /**
         * Gets each word that we'll try to put in the board
         */
        for (i = 0; i < p; i++) {
            gets(words[i]);
        }

        printf("Instancia %d\n", ++k);
        if (is_solvable(board, words, m, n, p)) {
            print_board(board, m, n);
            printf("\n");
        }
        else {
            printf("nao ha solucao\n");
        }
        
        /**
         * Destroys the pre-allocated arrays so that we don't
         * take on risks with memory leaks
         */
        destroy_array(board, m);
        destroy_array(words, p);

        /**
         * Reads the next input
         */
        scanf("%d %d", &m, &n);
    }
    
    return 0;
}