#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


/**
 * Creates a new stack, allocating memory to it and making
 * its size to start in 0. This is needed to prevent memory
 * errors coming from variables instatiated with dump.
 * 
 * @return stack* : a pointer to the stack just created
 */
stack* create_stack() {
	stack *s = (stack*) malloc(sizeof(stack));
	
	s->size = 0;
	s->top = NULL;
	return s;
}


/**
 * Responds whether a stack has something attached or not. Note that 'having
 * something attached' is equivalent to say that stack has a non-nullable top
 * 
 * @param stack *s 	: a stack pointer
 * @return int 		: a booleany representing whether stack has something
 */
int is_empty_stack(stack *s) {
	return (s->top == NULL);
}


/**
 * Returns how many items are chained in given stack
 * 
 * @param stack*s 	: a pointer to a stack
 * @return int 		: an int representing the number items chained in stack
 */
int height_of_stack(stack *s) {
	return s->size;
}


/**
 * Inserts a new value in given stack. Note it instantiates a new
 * stack_item and make it the stack's new top, chaining it at the
 * beginning of the stack 
 * 
 * @param stack *s 			: a pointer to the stack that will receive value
 * @param stack_type value 	: the value we'll insert into stack
 * @return stack* 			: the stack that received the value
 */
stack* insert_into_stack(stack *s, stack_type value) {
	stack_item *item = (stack_item*) malloc(sizeof(stack_item));

	item->value = value;
	item->next = s->top;
	
	s->top = item;
	s->size++;
	
	return s;
}


/**
 * Extracts the topmost value stored in stack. Note this will
 * return NULL if stack is empty
 * 
 * @param stack *s 		: stack from which we will get the top
 * @return stack_type 	: the item at the top of stack
 */
stack_type top_of_stack(stack *s) {
	if (is_empty_stack(s)) {
		return empty_value;
	} else {
		return s->top->value;
	}
}


/**
 * Removes the topmost item of stack, returning it. This
 * function also disallocates the memory once occupied by 
 * the then topmost item, guaranteeing no memory is leaked
 * 
 * @param stack *s 		: a pointer to the stack to be popped
 * @return stack_type 	: the then topmost value in stack
 */
stack_type pop_from_stack(stack *s) {
	stack_item* popped_item = s->top;
	stack_type popped_value;

	if (is_empty_stack(s)) {
		return empty_value;
	} else {
		popped_value = popped_item->value;
		
		s->top = popped_item->next;
		s->size--;

		free(popped_item);
		return popped_value;
	}
}


/**
 * Prints a stack in such a way that the topmost item appears
 * before its next counterparts
 * 
 * @param stack* s : a pointer to the stack that will be printed
 * @return void
 */
void print_stack(stack *s) {
	int i;
	stack_item *current_item = s->top;

	printf("\n [");
	for (i = 0; i < s->size; i++) {
		printf("%d", current_item->value);

		if (current_item->next != NULL) {
			printf(", ");
		}

		current_item = current_item->next;
	}
	printf("] \n");
}


/**
 * A function that desallocates memory used by a stack
 * data structure
 * 
 * @param stack* s : a pointer representing a stack
 * @return void
 */
void destroy_stack(stack *s) {
	stack_item *current_item = s->top;
	stack_item *aux;

	while (current_item != NULL) {
		aux = current_item->next;
		free(current_item);
		current_item = aux;
	}
	free(s);
}
