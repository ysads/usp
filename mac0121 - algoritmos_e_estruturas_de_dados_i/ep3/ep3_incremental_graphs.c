#include <stdio.h>
#include <stdlib.h>

typedef int Pancake;

typedef struct s_flip {
    Pancake* permutation;
    int index;
} Flip;

typedef Flip Content;

typedef struct s_node {
    Content content;
    struct s_node *next;
} Node;

typedef struct s_queue {
    int length;
    Node *start;
    Node *end;
} Queue;

typedef struct s_list {
    int length;
    Node *head;
    Node *tail;
} FlipList;

typedef struct s_tree_node {
    long flattened_permutation;
    struct s_tree_node *left;
    struct s_tree_node *right;
} TreeNode;

typedef struct s_tree {
    TreeNode *root;
} Tree;


/**
 * DEBUGGIN FUNCTIONS
 */
void print_array(Pancake *arr, int n) {
    int i;

    printf("array: [ ");
    for (i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("]\n");
}

void inspect_queue(Queue *q, int pancake_length) {
    printf("queue length now: %d\n", q->length);
    printf("queue start: %p\n", q->start);
    printf("queue end: %p\n", q->end);

    Node *aux = q->start;
    printf("current queue: [ ");
    while (aux) {
        print_array(aux->content.permutation, pancake_length);
        aux = aux->next;
    }
    printf("]\n\n\n");
}

void inspect_list(FlipList *l, int pancake_length) {
    printf("list length now: %d\n", l->length);
    printf("list start: %p\n", l->head);

    Node *aux = l->head;
    printf("current list: [ ");
    while (aux) {
        print_array(aux->content.permutation, pancake_length);
        aux = aux->next;
    }
    printf("]\n\n\n");
}



/**
 *
 * ======================== ARRAY HELPER FUNCTIONS ======================== 
 * 
 */

/**
 * Copy element by element all pancakes from origin to destination.
 * 
 * @param dest: the array in which we will insert the pancakes
 * @param origin: the array from which we will copy pancakes
 * @param n: the length of the arrays
 */
void copy_array(Pancake* dest, Pancake* origin, int n) {
    int i;

    for (i = 0; i < n; i++) {
        dest[i] = origin[i];
    }
}


/**
 * Swaps items at positions pos1 and pos2 in array 
 * 
 * @param array: array in which items will be swaped
 * @param pos1: one of the items index
 * @param pos2: the other index
 */
void swap_items(Pancake *array, int pos1, int pos2) {
    Pancake aux = array[pos1];
    array[pos1] = array[pos2];
    array[pos2] = aux;
}


/**
 *
 * ======================== QUEUE HELPER FUNCTIONS ======================== 
 * 
 */

/**
 * Creates an empty queue, assigning NULL to its start and ending
 * pointers. Also initializes its length to zero.
 * 
 * @return a pointer to a queue
 */
Queue* create_queue() {
    Queue *queue  = (Queue*) malloc(sizeof(Queue));

    queue->start  = NULL;
    queue->end    = NULL;
    queue->length = 0;

    return queue;
}


/**
 * Destroy a given queue by freeing each of its nodes,
 * updating its starting point in the process
 * 
 * @param queue: the queue to be freed
 */
void destroy_queue(Queue* queue) {
    Node *aux;

    while (queue->start) {
        aux = queue->start->next;
        free(queue->start);
        queue->start = aux;
    }
    free(queue);
}


/**
 * Enqueue a given content to a given queue. Note this function
 * has a particular behaviour when the queue is empty, since in
 * this case it needs to update both the start and the end pointer.
 * Otherwise, just the end pointer is updated
 * 
 * @param queue: the queue in which content will be enqueued
 * @param permutation: the content to enqueue
 */
void enqueue(Queue *queue, Content permutation) {
    Node *node = (Node*) malloc(sizeof(Node));

    node->content = permutation;
    node->next = NULL;

    if (queue->length) {
        queue->end->next = node;
        queue->end = node;
    }
    else {
        queue->end = node;
        queue->start = node;
    }
    queue->length++;
}


/**
 * Dequeues an item from queue. Important: this function DOES free
 * from memory the node from which the content was dequeued. Bear
 * that not to run into issues regarding NULL pointers
 * 
 * @param  queue: queue to extract item from
 * @return the content of queue's first node
 */
Content dequeue_from(Queue *queue) {
    Node *first_node = queue->start;
    Content first_pancake;

    /**
     * It only makes sense to dequeue something if the queue
     * is not empty, otherwise just sends a NULL pointer
     */
    if (first_node) {
        
        /**
         * We have to hold the first node and the its content
         * because, once the node is freed, we loose the former
         * AND we can't update the queue start to following node
         */
        first_pancake = first_node->content;
        queue->start = first_node->next;

        free(first_node);
        queue->length--;
        
        return first_pancake;
    }
}

int is_queue_empty(Queue *queue) {
    return (queue->length == 0);
}



/**
 *
 * ======================== LIST HELPER FUNCTIONS ======================== 
 * 
 */

/**
 * Creates an empty list, assigning NULL to its start and ending
 * pointers. Also initializes its length to zero.
 * 
 * @return a pointer to a list
 */
FlipList* create_list() {
    FlipList *list  = (FlipList*) malloc(sizeof(FlipList));

    list->head = NULL;
    list->tail = NULL;
    list->length = 0;

    return list;
}


/**
 * Destroy a given list by freeing each of its nodes,
 * updating its starting point in the process
 * 
 * @param list: the list to be freed
 */
void destroy_list(FlipList* list) {
    Node *aux;

    while (list->head) {
        aux = list->head->next;
        free(list->head);
        list->head = aux;
    }
    free(list);
}


/**
 * Inserts a flip into a breadcrumbs-like list. This is
 * meant to save the path we took until finding the correct
 * ordered pancake array. Note this acts like a horizontal
 * stack, always pushing items at the front of the list
 * 
 * @param list: the list that will store the flips
 * @param flip: a flip, representing a permutation originated
 *              from a flip at a given index
 */
void insert_into_list(FlipList *list, Flip flip) {
    Node *node = (Node*) malloc(sizeof(Node));

    node->content = flip;
    node->next = list->head;
    
    list->head = node;
    list->length++;
}


/**
 * Pops the first item from flip list. This also acts like
 * a horizontal stack, in which we save the steps we did
 * 
 * @param  list: a list of flips already saved
 * @return the item in front of the list
 */
Content pop_from_list(FlipList *list) {
    Node *first_node = list->head;
    Content permutation;

    /**
     * We only pop something from list if it has
     * something within it
     */
    if (first_node) {
        permutation = first_node->content;
        list->head  = first_node->next;

        free(first_node); 
        list->length--;

        return permutation;
    }
}


/**
 * 
 */

Tree* create_tree() {
    Tree* tree = (Tree*) malloc(sizeof(Tree));
    tree->root = NULL;

    return tree;
}

void mark_as_visited(Tree *tree, Pancake *permutation, int length) {
    TreeNode *node = (TreeNode*) malloc(sizeof(TreeNode));
    TreeNode *aux  = tree->root;;
    TreeNode *parent;

    long flattened = 0;
    long power = 1;

    /**
     * Calculates the flattened permutation, which a flat way of representing
     * a given permutation. This number acts as way to simplify comparisons, 
     * since each permutation becomes a simple integer, far easier to compare
     * to other integer than a complex array
     */
    while (length--) {
        flattened += permutation[length] * power;
        power *= 10;
    }

    node->flattened_permutation = flattened;
    node->right = NULL;
    node->left  = NULL;

    if (tree->root == NULL) {
        tree->root  = node;
    }
    else {
        while (aux != NULL) {
            parent = aux;

            if (flattened > aux->flattened_permutation) {    
                aux = aux->right;
            }
            else {
                aux = aux->left;
            }
        }
    } 
}



/**
 *
 * ==================== PANCAKE SPECIFIC FUNCTIONS ===================== 
 * 
 */

/**
 * Iterates through the pancakes array, starting from its end,
 * until finding a pair of consecutive values that are not in
 * decremental order; if so stops the function and returns the
 * correct value. Otherwise, if no misplaced pair is found, then
 * the array is ordered
 * 
 * @param  pancakes: the array of pancakes
 * @param  n       : the number of pancakes in the array
 * @return whether the array is ordered decrementally or not
 */
int are_pancakes_ordered(Pancake *pancakes, int n) {
    while (n--) {
        if (pancakes[n] < pancakes[n+1]) {
            return 0;
        }
    }
    return 1;
}


/**
 * Reverselly flip an array of pancakes at a given index. This means that,
 * starting from a given index, we swap it with last index and so on, until
 * the "center" of the interval [index, end[. That is, we change position of
 * items that are symmetric with respect to (index length+index_to_flip)/2
 * 
 * @param pancakes: the array of pancakes
 * @param index_to_flip: the index from which we should start fliping
 * @param length: number of pancakes in array
 */
void flip_array(Pancake *pancakes, int index_to_flip, int length) {
    int i, k;

    /**
     * We should swap items diametrically oposed, considering the
     * "center" the mean between the flip index and the array length
     */
    int section_half = (length + index_to_flip) / 2;

    /**
     * k here is an arbitrary counter. Its only purpose is to mark how
     * many pancakes have been already swaped. This allows us to copy
     * swap items with respect to "center" of our interval
     */
    k = 0;
    for (i = index_to_flip; i < section_half; i++) {
        /*
         * -1 here fixes the fact that array indexes start at 0 in C lang
         */
        swap_items(pancakes, i, length - k - 1);
        k++;
    }
}


/**
 * Generates a copy of the original pancake array and then permutates
 * the array, flipping it at a given position in all possible positions,
 * then enqueue the just flip list
 * 
 * @param queue: the queue which will receive the pe
 * @param pancakes: the pancake array to be flipped
 * @param length: number of pancakes in the array
 */
void enqueue_permutations(Queue *queue, Pancake *pancakes, int length) {
    int i;

    for (i = 0; i < length - 1; i++) {

        Flip flip;
        Pancake *permutation = (Pancake*) malloc(length * sizeof(Pancake));
        
        /**
         * Generates a copy of the original pancake list. That's
         * important because we don't wanna lose the original reference
         */
        copy_array(permutation, pancakes, length);
        flip_array(permutation, i, length);

        /**
         * Enqueues the flip. Note we save the index so furtherly we
         * may rebuild the path we made through
         */
        flip.permutation = permutation;
        flip.index = i;
        enqueue(queue, flip);
    }
}


/**
 * Computes how many flips we need to perform if we want to
 * order a given pancake array.
 * 
 * @param  pancakes: a pancake array to be ordered
 * @param  length: the number of pancakes in the array
 * @return the number of flips we performed
 */
FlipList* compute_minimum_path(Pancake *pancakes, int length) {
    Queue *queue = create_queue();
    // Tree *visited_nodes = create_tree();
    FlipList *flip_list = create_list();
    Flip current_flip;

    int exhausted  = 0;
    int flip_count = 0;

    /**
     * Enqueue the first flips, which are permutations from the
     * base pancakes list
     */
    enqueue_permutations(queue, pancakes, length);

    while (!is_queue_empty(queue)) {
        /**
         * We dequeue the next item to process it. As this is
         * a do-first-fix-if-needed approach, we already put
         * it into the flip_list.
         */
        current_flip = dequeue_from(queue);

        if (is_permutation_already_visited(current_flip->permutation)) {
            continue;    
        }
        else {
            insert_into_list(list, current_flip->permutation);
            // mark_as_visited(visited_nodes, current_flip->permutation, length);
        }
        
        /**
         * If we found the ordered permutation then we're done. Thus
         * we can escape the loop to return our flip_list containing
         * the path we made until here.
         */
        if (are_pancakes_ordered(current_flip.permutation, length)) {
            break;
        }
        else {
            enqueue_permutations(queue, current_flip.permutation, length);
        }

    }
    return flip_list;
}


int main() {

    int length = 5;
    // Pancake pancakes[4] = {1, 3, 2, 4};
    // Pancake pancakes[5] = {0, 24, 6, 1, 2};
    Pancake pancakes[5] = {2, 4, 61, 2, 0};

    mark_as_visited(NULL, pancakes, length);

    
    // compute_minimum_path(pancakes, length);
    // printf("%d", are_pancakes_ordered(pancakes, length));

    return 0;
}