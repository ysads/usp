/**
 * Universidade de São Paulo
 * MAC0121 - Algoritmos e Estruturas de Dados I
 * 
 * Author: Ygor Sad Machado
 * NUSP: 8910368
 */
#include <stdio.h>
#include <stdlib.h>


typedef int Pancake;

typedef struct s_node {
    int index;
    struct s_node *next;
} Flip;

typedef struct s_flip_list {
    int moves;
    Flip *head;
    Flip *tail;
} FlipList;


/**
 * Creates an empty list, assigning NULL to its start and ending
 * pointers. Also initializes its length to zero.
 * 
 * @return a pointer to a list
 */
FlipList* create_list() {
    FlipList *list  = (FlipList*) malloc(sizeof(FlipList));

    list->head = NULL;
    list->tail = NULL;
    list->moves = 0;

    return list;
}


/**
 * Destroy a given list by freeing each of its nodes,
 * updating its starting point in the process
 * 
 * @param list: the list to be freed
 */
void destroy_list(FlipList* list) {
    Flip *aux;

    while (list->head) {
        aux = list->head->next;
        free(list->head);
        list->head = aux;
    }
    free(list);
}


/**
 * Inserts a flip into a breadcrumbs-like list. This is
 * meant to save the path we took until finding the correct
 * ordered pancake array.
 * 
 * @param list: the list that will store the flips
 * @param flip: a flip, representing a permutation originated
 *              from a flip at a given index
 */
void insert_into_list(FlipList *list, int index) {
    Flip *flip = (Flip*) malloc(sizeof(Flip));

    flip->index = index;
    flip->next = NULL;

    if (list->moves) {
        list->tail->next = flip;
        list->tail = flip;
    }
    else {
        list->head = flip;
        list->tail = flip;
    }
    list->moves++;
}


/**
 * Prints a list of flips running from node to node until
 * the end. In each node, the index stored is printed
 * 
 * @param list: the list to be printed
 */
void print_list(FlipList *list) {
    Flip *flip = list->head;

    while (flip != NULL) {
        printf("%d ", flip->index);
        flip = flip->next;
    }
}


/**
 * Swaps items at positions pos1 and pos2 in array 
 * 
 * @param array: array in which items will be swaped
 * @param pos1: one of the items index
 * @param pos2: the other index
 */
void swap_items(Pancake *array, int pos1, int pos2) {
    Pancake aux = array[pos1];
    array[pos1] = array[pos2];
    array[pos2] = aux;
}


/**
 * Reverselly flip an array of pancakes at a given index. This means that,
 * starting from a given index, we swap it with last index and so on, until
 * the "center" of the interval [index, end[. That is, we change position of
 * items that are symmetric with respect to (index length+index_to_flip)/2
 * 
 * @param pancakes: the array of pancakes
 * @param index_to_flip: the index from which we should start fliping
 * @param length: number of pancakes in array
 */
void flip_array(Pancake *pancakes, int index_to_flip, int length) {
    int i, k;

    /**
     * We should swap items diametrically oposed, considering the
     * "center" the mean between the flip index and the array length
     */
    int section_half = (length + index_to_flip) / 2;

    /**
     * k here is an arbitrary counter. Its only purpose is to mark how
     * many pancakes have been already swaped. This allows us to copy
     * swap items with respect to "center" of our interval
     */
    k = 0;
    for (i = index_to_flip; i < section_half; i++) {
        /*
         * -1 here fixes the fact that array indexes start at 0 in C lang
         */
        swap_items(pancakes, i, length - k - 1);
        k++;
    }
}


/**
 * Finds the index of the largest pancake still not sorted. In order
 * to find it, deices
 * @param  pancakes: an array of pancakes
 * @param  max_sorted_index: index in which the pancake will be placed
 * @param  length: the number of pancakes in the array
 * @return the index of the largest pancake not sorted yet
 */
int index_of_largest_pancake(Pancake *pancakes, int max_sorted_index, int length) {
    int i;
    int largest_index = length - 1;
    int largest_pancake = pancakes[largest_index];

    /**
     * Runs backwards from top until the index in which the pancake will
     * be placed. The main goal is to find the largest pancake in this
     * section, so that we can place it
     */
    for (i = length - 1; i >= max_sorted_index; i--) {
        if (pancakes[i] >= largest_pancake) {
            largest_index = i;
            largest_pancake = pancakes[i];
        }
    }
    return largest_index;
}


/**
 * Sorts an array using pancake sort algorithm. To do so, basically
 * we look for the largest pancake still not sorted. If it's not at
 * the topmost position in pancake list, we flip just below it, so
 * we make it the topmost pancake. Then we flip right after the last
 * sorted pancake - which, in beginning, is the 0-index one -, in
 * such a way that the topmost pancake assumes its correct position
 * 
 * @param  pancakes: an array of pancakes
 * @param  length: length of the array to pancakes
 * @return          [description]
 */
FlipList* pancake_sort(Pancake *pancakes, int length) {
    int i, max_index, cur_index;
    int last_index = length - 1;
    int largest_pancake;

    FlipList *list = create_list();

    for (i = last_index; i > 0; i--) {
        cur_index = length - i - 1;
        max_index = index_of_largest_pancake(pancakes, cur_index, length);

        /**
         * If the current max item is already at the index
         * we are trying to fill, then we're done and nothing
         * needs to be done
         */
        if (max_index != cur_index) {
            
            /**
             * Now we check if the item is already at the topmost
             * position. This is important because saves one flip,
             * since we don't need to flip in cur_index to make
             * the current largest item go to top
             */
            if (max_index != last_index) {
                insert_into_list(list, max_index);
                flip_array(pancakes, max_index, length);
            }

            /**
             * Finally we flip the array in such a way that the
             * topmost item goes to cur_index, assuming its
             * correct position within the ordered array
             */
            insert_into_list(list, cur_index);
            flip_array(pancakes, cur_index, length);
        }
    }
    return list;
}


int main() {
    int i, length;
    Pancake* pancakes;
    FlipList* list;

    /**
     * Receives the length of the pancake array to be sorted
     */
    scanf("%d", &length);

    /**
     * Allocates dinamically the space for the needed pancakes,
     * an then reads them from buffer
     */
    pancakes = (Pancake*) malloc(length * sizeof(Pancake));
    for (i = 0; i < length; i++) {
        scanf("%d", &pancakes[i]);
    }

    /**
     * Sorts the list and the prints the movements made to sort it.
     * Then, destroies the list since we don't need it anymore
     */
    list = pancake_sort(pancakes, length);
    print_list(list);
    destroy_list(list);

    return 0;
}