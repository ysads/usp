#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "entry.h"

/**
 * Allocates an array of entries and set all of the values
 * to start with value 0
 * 
 * @param  sorted: 1 if array should be lexicographically sorted
 *                 or 0 otherwise
 * @returnm a pointer to the array just allocated
 */
Array* array_create(int sorted) {
    Array* array = (Array*) malloc(sizeof(Array));

    array->data     = (Entry*) calloc(BASE_LENGTH, sizeof(Entry));
    array->capacity = BASE_LENGTH;
    array->size     = 0;
    array->sorted   = sorted;

    return array;
}


/**
 * Expands the current array capacity by doubling it. This
 * function uses realloc to safely increase array capacity.
 * If there's no memory left to expand the array, then we
 * can't do anything and execution is aborted
 * 
 * @param array: the array whose capacity will be doubled
 */
void array_expand(Array *array) {
    int i;
    int new_size = array->capacity * 2;
    Entry *new_address = realloc(array->data, new_size * sizeof(Entry));

    /**
     * We hope this condition never gets triggered, but in
     * case we really have no memory left, should we stop
     * program execution, as nothing can be done 
     */
    if (new_address == NULL) {
        printf("Aborting... No memory left.\n");
        exit(1);
    }

    /**
     * Assigns the just allocated space to array's data pointer
     * and clean all the newly allocated positions
     */
    array->data = new_address;
    for (i = array->size; i < new_size; i++) {
        array->data[i].value = 0;
    }
    array->capacity = new_size;
}


/**
 * Performs a binary search in the array. For this to work
 * it assumes array is lexicographically sorted, so we can
 * safely discard array areas we already know don't contain
 * the searched key 
 *  
 * @param  array: array to perform search
 * @return the index of the found element, or -1, if not found
 */
int array_search_sorted(Array *array, char *key) {
    int start = 0;
    int end = array->size;
    int mid;
    int equalty;

    while (start < end) {
        mid = (start + end) / 2;
        equalty = strcmp(key, array->data[mid].key);

        /**
         * If found, then return immediately the index
         */
        if (equalty == 0) {
            return mid;
        }
        
        /**
         * If searched key is "smaller", then we restrict
         * the search to the subarray to the left of mid.
         * Otherwise, we do that to it's right subarray.
         */
        if (equalty < 1) {
            end = mid;
        }
        else {
            start = mid + 1;
        }
    }
    return -1;
}


/**
 * Execute a simple linear search through the array. Since
 * this function assumes array has no sorting, we can't
 * improve its performance
 * 
 * @param  array: array to search item
 * @param  key: key we're looking for
 * @return the index of the found element, or -1, if not found
 */
int array_search_unsorted(Array *array, char *key) {
    int i;

    for (i = 0; i < array->size; i++) {
        if (strcmp(array->data[i].key, key) == 0) {
            return i;
        }
    }
    return -1;
}


/**
 * Inserts an item into an array. Note this function makes no
 * assumption about array's current sorting status. It's only
 * concern is knowing when the array capacity is over, meaning
 * we need to expand array
 * 
 * @param array: an array pointer
 * @param entry: the entry to insert into array
 */
void array_insert_unsorted(Array *array, char *key, int value) {
    int entry_index;
    Entry entry;

    if (array->size == array->capacity) {
        array_expand(array);
    }

    /**
     * If a given key is already at the table, we only have
     * to add the value to it's entry. Otherwise, we create
     * a new entry and add it to the array
     */
    entry_index = array_search_unsorted(array, key);
    if (entry_index == -1) {
        entry.key   = key;
        entry.value = value;

        array->data[array->size] = entry;
        array->size++;
    }
    else {
        array->data[entry_index].value = array->data[entry_index].value + value;
    }
}


/**
 * Inserts an item into an array. This function does care
 * about array sorting status, which means after every
 * insertion we must resort the array so it keeps sorted.
 * Note we also take care about capacity here
 * 
 * @param array: an array pointer
 * @param key: the key of the entry we want to insert
 * @param value: the value to add at the given key
 */
void array_insert_sorted(Array *array, char *key, int value) {
    int entry_index;
    Entry entry;

    if (array->size == array->capacity) {
        array_expand(array);
    }

    /**
     * If a given key is already at the table, we only have
     * to add the value to it's entry. Otherwise, we create
     * a new entry and add it to the array
     */
    entry_index = array_search_sorted(array, key);
    if (entry_index == -1) {
        entry.key   = key;
        entry.value = value;

        array->data[array->size] = entry;
        array->size++;
    }
    else {
        array->data[entry_index].value = array->data[entry_index].value + value;
    }

    /**
     * Sorts using qsort function. This delegates to qsort
     * responsibility for sorting the array with highest
     * possible efficiency, leaving our code dry and agnostic
     */
    qsort(array->data, array->size, sizeof(Entry), entry_compare_lexicographic);
}


/**
 * Desallocate the space of the array passed as param
 * 
 * @param entries: the array to desallocate
 */
void array_destroy(Array *array) {
    free(array->data);
    free(array);
}


/**
 * Prints an array according to a given sort_mode, resorting the
 * array whenever needed
 * 
 * @param array: an array to be printed
 * @param sort_mode: the print sort mode, being either lexicographically
 *                   or by occurrences
 */
void array_print(Array *array, SortMode sort_mode) {
    int i;

    /**
     * Both sorted and unsorted arrays must be resorted by occurrences
     * if this is asked, since none of them are this way by default
     */
    if (sort_mode == OCCURRENCES) {
        qsort(array->data, array->size, sizeof(Entry), entry_compare_occurrences);
    }
    else if (!array->sorted) {
        /**
         * However, if sort_mode is LEXICOGRAPHIC, we should only resort
         * the unsorted arrays, since the sorted arrays are already
         * lexicographically sorted from beginning
         */
        qsort(array->data, array->size, sizeof(Entry), entry_compare_lexicographic);
    }

    for (i = 0; i < array->size; i++) {
        printf("%-20s %-6d\n", array->data[i].key, array->data[i].value);
    }
}
