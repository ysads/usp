#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "entry.h"
#include "list.h"
#include "symbol_table.h"


StorageType identify_storage_type(char *type) {
    if (strcmp(type, "VD") == 0) {
        return VD;
    }
    else if (strcmp(type, "VO") == 0) {
        return VO;
    }
    else if (strcmp(type, "LD") == 0) {
        return LD;
    }
    else if (strcmp(type, "LO") == 0) {
        return LO;
    }
    else if (strcmp(type, "AB") == 0) {
        return AB;
    }
    else {
        printf("Aborting... Unavailable storage type.\n");
        exit(1);
    }
}

SortMode identify_sort_mode(char *sort_mode) {
    if (strcmp(sort_mode, "A") == 0) {
        return LEXICOGRAPHIC;
    }
    else if (strcmp(sort_mode, "O") == 0) {
        return OCCURRENCES;
    }
    else {
        printf("Aborting... Unavailable print mode.\n");
        exit(1);
    }
}


Table* instantiate_table(SymbolTable *symbol_table) {
    Table *table = (Table*) malloc(sizeof(Table));

    switch (symbol_table->type) {
        case VD:
            table->array = array_create(0);
            break;

        case VO:
            table->array = array_create(1);
            break;

        case LD:
            table->list = list_create(0);
            break;

        case LO:
            table->list = list_create(1);
            break;

        case AB:
            table->tree = tree_create();
            break;
    }

    return table;
}


SymbolTable* symbol_table_create(char *storage_type, char *sort_mode) { 
    SymbolTable *symbol_table = (SymbolTable*) malloc(sizeof(SymbolTable));

    symbol_table->size  = 0;
    symbol_table->type  = identify_storage_type(storage_type);
    symbol_table->sort  = identify_sort_mode(sort_mode);
    symbol_table->table = instantiate_table(symbol_table);
    
    return symbol_table;
}


void symbol_table_insert(SymbolTable *symbol_table, char *key, int difference) {
    switch (symbol_table->type) {
        case VD:
            array_insert_unsorted(symbol_table->table->array, key, difference);
            break;
        
        case VO:
            array_insert_sorted(symbol_table->table->array, key, difference);
            break;

        case LD:
            list_insert_unsorted(symbol_table->table->list, key, difference);
            break;

        case LO:
            list_insert_sorted(symbol_table->table->list, key, difference);
            break;

        case AB:
            tree_insert(symbol_table->table->tree, key, difference);
            break;
    }
}


void symbol_table_print(SymbolTable *symbol_table) {
    switch (symbol_table->type) {
        case VD:
        case VO:
            array_print(symbol_table->table->array, symbol_table->sort);
            break;

        case LD:
        case LO:
            list_print(symbol_table->table->list, symbol_table->sort);
            break;

        case AB:
            tree_print(symbol_table->table->tree, symbol_table->sort);
            break;
    }
}


void symbol_table_destroy(SymbolTable *symbol_table) {
    switch (symbol_table->type) {
        case VD:
        case VO:
            array_destroy(symbol_table->table->array);
            break;

        case LD:
        case LO:
            list_destroy(symbol_table->table->list);
            break;

        case AB:
            tree_destroy(symbol_table->table->tree);
            break;
    }
    free(symbol_table->table);
    free(symbol_table);
}
