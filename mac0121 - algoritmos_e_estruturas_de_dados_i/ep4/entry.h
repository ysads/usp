#pragma once

typedef enum {
    LEXICOGRAPHIC = 0,
    OCCURRENCES = 1
} SortMode;

typedef struct {
    char *key;
    int value;
} Entry;

int entry_compare_lexicographic(const void *a, const void *b);
int entry_compare_occurrences(const void *a, const void *b);
