#pragma once

#include "entry.h"

typedef struct list_node {
    Entry entry;
    struct list_node *next;
} ListNode;

typedef struct {
    ListNode *head;
    ListNode *tail;

    int size;
    int sort;
} List;

List* list_create(int sorted);
void list_insert_unsorted(List *list, char *key, int value);
void list_insert_sorted(List *list, char *key, int value);
void list_destroy(List *list);
void list_print(List *list, SortMode sort_mode);
