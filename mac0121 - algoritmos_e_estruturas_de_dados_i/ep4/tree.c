#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "tree.h"


/**
 * Creates a binary tree. This is as easy as creating
 * a Tree object and setting it's root to NULL
 * 
 * @return an allocated Tree
 */
Tree* tree_create() {
    Tree *tree = (Tree*) malloc(sizeof(Tree));
    tree->root = NULL;
    return tree;
}


/**
 * Performs a binary search in the Tree. This consists
 * of looking at a given node. If the searched value is
 * greater than the one in the current node, search for
 * it in the right right; if it's smaller, then search
 * in the left one and so on
 * 
 * @param  tree: the Tree in which binary search will be performed
 * @param  key: the key to look for
 * @return the desired node if it exists; NULL otherwise
 */
TreeNode* tree_search(Tree *tree, char *key) {
    TreeNode *node = tree->root;
    int equalty;

    while (node != NULL) {
        equalty = strcmp(key, node->entry.key);

        if (equalty == 0) {
            return node;
        }
        else if (equalty < 0) {
            node = node->lft;
        }
        else {
            node = node->rgt;
        }
    }
    return node;
}


/**
 * Inserts an item into the tree. It follows a basic
 * binary search logic, in which smaller items are put
 * into the left subtree while larger ones are put in
 * to the right
 * 
 * @param tree: the tree in which we'll insert the entry
 * @param key: the entry key
 * @param value: the entry value
 */
void tree_insert(Tree *tree, char *key, int value) {
    TreeNode *node = tree_search(tree, key);
    TreeNode *aux;

    if (node) {
        node->entry.value = node->entry.value + value;
    }
    else {
        node = (TreeNode*) malloc(sizeof(TreeNode));
        node->entry.key = key;
        node->entry.value = value;

        node->lft = NULL;
        node->rgt = NULL;

        /**
         * If the tree has a root, then we have to follow
         * the aforementioned logic. Otherwise, the new
         * element is already the root
         */
        if (tree->root) {
            aux = tree->root;

            /**
             * Keeps iterating until we found a leaf (node
             * with no siblings). At this point, we found
             * the place in which we need to put our entry.
             * Note this while is granted to stop, since
             * there will always be a leaf
             */
            while (1) {
                if (strcmp(key, aux->entry.key) < 0) {
                    if (aux->lft == NULL) {
                        aux->lft = node;
                        break;
                    }
                    else {
                        aux = aux->lft;  
                    }
                }
                else {
                    if (aux->rgt == NULL) {
                        aux->rgt = node;
                        break;
                    }
                    else {
                        aux = aux->rgt;    
                    }
                }    
            }
        }
        else {
            tree->root = node;
        }
    }
}


/**
 * Disallocates a subtree recursively.
 * 
 * @param node: the node whose subtrees will be disallocated
 */
void tree_destroy_subtree(TreeNode *node) {
    if (node->lft) {
        tree_destroy_subtree(node->lft);    
    }
    if (node->rgt) {
        tree_destroy_subtree(node->rgt);    
    }
    free(node);
}


/**
 * Disallocates a tree. Note this function uses the auxiliary
 * function that disallocates subtrees recursively
 * 
 * @param tree [description]
 */
void tree_destroy(Tree *tree) {
    if (tree->root) {
        tree_destroy_subtree(tree->root->lft);
        tree_destroy_subtree(tree->root->rgt);    
    }
    free(tree);
}


/**
 * Recursively enqueues a given node subtrees into an array. Doing
 * this we're able to perform other operations onto the array, such
 * as sorting or printing it
 * 
 * @param node: the node whose subtrees will be enqueued
 * @param array: the array in which we will enqueue the nodes
 */
void tree_enqueue_subtree(TreeNode *node, Array *array) {
    if (node != NULL) {
        tree_enqueue_subtree(node->lft, array);
        array_insert_sorted(array, node->entry.key, node->entry.value);
        tree_enqueue_subtree(node->rgt, array);
    }
}


/**
 * Prints a tree base upon the given sorting mode. To do
 * that we call the function delegated to recursively
 * print a node's subtree.
 * 
 * @param tree: the Tree we want to print
 * @param sort_mode: the desired sorting. mode
 */
void tree_print(Tree *tree, SortMode sort_mode) {
    Array *array = array_create(1);

    if (tree->root) {
        tree_enqueue_subtree(tree->root->lft, array);
        tree_enqueue_subtree(tree->root->rgt, array);

        /**
         * Next thing to do is printing the array in the given
         * sort_mode, once again using the array approppriate function
         */
        array_print(array, sort_mode);
    }
}
