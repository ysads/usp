#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "list.h"


/**
 * Dynamically instantiates a generic list that is able 
 * to hold different kinds of items.
 *
 * @return a pointer to a list
 */
List* list_create(int sorted) {
    List *list = (List*) malloc(sizeof(List));

    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    list->sort = sorted;

    return list;
}


/**
 * Destroys the whole list by removing each of its nodes.
 * Note this function does not remove each node's associated
 * value. Since the list didn't allocate the value when it was
 * inserted into here, the list can't disallocate it.
 * 
 * @param list: list to be freed
 */
void list_destroy(List *list) {
    if (list != NULL) {
        ListNode *aux = list->head;
        
        while (aux != NULL) {
            list->head = aux->next;
            free(aux);
            aux = list->head;
        }
        free(list);
    }
}

/**
 * Searches the list for a value
 * 
 * @param  list: list to search in
 * @param  key: what will be searched for
 * @returns a pointer to the found node if it's found or
 * a NULL pointer if key isn't in the list
 */
ListNode* list_search(List *list, char *key) {
    ListNode *node;

    if (list == NULL) {
        return NULL;
    }

    node = list->head;
    while (node != NULL) {
        if (strcmp(node->entry.key, key) == 0) {
            return node;
        }
        node = node->next; 
    }
    return NULL;
}


/**
 * A function to insert a given entry to list considering the
 * list lexicographic sorting. Note this function operates in
 * such a way that we always iterate through the list until
 * we find the correct position to put the new node.
 * 
 * @param list: the list in which we'll insert the entry
 * @param key: the entry key
 * @param value: the entry value
 */
void list_insert_sorted(List *list, char *key, int value) {
    ListNode *prev, *aux;
    ListNode *node = list_search(list, key);

    if (!node) {
        node = (ListNode*) malloc(sizeof(ListNode));
        node->entry.key = key;
        node->entry.value = value;

        /**
         * Keeps iterating until we find a key larger than the
         * one we're trying to insert in the list. When this
         * happens, just update the pointers so that we have 
         * a new node in the list
         */
        prev = NULL;
        aux  = list->head;

        /**
         * If there's nothing in the list, it's easy to insert
         * something since we only have to change head and tail
         */
        if (aux) {
            while (aux != NULL && strcmp(aux->entry.key, key) <= 0) {
                prev = aux;
                aux = aux->next;
            }

            /**
             * We only update prev pointer if we actually have a
             * prev pointer. Otherwise, it's list head that must
             * be updated
             */
            if (prev) {
                prev->next = node;
                node->next = aux;

                /**
                 * aux became NULL, then we've reached the end of
                 * the list. List's tail becomes node
                 */
                if (aux == NULL) {
                    list->tail = node;
                }
            }
            else {
                node->next = list->head;
                list->head = node;
            }
        }
        else {
            list->head = node;
            list->tail = node;   
        }
        node->next = aux;
        list->size++;
    }
    else {
        node->entry.value = node->entry.value + value;
    }
}


/**
 * Inserts a value into the list. This function inserts
 * the element at the end of the list.
 * 
 * @param  list: list to insert info into
 * @param  key: a unique key to represent this value
 * @param  value: what will be inserted into list
 * @return the field value of the node just created
 */
void list_insert_unsorted(List *list, char *key, int value) {
    ListNode *node = list_search(list, key);

    if (!node) {
        node = (ListNode*) calloc(1, sizeof(ListNode));
        node->entry.key = key;
        node->entry.value = value;
        node->next = NULL;

        /**
         * We check whether this is the first element being
         * inserted into the list, and, if so, we also set
         * the head pointer
         */
        if (list->tail) {
            list->tail->next = node;
        }
        else {
            list->head = node;
        }
        list->tail = node;
        list->size++;
    }
    else {
        node->entry.value = node->entry.value + value;
    }
}


/**
 * Prints a list using each node's associated key
 *
 * @param list: a pointer to the list to print
 */
void list_print(List *list, SortMode sort_mode) {
    Array *array = array_create(list->sort);
    ListNode *node = list->head;

    while (node != NULL) {
        if (list->sort) {
            array_insert_sorted(array, node->entry.key, node->entry.value);
        }
        else {
            array_insert_unsorted(array, node->entry.key, node->entry.value);
        }
        node = node->next;
    }
    array_print(array, sort_mode);
}
