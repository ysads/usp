#pragma once

#include "entry.h"

#define BASE_LENGTH 40

typedef struct {
    Entry *data;
    int capacity;
    int size;
    int sorted;
} Array;

Array* array_create(int sorted);
void array_insert_unsorted(Array *array, char *key, int value);
void array_insert_sorted(Array *array, char *key, int value);
void array_destroy(Array *array);
void array_print(Array *array, SortMode sort_mode);
