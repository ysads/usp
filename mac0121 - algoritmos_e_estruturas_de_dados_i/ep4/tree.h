#pragma once

#include "entry.h"

typedef struct tree_node {
    Entry entry;
    struct tree_node *lft;
    struct tree_node *rgt;
} TreeNode;

typedef struct {
    TreeNode *root;
} Tree;

Tree* tree_create();
void tree_insert(Tree *tree, char *key, int value);
void tree_destroy(Tree *tree);
void tree_print(Tree *tree, SortMode sort_mode);
