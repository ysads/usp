\documentclass[11pt,reqno,a4paper]{amsart}
%SetFonts

%SetFonts

%%%% Pacotes adicionais para enumeração de items e alinhamento de equações
%%%%
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{mathtools}


%%%% Escrevendo em português
%%%% 
\usepackage[utf8x]{inputenc}
\usepackage[brazil]{babel}
% \usepackage[latin1]{inputenc} % to offline compiling, use this

%%%% Configurações para output correto de pdf
%%%%
\usepackage[T1]{fontenc}
\usepackage{ae}
\usepackage{aecompl}
\usepackage{amssymb}

%%%% Layout de página
%%%%
\usepackage{fullpage}
\usepackage{setspace}
\usepackage{bbm}

\usepackage{xcolor}
\definecolor{light-gray}{gray}{0.95}
\definecolor{dark-gray}{gray}{0.35}
\newcommand{\code}[1]{\colorbox{light-gray}{\textcolor{dark-gray}{\fontsize{10}{10}{\texttt{#1}}}}}

\begin{document}

\parindent=0pt

\title[MAC105 Folha de solução]
{\text{MAC0121 - Algoritmos e Estruturas de Dados I}
 \author{Ygor Sad Machado - N. USP: 8910368}}

\pagestyle{plain}
\onehalfspace

\maketitle

\textbf{Relatório}\hfill
\textbf{Quarto Exercício Programa}\null
\medskip

\noindent\rule{\textwidth}{0.4pt}
\medskip

O presente exercício-programa destina-se à verificação empírica de eficiência para a implantação da estrutura de dados conhecida como \textbf{Tabela de Símbolos}. Nessa estrutura de dados, apenas duas operações são importantes: inserção e busca.

\bigskip
Para obter uma ideia da complexidade assintótica das possíveis implementações que essa estrutura de dados pode ter, foram escolhidas cinco técnicas diferentes de implantação: vetor desordenado, vetor ordenado, lista ordenada, lista desordenada e árvore de busca binária. Antes de discutir os testes feitos, serão apresentados rápidos comentários acerca de decisões de implantação específicas de cada uma dessas técnicas.

\bigskip
\textbf{Vetor desordenado e Vetor ordenado}

\smallskip
Tanto nessa estrutura quanto no vetor ordenado, foi definida uma struct auxiliar \code{Array}, que armazena, além dos dados propriamente ditos, o número de itens atualmente no vetor e a capacidade de armazenamento dele. Como aqui temos uma estrutura estática, é preciso manter controle sobre quanto espaço ainda há disponível no vetor. Observe que a função de inserção desordenada - \code{array\_search\_unsorted} -, faz uma chamada à função \code{array\_expand} sempre que o espaço se esgota. Tal observação é importante na medida em que o espaço inicialmente reservado ao vetor impacta no tempo de execucação do código, já que a operação de realocar memória é lenta e custosa.

\medskip
Outra decisão de projheto importante diz respeito à separação entre as funções de inserção e busca de acordo com o estado de ordenação lexicográfica do vetor. Isto é, tais operações no vetor ordenado são feitas nas funções \code{array\_insert\_sorted} e \code{array\_search\_sorted}, ao passo que nos vetores desordenados elas são feitas em \code{array\_insert\_unsorted} e \code{array\_search\_unsorted}.

\bigskip
\textbf{Lista desordenada e Lista ordenada}

\smallskip
Na implementação da lista foi utilizada uma técnica que utiliza um ponteiro auxiliar para a cauda da lista - \code{tail}. Essa decisão impacta no tempo de inserção da lista desordenada, pois permite que tal operação seja feita com tempo constante. Essa escolha foi preterida em detrimento à inserção na cabeça da lista com o objetivo de manter a ordem lexicográfica intacta, na lista ordenada. Além disso, para a ordenação da saída foi utilizado como suporte um objeto \code{Array}, para o qual o conteúdo da lista é copiado antes de ser ordenado pelas funções específicas de vetor. Essa abordagem reduz a quantidade de código repetitivo e torna a manutenção do exercício-programa mais simples.


\bigskip
\textbf{Árvore de Busca Binária}

\smallskip
A implementação usando árvore de busca binária tenta fazer o maior uso possível de funções recursivas, com o objetivo de reduzir a repetição desnecessária de código. Por isso, as funções \code{tree\_print} e \code{tree\_destroy} são apenas "cápsulas" em torno de funções recursivas que, efetivamente, executam as ações desejadas. Aqui também foi utilizado um vetor auxiliar para a ordenação de saída da árvore, o qual é criado e populado usando uma abordagem \textit{depth-first}.

\bigskip
Em termos de desempenho comparado, é importante pontuar que as operações de inserção e busca são usadas em conjunto durante a contagem de palavras no texto. Assim, é difícil analisar um caso de uso prático desse problema isolando os tempos de utilização dessas funções. No entanto, o tempo de exibição da saída do algoritmo é determinante para o tempo final do código, já que operações de ordenação podem ser custosas. Abaixo, segue um exemplo de tempos obtidos variando-se os parâmetros de chamada do programa para o livro \textit{Tom Sawyer}. Nesse exemplo, considere este o tempo apenas para leitura e contagem das palavras, sem levar em conta o tempo de ordenação e saída.

\bigskip
\begin{table}[h]
\begin{tabular}{lllll}
    &  O   &  A   & \\
 VD & 0,45 & 0,47 & \\
 VO & 6,52 & 6,50 & \\
 LD & 0,78 & 0,80 & \\
 LO & 3,41 & 3,37 & \\
 AB & 0,04 & 0,05 & 
\end{tabular}
\end{table}

\bigskip
Como é possível perceber, para um texto relativamente grande em língua inglesa, o tempo de classificação das palavras usando árvores binárias de busca é absurdamente menor que todos os demais. Nem mesmo os vetores estáticos oferecem desempenho equiparável às árvores. Assim, se em uma dada situação-problema o objetivo é ter desempenho acima da média em contagem e classificação de palavras, elas são a escolha natural.

\bigskip
É interessante observar também que o desempenho dos vetores ordenados é particularmente muito ruim, mesmo quando comparado às listas ordenadas. Isso muito provavelmente se deve à quantidade alta de condições e testes que devem ser feitos durante a inserção para que a estabilidade e a ordernação do vetor não sejam perdidos. O mesmo se aplica às listas, ainda que de maneira menos agressiva. Por isso, o uso de estruturas de dados ordenadas não é muito eficaz em se tratando de tabelas de símbolos.

\bigskip
No entanto, o cenário do teste acima é particularmente especial. Ao considerar um texto em língua inglesa - que é um idioma construído com alfabeto latino e sem marcações diacríticas - a segmentação das palavras é facilitada. Basta usar a função \code{isalnum} da biblioteca \code{ctype.h} para fazer a identificação dos termos e tudo está resolvido. Em línguas com sinais diacríticos - português, polonês -, ou línguas que não utilizam alfabeto lativo - russo, mandarim, árabe -, essa tarefa se torna mais complexa, não estando esse código, em particular, pronto para lidar com tais cenários.

\bigskip
Ainda assim, em que as eventuais falhas na segmentação das palavras e o tamanho diferente do texto, foram executados testes com o livro \textit{Dom Casmurro}, de Machado de Assis, cujos resultados estão abaixo explicitados.

\bigskip
\begin{table}[h]
\begin{tabular}{lllll}
    &  O   &  A   & \\
 VD & 0,57 & 0,56 & \\
 VO & 7,29 & 7,36 & \\
 LD & 1,01 & 1,04 & \\
 LO & 3,85 & 3,88 & \\
 AB & 0,05 & 0,05 & 
\end{tabular}
\end{table}

\bigskip
Uma vez mais é perceptível a vantagem das árvores de busca binária sobre as demais estruturas. Observe como o tempo médio de execução se mantém baixo estável, mesmo com um tempo consideravelmente mais complexo de analisar. A complexidade logarítmica da busca nessas árvores demonstra todo o seu potencial em situações como essas, nas quais a busca linear certamente fracassa. Outro detalhe importante é a estabilidade dos tempos médios da lista ordenada, que mesmo em situações de piora do cenário mantém um tempo previsível, diferentemente dos vetores ordenados, que pioram seu desempenho consideravelmente.

\bigskip
É importante, também, frisar que as implementações das estruturas de dados aqui contidas tentam, na medida do possível, obter o melhor desempenho esperados dessas estruturas. No entanto, dificuldades técnicas ou decisões de projeto equivocadas podem ter comprometido a máxima eficiência esperada para os algoritmos, ainda que, na média, eles estejam com sua \textit{performance} dentro do esperado.

\bigskip
Outra informação relevante é que foram executados testes para textos em árabe, hebraico e mandarim. Porém, conforme esperado, nenhuma palavra nesses idiomas foi classificada. Somente termos em inglês, provenientes dos cabeçalhos inseridos pelo Project Gutemberg em seus arquivos, são lidos e classificados. Mesmo assim, trata-se de um teste importante, pois confirma empiricamente os resultados previstos pela teoria.

\endgroup
\end{document}