#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include "array.h"
#include "entry.h"
#include "symbol_table.h"

#define AUX_STR_LEN 51


/**
 * Opens a file stream containing a text
 * 
 * @param  filename: the name of the file to be opened
 * @return a pointer to the file stream
 */
FILE* open_file(char *filename) {
    FILE *file = fopen(filename, "r");

    if (file == NULL) {
        printf("Aborting. File could not be opened!\n");
    }
    return file;
}


/**
 * Allocates a string to handle the words from text. Note this
 * is a silly function that will only allocate strings from a
 * same size
 * 
 * @return a pointer to the string just allocated
 */
char* string_create() {
    char *string = (char*) malloc(AUX_STR_LEN * sizeof(char));

    if (string == NULL) {
        printf("Aborting. No memory left!\n");
    }
    return string;
}


/**
 * Reads a word from the file stream. For the sake of simplicity
 * a word is considered a maximal chain of alphanumeric characters
 * 
 * @param  file: a pointer to a text file
 * @param  string: the string in which we will read the word
 * @return number of characters on the word
 */
int word_read(FILE *file, char *string) {
    int i = 0;
    char letter;
    
    /**
     * We start reading the first character outside the loop
     * for safety reasons
     */
    letter = fgetc(file);

    /**
     * We keep on reading characters until we find a non-alphanumeric
     * one. At this point, we know, we have a word
     */
    while (letter != EOF && isalnum(letter)) {
        string[i++] = tolower(letter);
        letter = fgetc(file);
    }
    return i;
}


/**
 * Process the whole text by reading every word, once a time.
 * By doing so, we're able to count the word occurences in
 * the text, adding it to symbol table
 * 
 * @param filename: the name of the file whose content we'll analyze
 * @param symbol_table: the symbol table in which we'll insert the words
 */
void process_text(char* filename, SymbolTable* symbol_table) {
    FILE *file = open_file(filename);
    char *word;
    int num_characters;

    while (!feof(file)) {
        word = string_create();
        
        /**
         * Reads the next word from file, allocating it into a string
         */
        num_characters = word_read(file, word);

        /**
         * If we read something from the file, we should insert it
         * into the symbol table, adding one to its count. If no
         * word is read, we can free the allocated word so we can
         * prevent memory leaks
         */
        if (num_characters) {
            symbol_table_insert(symbol_table, word, 1);    
        }
        else {
            free(word);
        }
    }
}


int main(int argc, char *argv[]) {

    if (argc < 4) {
        printf("Você deve informar os argumentos da seguinte forma: \n");
        printf("./ep4 [nome_arquivo] [VD/VO/LD/LO/AB] [O/A]\n");
    }

    char *file = argv[1];
    char *type = argv[2];
    char *sort = argv[3];

    /**
     * Additional param used to measure time taken to process words
     */
    char *dev  = argv[4];

    clock_t start, end;
    double time_taken;

    /**
     * Creates a symbol table according to the options passed
     * via terminal arguments
     */
    SymbolTable *symbol_table = symbol_table_create(type, sort);

    /**
     * Reads the whole text file, counting the words therein so
     * we can later print the statistics we found
     */
    start = clock();
    process_text(file, symbol_table);
    
    /**
     * Prints according to the given arguments and then destroys
     * the symbol table, including the allocated words
     */
    end = clock();
    symbol_table_print(symbol_table);

    /**
     * Show time spent if requested
     */
    if ((dev != NULL) && (strcmp(dev, "--dev") == 0)) {
        time_taken = ((double) (end - start)) / CLOCKS_PER_SEC;
        printf("\n\nTime spent: %lf\n", time_taken);
    }

    symbol_table_destroy(symbol_table);

    return 0;
}