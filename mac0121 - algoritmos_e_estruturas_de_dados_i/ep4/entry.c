#include <string.h>

#include "entry.h"


/**
 * Auxiliary function used to compare two different Entry
 * structs. This comparison is done lexicographically.
 * 
 * @param  a: first struct to compare
 * @param  b: second struct to compare
 * @return an integer indicating what string is lexicographically
 * smaller than the the other (same return convention as strcmp)
 */
int entry_compare_lexicographic(const void *a, const void *b) {
    return strcmp(((Entry*)a)->key, ((Entry*)b)->key);
}


/**
 * Auxiliary function used to compare two Entry object by their
 * occurrences value, doing it by subtracting their values
 * 
 * @param  a: first struct to compare
 * @param  b: second struct to compare
 * @return negative value if a < b, 0 if they are equal, or a
 * positive value if b > a
 */
int entry_compare_occurrences(const void *a, const void *b) {
    return ((Entry*)b)->value - ((Entry*)a)->value;
}
