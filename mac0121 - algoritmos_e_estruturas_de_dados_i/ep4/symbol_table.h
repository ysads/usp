#pragma once

#include "entry.h"
#include "array.h"
#include "list.h"
#include "tree.h"

typedef struct {
    Array *array;
    List *list;
    Tree *tree;
} Table;


typedef enum {
    VD = 0,
    VO = 1,
    LD = 2,
    LO = 3,
    AB = 4
} StorageType;


typedef struct {
    Table *table;
    SortMode sort;
    StorageType type;
    int capacity;
    int size;
} SymbolTable;


SymbolTable* symbol_table_create(char* storage_type, char *sort_mode);
void symbol_table_insert(SymbolTable *symbol_table, char *key, int difference);
void symbol_table_destroy(SymbolTable *symbol_table);
void symbol_table_print(SymbolTable *symbol_table);