#include <stdio.h>
#include <stdlib.h>
#define MAX 10000

/**
 * Caching related functions to avoid unnecessary calculations
 * 
 */
long long cache_array[MAX][MAX];

long long cache_retrieve(int m, int n) {
  return ((cache_array[m][n]) ? cache_array[m][n] : -1);
}

void cache_store(int m, int n, long long value) {
  cache_array[m][n] = value;
}


long long ackermann(int m, int n) {
  long long value_cached = cache_retrieve(m, n);
  long long ackermann_value;

  if (value_cached != -1) {
    return value_cached;
  }

  if (m == 0) {
    ackermann_value = (n + 1);
  } else if (m > 0 && n == 0) {
    ackermann_value = ackermann(m - 1, 1);
  } else {
    ackermann_value = ackermann(m - 1, ackermann(m, n - 1));
  }

  cache_store(m, n, ackermann_value);
  return ackermann_value;
}

int main() {

  printf("%lld\n", ackermann(1, 2));
  printf("%lld\n", ackermann(3, 2));
  printf("%lld\n", ackermann(2, 1));
  printf("%lld\n", ackermann(3, 3));
  printf("%lld\n", ackermann(4, 0));

  return 0;
}