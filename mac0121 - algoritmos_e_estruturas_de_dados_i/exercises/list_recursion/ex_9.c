#include <stdio.h>

typedef struct pair {
  int x;
  int y;
} t_pair ;


/**
 * Function calculates in which coordinates the number would be inserted
 * considering the table in the exercise's description. 
 * 
 * @param  n: number whose coordinates should be located
 * @return t_pair containing the (x,y) position for the number n
 */
t_pair tabela(int n) {
  int radius, column, line, num;
  t_pair position = {0, 0};

  radius = column = num = 0;

  /**
   * While number hasn't been found, we should keep on looking for it
   */
  while (num < n) {
    /**
     * Think of these solutions as if we were walking onto concentric
     * quarters of circles. We start with the unitary circumference
     * and keep on increasing its radius until we find the number n
     */
    radius++;

    line = radius;
    column = 0;

    /**
     * Starting from the coordinate (0, radius) we continuously decrease
     * the abscisse value, until we haved searched the whole circumference
     * for a given circle
     */
    while (line >= 0) {
      num++;

      if (num == n) {
        position.x = line;
        position.y = column;
        return position;
      }
      column++;
      line--;
    }
  }
  return position;
}


void test_tabela(int n) {
  t_pair pos = tabela(n);
  printf("%d => (%d, %d)\n", n, pos.x, pos.y);
}

int main() {

  test_tabela(0);
  test_tabela(1);
  test_tabela(3);
  test_tabela(4);
  test_tabela(5);
  test_tabela(13);
  test_tabela(18);

  return 0;
}