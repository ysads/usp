#include <stdio.h>

int digito(int n) {
  if (n < 10) return 1;
  return 1 + digito(n / 10);
}

int main() {

  printf("%d\n", digito(123));
  printf("%d\n", digito(42));
  printf("%d\n", digito(16024));

  return 0;
}