#include <stdio.h>

void misterio(int *a, int start, int end) {
  int aux;
  
  while (a[end] % 2 == 0 && start < end) {
    end++;
  }

  while (a[start] % 2 == 1 && start < end) {
    start++;
  }

  if (start < end) {
    aux = a[start];
    a[start] = a[end];
    a[end] = aux;

    misterio(a, start, end);
  }
}


int main() {
  int a[] = {10, 3, 5, 7, 2, 4, 6, 8};
  
  misterio(a, 0, 7);
  
  int i;
  for (i = 0; i < 7; i++) {
    printf("%d\n", a[i]);
  }

  return 0;
}