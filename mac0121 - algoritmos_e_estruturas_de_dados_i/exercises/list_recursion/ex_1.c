#include <stdio.h>


void max_min(int* numbers, int n, int index, int* max, int* min) {
  if (index < n - 1) {
    if (numbers[index] > *max) *max = numbers[index];
    if (numbers[index] < *min) *min = numbers[index];

    max_min(numbers, n, index + 1, max, min);
  }
}

void test_max_min(int* numbers, int size) {
  int max = -1;
  int min = 1E9;

  max_min(numbers, size, 0, &max, &min);
  printf("max: %d - min: %d\n", max, min);
}

int main() {

  int size;

  int nums[100] = {2, 4, 1, 5, 8, 10, 3};
  size = 7;
  test_max_min(nums, size);

  int nums_2[100] = { 21, 4, 25, 98, 10923, -20, 45, 240, 129 };
  size = 9;
  test_max_min(nums_2, size);

  return 0;
}