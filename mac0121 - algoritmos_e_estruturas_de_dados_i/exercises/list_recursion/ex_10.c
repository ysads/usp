#include <stdio.h>

int euclides(int m, int n) {
  if (m % n == 0) return n;
  return euclides(n, m % n);
}

int main() {

  printf("%d\n", euclides(3, 4)); // 1
  printf("%d\n", euclides(10, 22)); // 2
  printf("%d\n", euclides(221, 169)); // 13

  return 0;
}