#include <stdio.h>
#include "stack.h"


int string_size(char *string) {
  int i = 0;
  while (string[i] != '\0') { 
    i++;
  }
  return i;
}

int precedence(char symbol) {
  if (symbol == '/' || symbol == '*') return 3;
  if (symbol == '-' || symbol == '+') return 2;
  if (symbol == '(' || symbol == ')') return 1;
  return 0;
}

int is_operator(char symbol) {
  switch (symbol) {
    case '/':
    case '*':
    case '-':
    case '+':
      return true;

    default:
      return false;
  }
}

void to_postfix(stack *operators, char *expression, char* output, int n) {
  int i, k = 0;

  /**
   * Decrease start index because in C strings are 0-indexed 
   */
  for (i = 0; i < n; i++) {

    if (expression[i] >= 'A' && expression[i] <= 'Z') {
      output[k++] = expression[i];
    }
    else if (expression[i] == '(') {
      insert_into_stack(operators, expression[i]);
    }
    else if (expression[i] == ')') {

      while (top_of_stack(operators) != '(') {
        output[k++] = pop_from_stack(operators);
      }
      pop_from_stack(operators);
    }
    else {
      if (is_operator(top_of_stack(operators))) {

        while (precedence(top_of_stack(operators)) >= precedence(expression[i])) {
          output[k++] = pop_from_stack(operators);
        }
        insert_into_stack(operators, expression[i]);
      }
    }
    n--;
  }
  output[k] = '\0';
}


int main() {

  stack *s_operators = create_stack();

  char expression[100] = "A∗B/C";
  char output[100];

  // printf("type expression: ");
  // gets(expression);

  to_postfix(s_operators, expression, output, string_size(expression));
  printf("%s\n", output);

  return 0;
}