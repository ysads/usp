#define stack_type int
#define empty_value -1


typedef struct st_stack_item {
	stack_type value;
	struct st_stack_item *next;
} stack_item;

typedef struct st_stack {
	stack_item *top;
	int size;
} stack;


stack* create_stack();
int is_empty_stack(stack*);
int height_of_stack(stack*);
stack* insert_into_stack(stack*, stack_type);
stack_type top_of_stack(stack*);
stack_type pop_from_stack(stack*);
void print_stack(stack*);
void destroy_stack(stack*);