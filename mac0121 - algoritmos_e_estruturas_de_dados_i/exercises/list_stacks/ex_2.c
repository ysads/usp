#include <stdio.h>
#include "stack.h"

int main() {

  int i = 0;
  char sequence[20] = "UTA*EC**R**O**";
  stack* s = create_stack();

  while (sequence[i] != '\0') {
    
    if (sequence[i] == '*') {
      printf("%c", pop_from_stack(s));
    } else {
      insert_into_stack(s, sequence[i]);
    }

    i++;
  }

  return 0;
}
