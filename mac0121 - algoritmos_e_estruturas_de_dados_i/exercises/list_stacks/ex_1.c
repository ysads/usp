#include <stdio.h>
#include "stack.h"

int main() {

	stack *s1 = create_stack();

	insert_into_stack(s1, 5);
	print_stack(s1);

	insert_into_stack(s1, 4);
	print_stack(s1);

	insert_into_stack(s1, 3);
	print_stack(s1);

	printf("top is: %d\n", top_of_stack(s1));

	printf("we popped %d\n", pop_from_stack(s1));
	printf("top is: %d\n", top_of_stack(s1));

	printf("we popped %d\n", pop_from_stack(s1));
	printf("we popped %d\n", pop_from_stack(s1));
	printf("we popped %d\n", pop_from_stack(s1));

	print_stack(s1);

	insert_into_stack(s1, 2);
	print_stack(s1);

	insert_into_stack(s1, 1);
	print_stack(s1);

	printf("top is: %d\n", top_of_stack(s1));
	printf("height is: %d\n", height_of_stack(s1));

	print_stack(s1);
	destroy_stack(s1);
	
	return 0;
}
