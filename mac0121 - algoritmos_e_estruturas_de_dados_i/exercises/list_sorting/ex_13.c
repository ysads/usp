#include <stdio.h>
#include "heap.h"

int main() {

    int length = 17;
    int array[17] = {12, 23, 5, 9, 19, 0, 24, 4, 1, 13, 21, 2, 7, 14, -9, 10, 14};

    printf("array before heap: \n");
    print_heap(array, length);

    build_max_heap(array, length);

    printf("\n\n");
    printf("array after heap: \n");
    print_heap(array, length);

    return 0;
}
