#include <stdio.h>
#include "heap.h"

/**
 * [heap_sort description]
 * @param heap   [description]
 * @param length [description]
 */
void heap_sort(int *heap, int length) {
    /**
     * Builds a max-heap structure, so we have all parent nodes
     * greater than its chidren, and also the largest one at 0
     */
    build_max_heap(heap, length);

    while (length > 0) {
        /**
         * Swaps the first and the last item in the heap. Now
         * we have the biggest element in the end of the array
         */
        swap(heap, 0, length - 1);

        /**
         * Reduces the length, so we can take our just-sorted
         * item out of our heap 
         */
        length--;

        /**
         * Then, heapifies the rest of our now messed heap.
         * This is needed because it's very unlikely that,
         * after swap, the new first item is in right index
         */
        heapify(heap, length, 0);
    }
}


int main() {

    int length = 17;
    int heap[17] = {12, 23, 5, 9, 19, 0, 24, 4, 1, 13, 21, 2, 7, 14, -9, 10, 14};

    printf("unsorted heap...\n");
    print_heap(heap, length);

    heap_sort(heap, length);

    printf("\n\n\n");
    printf("sorted heap...\n");
    print_heap(heap, length);

    return 0;
}

