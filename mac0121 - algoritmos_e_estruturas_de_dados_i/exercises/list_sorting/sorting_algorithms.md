
## Merge Sort

##### Complexidade:
  - `O(nlog(n))` em processamento
  - `O(n)` em espaço

##### Descrição:

Baseia-se na ideia de que um vetor de um único item sempre está ordenado. Dessa forma, o algoritmo trabalha dividindo sucessivamente o vetor original em blocos cada vez menores, até que se obtenham vetores de comprimento 1. Em seguida, compara esses vetores dois a dois, fazendo o merge deles em um único vetor ordenado.

##### Passo-a-passo:

  - dividir um vetor de tamanho `n` ao meio, sucessivamente, até ter compartimentos de tamanho 1;
  - após, chama a função `merge`, que vai intercalar os dois vetores ordenados em um único vetor;
  - vai fazendo isso com compartimentos cada vez maiores até que todo o vetor original esteja ordenado.


## Heap Sort