#include <stdio.h>

/**
 * Prints the heap as a pyramid to emphasize the tree
 * structure inherited to her
 * 
 * @param heap   : the heap to print
 * @param length : the number of items in the heap
 */
void print_heap(int *heap, int length) {
    int i, new_line;
    new_line = 1;

    for (i = 0; i < length; i++) {
        printf("%d ", heap[i]);

        if (i == (new_line - 1)) {
            printf("\n");
            new_line = (new_line * 2) + 1;
        }
    }
}

/**
* Swap two items of an array
* 
* @param array : an array
* @param pos1  : the first position to swap
* @param pos2  : the second position to swap
*/
void swap(int *array, int pos1, int pos2) {
    int aux = array[pos2];
    array[pos2] = array[pos1];
    array[pos1] = aux;
}

/**
* Transforms into a heap the subtree starting from index
* pos. To do that, checks if any of the elements is
* smaller than its siblings, swapping them if so
* 
* @param array  : the array to transform into a heap
* @param length : the number of items in the array
* @param pos    : the position to start transforming into a heap
*/
void heapify(int *array, int length, int pos) {
    int index, lft_son, rgt_son;

    /**
     * Keeps iterating until the end of the array, fixing
     * troubled subtrees
     */
    while (pos < length) {
        index = pos;

        lft_son = 2 * index + 1;
        rgt_son = lft_son + 1;

        /**
         * Left son is bigger than its parent? If so, save the
         * son's index so we can swap it with its parent later
         */
        if (lft_son < length && array[lft_son] > array[index]) {
                index = lft_son;
        }

        /**
         * Right son is bigger than its parent? If so, save the
         * son's index so we can swap it with its parent later
         */
        if (rgt_son < length && array[rgt_son] > array[index]) {
                index = rgt_son;
        }

        /**
         * We don't have to do anything, since the sons are actually
         * smaller than its siblings. Just returns and aborts function
         */
        if (index == pos) {
                return;
        }

        /**
         * Otherwise, we have found of our siblings to be bigger 
         * than its parents, which means they should be swaped
         */
        swap(array, pos, index);
        
        pos = index;
    }
}

/**
* Builds a max_heap structure based on an array. Note this function
* starts build the heapfied subtrees from the middle of the array.
* This saves time since we don't need to touch the trees from the
* downmost part of the array
* 
* @param array  : the array to transform into a heap
* @param length : the number of items in the array
*/
void build_max_heap(int *array, int length) {
    int i = length / 2 - 1;

    while (i >= 0) {
        heapify(array, length, i);
        i--;
    }
}
