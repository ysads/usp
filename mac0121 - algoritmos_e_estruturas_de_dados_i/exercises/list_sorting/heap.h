void print_heap(int *heap, int length);
void swap(int *array, int pos1, int pos2);
void build_max_heap(int *array, int length);
void heapify(int *array, int length, int pos);