#include <stdio.h>
#include "heap.h"


int heap_remove(int *heap, int length, int pos) {
    if (pos > length) {
        return -1;
    }

    /**
     * Swaps the item at pos and length-1. Keeping it
     * at the end of the array makes it easy to remove it
     */
    swap(heap, pos, length - 1);
    length--;

    heapify(heap, length, pos);
    // bubble_down(heap, length, pos);
    // 
    return length;
}


int main() {

    int length = 17;
    int array[17] = {12, 23, 5, 9, 19, 0, 24, 4, 1, 13, 21, 2, 7, 14, -9, 10, 14};

    printf("array before heap: \n");
    print_heap(array, length);

    build_max_heap(array, length);

    printf("\n\n");
    printf("array after heap: \n");
    print_heap(array, length);

    printf("\n\n");
    printf("removing item at index 4 (%d)...\n", array[4]);
    length = heap_remove(array, length, 4);

    printf("\n\n");
    printf("array: \n");
    print_heap(array, length);

    printf("\n\n");
    printf("removing item at index 2 (%d)...\n", array[2]);
    length = heap_remove(array, length, 2);

    printf("\n\n");
    printf("array: \n");
    print_heap(array, length);

    return 0;
}
