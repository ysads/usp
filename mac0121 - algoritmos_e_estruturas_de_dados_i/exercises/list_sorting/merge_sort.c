#include <stdio.h>
#include <stdlib.h>


/**
 * Prints an array
 * 
 * @param array  : the array to be printed
 * @param length : the length of the array
 */
void print_array(int *array, int length) {
    int i;

    printf("[ ");
    for (i = 0; i < length; i++) {
        printf("%d ", array[i]);
    }
    printf("]");
}


/**
 * Performs a merge between two sections of an array,
 * the whole section [start, end] sorted
 * 
 * @param array : the array in which the sections are
 * @param start : the start of the first section 
 * @param half  : the end of the first section and the
 *                end of the second section
 * @param end   : the end of the second section
 */
void merge(int *array, int start, int half, int end) {
    int i, j, k;

    /**
     * +1 here is for the sake of C being a zero-based indexes
     * language, which would make our size slightly smaller
     */
    int size = end - start + 1;

    i = start;
    j = half + 1;
    k = 0;

    /**
     * This auxiliary buffer serves as the place to merge our
     * two previously and independentely sorted arrays
     */
    int *buffer = (int *) malloc(size * sizeof(int));

    while (i <= half && j <= end) {
        /**
         * If the element in the first section of the array is
         * smaller than the one in the latter copy it to the
         * buffer array 
         */
        if (array[i] < array[j]) {
            buffer[k] = array[i];
            i++;
        }
        else {
            buffer[k] = array[j];
            j++;
        }
        k++;
    }

    /**
     * Copy the remaining items from first section
     */
    while (i <= half) {
        buffer[k] = array[i];
        k++;
        i++;
    }

    /**
     * Copy the remaining items from second section
     */
    while (j <= end) {
        buffer[k] = array[j];
        k++;
        j++;
    }

    /**
     * Copies the items from buffer to the original
     * array, so it becomes sorted
     */
    for (i = start; i <= end; i++) {
        array[i] = buffer[i - start];
    }

    free(buffer);
}


/**
 * Sorts an array by dividing it into progressively
 * smaller sections, that are then merged into one
 * only section sorted
 * 
 * @param array : the array to sort
 * @param start : the start index of the array
 * @param end   : the last position of the array
 */
void merge_sort(int *array, int start, int end) {
    if (start >= end) {
        return;
    }

    int half = (start + end) / 2;

    /**
     * Note that merge_sort slices the array
     * in sections of kind [start,end[, to
     * avoid seg faults 
     */
    merge_sort(array, start, half);
    merge_sort(array, half + 1, end);
    merge(array, start, half, end);
}


int main() {

    int length = 15;
    int array[15] = {8, 2, 1, 2, 3, 4, 7, 11, -1, 139, -30, 255, -120e3, 12, 52};

    printf("unsorted array: \n");
    print_array(array, length);

    merge_sort(array, 0, length - 1);

    printf("\n\n");
    printf("sorted array: \n");
    print_array(array, length);

    return 0;
}