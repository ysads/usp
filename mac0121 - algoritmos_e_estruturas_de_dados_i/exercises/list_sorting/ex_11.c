#include <stdio.h>

/**
 * The main ideia of this code is to find an element with a column- 
 * and row-sorted matrix. To do that we start from a corner and keep
 * on walking either eastwards or downwards. Combining movements, we
 * are able to cover the entire matrix positions with at most lin+col
 * movements, heading southwestern.
 *
 * @param matrix  : a matrix sorted both row- and column-based
 * @param lin     : number of lines in matrix
 * @param col     : number of columns in matrix
 * @param element : the element we're looking for
 * @return whether this element is in matrix or not
 */
int is_in_matrix(int matrix[5][4], int lin, int col, int element) {
    int i, j;

    /**
     * Starting from top-right-most element. This is important 
     * because along with the bottom-left-most element they
     * are the only corners that have both bigger and smaller
     * neighbors.
     */
    i = 0;
    j = col - 1;

    while (i < lin && j >= 0) {

        if (element == matrix[i][j]) {
            return 1;
        }
        else if (element < matrix[i][j]) {
            
            /**
             * If the element we're looking for is smaller than
             * the one we're analyzing, this means it might be in
             * southwest or west directions. Thus we reduce j.
             */
            j--;
        }
        else {
            
            /**
             * If the element we're looking for is larger than
             * the one we're analyzing, this means it might be
             * below us or in southwest direction, thus we make
             * reduce i value.
             */
            i++;
        }
    }
    return 0;
}


int main() {

    int m = 5, n = 4;
    int element = 28;
    int matrix[5][4] = {{12, 20, 21, 42},
                        {15, 22, 25, 51},
                        {16, 28, 31, 94},
                        {23, 32, 51, 98},
                        {77, 91, 93, 99}};

    if (is_in_matrix(matrix, m, n, element)) {
        printf("Achei! c:\n");
    }
    else {
        printf("Não achei :(\n");
    }

    return 0;
}