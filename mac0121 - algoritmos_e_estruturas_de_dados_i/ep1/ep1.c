#include <stdio.h>
#include <stdlib.h>


/**
 * Calculates the Collatz conjecture for a given number
 * 
 * @param  number: the number to which Collatz conjecture should be calculates 
 * @return a long int representing the Collactz conjecture for given param
 */
long collatz_conjecture_for(long number) {
    if (number == 1) {
        return 1;
    }
    else if (number % 2 == 0) {
        return number / 2;
    }
    else {
        return 3 * number + 1;
    }
}


/**
 * Count how many steps should be taken until we finally end with the
 * stationery return for collatz conjecture. Note this function works
 * in a recursive way using next collatz next items
 * 
 * @param  number: integer we're interested in calculating number of steps
 *                 until reaching collatz conjecture stationery value
 * @return long int representing the number of steps taken
 */
long number_of_steps_for(long number) {
    if (number == 1) {
        return 0;
    }
    else {
        return 1 + number_of_steps_for(collatz_conjecture_for(number));
    }
}


int main() {

    long i, f, k;

    printf("type i: ");
    scanf("%ld", &i);

    printf("type f: ");
    scanf("%ld", &f);

    printf("\n");
    for (k = i; k <= f; k++) { 
        printf("steps(%ld) = %ld\n", k, number_of_steps_for(k));
    }

    return 0;
}