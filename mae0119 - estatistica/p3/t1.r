### MAE0119 - Estatística
### Author: Ygor Sad Machado - 8910368
###

# **************************************** #
#            Distribuição Gamma            #
# **************************************** #

k <- 5
theta <- 5
g <- function(x) 75000 * sin(x)

##### 1a observação -> n=1e4
n <- 1e4

set.seed(666)
x <- rgamma(n, shape=k, scale=theta)
E <- mean(g(x))
E

eps <- 1100
var <- mean((g(x) - E)^2)
prob = var / (n * eps^2 )
prob

############################
##### 2a observação -> n=1e5
n <- 1e5

set.seed(666)
x <- rgamma(n, shape=k, scale=theta)
E <- mean(g(x))
E

eps <- 920
var <- mean((g(x) - E)^2)
prob = var / (n * eps^2 )
prob

############################
##### 3a observação -> n=1e5
n <- 1e6

set.seed(666)
x <- rgamma(n, shape=k, scale=theta)
E <- mean(g(x))
E

eps <- 275
var <- mean((g(x) - E)^2)
prob = var / (n * eps^2 )
prob

############################
##### 4a observação -> n=1e5
n <- 1e7

set.seed(666)
x <- rgamma(n, shape=k, scale=theta)
E <- mean(g(x))
E

eps <- 90
var <- mean((g(x) - E)^2)
prob = var / (n * eps^2 )
prob

############################
##### 5a observação -> n=1e5
n <- 1e8

set.seed(666)
x <- rgamma(n, shape=k, scale=theta)
E <- mean(g(x))
E

eps <- 20
var <- mean((g(x) - E)^2)
prob = var / (n * eps^2 )
prob


# **************************************** #
#         Distribuição Exponencial         #
# **************************************** #

lambda <- 1/5
h <- function(x) 5 * x^4 * sin(x)

##### 1a observação -> n=1e4
n <- 1e4

set.seed(666)
x <- rexp(n, lambda)
E <- mean(h(x))
E

eps <- 15000
var <- mean((h(x) - E)^2)
prob = var / (n * eps^2 )
prob

##### 2a observação -> n=1e5
n <- 1e5

set.seed(666)
x <- rexp(n, lambda)
E <- mean(h(x))
E

eps <- 5500
var <- mean((h(x) - E)^2)
prob = var / (n * eps^2 )
prob

##### 3a observação -> n=1e6
n <- 1e6

set.seed(666)
x <- rexp(n, lambda)
E <- mean(h(x))
E

eps <- 2500
var <- mean((h(x) - E)^2)
prob = var / (n * eps^2 )
prob

##### 4a observação -> n=1e5
n <- 1e7

set.seed(666)
x <- rexp(n, lambda)
E <- mean(h(x))
E

eps <- 750
var <- mean((h(x) - E)^2)
prob = var / (n * eps^2 )
prob

##### 5a observação -> n=1e8
n <- 1e8

set.seed(666)
x <- rexp(n, lambda)
E <- mean(h(x))
E

eps <- 300
var <- mean((h(x) - E)^2)
prob = var / (n * eps^2 )
prob

