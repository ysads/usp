/**
 * 492B - Vanya and Lanterns
 * Codeforces
 */
#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
    int n, l, r;
    
    cin >> n >> l;
    
    int p[n];

    for (int i = 0; i < n; i++)
    {
        cin >> p[i];
    }

    sort(p, p + n);
    r = max(p[0], l - p[n - 1]) * 2;

    for (int i = 0; i < n - 1; i++)
    {
        r = max(r, p[i + 1] - p[i]);
    }

    printf("%20.20f", (r / 2.0));
 
    return 0;
}
