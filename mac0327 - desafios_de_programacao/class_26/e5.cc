/**
 * 1175E - Minimal Segment Cover
 * Codeforces
 */
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

const long long lim = 500000;
const long long k_lim = 20;

int main()
{

    int it, y;
    int n, q;

    cin >> n >> q;
    
    vector<pair<int, int> > seg_tree(n);
    
    for (auto& s : seg_tree)
    {
        cin >> s.first >> s.second;
    }

    sort(seg_tree.begin(), seg_tree.end());
    vector<vector<int> > to(k_lim + 1, vector<int>(lim + 1));

    it = 0;
    y = 0;
    for (int x = 0; x <= lim; ++x)
    {
        while (it < n && seg_tree[it].first <= x)
        {
            y = max(y, seg_tree[it].second);
            ++it;
        }
        to[0][x] = max(x, y);
    }

    for (int k = 1; k <= k_lim; ++k)
    {
        for (int x = 0; x <= lim; ++x)
        {
            to[k][x] = to[k - 1][to[k - 1][x]];
        }
    }

    for (int i = 0; i < q; ++i)
    {
        int x, y;
        cin >> x >> y;

        int ans = 0;
        while (x < y)
        {
            if (to[0][x] == x)
            {
                ans = -1;
                break;
            }

            int k;
            for (k = k_lim; k >= 0; --k)
            {
                if (to[k][x] < y)
                {
                    break;
                }
            }

            if (k < 0)
            {
                ans += 1;
                x = to[0][x];
            }
            else
            {
                ans += (1 << k);
                x = to[k][x];
            }
        }
        cout << ans << "\n";
    }
    return 0;
}
