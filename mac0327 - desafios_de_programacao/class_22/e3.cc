/**
 * FENTREE - Fenwick Trees
 * SPOJ
 */
#include <iostream>

#define ll long long

using namespace std;

ll bit[1000000+1] = { 0 };
ll a[1000000+1];

int n, q;

ll get_sum(int idx) {
    ll sum = 0;
    idx++;
    while (idx > 0) {
        sum += bit[idx];
        idx -= idx & (-idx);
    }
    return sum;
}

void update(int idx, int v) {
    idx++;
    while (idx <= n) {
        bit[idx] += v;
        idx += idx & (-idx);
    }
}

int main() {
    
    char op;
    int u, v;
    
    cin >> n;

    for (int i = 0; i < n; i ++) {
        cin >> a[i];
    }
    for (int i = 1; i <= n; i ++) {
        bit[i] = 0;
    }
    for (int i = 0; i < n; i ++) {
        update(i,a[i]);
    }

    cin >> q;
    while (q--) {
        cin >> op;
        cin >> u >> v;

        if (op == 'q') {
            cout << get_sum(v - 1) - get_sum(u - 2) << endl;
        }
        else {
            update(u - 1, v);
        }
    }
    return 0;
}