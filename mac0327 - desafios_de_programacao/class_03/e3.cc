/**
 * C - Equality 
 */

#include <iostream>

using namespace std;

int main() {

    int occ[26];
    int i, n, k;
    long int m = 9999999;
    string str;

    cin >> n >> k;
    cin >> str;

    for (i = 0; i < 26; i++) {
        occ[i] = 0;
    }

    for (i = 0; i < n; i++) {
        occ[str[i] - 'A']++;
    }

    for (i = 0; i < k; i++) {

        if (occ[i] == 0) {
            m = 9999999;
            break;
        }

        if (occ[i] < m) {
            m = occ[i];
        }
    }

    cout << ((m == 9999999) ? 0 : m * k) << endl;

    return 0;
}
