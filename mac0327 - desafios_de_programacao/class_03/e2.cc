/**
 * B - Cookies
 */

#include <iostream>

using namespace std;

int main() {

    int i, n;
    int sum = 0, ways = 0;
    int bags[100];

    cin >> n;

    for (i = 0; i < n; i++) {
        cin >> bags[i];
        sum += bags[i];
    }

    for (i = 0; i < n; i++) {
        if ((sum - bags[i]) % 2 == 0) {
            ways++;
        }
    }

    cout << ways << endl;

    return 0;
}