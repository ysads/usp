/**
 * D - Lucky Numbers 
 */

#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n;
    unsigned long long w;

    cin >> n;

    w = 0;
    for (int i = 1; i <= n; i++) {
        w += (unsigned long long) pow(2, i);
    }

    cout << w << endl;

    return 0;
}