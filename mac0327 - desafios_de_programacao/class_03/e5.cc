/**
 * E - Divisors of Two Integers 
 */

#include <algorithm>
#include <iostream>
#include <vector> 

using namespace std;

int main() {

    int n, a, prev;
    int x, y;
    vector<int> ds;

    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> a;
        ds.push_back(a);
    }

    sort(ds.begin(), ds.end());
    
    prev = -1;
    x = ds.back();

    vector<int>::iterator it = ds.begin();
    while (it != ds.end()) {
        a = *it;
        if ((x % a == 0) && (prev != a)) {
            prev = a;
            ds.erase(it);
        }
        else {
            it++;
        }
    }
    y = ds.back();

    cout << x << " " << y << endl;
    return 0;
}