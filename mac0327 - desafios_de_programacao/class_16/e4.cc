/**
 * D - Substring 
 */
#include <iostream>
#include <vector>
#include <set>
#include <stack>
#include <time.h>

#define ll long long
#define MAX 300005
#define pvec(v) for (auto c : v) cout << c << " "
#define pline cout << "\n"

using namespace std;

set<ll> graph[MAX];
stack<ll> path;

bool has_cycle;
bool visited[MAX];
bool vertice_found[MAX];

ll occurrences[26];
ll largest_seq = 0;

char sequence[MAX];

void move_ahead(ll vertex)
{
    if (visited[vertex])
    {
        has_cycle = true;
        return;
    }

    int idx_char = sequence[vertex - 1] - 'a';

    vertice_found[vertex] = true;
    
    /**
     * Update the search state so we make it aware of this
     * newly discovered vertex
     */
    path.push(vertex);
    visited[vertex] = true;
    occurrences[idx_char]++;

    /**
     * If this newly found vertex creates a larger path value,
     * update the currente largest_seq value
     */
    if (occurrences[idx_char] > largest_seq)
    {
        largest_seq = occurrences[idx_char];
    }

    if (graph[vertex].size() > 0)
    {
        for (auto neighbor : graph[vertex])
        {
            move_ahead(neighbor);
            
            /**
             * If a cycle is found, return immediately, since we
             * already have the answer
             */
            if (has_cycle)
            {
                return;
            }    
        }
    }

    /**
     * We have no where else to go, so move back and leave
     * the search state the way we initially found it
     */
    path.pop();
    visited[vertex] = false;
    occurrences[idx_char]--;
}

int main()
{
    ll n, m;
    ll u, v;

    scanf("%lld %lld", &n, &m);
    scanf("%s", sequence);

    for (ll i = 0; i < m; i++)
    {
        scanf("%lld %lld", &u, &v);
        
        graph[u].insert(v);

        connections[u].out++;
        connections[v].in++;
    }

    for (ll vertex = 1; vertex <= n; vertex++)
    {
        // cout << vertex << ": ";
        // pvec(graph[vertex]);
        // cout << "in:  (" << connections[vertex].in << ") ~~~ ";
        // cout << "out: (" << connections[vertex].out << ")\n";
        // pline;

        /**
         * If a vertex has already been part of previous, larger
         * path, it doesn't make sense to analyse it again
         */
        if (vertice_found[vertex])
        {
            continue;
        }

        move_ahead(vertex);

        if (has_cycle)
        {
            printf("-1\n");
            return 0;
        }
    }
    printf("%lld\n", largest_seq);

    return 0;
}
