/**
 * D - Substring 
 */
#include <iostream>
#include <vector>
#include <set>
#include <stack>
#include <queue>
#include <cmath>

#define ll long long
#define MAX 300005
#define pvec(v) for (auto c : v) cout << c << " "
#define pline cout << "\n"

using namespace std;

set<ll> graph[MAX];
queue<ll> nodes;

ll degree[MAX];
ll occurrences[MAX][27];

ll largest_seq = 0;

int sequence[MAX];

int main()
{
    ll n, m;
    ll u, v;
    ll ans;
    
    ll vertex;
    ll vertice_visited = 0;

    char the_char;

    scanf("%lld %lld", &n, &m);
    getchar();
    
    for (ll i = 0; i < n; i++)
    {
        scanf("%c", &the_char);
        sequence[i] = (int) the_char - 'a';
    }
    getchar();

    for (ll i = 0; i < m; i++)
    {
        scanf("%lld %lld", &u, &v);
        
        graph[u].insert(v);
        degree[v]++;
    }

    /**
     * Enqueue only the initial 0-degree vertices, so the
     * search start by them 
     */
    for (ll vertex = 1; vertex <= n; vertex++)
    {
        if (degree[vertex] == 0)
        {
            nodes.push(vertex);
            occurrences[vertex][sequence[vertex]]++;
        }
    }

    while (!nodes.empty())
    {
        vertex = nodes.front();
        
        nodes.pop();
        vertice_visited++;

        for (auto neighbour : graph[vertex])
        {
            for (int c = 0; c < 27; c++)
            {
                if (c == sequence[neighbour])
                {
                    occurrences[neighbour][c] = max(occurrences[neighbour][c], occurrences[vertex][c] + 1);
                }
                else
                {
                    occurrences[neighbour][c] = max(occurrences[neighbour][c], occurrences[vertex][c]);
                }
            }
            degree[neighbour]--;

            if (degree[neighbour] == 0)
            {
                nodes.push(neighbour);
            }
        }
    }

    pline;
    for (int i = 1; i <= n; i++) {
        cout << i << ": ";
        for (int j = 0; j < 26; j++) {
            cout << occurrences[i][j] << " ";
        }
        pline;
    }

    if (vertice_visited == n)
    {
        ans = 0;
        for (int i = 1; i <= n; i++){
            for (int j = 0; j < 26; j++){
                ans = max(ans, occurrences[i][j]);
            }
        }

        cout << ans << "\n";
    }
    else
    {
        cout << "-1\n";
    }

    return 0;
}
