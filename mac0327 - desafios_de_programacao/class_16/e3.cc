/**
 * C - Cyclic Components
 */
#include <iostream>
#include <vector>

#define ll long long
#define MAX 200005
#define pvec(v) for (auto c : v) cout << c << " "
#define pline cout << "\n"

using namespace std;

bool visited[MAX];
ll conn[MAX];

vector<ll> graph[MAX];
vector<ll> path;

void dfs(ll vertex)
{
    if (visited[vertex]) return;

    path.push_back(vertex);
    visited[vertex] = true;

    for (auto neighbour : graph[vertex])
    {
        dfs(neighbour);
    }
}

int main()
{
    ll n, m;
    ll u, v;

    bool ring;
    ll cycles;

    cin >> n >> m;

    for (ll i = 0; i < m; i++)
    {
        cin >> u >> v;

        graph[u].push_back(v);
        graph[v].push_back(u);

        conn[u]++;
        conn[v]++;
    }

    cycles = 0;
    for (ll vertex = 0; vertex < n; vertex++)
    {
        path.clear();
        dfs(vertex);

        if (path.size() > 0) {
            ring = true;

            for (auto c : path)
            {
                ring &= (conn[c] == 2);
            }

            if (ring)
            {
                cycles++;
            }
        }
    }
    cout << cycles << endl;

    return 0;
}
