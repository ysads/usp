/**
 * A - Divisor Subtraction
 */
#include <iostream>
#include <algorithm>
#include <cmath>

#define ll long long
#define MAX 1000000

using namespace std;

ll s_div(ll n) {
    ll d;
    for (d = 3; d * d <= n; d++) {
        if (n % d == 0) {
            return d;
        }
    }
    return n;
}

int main() {
    ll n, d, s;
    ll i, j;

    cin >> n;

    s = 0;
    d = n;
    
    /**
     * If n is not even, find the smaller divisor possible
     */
    if (n % 2) {
        n -= s_div(n);
        s++;
    }
    cout << s + n / 2 << endl;

    return 0;
}