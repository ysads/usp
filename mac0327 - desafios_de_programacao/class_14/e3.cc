/**
 * 977D - Divide by three, multiply by two
 */
#include <iostream>
#include <deque>
#include <vector>

#define ull unsigned long long

using namespace std;

int main() {
    int n, i;
    ull a;

    vector<bool> stacked;
    vector<pair<ull, int> > pairs;
    deque<pair<ull, int> > stacked_pairs;

    pair<ull, int> popped;

    cin >> n;
    for (i = 0; i < n; i++) {
        cin >> a;
        
        pair<ull, int> p;
        p.first = a;
        p.second = i;

        pairs.push_back(p);
        stacked.push_back(false);
    }

    i = 0;
    while (stacked_pairs.size() < n) {

        if (i == n) {
            popped = stacked_pairs.back();

            stacked_pairs.pop_back();
            stacked[popped.second] = false;
            i = popped.second + 1;
        }
        else {
            if (stacked_pairs.empty()) {
                stacked_pairs.push_back(pairs[i]);
                stacked[i] = true;
                i = 0;
            }
            else if (!stacked[i] && ((pairs[i].first == stacked_pairs.back().first * 2) || (((stacked_pairs.back().first % 3) == 0) && (pairs[i].first == stacked_pairs.back().first / 3)))) {
                stacked_pairs.push_back(pairs[i]);
                stacked[i] = true;
                i = 0;
            }
            else {
                i++;
            }
        }
    }

    for (i = 0; i < n; i++) {
        cout << stacked_pairs.front().first << " ";
        stacked_pairs.pop_front();
    }

    return 0;
}