/**
 * 1095C - Powers of Two
 */
#include <iostream>
#include <set>

using namespace std;

int main() {
    
    int n, k;
    int m, i, a;
    multiset<int> s;
    
    cin >> n >> k;
    
    m = n;
    i = 0;
    while (m) {
        if (m & 1) {
            s.insert(1 << i);
        }
        i++;
        m = m >> 1;
    }

    if ((n < k) || (s.size() > k)) {
        cout << "NO" << endl;
    }
    else {
        while (s.size() < k) {
            a = *(--s.end());
            s.erase(--s.end());
            
            /**
             * Nothing left to accumulate. Which means we have to
             * stop the iteration
             */
            if (a == 1) {
                break;
            }

            s.insert(a / 2);
            s.insert(a / 2);
        }

        cout << "YES" << endl;
        
        /**
         * Print the powers accumulated into the multiset
         */
        while (s.size()) {
            cout << *(s.begin()) << " ";
            s.erase(s.begin());
        }
    }
}