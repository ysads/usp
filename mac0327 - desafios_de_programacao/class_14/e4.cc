/**
 * 1060D - Social Circles
 */
#include <iostream>
#include <algorithm>
#include <vector>

#define ull long long

using namespace std;

int main() {
    ull n, a, i, s;
    vector<ull> l, r;

    cin >> n;
    for (i = 0; i < n; i++) {
        cin >> a;
        l.push_back(a);

        cin >> a;
        r.push_back(a);
    }

    sort(l.begin(), l.end());
    sort(r.begin(), r.end());

    s = 0;
    for (i = 0; i < n; ++i)
    {
        s += 1 + max(l[i], r[i]);
    }

    cout << s;

    return 0;
}