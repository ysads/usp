# 
# B - Anton and Letters
# 
str = gets.chomp

str.gsub!('{', '')
   .gsub!('}', '')
   .gsub!(',', '')

uniq = 0
counts = {}

# puts str

str.split.each do |char|
  if counts[char].nil?
    uniq += 1
    counts[char] = 0 
  end
  counts[char] += 1
end

puts uniq
