n = gets.to_i

v = (0..n).map { |i| false }

l = 0;
a = gets.chomp.split.map(&:to_i)

a.each do |d| 
  l = d

  if (not v[d])
    v[d] = true
  else
    j = d
    while v[j] == true
      v[j] = false
      j += 1
    end
    v[j] = true
  end
end

m = 0
l.times do |i|
  m += 1 if not v[i]
end

puts m