/**
 * A - Design Tutorial: Learn from Math 
 */
#include <iostream>

using namespace std;

int main() {
    long n, h;
    long i, j, m;
    bool a[1000000];

    cin >> n;
    
    for (i = 1; i <= n; i++) {
        int n;
    }

    i = 2;
    while (i <= n) {
        if (a[i] == true) {
            i++;
            continue;
        }

        m = 2;
        j = m * i;
        while (j <= n) {
            a[j] = true;
            m++;
            j = m * i;
        }
        i++;
    }

    h = n / 2;
    while (!a[h] || !a[n - h]) {
    	h--;
    }

    cout << h << " " << n - h << endl;

    return 0;
}
