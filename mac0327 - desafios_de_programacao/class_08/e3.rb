# 
# C - k-rounding
# 

n, k = gets.split.map(&:to_i)

e = 10 ** k

puts (n * e) / n.gcd(e)