#include <iostream>
#include <vector>

#define ull unsigned long long
#define MAX 2000000001
#define p(x) cout << x << endl;

using namespace std;

int main() {

    ull n, d, l, m;
    vector<bool> v;

    cin >> n;
    getchar();
    
    for (ull i = 0; i < n; i++) {
        v.push_back(false);
    }

    for (ull i = 0; i < n; i++) {
        cin >> d;

        l = d;
        if (!v[d]) {
            v[d] = true;
        }
        else {
            ull j;
            for (j = d; v[j]; j++) {
                v[j] = false;
            }
            v[j] = true;
        }
    }

    m = 0;
    for (ull i = 0; i < l; i++) {
        if (!v[i]) {
            m++;
        }
    }

    p(m);

    return 0;
}
