# 
# E - Number of Ways
# 

n = gets.to_i
a = gets.split.map(&:to_i)

s = a.reduce(:+)

if (s % 3 != 0)
  puts 0
else

  s /= 3
  count = 0
  ans = 0
  sub_sum = 0

  for i in range(n-1)
    sub_sum += a[i]

    ans += count if sub_sum == 2 * s
    count += 1 if sub_sum == s
  end

  puts ans
end
