#include <iostream>
#define ll long long

using namespace std;

int main() {
	ll n, s;
	ll a, v, t, d;

	cin >> n >> s;

	a = n;
	v = 0;

	for (ll i = s; i <= n && i <= s + 200; i++) {
		t = i;
		d = 0;

		while (t) {
			d += t % 10;
			t /= 10;
		}
		if (i - d >= s)
			v++;
		a = i;
	}
	v += n - a;
	cout << v << endl;
}