/**
 * B - Badge 
 */

#include <iostream>
#include <algorithm>

using namespace std;

int main() {

    int n, f, h;
    int p[1002];

    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> p[i];
    }

    for (int i = 1; i <= n; i++) {
        int r[10002] = {0};

        h = i;
        while (1) {
            r[h]++;
            h = p[h];
            if (r[h] > 1) break;
        }
        cout << h << " ";
    }
    return 0;
}