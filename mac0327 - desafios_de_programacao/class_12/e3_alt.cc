/**
 * C - Good Sequence
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#define ll long long

ll gcd(ll a, ll b) {
   if (b == 0) return a;
   a %= b;
   return gcd(b, a);
}

using namespace std;

int main() {

    ll n, m, j;
    ll lgs, lcs;
    ll g[1000000];

    cin >> n;

    for (ll i = 0; i < n; i++) {
        cin >> g[i];
    }

    if (n == 1) {
        cout << 1 << endl;
    }
    else {
        lgs = 0;

        for (ll k = 0; k < n - 1; k++) {
            lcs = 1;
            m = k;
            j = k + 1;

            while (j < n) {
                if (gcd(g[m], g[j]) > 1) {
                    m = j;
                    lcs++;
                }
                j++;
            }
            if (lcs > lgs) lgs = lcs;
        }
        cout << lgs << endl;
    }
    return 0;
}
