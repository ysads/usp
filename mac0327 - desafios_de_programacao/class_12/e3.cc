/**
 * C - Good Sequences
 */
#include <iostream>

using namespace std;

int a[100001];
int d[100001];

int main() {
    int n, x;
    int lgs, lcs;

    cin >> n;

    for (int i = 1;i <= n;i ++) {
        cin >> a[i];
    }

    lgs = 0;
    for (int i = 1; i <= n; i++) {
        x = a[i];    
        lcs = 0;

        for (int j = 1; j * j <= x; j++) {
            if (x % j == 0) {
                if (j > 1) { 
                    lcs = max(lcs, d[j]);
                }
                lcs = max(lcs, d[x / j]);
            }
        }

        lgs = max(lgs, lcs + 1);

        for (int j = 1; j * j <= x; j++) {
            if (x % j == 0) {
                d[j] = max(d[j], lcs + 1);
                d[x / j] = max(d[x / j], lcs + 1);
            }
        }
    }
    cout << lgs;
    return 0;
}
