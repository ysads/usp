#include <iostream>
#include <string>
#define MAX 50

using namespace std;

int a[MAX + 5][MAX + 5],x[MAX + 5];

int main() {
    int n, i, j, k, cnt;
    
    while (cin >> n) {
        for (i = 1; i <= n; i++) {
            for(j = 1; j <= n; j++) {
                scanf("%d", &a[i][j]);
            }
        }
        for (i = 1; i <= n; i++) {
            for (j = 1; j <= n; j++) {
                cnt = 0;
                for (k = 1; k <= n; k++) {
                    if (a[j][k] == i) {
                        cnt++;
                    }
                }
                if (cnt == n-i && !x[j]) {
                    x[j] = i;
                    break;
                }
            }
        }
        for (i = 1; i <= n; i++) {
            printf("%d ",x[i]);
        }
        printf("\n");
    }
    return 0;
}