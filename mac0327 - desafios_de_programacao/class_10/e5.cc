#include <iostream>
#define ull long long

using namespace std;

ull n,m;
string s;

ull check(char x) {
    ull i, j, count, res;

    j = count = res = 0;

    for (i = 0; i < n; i++) {
        if (s[i] == x) {
            count++;
        }
        if (count > m) {
            while (count > m) {
                if (s[j++] == x) {
                    count--;
                }
            }
        }
        res = max(res, i - j + 1);
    }
    return res;
}

int main() {
    while (cin >> n >> m) {
        cin >> s;
        cout << max(check('b') , check('a')) << endl;
    }
    return 0;
}