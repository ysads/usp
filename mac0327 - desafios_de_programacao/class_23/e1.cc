/**
 * Codeforces - 1062A
 * A Prank
 */
#include <iostream>

using namespace std;

int main() {

    int n, c;
    int a[1001];

    c = 0;

    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }
    a[n+1] = 1001;

    c = 0;
    for (int i = 0 ; i <= n +1;  i++){
        for (int j = i + 1; j <= n+1 ; j ++){
            if (a[j] - a[i] == j - i) {
                c = max(c,j - i - 1);
            }
        }
    }
    cout << c << endl;

    return 0;
}