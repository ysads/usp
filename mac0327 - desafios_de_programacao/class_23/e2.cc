/**
 * 922C - Cave Painting
 * Codeforces
 */
#include <iostream>
#define ll long long

using namespace std;

int main()
{
    ll n, k;
    
    int lim;
    bool d;

    cin >> n >> k;

    lim = 43;
    d = true;

    if (k < lim) {
        for (int i = 1; i <= k; i++) {
            if ((n + 1) % i != 0) {
                d = false;
                break;
            }
        }
    }

    if (k < lim && d) {
        cout << "Yes\n";
    }
    else {
        cout << "No\n";
    }
    return 0;
}