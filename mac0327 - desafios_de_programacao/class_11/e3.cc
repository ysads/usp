#include <iostream>
#include <algorithm>

#define ll long long
#define p(a) cout << a << endl

using namespace std;

int n, x, ans;
int s[11];

bool same_num(int val) {
  while (val) {
    if (s[val % 10]) return true;
    val /= 10;
  }
  return false;
}

int main() {
  cin >> n;

  for (int t = n; t ; t /= 10) {
    s[t % 10]++;
  }
  ans = 0;
  
  for (int i = 1; i * i <= n; i++) {
    if (n % i) continue;
    x = n / i;
    ans += same_num(i);
    if (x > i) {
      ans += same_num(x);
    }
  }
  p(ans);
  
  return 0;
}