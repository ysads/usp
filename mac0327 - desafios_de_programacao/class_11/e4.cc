#include <iostream>
#include <cstdio>
#include <algorithm>

#define ll long long
#define p(a) cout << a << endl

#define MAX_LENGTH 300000 + 2

using namespace std;

int main() {
    char c[MAX_LENGTH];
    scanf("%s", c);

    ll n4 = ((c[0] - '0') % 4 == 0);

    for(int i = 0; c[i + 1] != '\0'; i++) {
        int cs = (c[i] - '0') * 10 + (c[i + 1] - '0');
        int ns = i + 1;

        if (cs % 4 == 0) {
            n4 += ns;
        }
        n4 += ((c[i + 1] - '0') % 4 == 0);
    }

    printf("%lld\n", n4);
    return 0;
}