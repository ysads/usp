#include <iostream>
#include <algorithm>

#define ll long long
#define p(a) cout << a << endl

using namespace std;

ll arr[100010] = {0};
ll aa[100010];

int main(){

   ll n, m;
   ll a = 0, b = 0;
   ll k = 0;

   cin >> n >> m;

    for (ll i = 1; i <= n; i++){
        cin >> arr[i];
        aa[i] = aa[i-1] + arr[i];
    }

    while (m--) {
        cin >> a >> b;

        k = aa[b] - aa[a-1];
        
        if (k % 2 == 0) {
            p("Sim");
        }
        else {
            p("Nao");
        }
    }
}