#include <iostream>
#include <algorithm>

#define ll long long
#define p(a) cout << a << endl

using namespace std;

int main() {

    ll tc, num, t = 1, choose;

    cin >> num;

    ll arr[num];

    for (ll i = 0; i < num; i++) {
        cin >> arr[i];
    }

    ll ans = 0, now = 0, ck;
    for (ll i = 0; i < num; i++){
        now = 0;
        for (ll j = i; j < num; j++){
            now = now ^ arr[j];
            ans = max(ans, now);
        }
    }
    p(ans);

    return 0;
}