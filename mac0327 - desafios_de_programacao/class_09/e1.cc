/**
 * A - Double Cola
 */
#include <iostream>
#define ull long long
#define p(x) cout << x << endl;

using namespace std;

int main() {

    string names[] = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};
    ull n;

    cin >> n;

    n--;
    while (n >= 5) {
        n = n - 5;
        n = n / 2;
    }
    p(names[n]);

    return 0;
}