/**
 * B - Palindromic Twist 
 */
#include <iostream>
#include <vector>

#define ull long long
#define p(x) cout << x << endl
#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)

using namespace std;

char n_lft(char c) {
    return (c == 'a') ? '*' : (char)(c - 1);
}

char n_rgt(char c) {
    return (c == 'z') ? '?' : (char)(c + 1);
}

int main() {

    ull t, i, e;
    string str;
    vector<bool> res;

    cin >> t;
    while (t--) {
        cin >> e;
        cin >> str;

        e = str.size();
        for (i = 0; i < e / 2; i++) {
            bool d_r = (n_rgt(str[i]) != n_rgt(str[e-i-1])) && (n_rgt(str[i]) != n_lft(str[e-i-1]));
            bool d_f = (n_lft(str[i]) != n_rgt(str[e-i-1])) && (n_lft(str[i]) != n_lft(str[e-i-1]));
            
            if (d_f && d_r) {
                res.push_back(false);
                break;
            }
        }
        if (i == e / 2) res.push_back(true);
    }
    
    forit(it, res) {
        *it ? p("YES") : p("NO");
    }

    return 0;
}