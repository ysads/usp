#include <iostream>
#include <algorithm>
#include <vector>

#define ll unsigned long long
#define p(a) cout << a << endl

using namespace std;

int main() {

    int n, k;
    int l, r, p;
    bool used[100000];

    cin >> n >> k;

    for (int i = 0; i < n; i++) {
        used[i] = false;
    }

    l = 0;
    r = n - 1;

    for (int i = 0; i < k; ++i) {
        if ((i & 1) == 0)
            p = l;
        else
            p = r;

        used[p] = true;

        if ((i & 1) == 0) {
            cout << (1 + (l++)) << " ";
        }
        else {
            cout << (1 + (r--)) << " ";
        }
    }

    if ((k & 1) == 1) {
        for (int i = 0; i < n; ++i) {
            if (!used[i]) {
                cout << i + 1 << " ";
            }
        }
    } else {
        for (int i = n - 1; i >= 0; --i) {
            if (!used[i]) {
            cout << i + 1 << " ";
            }
        }
    }
    return 0;
}
