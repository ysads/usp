/**
 * C - Lala Land and Apple Trees 
 */
#include <iostream>
#include <algorithm>
#include <vector>

#define ll unsigned long long
#define p(a) cout << a << endl

using namespace std;

int main() {

    int n, k, v;
    vector< pair<int, int> > neg;
    vector< pair<int, int> > pos;

    cin >> n;
    while (n--) {
        cin >> k >> v;

        if (k < 0)
            neg.push_back(pair<int, int>(k, v));
        else
            pos.push_back(pair<int, int>(k, v));
    }

    sort(neg.begin(), neg.end(), greater<pair<int, int> >());
    sort(pos.begin(), pos.end());

    int i;
    ll sum = 0;
    if (neg.size() == pos.size()) {
        for (i = 0; i < pos.size(); i++) {
            sum += neg[i].second + pos[i].second;
        }
    }
    else if (neg.size() < pos.size()) {
        for (i = 0; i < neg.size(); i++) {
            sum += neg[i].second + pos[i].second;
        }
        sum += pos[i].second;
    }
    else {
        for (i = 0; i < pos.size(); i++) {
            sum += neg[i].second + pos[i].second;
        }
        sum += neg[i].second;
    }

    p(sum);

    return 0;
}
