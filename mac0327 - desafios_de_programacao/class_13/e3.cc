#include <bits/stdc++.h>
#define ll long long

using namespace std;

int main() {
    int n, l;
    ll ans, sum;

    cin >> n >> l;

    vector<int> c(n);

    for (int i = 0; i < n; i++) {
        scanf("%d", &c[i]);
    }
    for (int i = 0; i < n - 1; i++) {
        c[i + 1] = min(c[i + 1], 2 * c[i]);
    }
    ans = (ll) 4e18;
    sum = 0;
    
    for (int i = n - 1; i >= 0; i--) {
        int need = l / (1 << i);
        sum += (ll) need * c[i];
        l -= need << i;
        ans = min(ans, sum + (l > 0) * c[i]);
    }
    cout << ans << endl;
    
    return 0;
}