/**
 * A - Appleman and Card Game
 */
#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

typedef long long ll;

int main() {
    ll n, k, ans = 0;
    ll c[27] = {};
    string s;

    cin >> n >> k;
    cin >> s;

    for (int i = 0; i < n; ++i) {
        c[s[i] - 'A']++;
    }

    sort(c, c + 27);

    for (int i = 26; i >= 0 && k; --i){
        if (k >= c[i]) {
            k -= c[i];
            ans += c[i] * c[i];
        }
        else {
            ans += k * k;
            break;
        }
    }
    cout << ans << endl;
    return 0;
}