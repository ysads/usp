#include<bits/stdc++.h>

using namespace std;

typedef long long ll;

#define sc1(a) scanf("%lld",&a)
#define sc2(a,b) scanf("%lld %lld",&a,&b)

#define pf1(a) printf("%lld\n", a)
#define pf2(a,b) printf("%lld %lld\n",a,b)

int main() {
    ll tc, num, t = 1, choose;
    ll arr[10000000000];

    sc1(num);

    for(ll i = 0; i < num; i++)
        sc1(arr[i]);

    ll ans = 0, now = 0, cnt = 0, j = 0;

    for(ll i = 0; i < num; i++){
        if(arr[i] == 1){
            cnt++;
            if(cnt == 1) ans = 1;
            else ans *= i - j;
            j = i;
        }
    }

    pf1(ans);

    return 0;
}
