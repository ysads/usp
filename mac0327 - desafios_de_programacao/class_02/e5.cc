#include <iostream>
#include <string>

using namespace std;

int main() {

    string str_ori;
    string str0s = "";
    string str1s = "";
    string str2s = "";

    bool found_2 = false;
    
    cin >> str_ori;
    
    for (char c : str_ori) {

        if (c == '1') {
            str1s += '1';
        }
        else if ((c == '0') && (!found_2)) {
            str0s += '0';
        }
        else {
            if (c == '2') {
                found_2 = true;
            }
            str2s += c;
        }
    }

    cout << str0s << str1s << str2s;

    return 0;
}