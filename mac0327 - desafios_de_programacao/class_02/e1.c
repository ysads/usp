#include <stdio.h>

int main() {
	int m, n, k;
	int l, r;
	int i, j;
	int arr[100];

	scanf("%d %d", &n, &m);
	
	for (i = 1; i <= m; i++) {
		arr[i] = 0;
	}

	k = m;
	for (i = 0; i < n; i++) {
		scanf("%d %d", &l, &r);

		for (j = l; j <= r; j++) {
			if (!arr[j]) {
			       	k--;
				arr[j] = 1;
			}
		}
	}

	printf("%d\n", k);
	for (i = 1; i <= m; i++) {
		if (!arr[i]) {
			printf("%d ", i);
		}
	}

	return 0;
}
