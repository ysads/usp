//
// C - Little C Loves 3 I
//

#include <stdio.h>

int main() {

	long n, acc, i, j, k;
	long a, b, c;

	scanf("%ld", &n);

	for (i = n - 2; i > 0; i--) {
		a = i;

		if (a % 3 == 0) continue;

		for (j = n - i - 1; j > 0; j--) {
			b = j;
			c = n - a - b;

			if ((b % 3 == 0) || (c % 3 == 0)) continue;

			if (a + b + c == n) {
				printf("%ld %ld %ld", a, b, c);
			}
			return 0;
		}
	}

	return 0;
}
