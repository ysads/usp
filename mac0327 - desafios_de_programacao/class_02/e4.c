#include <stdio.h>

int main() {

	int n, k;
	int w, ks;
	int i;

	int s[26];
	char stg[51];

	scanf("%d %d", &n, &k);
	getchar();

	fgets(stg, n+1, stdin);
	stg[n] = '\0';

	for (i = 0; i < 26; i++) {
		s[i] = 0;
	}

	for (i = 0; i < n; i++) {
		s[stg[i]-'a'] = 1;
	}

	ks = 0;
	w = 0;
	i = 0;
	while (i < 26) {
		if (s[i] != 1) {
			i++;
			continue;
		}

		w = w + i + 1; // weight of this stage
		ks++;

		if (ks == k) {
			break;
		}
		i += 2;
	}


	printf("%d", ((ks == k) ? w : -1));
	return 0;
}
