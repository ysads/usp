#include <stdio.h>

int main() {
	long long n, s, acc;
	int c;
	
	scanf("%lld %lld", &n, &s);

	acc = (s > n) ? n : s;
	c = 1;

	while (acc < s) {
		if (acc + n > s) {
			n--;
		}
		else {
			acc += n;
			c++;
		}
	}
	printf("%d", c);
	return 0;
}
