/**
 * 1105B - Zuhair and Strings
 * Codeforces
 */
#include <iostream>
#include <map>

using namespace std;

typedef long long ll;

map <char, ll> mp;
map <char, ll> :: iterator it;

int main()
{
    ll tc, t = 0;
    ll ln, k;
    ll ml, cnt;
    
    string arr;

    cin >> ln >> k;
    cin >> arr;

    cnt = 0;
    for (ll i = 0; i < ln; i++){
        if (arr[i] == arr[i - 1]){
            cnt ++;
        }
        else {
            cnt = 1;
        }

        if (cnt >= k){
            mp[arr[i]]++;
            cnt = 0;
        }
    }

    ml = 0;
    for (it = mp.begin(); it != mp.end(); it++){
        ml = max (ml, it->second);
    }
    cout << ml << endl;
}