/**
 * 577C - Vasya and Petya's Game
 * Codeforces
 */
#include <iostream>
#include <algorithm>

#define N 1005

using namespace std;

int n, cnt;
int pr[N], ans[N];

int main() {
    
    cin >> n;

    for (int i = 2; i <= n; i++) {
        if (!pr[i]) {
            for (int j = i * 2; j <= n; j += i) {
                pr[j]++;
            }
        }
    }

    for (int i = 2; i <= n; i++) {
        if (pr[i] < 2) {
            ans[cnt++] = i;
        }
    }
    
    cout << cnt << endl;
    for (int i = 0; i < cnt; i++) {
        cout << ans[i] << " ";
    }
}
