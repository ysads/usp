/**
 * 1151D - Stas and the Queue at the Buffet
 * Codeforces
 */
#include<algorithm>
#include<iostream>

#define ll long long

using namespace std;

int main() {
    ll n;
    ll a, b, g;
    
    cin >> n;

    ll arr[n];
    ll sa = 0;
    ll sb = 0;
    
    for (ll i = 0; i <= n - 1; i++) {
        cin >> a >> b;
        
        sa = sa + a;
        sb = sb + b;
        
        arr[i] = a - b;
    }

    sort(arr, arr + n);
    
    g = 0;
    for (ll i = 1; i <= n; i++){
        g = g + (arr[n - i] * i);
    }
    
    g = g + (sb * n) - sa;
    cout << g << endl;
}