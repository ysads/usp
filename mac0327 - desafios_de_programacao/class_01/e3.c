/*
 * Way too long words
 */

#include <stdio.h>
#include <string.h>

int main() {
    int i = 0, len, num;
    char words[100][101];

    scanf("%d", &num);

    while (i < num) {
        scanf("%s", words[i++]);
    }

    for (i = 0; i < num; i++) {
        int len = strlen(words[i]);

        if (len <= 10) {
            printf("%s\n", words[i]);
        }
        else {
            printf("%c%d%c\n", words[i][0], len-2, words[i][len-1]);
        }
    }
    return 0;
}
