/**
 * Watermelon
 */

#include <stdio.h>

int even_divisible(int num) {
    if (num % 2) {
        return 0;
    }

    int half = num / 2;
    int i = 0;

    while ((half + i <= num) && (half - i >= 1)) {
        if (((half + i) % 2 == 0) && ((half - i) % 2 == 0)) {
            return 1;
        }
        i++;
    }
    return 0;
}

int main() {
    int weight;
    scanf("%d", &weight);

    if (even_divisible(weight)) {
        printf("YES");
    }
    else {
        printf("NO");
    }
    return 0;
}
