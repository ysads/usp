/**
 * D - Two distinct points
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
    int q;
    long l1, r1, l2, r2, aux;
    long a[500], b[500];

    scanf("%d", &q);
    for (int i = 0; i < q; i++) {
        scanf("%ld %ld %ld %ld", &l1, &r1, &l2, &r2);

        do {
            aux = (rand() % (r2-l2+1)) + l2;
        } while (aux == l1);

        a[i] = l1;
        b[i] = aux;
    }

    for (int i = 0; i < q; i++) {
        printf("%ld %ld\n", a[i], b[i]);
    }
    return 0;
}
