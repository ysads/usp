/**
 * B - MaratonIME educates
 */

#include <stdio.h>

int main() {
    int num, aux, sum = 0;
    scanf("%d", &num);

    while (num-- > 0) {
        scanf("%d", &aux);
        sum += aux;
    }

    printf("%d", (sum % 5 == 0 ? (sum / 5) : (sum / 5 + 1)));
    return 0;
}
