/**
 * 818D - Multicolored Cars
 * Codeforces
 */

#include <bits/stdc++.h>

typedef long long int ll;

#define pp pair<ll,ll>
#define inf 1000000000000

using namespace std;

int main() {
    
    map<ll,ll> mp;

    ll i, j, k;
    ll n, m, aa;

    cin >> n >> aa;
    ll a[n];

    
    for (i = 0; i < n; i++) {
        cin >> a[i];

        if (a[i] == aa) {
            mp[aa]++;
            continue;
        }

        if (mp[a[i]] != -1) {
            if (mp[aa] <= mp[a[i]]) {
                mp[a[i]]++;
            }
            else {
                mp[a[i]] = -1;
            }
        }
    }

    j = -1;
    map<ll,ll>::iterator it = mp.begin();
    
    while (it != mp.end()) {
        if (it->second != -1 && it->first != aa && it->second >= mp[aa]) {
            j = it->first;
            break;
        }
        it++;
    }

    cout << j;

    return 0;
}