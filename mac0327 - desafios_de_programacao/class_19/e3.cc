/**
 * 1101E - Polycarp's New Job 
 * Codeforces
 */
#include <bits/stdc++.h>

using namespace std;

typedef long long int ll;

#define sc1(a) scanf("%lld",&a)
#define sc2(a,b) scanf("%lld %lld",&a,&b)

#define pf1(a) printf("%lld\n",a)
#define pf2(a,b) printf("%d %d\n",a,b)

#define mx 10000007
#define mod 1000000007
#define PI acos(-1.0)

int main()
{
    ll num, tc, k;

    scanf("%lld", &tc);

    ll ans = 0;

    ll x = 0, y = 0, h, w;
    char arr[5];

    for (ll i = 0; i < tc; i++) {
        scanf("%s %lld %lld", arr, &h, &w);

        if (h < w) {
            swap(h, w);
        }

        if (arr[0] == '+') {
            x = max(x, h);
            y = max(y, w);
        }
        else {
            if (x <= h && y <= w) {
                printf("YES\n");
            }
            else {
                printf("NO\n");
            }
        }
    }
}
