#include <iostream>

using namespace std;

typedef long long ll;

const int N = 200005;

int a[N], ans[N];
int n, mx, mxPos, mx2, cnt;

ll sum;

int main(void) {
	cin >> n;

    for (int i = 1; i <= n; ++i) {
    	cin >> a[i];
        sum += a[i];
        
        if (a[i] >= mx) {
            if (mx >= mx2) {
            	mx2 = mx;
            }

            mx = a[i];
            mxPos = i;
        }
        else if (a[i] >= mx2) {
            mx2 = a[i];
        }
    }

    if (sum - mx == mx2 * 2) {
        ans[cnt++] = mxPos;
    }
    for (int i = 1; i <= n; ++i) {
        if (i != mxPos && sum - a[i] == mx * 2) {
            ans[cnt++] = i;
        }
    }
    cout << cnt << "\n";
    
    for (int i = 0; i < cnt; ++i) {
        if (i == cnt - 1) {
            cout << ans[i] << "\n";
        }
        else {
            cout << ans[i] << " ";
        }
    }
    return 0;
}