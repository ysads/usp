/**
 * C - Tricky Sum
 */

#include <iostream>
#define ll long long 

using namespace std;

int main() {

        ll t, i;
        ll sa, sg;
        ll n[100];
        ll np;

        cin >> t;
        for (i = 0; i < t; i++) {
            cin >> n[i];
        }

        for (i = 0; i < t; i++) {
            sa = (1 + n[i]) * n[i] / 2;

            np = 1;
            sg = 0;
            while (np <= n[i]) {
                sg += np;
                np <<= 1;
            }
            cout << sa - 2 * sg << endl;
        }

        return 0;
}