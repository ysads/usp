/**
 * E - k-th Divisor
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>

#define ll unsigned long long
#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)
#define p(x) cout << x << endl;

using namespace std;

int main() {

    ll n, k, s;
    vector<ll> dd;

    cin >> n >> k;

    s = (ll) sqrt(n);

    for (int i = 1; i <= s; i++) {
        if (n % i == 0) {
            if (n / i == i) {
                dd.push_back(i);
            }
            else {
                dd.push_back(i);
                dd.push_back(n / i);
            }
        }
    }

    sort(dd.begin(), dd.end());

    forit (it, dd) {
        if (k == 1) {
            p(*it);
            return 0;
        }
        k--;
    }

    if (k >= 1) {
        p(-1);
    }

    return 0;
}