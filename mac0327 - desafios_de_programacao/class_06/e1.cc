/**
 * A - Function Height
 */
#include <iostream>

#define ll unsigned long long
#define p(x) cout << x << endl

using namespace std;

int main() {

    ll n, k;

    cin >> n >> k;
    p((k + n - 1) / n);

    return 0;
}