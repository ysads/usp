/**
 * D - Dividing the numbers 
 */

#include <iostream>
#include <vector>

#define ll long long
#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)

using namespace std;

int main() {

    ll n, s, h, d;
    vector<ll> g1;

    cin >> n;

    s = (1 + n) * n / 2;

    if (s % 2 == 0) {
        cout << 0 << endl;
    }
    else {
        cout << 1 << endl;
    }

    // Our goal is to react half of the sum with the biggest possible
    // numbers available
    h = s / 2;

    while (n <= h) {
        g1.push_back(n);
        
        h -= n;
        n--;
    }

    // If there's something left, push this into list because this is
    // the remaining amount needed to empty the sum
    if (h > 0) {
        g1.push_back(h);
    }

    cout << g1.size() << " ";
    forit(it, g1) {
        cout << *it << " ";
    }
    cout << endl;

    return 0;
}