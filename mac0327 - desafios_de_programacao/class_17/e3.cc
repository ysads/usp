#include <iostream>

typedef long long int ll;
typedef unsigned long long int ull;

using namespace std;

int main() {
    ll n, m;
    ll x, d;
    ll fe = 0, le = 0, me = 0;

    double sum;
    bool neg = false;
    
    cin >> n >> m;
    
    while (m--) {
        cin >> x >> d;
        
        if (d < 0) {
            neg = true;
            if(n % 2 == 0){
                fe += x + d * (n/2);
                le += x + d * (n/2 - 1);
            }
            else{
                fe += x + d * (n/2);
                le += x + d * (n/2);
            }
            me += x;
        }
        else{
            fe += x;
            le += x + d * (n - 1);
            me += x + d * (n / 2);
        }
    }

    sum = (n/2 + 1) / 2.0 * (fe + me) + (n%2 == 0? n/2 : (n/2 + 1)) / 2.0 * (me + le) - me;
    printf("%f\n", 1.0 * sum / n);
    return 0;
}
