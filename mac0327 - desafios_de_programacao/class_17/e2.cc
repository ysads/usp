#include <iostream>
#include <cmath>

typedef long long int ll;
typedef unsigned long long int ull;

using namespace std;

int main()
{
    ll x, y;
    double a, b;

    cin >> x >> y;

    if (x == y) {
        cout << "=";
        return 0;
    }

    a = y * log(x);
    b = x * log(y);

    if (a == b)
        cout << "=";
    else if (a > b)
        cout << ">";
    else
        cout << "<";

    return 0;
}