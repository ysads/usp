#include <iostream>
#include <vector>

#define MAX_NODES 150005

using namespace std;

int pre[MAX_NODES];
int n;
vector<int> graph[MAX_NODES];
 
void init()
{
  for(int i = 0; i <= n; i++)
  {
    pre[i] = i;
  }
}
 
void dfs(int vertex)
{
  printf("%d ", vertex);
  for (int i = 0; i < graph[vertex].size(); i++)
  {
    dfs(graph[vertex][i]);
  }
}
 
int locate(int vertex)
{
  if (vertex != pre[vertex])
  {
    pre[vertex] = locate(pre[vertex]);
  }
  return pre[vertex];
}
 
void merge(int x, int y)
{
  int fx = locate(x);
  int fy = locate(y);

  if (fx != fy)
  {
    pre[fy] = fx;
    graph[fx].push_back(fy);
  }
}
 
int main()
{
  int u, v;

  cin >> n;
  
  init();

  for (int i = 1; i < n; i++)
  {
    cin >> u >> v;
    merge(u, v);
  }
  
  dfs(locate(1));

  return 0;
}
