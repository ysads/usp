#include <iostream>
#include <vector>

#define ll long long

const int MAX_NODES = int(2e5) + 7;
const int REMAINING = int (1e9) + 7;

using namespace std;

ll pot(ll p, ll q) {
    ll ret = 1;
    while (q) {
        if (q & 1) ret = ret * p % REMAINING;
        p = p * p % REMAINING;
        q >>= 1;
    }
    return ret;
}

vector<ll> edges[MAX_NODES];
bool visited[MAX_NODES];

ll vertex_count, n, k;

void dfs(ll u)
{
    visited[u] = true;
    vertex_count++;

    for (auto v : edges[u])
    {
        if (!visited[v])
        {
            dfs(v);
        }
    }
}

int main()
{
    ll ans;
    ll u, v, w;

    cin >> n >> k;

    ans = pot(n, k);

    for (ll i = 1; i < n; i++)
    {
        cin >> u >> v >> w;

        if (w == 0) {
            edges[u].push_back(v);
            edges[v].push_back(u);
        }
    }

    for (ll vertex = 1; vertex <= n; vertex++) {
        if (!visited[vertex]) {
            vertex_count = 0;
            dfs(vertex);
            ans = (ans - pot(vertex_count, k) + REMAINING) % REMAINING;
        }
    }
    cout << ans << "\n";

    return 0;
}