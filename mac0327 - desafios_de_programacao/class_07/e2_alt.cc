/**
 * B - Ehab and subtraction
 */
#include <iostream>
#include <set>

using namespace std;

int main() {
    
    int n, k, a;

    cin >> n >> k;

    set<int> s;
    s.insert(0);

    while (n--) {
        cin >> a;
        s.insert(a);
    }

    auto it = s.begin();
    
    for (int i = 0; i < k; i++) {
        
        if (next(it) == s.end()) {
            printf("0\n");    
        }    
        else {
            printf("%d\n", *next(it) - *it);
            it = next(it);
        }
    }
}
