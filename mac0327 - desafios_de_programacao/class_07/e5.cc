/**
 * E - Modular Equations
 */
#include <iostream>
#include <cmath>

#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)
#define p(x) cout << x << endl

#define ll long long

using namespace std;

int main() {

    ll a, b, n;
    ll s, k;

    cin >> a >> b;

    if (a == b) {
        p("infinity");
    }
    else if (a < b) {
        p(0);
    }
    else {

        n = a - b;
        s = (ll) sqrt(n);

        k = 0;
        for (int i = 1; i <= s; i++) {
            if (n % i == 0) {
                if (n / i == i) {
                    if (i > b) k++;
                }
                else {
                    if (i > b) k++;
                    if ((n / i) > b) k++;
                }
            }
        }

        p(k); 
    }

    return 0;
}