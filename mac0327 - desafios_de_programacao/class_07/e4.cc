/**
 * D - Guess The Number
 */
#include <iostream>
#include <cstring>
#include <stdint.h>

#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)
#define p(x) cout << x << endl

#define ll int_fast32_t
#define LB -2000000000
#define HB 2000000000

inline ll max(ll a, ll b) { return (a > b) ? a : b; }
inline ll min(ll a, ll b) { return (a < b) ? a : b; }

using namespace std;

int main() {

    ll n, b;
    ll lb, hb;
    char op[3], ans;

    hb = HB;
    lb = LB;

    cin >> n;
    for (int i = 0; i < n; i++) {
        scanf("%s", op);
        cin >> b;
        cin >> ans;

        if (ans == 'Y') {
            if (strcmp(">=", op) == 0) {
                lb = max(lb, b);
            }
            else if (strcmp(">", op) == 0) {
                lb = max(lb, b + 1);
            }
            else if (strcmp("<=", op) == 0) {
                hb = min(hb, b);
            }
            else if (strcmp("<", op) == 0) {
                hb = min(hb, b - 1);
            }
        }
        else {
            if (strcmp(">=", op) == 0) {
                hb = min(hb, b - 1);
            }
            else if (strcmp(">", op) == 0) {
                hb = min(hb, b);
            }
            else if (strcmp("<=", op) == 0) {
                lb = max(lb, b + 1);
            }
            else if (strcmp("<", op) == 0) {
                lb = max(lb, b);
            }
        }
    }

    if (lb > hb) {
        p("Impossible");
    }
    else if (lb == hb) {
        p(hb);
    }
    else {
        p(lb + 1);
    }

    return 0;
}