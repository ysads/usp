/**
 * C - Noldbach problem
 */
#include <algorithm>
#include <iostream>
#include <vector>

#define ll unsigned long long
#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)
#define p(x) cout << x << endl

using namespace std;

int main() {

    int n, k;
    int i, j, m;
    bool a[1001];
    vector<int> p;

    cin >> n >> k;

    for (i = 1; i <= n; i++) {
        a[i] = false;
    }

    i = 2;
    while (i <= n) {
        if (a[i] == true) {
            i++;
            continue;
        }

        p.push_back(i);

        m = 2;
        j = m * i;
        while (j <= n) {
            a[j] = true;
            m++;
            j = m * i;
        }
        i++;
    }

    forit(it, p) {
        if (it + 1 == p.end()) break;
        
        m = *it + *(it + 1) + 1;

        if (m > n) break;
        if (!a[m]) k--;
    }

    if (k <= 0) {
        p("YES");
    }
    else {
        p("NO");
    }

    return 0;
}