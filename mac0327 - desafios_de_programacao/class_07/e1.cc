/**
 * A - IQ test 
 */
#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>

#define ll unsigned long long
#define forit(it, x) for (__typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)
#define p(x) cout << x << endl;

using namespace std;

int main() {

    ll n, a;
    ll i_odd, i_evn, n_odd = 0, n_evn = 0;

    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> a;

        if (a % 2) {
            i_odd = i;
            n_odd++;
        }
        else {
            i_evn = i;
            n_evn++;
        }
    }

    if (n_evn == 1) {
        p(i_evn);
    }
    else {
        p(i_odd);
    }

    return 0;
}