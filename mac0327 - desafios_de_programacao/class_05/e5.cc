/**
 * E - Buttons
 */
#include <iostream>

using namespace std;

int main() {

    int n, a;
    unsigned long long s1, s2, i;

    cin >> n;

    a = n - 1;

    s1 = 0;
    for (i = 1; i <= n - 1; i++) {
        s1 += (n - i) * i;
    }

    // s1 = n * (1 + (n - 1)) * (n - 1) / 2;
    // s2 = a * (a + 1) * (2 * a + 1) / 6;

    cout << s1 + n;
}