#include <iostream>

using namespace std;

inline long     max(long a, long b)     { return (a > b ? a : b); }
inline long     min(long a, long b)     { return (a < b ? a : b); }

int main() {

    int d[101];
    int n, s, t;
    int dcw, dccw;

    cin >> n;

    for (int i = 1; i <= n; i++) {
        cin >> d[i];
    }

    cin >> s >> t;

    dcw = 0;
    for (int i = s; i != t; i++) {
        dcw += d[i];
        if (i + 1 > n)
            i = 0;
    }

    dccw = 0;
    for (int i = s-1; i != t-1; i--) {
        if (i == 0)
            i = n;
        dccw += d[i];
    }

    cout << min(dcw, dccw) << endl;

    return 0;
}