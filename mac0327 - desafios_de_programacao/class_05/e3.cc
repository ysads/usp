/**
 * C - Discounts
 */
#include <iostream>
#include <algorithm>

#define MAX 300001

using namespace std;

int main() {

    long m, n, c;
    long a[MAX];
    unsigned long long res[MAX];
    unsigned long long s = 0;

    cin >> n;

    for (int i = 0; i < n; i++) {
        cin >> a[i];
        s += a[i];
    }
    sort(a, a + n);

    for (long k = 0; k < MAX; k++ ) {
        res[k] = -1;
    }

    cin >> m;
    
    for (int i = 0; i < m; i++) {
        cin >> c;
        
        if (res[c] == -1) {
            res[c] = s - a[n - c];
        }

        cout << res[c] << endl;
    }

    return 0;
}