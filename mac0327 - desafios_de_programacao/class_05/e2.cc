#include <iostream>

using namespace std;

int stch(int v, int p) {
    int c = (v == 0) ? 1 : 0;
    return (p % 2) ? c : v;
}

void tog(int m[][3], int i, int j, int p) {
    if (p > 0) {
        m[i][j] = stch(m[i][j], p);

        if (i-1 >= 0) m[i-1][j] = stch(m[i-1][j], p);
        if (i+1 <= 2) m[i+1][j] = stch(m[i+1][j], p);

        if (j-1 >= 0) m[i][j-1] = stch(m[i][j-1], p);
        if (j+1 <= 2) m[i][j+1] = stch(m[i][j+1], p);
    }
}


int main() {

    int mat[3][3];
    int prs[3][3];
    int i, j;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            mat[i][j] = 1;
            cin >> prs[i][j];
        }
    }


    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            tog(mat, i, j, prs[i][j]);
        }
    }


    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << mat[i][j];
        }
        cout << endl;
    }

    return 0;
}