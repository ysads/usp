/**
 * 1175 - Electrification
 * Codeforces
 */
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long ll;

const ll INF = 1e18;

void solve_query()
{
    int n, k;
    ll ans, x;
    
    cin >> n >> k;

    vector<ll> points(n);
    
    for (int i = 0; i < n; ++i)
    {
        cin >> points[i];
    }
    sort(points.begin(), points.end());

    ans = INF;
    x = -1;

    for (int i = 0; i+k < n; ++i)
    {
        ll dk = (points[i + k] - points[i] + 1) / 2;
        
        if (dk < ans)
        {
          ans = dk;
          x = (points[i + k] + points[i]) / 2;
        }
    }
    cout << x << '\n';
}

int main()
{
    int t;
    
    cin >> t;
    
    while (t--)
    {
        solve_query();
    }
    return 0;
}
