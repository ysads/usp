/**
 * 52C - Circular RMQ
 * Codeforces
 */
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>

using namespace std;

#define N 200006
#define INF 0x3fffffff
#define lson ans<<1,ll,mid
#define rson ans<<1|1,mid+1,rr

__int64 m, n, sum[N], mins[N << 2], lazy[N << 2];
char str[200];

__int64 min(__int64 x,__int64 y)
{
    return (x > y) ? y : x;
}

void pushup(int ans)
{
    mins[ans] =min(mins[ans<<1],mins[ans<<1|1]);
}

void pushdown(int ans)
{
    if (lazy[ans])
    {
        lazy[ans << 1] += lazy[ans];
        lazy[ans << 1 | 1] += lazy[ans];
        mins[ans << 1] +=  lazy[ans];
        mins[ans << 1 | 1] += lazy[ans];
        lazy[ans] = 0;
    }
}


void build_tree(int ans, int ll, int rr)
{
    int mid;

    lazy[ans] = 0;

    if (ll == rr)
    {
        mins[ans] = sum[ll];
        return;
    }
    mid = (ll + rr) >> 1;
    build_tree(lson);
    build_tree(rson);
    pushup(ans);
}
 
void update(int ans, int ll, int rr, int L, int R, int value)
{
    int mid;

    if (L <= ll && R >= rr)
    {
        lazy[ans] += value;
        mins[ans] += value;
        return;
    }

    pushdown(ans);
    mid = (ll + rr) >> 1;

    if (mid < R)
    {
        update(rson, L, R, value);
    }
    if (mid >= L)
    {
        update(lson, L, R, value);
    }
    pushup(ans);
}
 
__int64 query(int ans, int ll, int rr, int L, int R)
{
    __int64 ss;
    int mid;


    if (ll >= L && rr <= R)
    {
        return mins[ans];
    }
    pushdown(ans);
    
    mid = (ll + rr) >> 1;
    ss = INF;
    
    if (mid >= L)
    {
        ss = min(ss, query(lson, L, R));
    }
    if (mid < R)
    {
       ss = min(ss, query(rson, L, R));
    }
    return ss;
}

int main()
{
    int a, b, c;

    while(~scanf("%I64d", &n))
    {
        n--;
        for (int i = 0; i <= n; i++)
        {
            scanf("%I64d", &sum[i]);
        }
        build_tree(1, 0, n);
        
        scanf("%I64d", &m);
        getchar();

        for (int i = 1;i <= m; i++)
        {
            gets(str);
            if (sscanf(str, "%d%d%d", &a, &b, &c) == 3)
            {
                if (a > b)
                {
                    update(1, 0, n, a, n, c);
                    update(1, 0, n, 0, b, c);
                }
                else
                {
                    update(1, 0, n, a, b, c);
                }
            }
            else
            {
                if (a > b)
                {
                    __int64 ans = min(query(1, 0, n, a, n), query(1, 0, n, 0, b));
                    printf("%I64d\n", ans);
                }
                else
                {
                    printf("%I64d\n", query(1, 0, n, a, b));
                }
            }
        }
    }
    return 0;
}