/**
 * E - k-string
 */
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
    int k;
    int occ[26];
    string str;
    string res = "";

    for (int i = 0; i < 26; i++) occ[i] = 0;

    cin >> k;
    cin >> str;

    for (string::iterator it = str.begin(); it != str.end(); ++it) {
        occ[*it - 'a']++;
    }

    for (int i = 0; i < 26; i++) {
        if (occ[i] % k != 0) {
            cout << -1 << endl;
            return 0;
        }

        for (int j = 0; j < (occ[i] / k); j++) {
            res = res + ((char) (i + 'a'));
        }
    }

    for (int i = 0; i < k; i++) {
        cout << res;
    }
    cout << endl;

    return 0;
}