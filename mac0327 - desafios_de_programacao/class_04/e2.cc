#include <iostream>
#include <string>

using namespace std;

int main() {
	int i, n;
	string str;

	cin >> n;
	cin >> str;

	for (i = 1; i < str.size(); i++) {
		if (str[i-1] != str[i]) {
			break;
		}
	}

	if (i == str.size()) {
		cout << "NO" << endl;
	}
	else {
		cout << "YES" << endl;
		cout << str[i-1] << str[i] << endl;
	}
	return 0;
}
