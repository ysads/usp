#include <algorithm>
#include <cmath>
#include <iostream>

using namespace std;

int main() {
	long s1, s2, s3;
	long a, b, c;

	cin >> s1 >> s2 >> s3;

	a = (long) sqrt((s1 * s3) / s2);
	b = (long) sqrt((s1 * s2) / s3);
	c = (long) sqrt((s2 * s3) / s1);

  long sum = 4 * (a + b + c);

	cout << sum << endl;

	return 0;
}
