#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

inline long     max(long a, long b)     { return (a > b ? a : b); }
inline long     min(long a, long b)     { return (a < b ? a : b); }
// inline long     mdc(long a, long b)     { return (__gcd(a, b)); }
// inline long     mmc(long a, long b)     { return (a * b / __gcd(a, b)); }
inline long     dig(long a)             { return (floor(log10(a)) + 1);  }
inline bool     isEven(long a)          { return (a & 1 ? true : false); }
inline bool     isPow2(long a)          { return a && (!(a & (a - 1))); }
inline double   hip(long a, long b)     { return (sqrt(pow(a, 2) + pow(b, 2))); }
inline double   cat(long a, long b)     { return (sqrt(pow(max(a, b), 2) - pow(min(a, b), 2))); }
inline void     swap(long& a, long& b)  { a ^= b; b ^= a; a ^= b; }
#define         p(a)                    cout << a << endl
