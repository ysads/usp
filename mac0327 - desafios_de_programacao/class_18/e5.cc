/**
 * E - Chladni Figure 
 */
#include <algorithm>
#include <iostream>
#include <vector>

#define ll long long;
#define MAX 100005

using namespace std;

vector<int> ang;
vector<int> mapp[MAX];

int main()
{
    int n, m;
    int s, t;
    int flag = 1;

    cin >> n >> m;

    for (int i = 1; i <= n / 2; i++)
    {
        if (n % i == 0)
        {
            ang.push_back(i);
        }
    }

    for (int i = 0; i < m; i++)
    {
        cin >> s >> t;

        if (s > t)
        {
        	swap(s, t);
        }

        mapp[s].push_back(t - s);
        mapp[t].push_back((s - t + n - 1) % n + 1);
    }
    
    for (int i = 1; i <= n; i++)
    {
        sort(mapp[i].begin(), mapp[i].end());
    }

    for (int i = 0; i < ang.size(); i++)
    {
        flag = 1;
        int now = ang[i];
        
        for (int j = 1; j <= n; j++)
        {
            if (mapp[j].size() != mapp[(j + now - 1) % n + 1].size())
            {
                flag = 0;
                break;
            }

            for (int k = 0; k < mapp[j].size(); k++)
            {
                if (mapp[(j + now - 1) % n + 1][k] != mapp[j][k])
                {
                    flag = 0;
                    break;
                }
            }
            if (flag == 0)
            {
            	break;
            }
        }
        if (flag == 1)
        {
        	break;
        }
    }

    if (flag == 0)
    {
    	printf("No\n");
    }
    else
    {
    	printf("Yes\n");
    }

    return 0;
}