/**
 * C - Minimum Spanning Tree
 */
#include <algorithm>
#include <iostream>
#include <vector>

#define ll long long
#define MAX_NODES 100001
#define MAX_EDGES 1000000
#define pline cout << "\n"

using namespace std;

ll parent[MAX_NODES];
vector<pair<ll, pair<ll,ll>>> graph;


ll find(ll vertex)
{
    if (parent[vertex] == vertex)
    {
        return vertex;
    }
    else
    {
        return find(parent[vertex]);
    }
}


void conj(ll u, ll v)
{
    ll parent_u = find(u);
    ll parent_v = find(v);

    parent[parent_u] = parent_v;
}


int comparator(pair<ll,pair<ll,ll> > edge_1, pair<ll,pair<ll,ll> > edge_2)
{
    return edge_1.first <= edge_2.first;
}


int main()
{
    ll n, m;
    ll u, v, w;
    ll min_weight;

    cin >> n >> m;
    
    for (ll i = 1; i <= m; i++)
    {
        cin >> u >> v >> w;
        graph.push_back(make_pair(w, make_pair(u, v)));
    }

    for (ll vertex = 1; vertex <= n; vertex++)
    {
        parent[vertex] = vertex;
    }

    sort(graph.begin(), graph.end(), comparator);

    min_weight = 0;
    for (auto edge : graph)
    {
        if (find((edge.second).first) != find((edge.second).second))
        {
            min_weight += edge.first;
            conj((edge.second).first, (edge.second).second);
        }
    }
    
    cout << min_weight << "\n";
    return 0;
}