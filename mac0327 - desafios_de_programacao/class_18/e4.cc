/**
 * D - Sasha and a Bit of Relax 
 */
#include <iostream>
#include <cmath>

using namespace std;

typedef long long ll;

const int maxn = 300010;

int a[maxn];
int pre[maxn];
int cnt[2][(1 << 20) + 3];
 
int main()
{
    int n;
    cin >> n;
    ll ans = 0;
    cnt[0][0] = 1;
    
    for (int i = 1; i <= n; i++)
    {
        cin >> a[i];
        pre[i] = pre[i-1] ^ a[i];
        ans += cnt[i % 2][pre[i]];
        cnt[i % 2][pre[i]]++;
    }
    cout << ans << endl;
}